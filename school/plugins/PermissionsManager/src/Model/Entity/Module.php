<?php
namespace PermissionsManager\Model\Entity;

use Cake\ORM\Entity;

/**
 * Module Entity
 *
 * @property int $id
 * @property int $parent_id
 * @property string $alias
 *
 * @property \App\Model\Entity\ParentModule $parent_module
 * @property \App\Model\Entity\ChildModule[] $child_modules
 * @property \App\Model\Entity\Role[] $roles
 */
class Module extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
