<section class="content-header">
  <h1>
    <?php echo __('Role'); ?>
  </h1>
  <ol class="breadcrumb">
    <li>
    <?= $this->Html->link('<i class="fa fa-dashboard"></i> ' . __('Back'), ['action' => 'index'], ['escape' => false])?>
    </li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-md-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <i class="fa fa-info"></i>
                <h3 class="box-title"><?php echo __('Information'); ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <dl class="dl-horizontal">
                                                                                                                <dt><?= __('Name') ?></dt>
                                        <dd>
                                            <?= h($role->name) ?>
                                        </dd>
                                                                                                                                                            <dt><?= __('Alias') ?></dt>
                                        <dd>
                                            <?= h($role->alias) ?>
                                        </dd>
                                                                                                                                    
                                            
                                                                                                                                            
                                            
                                            
                                    </dl>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- ./col -->
</div>
<!-- div -->

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <i class="fa fa-share-alt"></i>
                    <h3 class="box-title"><?= __('Related {0}', ['Users']) ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">

                <?php if (!empty($role->users)): ?>

                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                                                    
                                    <th>
                                    Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Username
                                    </th>
                                        
                                                                    
                                    <th>
                                    Password
                                    </th>
                                        
                                                                    
                                    <th>
                                    First Name
                                    </th>
                                        
                                                                    
                                    <th>
                                    Last Name
                                    </th>
                                        
                                                                    
                                    <th>
                                    New Photo
                                    </th>
                                        
                                                                    
                                    <th>
                                    Reset Password Token
                                    </th>
                                        
                                                                    
                                    <th>
                                    Reset Password Token Created At
                                    </th>
                                        
                                                                    
                                    <th>
                                    Hash Password
                                    </th>
                                        
                                                                    
                                    <th>
                                    User Reference Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Accept Demands
                                    </th>
                                        
                                                                    
                                    <th>
                                    Moderator
                                    </th>
                                        
                                                                    
                                    <th>
                                    Role Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Notification Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Enterprise Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Access Client
                                    </th>
                                        
                                                                    
                                    <th>
                                    Access Admin
                                    </th>
                                        
                                                                    
                                    <th>
                                    Access Specialist
                                    </th>
                                        
                                                                    
                                    <th>
                                    Must Change Password
                                    </th>
                                        
                                                                    
                                    <th>
                                    Hash Invite
                                    </th>
                                        
                                                                    
                                    <th>
                                    Lat
                                    </th>
                                        
                                                                    
                                    <th>
                                    Lng
                                    </th>
                                        
                                                                    
                                    <th>
                                    Facebook Pass
                                    </th>
                                        
                                                                    
                                    <th>
                                    Facebook Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Black List
                                    </th>
                                        
                                                                    
                                    <th>
                                    Timezone
                                    </th>
                                        
                                                                    
                                    <th>
                                    Available
                                    </th>
                                        
                                                                    
                                    <th>
                                    Active
                                    </th>
                                        
                                                                                                                                            
                                    <th>
                                    Rating
                                    </th>
                                        
                                                                    
                                    <th>
                                    Uid User
                                    </th>
                                        
                                                                    
                                    <th>
                                    Uid Geolocation
                                    </th>
                                        
                                                                    
                                    <th>
                                    Full Record
                                    </th>
                                        
                                                                    
                                    <th>
                                    Group Id
                                    </th>
                                        
                                                                    
                                <th>
                                    <?php echo __(''); ?>
                                </th>
                            </tr>

                            <?php foreach ($role->users as $users): ?>
                                <tr>
                                                                        
                                    <td>
                                    <?= h($users->id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($users->username) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($users->password) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($users->first_name) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($users->last_name) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($users->new_photo) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($users->reset_password_token) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($users->reset_password_token_created_at) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($users->hash_password) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($users->user_reference_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($users->accept_demands) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($users->moderator) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($users->role_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($users->notification_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($users->enterprise_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($users->access_client) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($users->access_admin) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($users->access_specialist) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($users->must_change_password) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($users->hash_invite) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($users->lat) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($users->lng) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($users->facebook_pass) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($users->facebook_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($users->black_list) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($users->timezone) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($users->available) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($users->active) ?>
                                    </td>
                                                                                                                                                
                                    <td>
                                    <?= h($users->rating) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($users->uid_user) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($users->uid_geolocation) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($users->full_record) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($users->group_id) ?>
                                    </td>
                                    
                                                                        <td class="actions">
                                    <?= $this->Html->link(__('View'), ['controller' => 'Users', 'action' => 'view', $users->id], ['class'=>'btn btn-info btn-xs']) ?>

                                    <?= $this->Html->link(__('Edit'), ['controller' => 'Users', 'action' => 'edit', $users->id], ['class'=>'btn btn-warning btn-xs']) ?>

                                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Users', 'action' => 'delete', $users->id], ['confirm' => __('Are you sure you want to delete # {0}?', $users->id), 'class'=>'btn btn-danger btn-xs']) ?>    
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                                    
                        </tbody>
                    </table>

                <?php endif; ?>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <i class="fa fa-share-alt"></i>
                    <h3 class="box-title"><?= __('Related {0}', ['Modules']) ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">

                <?php if (!empty($role->modules)): ?>

                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                                                    
                                    <th>
                                    Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Parent Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Alias
                                    </th>
                                        
                                                                    
                                <th>
                                    <?php echo __(''); ?>
                                </th>
                            </tr>

                            <?php foreach ($role->modules as $modules): ?>
                                <tr>
                                                                        
                                    <td>
                                    <?= h($modules->id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($modules->parent_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($modules->alias) ?>
                                    </td>
                                    
                                                                        <td class="actions">
                                    <?= $this->Html->link(__('View'), ['controller' => 'Modules', 'action' => 'view', $modules->id], ['class'=>'btn btn-info btn-xs']) ?>

                                    <?= $this->Html->link(__('Edit'), ['controller' => 'Modules', 'action' => 'edit', $modules->id], ['class'=>'btn btn-warning btn-xs']) ?>

                                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Modules', 'action' => 'delete', $modules->id], ['confirm' => __('Are you sure you want to delete # {0}?', $modules->id), 'class'=>'btn btn-danger btn-xs']) ?>    
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                                    
                        </tbody>
                    </table>

                <?php endif; ?>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
