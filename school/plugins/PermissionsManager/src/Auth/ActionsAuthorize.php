<?php
/**
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace PermissionsManager\Auth;

use Cake\Network\Request;
use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;

/**
 * An authorization adapter for AuthComponent. Provides the ability to authorize using the AclComponent,
 * If AclComponent is not already loaded it will be loaded using the Controller's ComponentRegistry.
 *
 * @see AuthComponent::$authenticate
 * @see AclComponent::check()
 */
class ActionsAuthorize extends BaseAuthorize
{

    /**
     * Authorize a user using the AclComponent.
     *
     * @param array $user The user to authorize
     * @param \Cake\Network\Request $request The request needing authorization.
     * @return bool
     */
    public function authorize($user, Request $request)
    {
        $modules = TableRegistry::get('Modules');
        $rolesModules = TableRegistry::get('RolesModules');
        $controller = $modules->find('all')->where(['alias' => Inflector::camelize($request->getParam('controller'))])->first();
        $action = $modules->find('all')->where(['alias' => Inflector::camelize($request->getParam('action')), 'parent_id' => $controller->id])->first();
        $role = $rolesModules->find('all')->where(['role_id' => $user['role_id'], 'module_id' => $action->id])->first();
        return ($role) ? true : false;
    }
}
