<?php

/**
 * CakePHP 3.x - Acl Manager
 * 
 * PHP version 5
 * 
 * Class PermissionsManagerComponent
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @category CakePHP3
 * 
 * @package PermissionsManager\Controller\Component
 * 
 * @author Ivan Amat <dev@ivanamat.es>
 * @copyright Copyright 2016, Iván Amat
 * @license MIT http://opensource.org/licenses/MIT
 * @link https://github.com/ivanamat/cakephp3-aclmanager
 * 
 * @author Jc Pires <djyss@live.fr>
 * @license MIT http://opensource.org/licenses/MIT
 * @link https://github.com/JcPires/CakePhp3-PermissionsManager
 */

namespace PermissionsManager\Controller\Component;

use Acl\Controller\Component\AclComponent;
use Acl\Model\Entity\Aro;
use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;

class PermissionsManagerComponent extends Component {

    private $Modules;
    /**
     * Basic Api actions
     *
     * @var array
     */
    protected $config = [];
    
    /**
     * Initialize all properties we need
     *
     * @param array $config initialize cake method need $config
     *
     * @return null
     */
    public function initialize(array $config) {
        $this->config = $config;
        $this->controller = $this->_registry->getController();
        $this->Modules = TableRegistry::get('Modules');
    }

    public function check($user, $action){
        debug($user);
        debug($action);
    }


}