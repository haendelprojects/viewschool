<?php

use Phinx\Migration\AbstractMigration;

class CakePhpDbAcl extends AbstractMigration
{
    public function change()
    {
        $table = $this->table('modules');
        $table->addColumn('parent_id', 'integer', ['null' => true])
            ->addColumn('alias', 'string', ['limit' => 255, 'null' => true])
            ->addColumn('created', 'datetime', ['default' => null, 'null' => false])
            ->addColumn('modified', 'datetime', ['default' => null, 'null' => false])
            ->addIndex(array('alias'))
            ->create();
        $table = $this->table('roles');
        $table->addColumn('name', 'string', ['limit' => 255, 'null' => true])
            ->addColumn('alias', 'string', ['limit' => 255, 'null' => true])
            ->addColumn('created', 'datetime', ['default' => null, 'null' => false])
            ->addColumn('modified', 'datetime', ['default' => null, 'null' => false])
            ->addIndex(array('alias', 'name'), ['unique' => true])
            ->create();
        $table = $this->table('roles_modules');
        $table->addColumn('role_id', 'integer', ['null' => false])
            ->addColumn('module_id', 'integer', ['null' => false])
            ->addColumn('_create', 'string', ['default' => '0', 'limit' => 2, 'null' => false])
            ->addColumn('_read', 'string', ['default' => '0', 'limit' => 2, 'null' => false])
            ->addColumn('_update', 'string', ['default' => '0', 'limit' => 2, 'null' => false])
            ->addColumn('_delete', 'string', ['default' => '0', 'limit' => 2, 'null' => false])
            ->addColumn('created', 'datetime', ['default' => null, 'null' => false])
            ->addColumn('modified', 'datetime', ['default' => null, 'null' => false])
            ->addIndex(array('role_id', 'module_id'), ['unique' => true])
            ->create();
    }
}