<?php
use Cake\Routing\Router;

Router::plugin(
    'Password',
    ['path' => '/password'],
    function ($routes) {
        $routes->fallbacks('DashedRoute');
    }
);
