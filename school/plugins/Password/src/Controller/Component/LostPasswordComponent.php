<?php

namespace Password\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security;
use EmailNotifications\Controller\Component\MailComponent;

/**
 * LostPassword component
 * Componente para recuperar senha
 *
 * @property \App\Model\Table\UsersTable $Users
 * @property \EmailNotifications\Controller\Component\MailComponent $Mail
 */
class LostPasswordComponent extends Component
{
    private $Users;

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];
    public $components = ['EmailNotifications.Mail'];

    public function initialize(array $config)
    {
        $this->Users = TableRegistry::get('Users');
    }

    /**
     * Criar um token e enviar email
     * @param $username
     * @return bool
     */
    public function requestNew($username)
    {
        $user = $this->Users->find('all')
            ->where(['username' => $username])
            ->contain(['Peoples'])
            ->first();

        if (!empty($user)) {
            $new_data = $this->__generatePasswordToken();
            $user = $this->Users->patchEntity($user, $new_data);
            if ($this->Users->save($user)) {
                //Enviar email
                $this->__sendForgotPasswordEmail($user);
                return true;
            }
        }

        return false;

    }

    /**
     * Liberar o reset se o token for valido
     * @param null $reset_password_token
     * @return bool
     */
    public function reset_password_token($reset_password_token = null)
    {
        $user = $this->Users->find('all')
            ->contain(['Peoples'])
            ->where(['reset_password_token' => $reset_password_token])->first();

        if (!empty($user['reset_password_token']) && !empty($user['reset_password_token_created_at']) && $this->__validToken($user['reset_password_token_created_at'])
        ) {
            $data = ['password' => $this->request->data('password')];
            $user = $this->Users->patchEntity($user, $data);
            if ($this->Users->save($user)) {
                $data = [
                    'reset_password_token' => null,
                    'reset_password_token_created_at' => null
                ];
                $user = $this->Users->patchEntity($user, $data);
                if ($this->Users->save($user)) {
                    return true;
                }
            }
        }

        return false;
    }

    public function validToken($token = null)
    {
        $user = $this->Users->find('all')
            ->where(['reset_password_token' => $token])->first();
        if (!empty($user['reset_password_token'])
            && !empty($user['reset_password_token_created_at']) && $this->__validToken($user['reset_password_token_created_at'])
        ) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Generate a unique hash / token.
     * @param Object User
     * @return Object User
     */
    private function __generatePasswordToken()
    {
        // Generate a random string 100 chars in length.
        $token = "";
        for ($i = 0; $i < 100; $i++) {
            $d = rand(1, 100000) % 2;
            $d ? $token .= chr(rand(33, 79)) : $token .= chr(rand(80, 126));
        }
        (rand(1, 100000) % 2) ? $token = strrev($token) : $token = $token;
        // Generate hash of random string
        $hash = Security::hash($token, 'sha256', true);;
        for ($i = 0; $i < 20; $i++) {
            $hash = Security::hash($hash, 'sha256', true);
        }
        $return = [
            'reset_password_token' => $hash,
            'reset_password_token_created_at' => date('Y-m-d H:i:s')
        ];
        return $return;
    }

    /**
     * Validate token created at time.
     * @param String $token_created_at
     * @return Boolean
     */
    private function __validToken($token_created_at)
    {
        $expired = strtotime($token_created_at->format('Y-m-d H:i')) + 86400;
        $time = strtotime("now");
        if ($time < $expired) {
            return true;
        }
        return false;
    }

    /**
     * Enviar email com o link para o reset
     * @param null $user
     * @return bool
     */
    private function __sendForgotPasswordEmail($user = null)
    {
        if ($user) {
            $this->Mail->resetPassword($user);
            return true;
        }

        return false;
    }

    /**
     * Notifies user their password has changed.
     * @param $id
     * @return boolean
     */
    private function __sendPasswordChangedEmail($id = null)
    {
//        if (!empty($id)) {
//            $this->User->id = $id;
//            $user = $this->User->read();
//            return ($this->Mail->sendPasswordChanged($user)) ? true : false;
//        }
        return true;
    }


}
