<?php
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;

Router::plugin(
    'Flow',
    ['path' => '/flow'],
    function (RouteBuilder $routes) {
        $routes->fallbacks('DashedRoute');
    }
);
