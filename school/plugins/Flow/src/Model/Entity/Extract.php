<?php
namespace Flow\Model\Entity;

use Cake\ORM\Entity;

/**
 * Extract Entity
 *
 * @property int $id
 * @property int $fund_id
 * @property int $contract_id
 * @property int $fund_month_id
 * @property int $occurrence_id
 * @property string $description
 * @property \Cake\I18n\FrozenTime $date
 * @property float $income
 * @property float $expense
 * @property bool $active
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\Fund $fund
 * @property \App\Model\Entity\Contract $contract
 * @property \App\Model\Entity\FundsMonth $funds_month
 * @property \App\Model\Entity\Occurrence $occurrence
 */
class Extract extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
