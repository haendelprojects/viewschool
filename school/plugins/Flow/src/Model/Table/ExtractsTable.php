<?php
namespace Flow\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Extracts Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Funds
 * @property \Cake\ORM\Association\BelongsTo $Contracts
 * @property \Cake\ORM\Association\BelongsTo $FundsMonths
 * @property \Cake\ORM\Association\BelongsTo $Occurrences
 *
 * @method \App\Model\Entity\Extract get($primaryKey, $options = [])
 * @method \App\Model\Entity\Extract newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Extract[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Extract|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Extract patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Extract[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Extract findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ExtractsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('extracts');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Funds', [
            'foreignKey' => 'fund_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Contracts', [
            'foreignKey' => 'contract_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('FundsMonths', [
            'foreignKey' => 'fund_month_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Occurrences', [
            'foreignKey' => 'occurrence_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->requirePresence('description', 'create')
            ->notEmpty('description');

        $validator
            ->dateTime('date')
            ->requirePresence('date', 'create')
            ->notEmpty('date');

        $validator
            ->numeric('income')
            ->allowEmpty('income');

        $validator
            ->numeric('expense')
            ->allowEmpty('expense');

        $validator
            ->boolean('active')
            ->requirePresence('active', 'create')
            ->notEmpty('active');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));
        $rules->add($rules->existsIn(['fund_id'], 'Funds'));
        $rules->add($rules->existsIn(['contract_id'], 'Contracts'));
        $rules->add($rules->existsIn(['fund_month_id'], 'FundsMonths'));
        $rules->add($rules->existsIn(['occurrence_id'], 'Occurrences'));

        return $rules;
    }
}
