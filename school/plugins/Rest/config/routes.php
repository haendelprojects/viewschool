<?php

/**
 * CakePHP 3.x - Permission Manager
 *
 * PHP version 5
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @category CakePHP3
 *
 * @author   Ivan Amat <dev@ivanamat.es>
 * @copyright     Copyright 2016, Iván Amat
 * @license  MIT http://opensource.org/licenses/MIT
 * @link     https://github.com/ivanamat/cakephp3-aclmanager
 *
 */

use Cake\Routing\Router;


Router::plugin('Rest', ['path' => '/rest'], function ($routes) {
    $routes->setExtensions(['json']);
    $routes->resources('Addresses');
    $routes->resources('Contracts');
    $routes->resources('Users');

    $routes->connect('/addresses/:action/*', [
        'plugin' => 'Rest',
        'controller' => 'Addresses'
    ]);


    $routes->connect('/contracts/:action/*', [
        'plugin' => 'Rest',
        'controller' => 'Contracts'
    ]);


    $routes->connect('/users/:action/*', [
        'plugin' => 'Rest',
        'controller' => 'Users'
    ]);
});