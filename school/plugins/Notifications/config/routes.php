<?php
use Cake\Routing\Router;

Router::plugin(
    'AppNotifications',
    ['path' => '/app-notifications'],
    function ($routes) {
        $routes->fallbacks('DashedRoute');
    }
);
