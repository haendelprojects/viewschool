<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * AppNotification Entity.
 *
 * @property int $id
 * @property string $type
 * @property bool $sent
 * @property \App\Model\Entity\Model $model
 * @property int $model_id
 * @property int $user_id
 * @property \App\Model\Entity\User $user
 * @property \Cake\I18n\Time $updated_at
 */
class AppNotification extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
