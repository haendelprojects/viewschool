<?php
/**
 * Created by PhpStorm.
 * User: Haendel
 * Date: 29/06/2016
 * Time: 13:54
 */

namespace Notifications\Controller\Component;


use Cake\Controller\Component;

class OneSignalComponent extends Component
{
    private $url;
    private $token;
    private $appId;
    private $heading = [];
    private $content = [];
    private $data = [];
    private $tags = [];


    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->url = "https://onesignal.com/api/v1/notifications";
    }

    /**
     * Configurar dados de acesso;
     * @param $token
     * @param $appId
     * @return $this
     */
    public function create($token, $appId)
    {
        $this->token = $token;
        $this->appId = $appId;
        return $this;
    }

    /**
     * Limpar todo o conteudo
     */
    public function clear()
    {
        $this->heading = [];
        $this->content = [];
        $this->data = [];
        $this->tags = [];

        return $this;
    }


    /**
     * Setar as informações de titulo da notificação
     * @param $lang
     * @param $text
     * @return $this
     */
    public function setHeading($lang, $text)
    {
        $this->heading[$lang] = $text;

        return $this;
    }

    /**
     * Setar contendo da notificação
     * @param $lang
     * @param $text
     * @return $this
     */
    public function setContent($lang, $text)
    {
        $this->content[$lang] = $text;

        return $this;
    }

    /**
     * Setar o conteudo extra da notificação
     * @param $key
     * @param $value
     * @return $this
     */
    public function setData($key, $value)
    {
        $this->data[$key] = $value;
        return $this;
    }

    /**
     * Identificar os usuarios que vão receber a notificação
     * @param $key
     * @param $relation
     * @param $value
     * @return $this
     */
    public function setTag($key, $relation, $value)
    {
        array_push($this->tags, ["key" => $key, "relation" => $relation, "value" => $value]);
        return $this;
    }

    /**
     * Enviar a notificação
     * @return mixed
     */
    public function send()
    {
        return $this->sendMessage($this->heading, $this->content, $this->data, $this->tags);
    }

    /**
     * @param array $fields
     * @param $type
     * @return mixed
     */
    private function post($fields = [])
    {
        $auth = $this->token;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
            'Authorization: Basic ' . $auth));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

    /**
     * @param array $heading
     * @param array $content
     * @param array $data
     * @param array $tags
     * @return mixed
     */
    public function sendMessage($heading = [], $content = [], $data = [], $tags = [])
    {
        $heading = $heading;
        $content = $content;
        $fields = [
            'app_id' => $this->appId,
            'included_segments' => array('All'),
            'data' => $data,
            'contents' => $content,
            'headings' => $heading,
            'tags' => $tags,
        ];

        $response = $this->post($fields);

        return $response;
    }

}