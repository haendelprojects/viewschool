<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CdusuariosClinicasFixture
 *
 */
class CdusuariosClinicasFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'cdUsuario_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'clinica_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'fk_usuarios_clinicas_usuarios1_idx' => ['type' => 'index', 'columns' => ['cdUsuario_id'], 'length' => []],
            'fk_usuarios_clinicas_clinicas1_idx' => ['type' => 'index', 'columns' => ['clinica_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_usuarios_clinicas_clinicas1' => ['type' => 'foreign', 'columns' => ['clinica_id'], 'references' => ['clinicas', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_usuarios_clinicas_usuarios1' => ['type' => 'foreign', 'columns' => ['cdUsuario_id'], 'references' => ['cdusuarios', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'cdUsuario_id' => 1,
            'clinica_id' => 1
        ],
    ];
}
