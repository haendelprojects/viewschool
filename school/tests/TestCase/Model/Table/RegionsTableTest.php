<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RegionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RegionsTable Test Case
 */
class RegionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\RegionsTable
     */
    public $Regions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.regions',
        'app.cities',
        'app.states',
        'app.addresses',
        'app.countries',
        'app.holidays',
        'app.individuals',
        'app.users',
        'app.individuals_tests',
        'app.tests',
        'app.documents',
        'app.individuals_auths',
        'app.auths',
        'app.socialmedias',
        'app.links',
        'app.phones',
        'app.service_providers',
        'app.enterprises',
        'app.contracts',
        'app.contracts_tools',
        'app.tools',
        'app.services',
        'app.credits',
        'app.account_statements',
        'app.extracts',
        'app.funds',
        'app.funds_months',
        'app.occurrences',
        'app.external_os',
        'app.prices',
        'app.services_contracts',
        'app.payment_methods',
        'app.coupons',
        'app.comments',
        'app.historics',
        'app.medias',
        'app.ratings',
        'app.reviews',
        'app.ratings_reviews',
        'app.disabled_reasons',
        'app.skills',
        'app.individuals_skills',
        'app.proposed_occurrences'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Regions') ? [] : ['className' => RegionsTable::class];
        $this->Regions = TableRegistry::get('Regions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Regions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
