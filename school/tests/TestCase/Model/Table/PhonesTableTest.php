<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PhonesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PhonesTable Test Case
 */
class PhonesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PhonesTable
     */
    public $Phones;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.phones',
        'app.service_providers',
        'app.individuals',
        'app.users',
        'app.individuals_tests',
        'app.tests',
        'app.addresses',
        'app.cities',
        'app.states',
        'app.regions',
        'app.holidays',
        'app.countries',
        'app.proposed_occurrences',
        'app.enterprises',
        'app.contracts',
        'app.contracts_tools',
        'app.tools',
        'app.services',
        'app.credits',
        'app.account_statements',
        'app.extracts',
        'app.funds',
        'app.funds_months',
        'app.occurrences',
        'app.external_os',
        'app.prices',
        'app.coupons',
        'app.comments',
        'app.historics',
        'app.medias',
        'app.ratings',
        'app.services_contracts',
        'app.disabled_reasons',
        'app.documents',
        'app.individuals_auths',
        'app.auths',
        'app.socialmedias',
        'app.links',
        'app.skills',
        'app.individuals_skills'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Phones') ? [] : ['className' => PhonesTable::class];
        $this->Phones = TableRegistry::get('Phones', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Phones);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
