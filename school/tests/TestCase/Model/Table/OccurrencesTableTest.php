<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OccurrencesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OccurrencesTable Test Case
 */
class OccurrencesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\OccurrencesTable
     */
    public $Occurrences;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.occurrences',
        'app.external_os',
        'app.contracts',
        'app.enterprises',
        'app.addresses',
        'app.cities',
        'app.states',
        'app.regions',
        'app.holidays',
        'app.countries',
        'app.proposed_occurrences',
        'app.individuals',
        'app.users',
        'app.individuals_tests',
        'app.tests',
        'app.documents',
        'app.individuals_auths',
        'app.auths',
        'app.socialmedias',
        'app.links',
        'app.phones',
        'app.skills',
        'app.individuals_skills',
        'app.disabled_reasons',
        'app.extracts',
        'app.funds',
        'app.funds_months',
        'app.contracts_tools',
        'app.tools',
        'app.services',
        'app.credits',
        'app.account_statements',
        'app.services_contracts',
        'app.prices',
        'app.coupons',
        'app.comments',
        'app.historics',
        'app.medias',
        'app.ratings'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Occurrences') ? [] : ['className' => OccurrencesTable::class];
        $this->Occurrences = TableRegistry::get('Occurrences', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Occurrences);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
