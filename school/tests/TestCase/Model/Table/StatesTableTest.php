<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\StatesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\StatesTable Test Case
 */
class StatesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\StatesTable
     */
    public $States;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.states',
        'app.countries',
        'app.addresses',
        'app.cities',
        'app.regions',
        'app.prices',
        'app.services_contracts',
        'app.contracts',
        'app.enterprises',
        'app.disabled_reasons',
        'app.users',
        'app.extracts',
        'app.funds',
        'app.funds_months',
        'app.occurrences',
        'app.external_os',
        'app.coupons',
        'app.services',
        'app.contracts_tools',
        'app.tools',
        'app.skills',
        'app.questions',
        'app.tests',
        'app.tests_questions',
        'app.answers',
        'app.individuals',
        'app.individuals_tests',
        'app.documents',
        'app.individuals_auths',
        'app.auths',
        'app.socialmedias',
        'app.links',
        'app.phones',
        'app.service_providers',
        'app.individuals_skills',
        'app.services_skills',
        'app.comments',
        'app.historics',
        'app.medias',
        'app.ratings',
        'app.reviews',
        'app.ratings_reviews',
        'app.credits',
        'app.account_statements',
        'app.payment_methods',
        'app.holidays',
        'app.proposed_occurrences'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('States') ? [] : ['className' => StatesTable::class];
        $this->States = TableRegistry::get('States', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->States);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
