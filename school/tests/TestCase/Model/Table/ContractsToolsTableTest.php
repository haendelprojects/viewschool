<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ContractsToolsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ContractsToolsTable Test Case
 */
class ContractsToolsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ContractsToolsTable
     */
    public $ContractsTools;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.contracts_tools',
        'app.tools',
        'app.services',
        'app.contracts',
        'app.enterprises',
        'app.credits',
        'app.extracts',
        'app.funds',
        'app.occurrences',
        'app.services_contracts'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ContractsTools') ? [] : ['className' => ContractsToolsTable::class];
        $this->ContractsTools = TableRegistry::get('ContractsTools', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ContractsTools);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
