<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ClassDisciplineTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ClassDisciplineTable Test Case
 */
class ClassDisciplineTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ClassDisciplineTable
     */
    public $ClassDiscipline;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.class_discipline',
        'app.class',
        'app.courses',
        'app.branchs',
        'app.addresses',
        'app.peoples',
        'app.users',
        'app.calendars',
        'app.comments',
        'app.schools',
        'app.frequencies',
        'app.registrations',
        'app.dicipline',
        'app.evaluations',
        'app.organizations'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ClassDiscipline') ? [] : ['className' => ClassDisciplineTable::class];
        $this->ClassDiscipline = TableRegistry::get('ClassDiscipline', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ClassDiscipline);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
