<?php
namespace App\Test\TestCase\Controller;

use App\Controller\StatesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\StatesController Test Case
 */
class StatesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.states',
        'app.countries',
        'app.addresses',
        'app.cities',
        'app.regions',
        'app.prices',
        'app.services_contracts',
        'app.contracts',
        'app.enterprises',
        'app.disabled_reasons',
        'app.users',
        'app.roles',
        'app.notifications',
        'app.facebooks',
        'app.availabilities',
        'app.comments',
        'app.occurrences',
        'app.external_os',
        'app.coupons',
        'app.services',
        'app.contracts_tools',
        'app.tools',
        'app.skills',
        'app.questions',
        'app.tests',
        'app.tests_questions',
        'app.answers',
        'app.trainings',
        'app.videos',
        'app.tutorials',
        'app.individuals',
        'app.individuals_tests',
        'app.documents',
        'app.individuals_auths',
        'app.auths',
        'app.socialmedias',
        'app.links',
        'app.phones',
        'app.service_providers',
        'app.individuals_skills',
        'app.services_skills',
        'app.extracts',
        'app.funds',
        'app.funds_months',
        'app.historics',
        'app.medias',
        'app.ratings',
        'app.reviews',
        'app.ratings_reviews',
        'app.configurations',
        'app.favorite_specialists',
        'app.messages',
        'app.proposed_occurrences',
        'app.transportations',
        'app.credits',
        'app.account_statements',
        'app.payment_methods',
        'app.holidays'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
