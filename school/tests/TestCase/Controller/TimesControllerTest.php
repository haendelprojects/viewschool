<?php
namespace App\Test\TestCase\Controller;

use App\Controller\TimesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\TimesController Test Case
 */
class TimesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.times',
        'app.class_discipline',
        'app.class',
        'app.courses',
        'app.branchs',
        'app.addresses',
        'app.peoples',
        'app.users',
        'app.calendars',
        'app.comments',
        'app.schools',
        'app.frequencies',
        'app.registrations',
        'app.dicipline',
        'app.evaluations',
        'app.organizations',
        'app.disciplines'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
