<?php
namespace App\Test\TestCase\Controller;

use App\Controller\ContractsToolsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\ContractsToolsController Test Case
 */
class ContractsToolsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.contracts_tools',
        'app.tools',
        'app.services',
        'app.occurrences',
        'app.external_os',
        'app.contracts',
        'app.enterprises',
        'app.addresses',
        'app.cities',
        'app.states',
        'app.countries',
        'app.holidays',
        'app.regions',
        'app.prices',
        'app.services_contracts',
        'app.payment_methods',
        'app.proposed_occurrences',
        'app.users',
        'app.roles',
        'app.notifications',
        'app.facebooks',
        'app.availabilities',
        'app.comments',
        'app.configurations',
        'app.disabled_reasons',
        'app.favorite_specialists',
        'app.historics',
        'app.individuals',
        'app.individuals_tests',
        'app.tests',
        'app.skills',
        'app.questions',
        'app.tests_questions',
        'app.answers',
        'app.individuals_skills',
        'app.services_skills',
        'app.trainings',
        'app.videos',
        'app.tutorials',
        'app.documents',
        'app.individuals_auths',
        'app.auths',
        'app.socialmedias',
        'app.links',
        'app.phones',
        'app.service_providers',
        'app.medias',
        'app.messages',
        'app.transportations',
        'app.extracts',
        'app.funds',
        'app.funds_months',
        'app.credits',
        'app.account_statements',
        'app.coupons',
        'app.ratings',
        'app.reviews',
        'app.ratings_reviews'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
