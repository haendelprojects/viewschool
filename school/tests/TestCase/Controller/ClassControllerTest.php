<?php
namespace App\Test\TestCase\Controller;

use App\Controller\ClassController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\ClassController Test Case
 */
class ClassControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.class',
        'app.courses',
        'app.branchs',
        'app.addresses',
        'app.peoples',
        'app.users',
        'app.calendars',
        'app.comments',
        'app.class_discipline',
        'app.dicipline',
        'app.grid',
        'app.frequencies',
        'app.registrations',
        'app.schools',
        'app.evaluations',
        'app.organizations'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
