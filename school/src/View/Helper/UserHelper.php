<?php

/**
 * CakePHP 3.x - Acl Manager
 *
 * PHP version 5
 *
 * Class AclHelper
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @category CakePHP3
 *
 * @package  AclManager\View\Helper
 *
 * @author Ivan Amat <dev@ivanamat.es>
 * @copyright Copyright 2016, Iván Amat
 * @license MIT http://opensource.org/licenses/MIT
 * @link https://github.com/ivanamat/cakephp3-aclmanager
 */

namespace App\View\Helper;

use Cake\Core\Configure;
use Cake\Utility\Inflector;
use Cake\View\Helper;

class UserHelper extends Helper
{
    public $helpers = ['Html'];
    private $User = [];


    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->User = $this->request->session()->read('Auth.User');
    }

    public function peopleId()
    {
        return $this->User['people']['id'];
    }

    public function getName()
    {
        return $this->User['people']['name'];
    }

    function getListClinicsByUser()
    {
        $clinics = [];
        foreach ($this->User['clinics'] as $clinic) {
            $clinics[$clinic['id']] = $clinic['name'];
        }

        return $clinics;
    }

    function getSchoolName()
    {
        return $this->User['school']['name'];
    }


    public function isSchool()
    {
        return $this->User['access_school'];
    }

    public function isTeacher()
    {
        return $this->User['access_teacher'];
    }

    public function isStudent()
    {
        return $this->User['access_student'];
    }
}