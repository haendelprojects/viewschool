<?php
/**
 * Created by PhpStorm.
 * User: Haendel
 * Date: 16/08/2016
 * Time: 16:46
 */

namespace App\View\Helper;


use Cake\View\Helper;

class StatusHelper extends Helper
{

    private $status = [
        'OPENED' => 'Buscando Técnico',
        'ACCEPTED' => 'Aceito',
        'GOING' => 'Saiu para Atendimento',
        'ON_SERVICE' => 'Em Atendimento',
        'AWAITING_PAYMENT' => 'Pagamento/Validação',
        'PAYED_SPECIALIST' => 'Técnico Pendente',
        'FINISHED' => 'Finalizado',
        'CANCELED' => 'Cancelado',
        'UNPRODUCTIVE' => 'Improdutivo',
        'INVALID' => 'Não Validado',
        'REJECTED' => 'Rejeitado',
        'RESCHEDULE' => 'Reagendado'
    ];

    public function getList()
    {
        return $this->status;
    }

    public function getName($status)
    {
        return ($status) ? $this->status[$status] : '';
    }

    public function statusCount($occurrences, $status)
    {
        foreach ($occurrences as $d) {
            if ($d->status == $status) {
                return $d->count;
            }
        }

        return 0;
    }

    public function slaDanger($schedule, $checkin, $status)
    {
        if (in_array($status, ['ON_SERVICE', 'AWAITING_PAYMENT', 'PAYED_SPECIALIST', 'FINISHED']) && ($schedule && $checkin)) {

            if ($schedule->add(new \DateInterval('PT10M')) < $checkin) {
                return 'label label-danger';
            } else {
                return 'label label-success';
            }
        } else {
            return 'label label-default';
        }
    }

    public function slaDangerText($schedule, $checkin, $status)
    {
        if (in_array($status, ['ON_SERVICE', 'AWAITING_PAYMENT', 'PAYED_SPECIALIST', 'FINISHED']) && ($schedule && $checkin)) {

            if ($schedule->add(new \DateInterval('PT10M')) < $checkin) {
                return 'Violado';
            } else {
                return 'Ok';
            }
        } else {
            return 'Ok';
        }
    }


}