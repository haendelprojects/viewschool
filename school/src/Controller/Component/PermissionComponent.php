<?php
/**
 * Created by PhpStorm.
 * User: Ananda Almeida
 * Date: 08/11/2017
 * Time: 21:03
 */

namespace App\Controller\Component;


use Cake\Controller\Component;

/**
 * Class PermissionComponent
 * @package App\Controller\Component
 *
 * @property Component\AuthComponent $Auth
 */
class PermissionComponent extends Component
{

    public $components = ['Auth'];

    public function initialize(array $config)
    {
        parent::initialize($config);
    }

    public function isSchool()
    {
        return $this->Auth->user('access_school');
    }

    public function isTeacher()
    {
        return $this->Auth->user('access_teacher');
    }

    public function isStudent()
    {
        return $this->Auth->user('access_student');
    }

    public function redirect()
    {
        return ['controller' => 'dashboard'];
    }
}