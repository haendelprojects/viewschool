<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Lessons Controller
 *
 * @property \App\Model\Table\LessonsTable $Lessons
 *
 * @method \App\Model\Entity\Lesson[] paginate($object = null, array $settings = [])
 */
class LessonsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Class.Courses', 'Disciplines'],
            'order' => [
                'day' => 'desc'
            ]
        ];

        $conditions = [
            'type' => 'Aula',
            'Lessons.school_id' => $this->Auth->user('school_id')
        ];

        if ($this->Auth->user('access_teacher')) {
            $conditions['user_id'] = $this->Auth->user('id');
        }
        $lessons = $this->paginate($this->Lessons->find('all')
            ->where($conditions));

        $this->set(compact('lessons'));
        $this->set('_serialize', ['lessons']);
    }

    /**
     * View method
     *
     * @param string|null $id Lesson id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $lesson = $this->Lessons->get($id, [
            'contain' => ['Class', 'Disciplines', 'Frequencies.Peoples'],
            'contains' => ['school_id' => $this->Auth->user('school_id')]
        ]);

        $this->set('lesson', $lesson);
        $this->set('_serialize', ['lesson']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $lesson = $this->Lessons->newEntity();
        if ($this->request->is('post')) {
            $this->request->data('day', \DateTime::createFromFormat('d/m/Y', $this->request->data('day_base')));
            $this->request->data('type', 'Aula');
            $this->request->data('school_id', $this->Auth->user('school_id'));
            $this->request->data('user_id', $this->Auth->user('id'));
            $lesson = $this->Lessons->patchEntity($lesson, $this->request->data);

            if ($this->Lessons->save($lesson)) {
                $this->Flash->success(__('Aula cadastrada com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Falha, tente novamente'));
            }
        }

        if (!$this->Auth->user('access_teacher')) {
            $all_classes = $this->Lessons->Class->find('all', ['limit' => 200])
                ->contain(['Courses'])
                ->where(['Class.school_id' => $this->Auth->user('school_id')]);

            foreach ($all_classes as $class) {
                $classes[$class->id] = $class->course->name . ' ' . $class->name . ' - ' . $class->school_year;
            }
        } else {
            $all_classes = $this->Lessons->Class->ClassDiscipline->find('all', ['limit' => 200])
                ->contain(['Class.Courses'])
                ->where([
                    'ClassDiscipline.people_id' => $this->Auth->user('people.id'),
                    'ClassDiscipline.school_id' => $this->Auth->user('school_id')
                ]);
            foreach ($all_classes as $c) {
                $classes[$c->clas->id] = $c->clas->course->name . ' ' . $c->clas->name . ' - ' . $c->clas->school_year;
            }
        }
        $disciplines = $this->Lessons->Disciplines->find('list', ['limit' => 200])->where(['school_id' => $this->Auth->user('school_id')]);
        $this->set(compact('lesson', 'classes', 'disciplines'));
        $this->set('_serialize', ['lesson']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Lesson id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $lesson = $this->Lessons->get($id, [
            'contain' => [],
            'conditions' => [
                'school_id' => $this->Auth->user('school_id')
            ]
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $lesson = $this->Lessons->patchEntity($lesson, $this->request->data);
            if ($this->Lessons->save($lesson)) {
                $this->Flash->success(__('Sucesso'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Falha, tente novamente'));
            }
        }
        $all_classes = $this->Lessons->Class->find('all', ['limit' => 200])
            ->contain(['Courses'])
            ->where(['Class.school_id' => $this->Auth->user('school_id')]);
        foreach ($all_classes as $class) {
            $classes[$class->id] = $class->course->name . ' ' . $class->name . ' - ' . $class->school_year;
        }
        $disciplines = $this->Lessons->Disciplines->find('list', ['limit' => 200])->where(['school_id' => $this->Auth->user('school_id')]);
        $this->set(compact('lesson', 'classes', 'disciplines'));
        $this->set('_serialize', ['lesson']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Lesson id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $lesson = $this->Lessons->get($id, [
            'conditions' => [
                'school_id' => $this->Auth->user('school_id')
            ]
        ]);
        if ($this->Lessons->delete($lesson)) {
            $this->Flash->success(__('Sucesso'));
        } else {
            $this->Flash->error(__('Falha, tente novamente'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
