<?php
/**
 * Created by PhpStorm.
 * User: erick
 * Date: 24/08/2017
 * Time: 11:31
 */

namespace App\Controller;

use App\Model\Table\ConfigurationsTable;

/**
 * Class ConfigurationsController
 * @package App\Controller
 *
 * @property ConfigurationsTable $Configurations
 */
class ConfigurationsController extends AppController
{


    public function initialize()
    {
        parent::initialize();

        if ($this->Auth->user()) {
            $this->Auth->allow(['saveList', 'delete']);
        }

        $this->loadModel('Configurations');
    }

    public function index()
    {

    }

    public function saveList()
    {


        $data = [
            'user_id' => $this->Auth->user('id'),
            'config_key' => $this->request->getData('config_key'),
            'config_value' => json_encode($this->request->getData('config_value')),
            'public' => $this->request->getData('public'),
            'active' => true
        ];

        $configuration = $this->Configurations->newEntity();

        $configuration = $this->Configurations->patchEntity($configuration, $data);

        if ($this->Configurations->save($configuration)) {
            $this->Flash->success('Lista salva');
            $this->redirect($this->referer());
        } else {
            $this->redirect($this->referer());
            $this->Flash->error('Falha, tente novamente');
        }
    }

    public function delete($id = null)
    {
        $occurrence = $this->Configurations->get($id);
        if ($this->Configurations->delete($occurrence)) {
            $this->Flash->success(__('Removido com sucesso'));
        } else {
            $this->Flash->error(__('Falha, tente novamente'));
        }
        return $this->redirect($this->referer());
    }
}