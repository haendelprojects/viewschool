<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Registration Entity
 *
 * @property int $id
 * @property string $year
 * @property float $value_material
 * @property float $value_monthly
 * @property float $value_registration
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property string $discount_type
 * @property float $discount_value
 * @property string $observation
 * @property int $people_id
 * @property int $class_id
 * @property int $branchs_id
 *
 * @property \App\Model\Entity\People $people
 * @property \App\Model\Entity\Clas $clas
 * @property \App\Model\Entity\Branch $branch
 */
class Registration extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
