<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Calendar Entity
 *
 * @property int $id
 * @property string $title
 * @property string $text
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $user_id
 * @property int $class_id
 * @property int $course_id
 * @property int $branch_id
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Clas $clas
 * @property \App\Model\Entity\Course $course
 * @property \App\Model\Entity\Branch $branch
 */
class Calendar extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
