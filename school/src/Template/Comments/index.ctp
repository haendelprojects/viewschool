<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Quadro de avisos

        <?php if ($this->User->isSchool()) : ?>
            <div class="pull-right"><?= $this->Html->link(__('Novo Aviso'), ['action' => 'add'], ['class' => 'btn btn-success btn-xs']) ?></div>
        <?php endif; ?>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <?php if ($this->User->isSchool()) : ?>

                <div class="box">
                    <div class="box-body table-responsive no-padding">


                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th><?= $this->Paginator->sort('title', 'Titulo') ?></th>
                                <th><?= __('Avisos') ?></th>
                            </tr>


                            <tr>
                                <?= $this->Form->create(null, ['valueSources' => ['query']]); ?>

                                <th>
                                    <?= $this->Form->input('title', ['label' => false, 'class' => 'form-control', 'placeholder' => ('Assunto')]); ?>
                                </th>
                                <th>
                                </th>
                                <th>
                                    <div class="form-group select">
                                        <?php
                                        echo $this->Form->button('<i class="fa fa-filter"></i>', ['type' => 'submit', 'class' => 'btn btn-default', ' escape' => false]);
                                        if ($isSearch) {
                                            echo $this->Html->link('<i class="fa fa-trash"></i>', ['action' => 'index'], ['class' => 'btn btn-danger ', 'style' => 'margin-left: 10px', 'escape' => false]);
                                        }
                                        ?>
                                    </div>
                                </th>
                                <?= $this->Form->end(); ?>
                            </tr>

                            </thead>
                            <tbody>
                            <?php foreach ($comments as $comment): ?>
                                <tr>
                                    <td><?= h($comment->title) ?></td>
                                    <td class="actions" style="white-space:nowrap">
                                        <?= $this->Html->link(__('Detalhes'), ['action' => 'view', $comment->id], ['class' => 'btn btn-info btn-xs']) ?>
                                        <?= $this->Html->link(__('Editar'), ['action' => 'edit', $comment->id], ['class' => 'btn btn-warning btn-xs']) ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>


                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix">
                        <ul class="pagination pagination-sm no-margin pull-right">
                            <?php echo $this->Paginator->numbers(); ?>
                        </ul>
                    </div>
                </div>


            <?php else: ?>
                <ul class="timeline">
                    <?php foreach ($comments as $comment): ?>
                        <li>
                            <i class="fa fa-envelope bg-blue"></i>

                            <div class="timeline-item">
                                <span class="time"><i class="fa fa-clock-o"></i> <?= $comment->created->format('d/m/Y H:i')?></span>

                                <h3 class="timeline-header"><a
                                            href="#"><?= $comment->title ?></a> <?= $comment->user->name ?></h3>

                                <div class="timeline-body">
                                    <?= $comment->text ?>
                                </div>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>
            <!-- /.box -->
        </div>
    </div>
</section>
<!-- /.content -->
