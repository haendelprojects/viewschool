<section class="content-header">
    <h1>
        <?php echo __('Aluno'); ?>
        <small><?= $people->name ?></small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <?= $this->Html->link('<i class="fa fa-arrow-left"></i> ' . __('Voltar'), ['action' => 'index'], ['escape' => false]) ?>
        </li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-3">

            <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <?php echo $this->Html->image(($people->foto) ? $people->foto : 'avatar.png', array('class' => 'profile-user-img img-responsive img-circle', 'alt' => 'User profile picture')); ?>
                    <p class="text-muted text-center"><?= $people->email ?></p>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#activity" data-toggle="tab"><i class="fa fa-info"></i> Informações</a>
                    </li>
                    <li><a href="#endereco" data-toggle="tab"><i class="fa fa-map"></i> Endereço</a></li>
                    <li><a href="#responsaveis" data-toggle="tab"><i class="fa fa-users"></i> Responsáveis</a></li>
                    <li><a href="#system" data-toggle="tab"><i class="fa fa-lock"></i> Sistema</a></li>
                </ul>
                <div class="tab-content">
                    <div class=" active tab-pane" id="activity">
                        <dl class="dl-horizontal">
                            <dt><?= __('Nome') ?></dt>
                            <dd>
                                <?= h($people->name) ?>
                            </dd>
                            <dt><?= __('CPF') ?></dt>
                            <dd>
                                <?= h($people->cpf) ?>
                            </dd>
                            <dt><?= __('Nascimento') ?></dt>
                            <dd>
                                <?= h($people->birth) ?>
                            </dd>
                            <dt><?= __('Sexo') ?></dt>
                            <dd>
                                <?= h($people->sexo) ?>
                            </dd>
                            <dt><?= __('Pai') ?></dt>
                            <dd>
                                <?= h($people->father_name) ?>
                            </dd>
                            <dt><?= __('Mãe') ?></dt>
                            <dd>
                                <?= h($people->mother_name) ?>
                            </dd>
                            <dt><?= __('Telefone') ?></dt>
                            <dd>
                                <?= h($people->phone) ?>
                            </dd>
                            <dt><?= __('Celular') ?></dt>
                            <dd>
                                <?= h($people->cell_phone) ?>
                            </dd>

                            <dt><?= __('Email') ?></dt>
                            <dd>
                                <?= h($people->email) ?>
                            </dd>

                        </dl>

                        <?= $this->element('Forms/peopleModal', ['data' => $people, 'url' => ['action' => 'edit', $people->id]]) ?>
                    </div>
                    <div class=" tab-pane" id="endereco">
                        <dl class="dl-horizontal">
                            <dt><?= __('Endereço') ?></dt>
                            <dd>
                                <?= h($people->address->street) ?>, <?= h($people->address->number) ?>
                            </dd>
                            <dt><?= __('Bairro') ?></dt>
                            <dd>
                                <?= h($people->address->neighborhood) ?>
                            </dd>
                            <dt><?= __('Cidade') ?></dt>
                            <dd>
                                <?= h($people->address->city) ?>
                            </dd>
                            <dt><?= __('Estado') ?></dt>
                            <dd>
                                <?= h($people->address->state) ?>
                            </dd>
                            <dt><?= __('Complemento') ?></dt>
                            <dd>
                                <?= h($people->address->complement) ?>
                            </dd>
                        </dl>

                        <?= $this->element('Forms/addressModal', ['data' => $people, 'prefix' => 'address', 'url' => ['action' => 'edit', $people->id]]) ?>
                    </div>
                    <div class="tab-pane" id="responsaveis">

                        <?= $this->element('Forms/responsibleModal', ['data' => $people_entity, 'people_id' => $people->id, 'url' => ['action' => 'responsible']]); ?>

                        <br>
                        <br>
                        <?php if (!empty($people->responsibles)): ?>

                            <table class="table table-hover">
                                <tbody>
                                <tr>
                                    <th>Nome</th>
                                    <th>Cpf</th>
                                    <th>Responsavel Financeiro</th>
                                    <th>Responsavel Pedagógico</th>
                                    <th>Email</th>
                                    <th>Telefone</th>
                                    <th>Celular</th>
                                    <th><?php echo __('Ações'); ?></th>
                                </tr>

                                <?php foreach ($people->responsibles as $responsible): ?>
                                    <tr>

                                        <td>
                                            <?= h($responsible->name) ?>
                                        </td>

                                        <td>
                                            <?= h($responsible->cpf) ?>
                                        </td>

                                        <td>
                                            <?= ($responsible->responsible_financial)  ?  'Sim': 'Não'?>
                                        </td>

                                        <td>
                                            <?= ($responsible->responsible_educational) ?  'Sim': 'Não' ?>
                                        </td>

                                        <td>
                                            <?= h($responsible->email) ?>
                                        </td>

                                        <td>
                                            <?= h($responsible->phone) ?>
                                        </td>

                                        <td>
                                            <?= h($responsible->cell_phone) ?>
                                        </td>

                                        <td class="actions">
                                            <?= $this->element('Forms/responsibleModal',
                                                [
                                                    'data' => $responsible,
                                                    'people_id' => $people->id,
                                                    'url' => ['action' => 'responsible', $responsible->id],
                                                    'text' => 'Editar',
                                                    'id' => $responsible->id
                                                ]); ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>

                                </tbody>
                            </table>

                        <?php endif; ?>
                    </div>
                    <div class="tab-pane" id="system">
                        <?php if ($people->user): ?>
                            <dl class="dl-horizontal">
                                <dt><?= __('Email') ?></dt>
                                <dd>
                                    <?= h($people->user->username) ?>
                                </dd>

                                <?php if ($people->user->password == '$2y$10$wPT7z.7HyMcLkL9zg4R93OZcJyRdDWHcqHCmLDDr1Ji1mP2t2.RlS'): ?>
                                    <dt><?= __('Senha') ?></dt>
                                    <dd>
                                        <?= $this->element('Forms/passwordModal', ['text' => 'Gerar', 'user' => $people->user, 'url' => ['controller' => 'users', 'action' => 'password', $people->user->id]]); ?>
                                    </dd>
                                <?php else: ?>
                                    <dt><?= __('Senha') ?></dt>
                                    <dd>
                                        <?= $this->element('Forms/passwordModal', ['text' => 'Editar', 'user' => $people->user, 'url' => ['controller' => 'users', 'action' => 'password', $people->user->id]]); ?>
                                    </dd>
                                <?php endif; ?>
                            </dl>
                        <?php else: ?>
                            <?= $this->element('Forms/userModal', ['user' => $user, 'prefix' => 'user', 'url' => ['action' => 'edit', $people->id]]); ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
