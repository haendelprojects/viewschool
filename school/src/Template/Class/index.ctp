<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Turmas

        <?php if ($this->User->isSchool()): ?>
            <div class="pull-right"><?= $this->Html->link(__('Novo'), ['action' => 'add'], ['class' => 'btn btn-success btn-xs']) ?></div>
        <?php endif; ?>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th><?= $this->Paginator->sort('course_id', 'Curso') ?></th>
                            <th><?= $this->Paginator->sort('name', 'Nome') ?></th>
                            <th><?= $this->Paginator->sort('school_year', 'Ano') ?></th>
                            <th><?= __('') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($class as $clas): ?>
                            <tr>
                                <td><?= $clas->has('course') ? $this->Html->link($clas->course->name, ['controller' => 'Courses', 'action' => 'view', $clas->course->id]) : '' ?></td>
                                <td><?= h($clas->name) ?></td>
                                <td><?= h($clas->school_year) ?></td>
                                <td class="actions" style="white-space:nowrap">
                                    <?= $this->Html->link(__('Detalhe'), ['action' => 'view', $clas->id], ['class' => 'btn btn-info btn-xs']) ?>
                                    <?php if ($this->User->isSchool()): ?>
                                        <?= $this->Html->link(__('Editar'), ['action' => 'edit', $clas->id], ['class' => 'btn btn-warning btn-xs']) ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right">
                        <?php echo $this->Paginator->numbers(); ?>
                    </ul>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
<!-- /.content -->
