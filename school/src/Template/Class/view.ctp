<section class="content-header">
    <h1>
        <?php echo __('Turma'); ?>
    </h1>
    <ol class="breadcrumb">
        <li>
            <?= $this->Html->link('<i class="fa fa-arrow-left"></i> ' . __('Voltar'), ['action' => 'index'], ['escape' => false]) ?>
        </li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">


            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#activity" data-toggle="tab"><i class="fa fa-info"></i>
                            Informações</a></li>
                    <li><a href="#disc" data-toggle="tab"><i class="fa fa-list"></i> Disciplinas</a></li>
                    <li><a href="#students" data-toggle="tab"><i class="fa fa-users"></i> Alunos Matriculados</a></li>
                    <li><a href="#grid" data-toggle="tab"><i class="fa fa-calendar"></i> Grade Horário</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane" id="grid">
                        <table class="table table-bordered">
                            <tr>
                                <th>Segunda</th>
                                <th>Terça</th>
                                <th>Quarta</th>
                                <th>Quinta</th>
                                <th>Sexta</th>
                                <th>Sábado</th>
                            </tr>

                            <tr>
                                <td>
                                    <?= $this->Util->dayDiscipline($clas->class_discipline, 'SEG'); ?>
                                </td>

                                <td>
                                    <?= $this->Util->dayDiscipline($clas->class_discipline, 'TER'); ?>
                                </td>

                                <td>
                                    <?= $this->Util->dayDiscipline($clas->class_discipline, 'QUA'); ?>
                                </td>

                                <td>
                                    <?= $this->Util->dayDiscipline($clas->class_discipline, 'QUI'); ?>
                                </td>

                                <td>
                                    <?= $this->Util->dayDiscipline($clas->class_discipline, 'SEX'); ?>
                                </td>

                                <td>
                                    <?= $this->Util->dayDiscipline($clas->class_discipline, 'SAB'); ?>
                                </td>
                            </tr>
                        </table>
                    </div>

                    <div class=" active tab-pane" id="activity">
                        <?php if ($this->User->isSchool()): ?>
                            <?= $this->Html->link('<i class="fa fa-pencil"></i> ' . __('Editar'), ['action' => 'edit', $clas->id], ['escape' => false, 'class' => 'pull-right btn btn-warning btn-xs']) ?>
                        <?php endif; ?>

                        <dl class="dl-horizontal">
                            <dt><?= __('Curso') ?></dt>
                            <dd>
                                <td><?= $clas->has('course') ? $this->Html->link($clas->course->name, ['controller' => 'Courses', 'action' => 'view', $clas->course->id]) : '' ?></td>
                            </dd>
                            <dt><?= __('Nome') ?></dt>
                            <dd>
                                <?= h($clas->name) ?>
                            </dd>
                            <dt><?= __('Unidade') ?></dt>
                            <dd>
                                <?= $clas->has('branch') ? $clas->branch->name : '' ?>
                            </dd>

                            <dt><?= __('Periodo') ?></dt>
                            <dd>
                                <?= h($clas->period) ?>
                            </dd>
                            <dt><?= __('Status') ?></dt>
                            <dd>
                                <?= h($clas->status) ?>
                            </dd>

                            <?php if ($this->User->isSchool()): ?>
                                <dt><?= __('Professor Responsável') ?></dt>
                                <dd>
                                    <td><?= $clas->has('people') ? $this->Html->link($clas->people->name, ['controller' => 'teachers', 'action' => 'view', $clas->people->id]) : '' ?></td>
                                </dd>
                            <?php endif; ?>

                            <dt><?= __('Carga Horária') ?></dt>
                            <dd>
                                <?= h($clas->class_time) ?>h
                            </dd>

                            <dt><?= __('Ano Letivo') ?></dt>
                            <dd>
                                <?= h($clas->school_year) ?>
                            </dd>

                            <dt><?= __('Data de inicio') ?></dt>
                            <dd>
                                <?= h($clas->init_date) ?>
                            </dd>
                            <dt><?= __('Data Final') ?></dt>
                            <dd>
                                <?= h($clas->end_date) ?>
                            </dd>

                            <dt><?= __('Critério Avaliativo') ?></dt>
                            <dd>
                                <td><?= $clas->has('evaluation') ? $clas->evaluation->name : '' ?></td>
                            </dd>


                        </dl>
                    </div>

                    <div class="tab-pane" id="disc">
                        <div class="">
                            <?php if ($this->User->isSchool()): ?>
                                <?= $this->element('Forms/addDisciplineModal', ['class_id' => $clas->id]); ?>
                            <?php endif; ?>
                            <br>
                            <br>

                            <table class="table table-striped  table-bordered">
                                <tr>
                                    <th>Disciplina</th>
                                    <th>Professor</th>
                                    <th>Nota Mínima</th>
                                    <th>Carga Horária</th>
                                    <th>Aulas</th>
                                    <th></th>
                                </tr>
                                <?php foreach ($clas->class_discipline as $discipline): ?>
                                    <tr>

                                        <td><?= $discipline->discipline->name ?></td>
                                        <td><?= $discipline->people->name ?></td>
                                        <td><?= $discipline->minimum_grade ?></td>
                                        <td><?= $discipline->workload ?> h</td>
                                        <td>
                                            <?php foreach ($discipline->times as $time) : ?>
                                                <?= $time->day_week ?> -  <?= $time->init_time ?>h às <?= $time->end_time ?>h
                                            <?php endforeach; ?>
                                        </td>
                                        <td class="actions" style="white-space:nowrap">
                                            <?php if ($this->User->isSchool()): ?>
                                                <?= $this->Html->link(__('Editar'), ['controller' => 'ClassDiscipline', 'action' => 'edit', $discipline->id], ['class' => 'btn btn-warning btn-xs']) ?>
                                                <?= $this->Form->postLink(__('Remover'), ['controller' => 'ClassDiscipline', 'action' => 'delete', $discipline->id], ['confirm' => __('Confirma a exclusão da disciplina para a classe?'), 'class' => 'btn btn-danger btn-xs']) ?>
                                            <?php endif ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </table>
                        </div>
                    </div>

                    <div class="tab-pane" id="students">
                        <?php if ($this->User->isSchool()): ?>
                            <?= $this->element('Forms/addRegistrationModal', ['class_id' => $clas->id]); ?>
                        <?php endif; ?>

                        <br>
                        <br>

                        <table class="table table-striped  table-bordered">
                            <tr>
                                <th>Aluno</th>
                                <th></th>
                            </tr>
                            <?php foreach ($clas->registrations as $registrate): ?>
                                <tr>

                                    <td><?= $registrate->people->name ?></td>
                                    <td class="actions" style="white-space:nowrap">
                                        <?php if ($this->User->isSchool()): ?>
                                            <?= $this->Html->link(__('Editar'), ['controller' => 'registrations', 'action' => 'edit', $registrate->id], ['class' => 'btn btn-warning btn-xs']) ?>
                                        <?php endif ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </table>

                    </div>
                </div>
            </div>
            <!-- /.box-body -->
            <!-- ./col -->
        </div>
        <!-- div -->

</section>
