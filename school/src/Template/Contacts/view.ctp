<section class="content-header">
    <h1>
        <?php echo __('Comentário'); ?>
    </h1>
    <ol class="breadcrumb">
        <li>
            <?= $this->Html->link('<i class="fa fa-arrow-left"></i> ' . __('Voltar'), ['action' => 'index'], ['escape' => false]) ?>
        </li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <i class="fa fa-info"></i>
                    <h3 class="box-title"><?php echo __('Informações'); ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt><?= __('Assunto') ?></dt>
                        <dd>
                            <?= h($comment->title) ?>
                        </dd>

                        <dt><?= __('Mensagem') ?></dt>
                        <dd>
                            <?= $this->Text->autoParagraph(h($comment->text)); ?>
                        </dd>

                        <?php if ($comment->has('people')) : ?>
                            <dt><?= __('Aluno') ?></dt>
                            <dd>
                                <?= $comment->people->name ?>
                            </dd>
                        <?php endif; ?>

                        <?php if ($comment->has('clas')) : ?>
                        <dt><?= __('Classe') ?></dt>
                        <dd>
                            <?= $comment->has('clas') ? $comment->clas->name : '' ?>
                        </dd>
                        <?php endif; ?>
                        <?php if ($comment->has('user')) : ?>
                        <dt><?= __('Enviado por') ?></dt>
                        <dd>
                            <?= $comment->has('user') ? $comment->user->username : '' ?>
                        </dd>
                        <?php endif; ?>
                    </dl>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- ./col -->
    </div>
    <!-- div -->

</section>
