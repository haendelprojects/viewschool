<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Contato com a escola

        <div class="pull-right"><?= ($this->User->isStudent()) ? $this->Html->link(__('Nova Mensagem para Escola'), ['action' => 'add'], ['class'=>'btn btn-success btn-xs'])  : ''?></div>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th><?= $this->Paginator->sort('title', 'Assunto') ?></th>
                            <th></th>
                        </tr>

                        <tr>
                            <?= $this->Form->create(null, ['valueSources' => ['query']]); ?>
                            <th>
                                <?= $this->Form->input('title', ['label' => false, 'class' => 'form-control', 'placeholder' => ('Assunto')]); ?>
                            </th>
                            <th>
                                <div class="form-group select">
                                    <?php
                                    echo $this->Form->button('<i class="fa fa-filter"></i>', ['type' => 'submit', 'class' => 'btn btn-default', ' escape' => false]);
                                    if ($isSearch) {
                                        echo $this->Html->link('<i class="fa fa-trash"></i>', ['action' => 'index'], ['class' => 'btn btn-danger ', 'style' => 'margin-left: 10px', 'escape' => false]);
                                    }
                                    ?>
                                </div>
                            </th>
                            <?= $this->Form->end(); ?>
                        </tr>

                        </thead>
                        <tbody>
                        <?php foreach ($comments as $comment): ?>
                            <tr>
                                <td><?= h($comment->title) ?></td>
                                <td class="actions" style="white-space:nowrap">
                                    <?= $this->Html->link(__('Detalhes'), ['action' => 'view', $comment->id], ['class' => 'btn btn-info btn-xs']) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right">
                        <?php echo $this->Paginator->numbers(); ?>
                    </ul>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
<!-- /.content -->
