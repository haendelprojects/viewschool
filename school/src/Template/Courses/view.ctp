<section class="content-header">
    <h1>
        <?php echo __('Curso'); ?>
    </h1>
    <ol class="breadcrumb">
        <li>
            <?= $this->Html->link('<i class="fa fa-arrow-left"></i> ' . __('Voltar'), ['action' => 'index'], ['escape' => false]) ?>
        </li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <i class="fa fa-info"></i>
                    <h3 class="box-title"><?php echo __('Informação'); ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt><?= __('Nome') ?></dt>
                        <dd>
                            <?= h($course->name) ?>
                        </dd>
                        <dt><?= __('Nível') ?></dt>
                        <dd>
                            <?= h($course->nivel) ?>
                        </dd>
                        <dt><?= __('Carga Horária') ?></dt>
                        <dd>
                            <?= $course->workload ?> h
                        </dd>
                        <dt><?= __('Ativo') ?></dt>
                        <dd>
                            <?= ($course->active) ? 'Sim' : 'Não' ?>
                        </dd>

                    </dl>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- ./col -->
    </div>
    <!-- div -->
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <i class="fa fa-list"></i>
                    <h3 class="box-title"><?= __('Turmas Relacionadas') ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">

                    <?php if (!empty($course->class)): ?>

                        <table class="table table-hover">
                            <tbody>
                            <tr>
                                <th>
                                    Inicio
                                </th>


                                <th>
                                    Fim
                                </th>


                                <th>
                                    Turno
                                </th>


                                <th>
                                    Nomeclatura
                                </th>

                                <th>
                                    Status
                                </th>

                                <th>
                                    Ano
                                </th>
                                <th>
                                    <?php echo __(''); ?>
                                </th>
                            </tr>

                            <?php foreach ($course->class as $class): ?>
                                <tr>
                                    <td>
                                        <?= h($class->init_date) ?>
                                    </td>

                                    <td>
                                        <?= h($class->end_date) ?>
                                    </td>

                                    <td>
                                        <?= h($class->period) ?>
                                    </td>

                                    <td>
                                        <?= h($class->name) ?>
                                    </td>

                                    <td>
                                        <?= h($class->status) ?>
                                    </td>
                                    <td>
                                        <?= h($class->school_year) ?>
                                    </td>

                                    <td class="actions">
                                        <?= $this->Html->link(__('Detalhes'), ['controller' => 'Class', 'action' => 'view', $class->id], ['class' => 'btn btn-info btn-xs']) ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>

                            </tbody>
                        </table>

                    <?php endif; ?>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
