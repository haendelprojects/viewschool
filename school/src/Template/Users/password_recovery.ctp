<div class="card">
    <div class="card-body ">

        <form accept-charset="utf-8"
              action="<?php echo $this->Url->build(array('controller' => 'users', 'action' => 'passwordRecovery')); ?>"
              method="post">
            <div class="text-center align-center">Por favor digite seu e-mail para
                que possamos recuperar sua senha.
            </div>

            <hr>

            <div class="form-group has-feedback">
                <input type="email" class="form-control" placeholder="E-mail" name="username">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div>
                <button type="submit" class="btn btn-primary btn-block btn-center ">Recuperar senha</button>
            </div>
            <hr>
            <div>
                <a href="<?php echo $this->Url->build(array('controller' => 'users', 'action' => 'login')); ?>"
                   class="btn btn-default btn-block btn-center" style="background: #D2D6DE">Voltar</a>
            </div>
            <!-- /.col -->
        </form>

    </div>
</div>