<section class="content-header">
    <h1>
        <?php echo __('User'); ?>
    </h1>
    <ol class="breadcrumb">
        <li>
            <?= $this->Html->link('<i class="fa fa-dashboard"></i> ' . __('Back'), ['action' => 'index'], ['escape' => false]) ?>
        </li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <i class="fa fa-info"></i>
                    <h3 class="box-title"><?php echo __('Information'); ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt><?= __('Email') ?></dt>
                        <dd>
                            <?= h($user->email) ?>
                        </dd>
                    </dl>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- ./col -->
    </div>
    <!-- div -->

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <i class="fa fa-share-alt"></i>
                    <h3 class="box-title"><?= __('Related {0}', ['Calendars']) ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">

                    <?php if (!empty($user->calendars)): ?>

                        <table class="table table-hover">
                            <tbody>
                            <tr>

                                <th>
                                    Id
                                </th>


                                <th>
                                    Title
                                </th>


                                <th>
                                    Text
                                </th>


                                <th>
                                    User Id
                                </th>


                                <th>
                                    Class Id
                                </th>


                                <th>
                                    Course Id
                                </th>


                                <th>
                                    Branch Id
                                </th>


                                <th>
                                    <?php echo __(''); ?>
                                </th>
                            </tr>

                            <?php foreach ($user->calendars as $calendars): ?>
                                <tr>

                                    <td>
                                        <?= h($calendars->id) ?>
                                    </td>

                                    <td>
                                        <?= h($calendars->title) ?>
                                    </td>

                                    <td>
                                        <?= h($calendars->text) ?>
                                    </td>

                                    <td>
                                        <?= h($calendars->user_id) ?>
                                    </td>

                                    <td>
                                        <?= h($calendars->class_id) ?>
                                    </td>

                                    <td>
                                        <?= h($calendars->course_id) ?>
                                    </td>

                                    <td>
                                        <?= h($calendars->branch_id) ?>
                                    </td>

                                    <td class="actions">
                                        <?= $this->Html->link(__('View'), ['controller' => 'Calendars', 'action' => 'view', $calendars->id], ['class' => 'btn btn-info btn-xs']) ?>

                                        <?= $this->Html->link(__('Edit'), ['controller' => 'Calendars', 'action' => 'edit', $calendars->id], ['class' => 'btn btn-warning btn-xs']) ?>

                                        <?= $this->Form->postLink(__('Delete'), ['controller' => 'Calendars', 'action' => 'delete', $calendars->id], ['confirm' => __('Are you sure you want to delete # {0}?', $calendars->id), 'class' => 'btn btn-danger btn-xs']) ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>

                            </tbody>
                        </table>

                    <?php endif; ?>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <i class="fa fa-share-alt"></i>
                    <h3 class="box-title"><?= __('Related {0}', ['Comments']) ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">

                    <?php if (!empty($user->comments)): ?>

                        <table class="table table-hover">
                            <tbody>
                            <tr>

                                <th>
                                    Id
                                </th>


                                <th>
                                    Text
                                </th>


                                <th>
                                    Title
                                </th>


                                <th>
                                    User Id
                                </th>


                                <th>
                                    People Id
                                </th>


                                <th>
                                    Class Id
                                </th>


                                <th>
                                    Branch Id
                                </th>


                                <th>
                                    <?php echo __(''); ?>
                                </th>
                            </tr>

                            <?php foreach ($user->comments as $comments): ?>
                                <tr>

                                    <td>
                                        <?= h($comments->id) ?>
                                    </td>

                                    <td>
                                        <?= h($comments->text) ?>
                                    </td>

                                    <td>
                                        <?= h($comments->title) ?>
                                    </td>

                                    <td>
                                        <?= h($comments->user_id) ?>
                                    </td>

                                    <td>
                                        <?= h($comments->people_id) ?>
                                    </td>

                                    <td>
                                        <?= h($comments->class_id) ?>
                                    </td>

                                    <td>
                                        <?= h($comments->branch_id) ?>
                                    </td>

                                    <td class="actions">
                                        <?= $this->Html->link(__('View'), ['controller' => 'Comments', 'action' => 'view', $comments->id], ['class' => 'btn btn-info btn-xs']) ?>

                                        <?= $this->Html->link(__('Edit'), ['controller' => 'Comments', 'action' => 'edit', $comments->id], ['class' => 'btn btn-warning btn-xs']) ?>

                                        <?= $this->Form->postLink(__('Delete'), ['controller' => 'Comments', 'action' => 'delete', $comments->id], ['confirm' => __('Are you sure you want to delete # {0}?', $comments->id), 'class' => 'btn btn-danger btn-xs']) ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>

                            </tbody>
                        </table>

                    <?php endif; ?>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
