<section class="content-header">
    <h1>
        <?php echo __('Class Discipline'); ?>
    </h1>
    <ol class="breadcrumb">
        <li>
            <?= $this->Html->link('<i class="fa fa-dashboard"></i> ' . __('Back'), ['action' => 'index'], ['escape' => false]) ?>
        </li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <i class="fa fa-info"></i>
                    <h3 class="box-title"><?php echo __('Information'); ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt><?= __('Clas') ?></dt>
                        <dd>
                            <?= $classDiscipline->has('clas') ? $classDiscipline->clas->name : '' ?>
                        </dd>
                        <dt><?= __('People') ?></dt>
                        <dd>
                            <?= $classDiscipline->has('people') ? $classDiscipline->people->name : '' ?>
                        </dd>
                        <dt><?= __('Branch') ?></dt>
                        <dd>
                            <?= $classDiscipline->has('branch') ? $classDiscipline->branch->name : '' ?>
                        </dd>


                        <dt><?= __('Discipline Id') ?></dt>
                        <dd>
                            <?= $this->Number->format($classDiscipline->discipline_id) ?>
                        </dd>


                    </dl>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- ./col -->
    </div>
    <!-- div -->

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <i class="fa fa-share-alt"></i>
                    <h3 class="box-title"><?= __('Related {0}', ['Frequencies']) ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">

                    <?php if (!empty($classDiscipline->frequencies)): ?>

                        <table class="table table-hover">
                            <tbody>
                            <tr>

                                <th>
                                    Id
                                </th>


                                <th>
                                    Status
                                </th>


                                <th>
                                    People Id
                                </th>


                                <th>
                                    Class Discipline Id
                                </th>


                                <th>
                                    <?php echo __(''); ?>
                                </th>
                            </tr>

                            <?php foreach ($classDiscipline->frequencies as $frequencies): ?>
                                <tr>

                                    <td>
                                        <?= h($frequencies->id) ?>
                                    </td>

                                    <td>
                                        <?= h($frequencies->status) ?>
                                    </td>

                                    <td>
                                        <?= h($frequencies->people_id) ?>
                                    </td>

                                    <td>
                                        <?= h($frequencies->class_discipline_id) ?>
                                    </td>

                                    <td class="actions">
                                        <?= $this->Html->link(__('View'), ['controller' => 'Frequencies', 'action' => 'view', $frequencies->id], ['class' => 'btn btn-info btn-xs']) ?>

                                        <?= $this->Html->link(__('Edit'), ['controller' => 'Frequencies', 'action' => 'edit', $frequencies->id], ['class' => 'btn btn-warning btn-xs']) ?>

                                        <?= $this->Form->postLink(__('Delete'), ['controller' => 'Frequencies', 'action' => 'delete', $frequencies->id], ['confirm' => __('Are you sure you want to delete # {0}?', $frequencies->id), 'class' => 'btn btn-danger btn-xs']) ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>

                            </tbody>
                        </table>

                    <?php endif; ?>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
