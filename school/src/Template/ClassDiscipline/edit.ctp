<section class="content-header">
    <h1>
        Editar Disciplina
    </h1>
    <ol class="breadcrumb">
        <li>
            <?= $this->Html->link('<i class="fa fa-dashboard"></i> ' . __('Back'), ['action' => 'index'], ['escape' => false]) ?>
        </li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">

                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <!-- /.box-header -->
                <!-- form start -->
                <?= $this->Form->create($classDiscipline, array('role' => 'form', 'align' => [
                    'sm' => [
                        'left' => 6,
                        'middle' => 6,
                        'right' => 12
                    ],
                    'md' => [
                        'left' => 3,
                        'middle' => 9,
                    ]
                ])) ?>
                <div class="box-body">

                    <?php
                    echo $this->Form->input('people_id', ['options' => $this->Util->listOptions('Peoples', ['name' => 'asc'], ['type' => 'PROFESSOR']), 'label' => 'Professor', 'required' => true]);
                    echo $this->Form->input('workload', ['label' => 'Carga Horária (Hrs)', 'required' => true]);
                    echo $this->Form->input('minimum_grade', ['label' => 'Nota Mínima', 'required' => true]);
                    ?>


                    <div class="row">
                        <div class="col-md-3">

                        </div>

                        <div class="col-md-9">
                            <table class="table table-bordered" id="table-more-time">
                                <tr>
                                    <th>Dia da Semana</th>
                                    <th>Hora Inicial</th>
                                    <th>Hora Final</th>
                                    <th></th>
                                </tr>


                                <?php foreach ($classDiscipline->times as $time) : ?>
                                    <tr>
                                        <td>
                                            <?= $this->Form->input('times.' . $time->id . '.id', ['type' => 'hidden', 'label' => false, 'value' => $time->id]); ?>
                                            <?= $this->Form->input('times.' . $time->id . '.day_week', ['value' => $time->day_week, 'options' => ['SEG' => 'Segunda', 'TER' => 'Terça', 'QUA' => 'Quarta', 'QUI' => 'Quinta', 'SEX' => 'Sexta'], 'label' => false]); ?>
                                        </td>
                                        <td> <?= $this->Form->input('times.' . $time->id . '.init_time', ['value' => $time->init_time, 'label' => false]); ?></td>
                                        <td> <?= $this->Form->input('times.' . $time->id . '.end_time', ['value' => $time->end_time, 'label' => false]); ?></td>
                                        <td>
                                            <?= $this->Form->postLink(__('Remover'), ['controller' => 'times', 'action' => 'delete', $time->id], ['confirm' => __('Confirma a exclusão do horário?'), 'class' => 'btn btn-warning btn-xs']) ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>

                            </table>

                            <a href="javascript:void(0)" class="btn btn-success btn-xs" id="more-time">Mais
                                Horário de Aula</a>
                        </div>
                    </div>

                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <?= $this->Form->button(__('Salvar')) ?>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</section>


<?php $this->start('scriptBottom'); ?>
<script>

    var btnMoreTime = document.getElementById('more-time');
    var countCells = 1;

    btnMoreTime.addEventListener('click', moreTimes);

    function moreTimes() {
        var table = document.getElementById('table-more-time');
        var tr = document.createElement('tr');

        tr.id = 'cell-' + countCells;

        tr.innerHTML = '' +
            '<td> <div class="form-group select"><div class="col-sm-6 col-md-9"><select name="times[' + countCells + '][day_week]" id="times-0-day-week" class="form-control"><option value="SEG">Segunda</option><option value="TER">Terça</option><option value="QUA">Quarta</option><option value="QUI">Quinta</option><option value="SEX">Sexta</option></select></div></div> </td>' +
            '<td> <div class="form-group text"><div class="col-sm-6 col-md-9"><input type="text" name="times[' + countCells + '][init_time]" id="times-0-init-time" class="form-control"></div></div></td>' +
            '<td> <div class="form-group text"><div class="col-sm-6 col-md-9"><input type="text" name="times[' + countCells + '][end_time]" id="times-0-end-time" class="form-control"></div></div></td>' +
            '<td><a href="javascript:void(0)" class="btn btn-warning btn-xs" onclick="removeElement(\'cell-' + countCells + '\')">Remover</a></td>';
        table.appendChild(tr);
        countCells++;
    }

    function removeElement(elementId) {
        // Removes an element from the document
        var element = document.getElementById(elementId);
        element.parentNode.removeChild(element);
    }


</script>
<?php $this->end(); ?>

