<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Disciplinas por Classe
    <div class="pull-right"><?= $this->Html->link(__('Novo'), ['action' => 'add'], ['class'=>'btn btn-success btn-xs']) ?></div>
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">

        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
          <table class="table table-hover">
            <thead>
              <tr>
                <th><?= $this->Paginator->sort('class_id', 'Classe') ?></th>
                <th><?= $this->Paginator->sort('discipline_id', 'Disciplica') ?></th>
                <th><?= __('Ações') ?></th>
              </tr>
            </thead>
            <tbody>
            <?php foreach ($classDiscipline as $classDiscipline): ?>
              <tr>
                <td><?= $classDiscipline->has('clas') ? $this->Html->link($classDiscipline->clas->name, ['controller' => 'Class', 'action' => 'view', $classDiscipline->clas->id]) : '' ?></td>
                <td><?= $this->Number->format($classDiscipline->discipline_id) ?></td>
                <td><?= $classDiscipline->has('people') ? $this->Html->link($classDiscipline->people->name, ['controller' => 'Peoples', 'action' => 'view', $classDiscipline->people->id]) : '' ?></td>
                <td><?= $classDiscipline->has('branch') ? $this->Html->link($classDiscipline->branch->name, ['controller' => 'Branchs', 'action' => 'view', $classDiscipline->branch->id]) : '' ?></td>
                <td class="actions" style="white-space:nowrap">
                  <?= $this->Html->link(__('View'), ['action' => 'view', $classDiscipline->id], ['class'=>'btn btn-info btn-xs']) ?>
                  <?= $this->Html->link(__('Edit'), ['action' => 'edit', $classDiscipline->id], ['class'=>'btn btn-warning btn-xs']) ?>
                  <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $classDiscipline->id], ['confirm' => __('Confirma a remoção?'), 'class'=>'btn btn-danger btn-xs']) ?>
                </td>
              </tr>
            <?php endforeach; ?>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
          <ul class="pagination pagination-sm no-margin pull-right">
            <?php echo $this->Paginator->numbers(); ?>
          </ul>
        </div>
      </div>
      <!-- /.box -->
    </div>
  </div>
</section>
<!-- /.content -->
