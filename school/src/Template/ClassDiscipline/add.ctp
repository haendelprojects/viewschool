<section class="content-header">
    <h1>
        Class Discipline
        <small><?= __('Add') ?></small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <?= $this->Html->link('<i class="fa fa-dashboard"></i> ' . __('Back'), ['action' => 'index'], ['escape' => false]) ?>
        </li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><?= __('Form') ?></h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <?= $this->Form->create($classDiscipline, array('role' => 'form')) ?>
                <div class="box-body">
                    <?php
                    echo $this->Form->input('class_id', ['value' => $class_id]);
                    echo $this->Form->input('discipline_id', ['options'=> $this->Util->listDisciplines()]);
                    echo $this->Form->input('people_id', ['options' => $peoples]);
                    echo $this->Form->input('branch_id', ['options' => $branchs]);
                    ?>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <?= $this->Form->button(__('Save')) ?>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</section>

