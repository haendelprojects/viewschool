<section class="content-header">
    <h1>
        Matrícula
        <small><?= __('Editar') ?></small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <?= $this->Html->link('<i class="fa fa-arrow-left"></i> ' . __('Voltar'), ['action' => 'index'], ['escape' => false]) ?>
        </li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><?= __('Detalhes') ?></h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <?= $this->Form->create($registration, array('role' => 'form', 'align' => [
                    'sm' => [
                        'left' => 6,
                        'middle' => 6,
                        'right' => 12
                    ],
                    'md' => [
                        'left' => 3,
                        'middle' => 9,
                    ]
                ])) ?>
                <div class="box-body">
                    <div class="box-body">
                        <?php
                        echo $this->Form->input('people_id', ['options' => $this->Util->listOptions('Peoples', ['name' => 'asc'], ['type' => 'ALUNO']), 'label' => 'Aluno', 'disabled' => true]);
                        echo $this->Form->input('people_id', ['options' => $this->Util->listOptions('Class', ['name' => 'asc'], []), 'label' => 'Classe', 'disabled' => true]);
                        echo $this->Form->input('value_material', ['label' => 'Taxa de Material', 'required' => true]);
                        echo $this->Form->input('value_monthly', ['label' => 'Valor da Mensalidade', 'required' => true]);
                        echo $this->Form->input('value_registration', ['label' => 'Valor da Matrícula', 'required' => true]);

                        echo $this->Form->input('discount_value', ['label' => 'Desconto até o vencimento', 'required' => true]);
                        echo $this->Form->input('discount_type', ['label' => 'Tipo de desconto', 'options' => ['R$' => 'R$', '%' => '%']]);
                        echo $this->Form->input('observation', ['label' => 'Observação']);
                        ?>

                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <?= $this->Form->button(__('Salvar'), ['class' => 'btn btn-success']) ?>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</section>

