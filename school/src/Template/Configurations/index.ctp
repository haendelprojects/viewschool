<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Configurações
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Usuários e Acesso Administrativo</h3>
                </div>
                <div class="box-body">
                    <p>Configurações relacionados ao sistema da escola.</p>
                    <?= $this->Html->link('<i class="fa fa-users"></i> Usuários', ['controller' => 'system', 'action' => 'index', 'plugin' => false], ['escape' => false, 'class' => 'btn btn-app']); ?>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Cursos, Turmas e Disciplicas</h3>
                </div>
                <div class="box-body">
                    <p>Configurações relacionadas a detalhes de cursos, turmas e tipos de disciplicas.</p>
                    <?= $this->Html->link('<i class="fa fa-book"></i> Disciplinas', ['controller' => 'disciplines', 'action' => 'index', 'plugin' => false], ['escape' => false, 'class' => 'btn btn-app']); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->
