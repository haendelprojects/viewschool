<?php $prefix = (isset($prefix)) ? $prefix . '.' : '' ?>


<button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#list-configuration-occurrence">
    <i class="fa fa-list"></i> Filtros Rápidos
</button>

<!-- Modal -->
<div id="list-configuration-occurrence" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Configurações</h4>
            </div>
            <div class="modal-body">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#private" data-toggle="tab">Listas Privadas</a></li>
                        <li><a href="#public" data-toggle="tab">Listas Públicas</a></li>
                        <li><a href="#new" data-toggle="tab">Nova Lista</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="active tab-pane" id="private">
                            <?= $this->ListConfiguration->getPrivate('list_occurrence', ['controller' => 'occurrences', 'action' => 'index']) ?>
                        </div>

                        <div class="tab-pane" id="public">
                            <?= $this->ListConfiguration->getPublic('list_occurrence', ['controller' => 'occurrences', 'action' => 'index']) ?>
                        </div>

                        <div class="tab-pane" id="new">
                            <?= $this->Form->create(null, array('url' => ['controller' => 'configurations', 'action' => 'saveList'], 'role' => 'form', 'align' => [
                                'sm' => [
                                    'left' => 6,
                                    'middle' => 6,
                                    'right' => 12
                                ],
                                'md' => [
                                    'left' => 3,
                                    'middle' => 9,
                                ]
                            ])) ?>

                            <?php

                            echo $this->Form->input('config_key', ['class' => 'form-control', 'type' => 'hidden', 'value' => 'list_occurrence']);
                            echo $this->Form->input('config_value.title', ['class' => 'form-control', 'label' => 'Titulo da Lista']);
                            echo $this->Form->input('public', ['class' => 'form-control', 'label' => 'Público', 'options' => ['Não', 'Sim']]);
                            echo $this->Form->input('config_value.filter.corporate_id', ['options' => $this->ListConfiguration->getCorporates(), 'empty' => true, 'class' => 'form-control', 'label' => 'Empresa', 'id' => 'corporate']);
                            echo $this->Form->input('config_value.filter.contract_id', ['options' => [], 'empty' => true, 'class' => 'form-control', 'label' => 'Contrato', 'id' => 'contracts']);
                            echo $this->Form->input('config_value.filter.country_id', ['options' => $this->Address->getCountries(31), 'empty' => 'Todos', 'class' => 'form-control', 'label' => 'Pais', 'id' => 'country']);
                            echo $this->Form->input('config_value.filter.state_id', ['options' => $this->Address->getStates(31), 'empty' => 'Todos', 'class' => 'form-control', 'label' => 'Estado', 'id' => 'state']);
                            echo $this->Form->input('config_value.filter.city_id', ['options' => $this->Address->getCities(1), 'empty' => 'Todos', 'class' => 'form-control', 'label' => 'Cidade', 'id' => 'city']);

                            echo $this->Form->input('config_value.filter.status', ['options' => $this->Status->getList(), 'empty' => 'Todos', 'class' => 'form-control', 'label' => 'Status']);
                            echo $this->Form->input('config_value.filter.responsible_user_id', ['options' => $this->ListConfiguration->getResponsibles(), 'empty' => 'Todos', 'class' => 'form-control', 'label' => 'Responsavel']);
                            ?>

                            <?= $this->Form->button(__('Salvar'), ['class' => 'btn btn-success pull-right']) ?>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Cancelar
                </button>
            </div>
        </div>
    </div>
    <?= $this->element('Scripts/contractSelect') ?>
    <?= $this->element('Scripts/address') ?>
</div>

