<div class=" col-md-12" id="values">
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-aqua">
                <span class="info-box-icon"><i class="fa fa-calendar-plus-o"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">UPs Disponiveis/Gastos</span>
                    <span class="info-box-number"><?= $all_month['spare_points'] ?>
                        /<?= $all_month['total_expense'] ?></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-aqua">
                <span class="info-box-icon"><i class="fa fa-dollar"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Custo Mensal</span>
                    <span class="info-box-number">$ <?= number_format($all_month['spare_points'] * 68, 2) ?></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-aqua">
                <span class="info-box-icon"><i class="fa fa-dollar"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Receita Mensal</span>
                    <span class="info-box-number">$ <?= number_format($all_month['total_points_quantity'] * $all_month['unique_point_value'], 2) ?></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box <?= (($all_month['total_points_quantity'] * 0.3) < $all_month['spare_points']) ? 'bg-green' : 'bg-red' ?>">
                                    <span class="info-box-icon"><i
                                                class="fa <?= (($all_month['total_points_quantity'] * 0.3) < $all_month['spare_points']) ? ' fa-thumbs-o-up' : ' fa-thumbs-o-down' ?>"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Disponíveis</span>
                    <span class="info-box-number"><?= $all_month['spare_points'] ?></span>
                </div>

                <div class="progress">
                    <div class="progress-bar"
                         style="width: <?= ($all_month['spare_points'] * 100) / ($all_month['total_points_quantity']) ?>%"></div>
                </div>
                <span class="progress-description">
                                        <?= ($all_month['spare_points'] * 100) / ($all_month['total_points_quantity']) ?>
                    % disponivel mês</span>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>


        <!-- /.col -->
    </div>

</div>