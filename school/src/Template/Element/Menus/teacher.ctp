<li>
    <?= $this->Html->link('<i class="fa fa-commenting"></i> Calendário Escolar', ['controller' => 'dashboard', 'action' => 'index', 'plugin' => false], ['escape' => false]); ?>
</li>

<li class="treeview">
    <a href="#">
        <i class="fa fa-book"></i>
        <span>Pedagógico</span>
        <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
    </a>
    <ul class="treeview-menu">
        <li>
            <?= $this->Html->link('Minhas Turmas', ['controller' => 'class', 'action' => 'index', 'plugin' => false], ['escape' => false]); ?>
        </li>

        <li>
            <?= $this->Html->link('Aulas e Frequência', ['controller' => 'lessons', 'action' => 'index', 'plugin' => false], ['escape' => false]); ?>
        </li>

        <li>
            <?= $this->Html->link('Avaliações', ['controller' => 'exams', 'action' => 'index', 'plugin' => false], ['escape' => false]); ?>
        </li>

    </ul>
</li>
