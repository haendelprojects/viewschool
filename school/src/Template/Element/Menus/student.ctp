<li>
    <?= $this->Html->link('<i class="fa fa-calendar"></i> Calendário Escolar', ['controller' => 'dashboard', 'action' => 'index', 'plugin' => false], ['escape' => false]); ?>
</li>

<li>
    <?= $this->Html->link('<i class="fa fa-user"></i> Matrícula do Aluno', ['controller' => 'registrations', 'action' => 'my', 'plugin' => false], ['escape' => false]); ?>
</li>

<li>
    <?= $this->Html->link('<i class="fa fa-commenting"></i> Mensagem para Escola', ['controller' => 'contacts', 'action' => 'index', 'plugin' => false], ['escape' => false]); ?>
</li>

<li>
    <?= $this->Html->link('<i class="fa fa-commenting"></i> Quadro de Avisos', ['controller' => 'comments', 'action' => 'index', 'plugin' => false], ['escape' => false]); ?>
</li>
