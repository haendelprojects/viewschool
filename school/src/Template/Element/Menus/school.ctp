

<li class="treeview">
    <a href="#">
        <i class="fa fa-user"></i>
        <span>Pessoas</span>
        <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
    </a>
    <ul class="treeview-menu">
        <li>
            <?= $this->Permission->link('Estudantes', ['controller' => 'students', 'action' => 'index', 'plugin' => false], ['escape' => false]); ?>
        </li>

        <li>
            <?= $this->Html->link(' Professores', ['controller' => 'teachers', 'action' => 'index', 'plugin' => false], ['escape' => false]); ?>
        </li>
        <li>
            <?= $this->Permission->link('Funcionários', ['controller' => 'clients', 'action' => 'index', 'plugin' => false], ['escape' => false]); ?>
        </li>

    </ul>
</li>


<li class="treeview">
    <a href="#">
        <i class="fa fa-book"></i>
        <span>Pedagógico</span>
        <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
    </a>
    <ul class="treeview-menu">
        <li>
            <?= $this->Html->link('Cursos/Séries', ['controller' => 'courses', 'action' => 'index', 'plugin' => false], ['escape' => false]); ?>
        </li>

        <li>
            <?= $this->Html->link('Turmas', ['controller' => 'class', 'action' => 'index', 'plugin' => false], ['escape' => false]); ?>
        </li>

        <li>
            <?= $this->Html->link('Aulas e Frequência', ['controller' => 'lessons', 'action' => 'index', 'plugin' => false], ['escape' => false]); ?>
        </li>

        <li>
            <?= $this->Html->link('Avaliações', ['controller' => 'exams', 'action' => 'index', 'plugin' => false], ['escape' => false]); ?>
        </li>

    </ul>
</li>

<li>
    <?= $this->Html->link('<i class="fa fa-calendar"></i> Calendário', ['controller' => 'calendars', 'action' => 'calendar', 'plugin' => false], ['escape' => false]); ?>
</li>

<li>
    <?= $this->Html->link('<i class="fa fa-newspaper-o"></i> Quadro de Avisos', ['controller' => 'comments', 'action' => 'index', 'plugin' => false], ['escape' => false]); ?>
</li>

<li>
    <?= $this->Html->link('<i class="fa fa-commenting"></i> Mensagens Pais', ['controller' => 'contacts', 'action' => 'index', 'plugin' => false], ['escape' => false]); ?>
</li>

<li>
    <?= $this->Html->link('<i class="fa fa-cog"></i> Configurações', ['controller' => 'configurations', 'action' => 'index', 'plugin' => false], ['escape' => false]); ?>
</li>