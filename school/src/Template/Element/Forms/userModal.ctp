<button type="button" class="btn btn-success btn-xs" data-toggle="modal"
        data-target="#modal-info-base">Criar usuário
</button>

<!-- Modal -->
<div id="modal-info-base" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <?= $this->Form->create($user, array('url' => $url, 'role' => 'form', 'align' => [
                'sm' => [
                    'left' => 6,
                    'middle' => 6,
                    'right' => 12
                ],
                'md' => [
                    'left' => 3,
                    'middle' => 9,
                ]
            ])) ?>
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Novo Usuário</h4>
            </div>
            <div class="modal-body">

                <div class="box-body">
                    <?php
                    echo $this->Form->input('username', ['class' => 'form-control', 'label' => 'Email']);
                    echo $this->Form->input('password', ['class' => 'form-control', 'label' => 'Senha']);
                    ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Cancelar
                </button>
                <?= $this->Form->button(__('Salvar'), ['class' => 'btn btn-success']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>

    </div>
</div>