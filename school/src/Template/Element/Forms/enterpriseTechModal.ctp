<?php $prefix = (isset($prefix)) ? $prefix . '.' : '' ?>


<button type="button" class="btn btn-success btn-xs" data-toggle="modal"
        data-target="#modal-enterprise-tech">Editar Endereço
</button>

<!-- Modal -->
<div id="modal-enterprise-tech" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <?= $this->Form->create($user, array('url' => $url, 'role' => 'form', 'align' => [
                'sm' => [
                    'left' => 6,
                    'middle' => 6,
                    'right' => 12
                ],
                'md' => [
                    'left' => 3,
                    'middle' => 9,
                ]
            ])) ?>
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Editar Empresa</h4>
            </div>
            <div class="modal-body">

                <div class="box-body">
                    <?php
                    echo $this->Form->input($prefix . 'name', ['label' => 'Nome', 'required' => true]);
                    echo $this->Form->input($prefix . 'cnpj', ['class' => 'form-control', 'label' => 'CNPJ', 'required' => true]);
                    echo $this->Form->input($prefix . 'type', ['class' => 'form-control', 'value' => 'CLIENT', 'type' => 'hidden']);
                    ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Cancelar
                </button>
                <?= $this->Form->button(__('Salvar'), ['class' => 'btn btn-success']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>

    </div>
</div>