<?php $prefix = (isset($prefix)) ? $prefix . '.' : '' ?>

<button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#modal-reschedule">
    <i class="fa fa-clock-o"></i> Reagendar
</button>

<!-- Modal -->
<div id="modal-reschedule" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <?= $this->Form->create($occurrence, array('url' => $url, 'role' => 'form')) ?>
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Reagendar</h4>
            </div>
            <div class="modal-body">

                <div class="box-body">
                    <?php
                    echo $this->Form->input('schedule_time', ['label' => 'Novo Dia/Horário']);
                    echo $this->Form->input('historic.reason', ['label' => 'Razão do agendamento', 'required' => true]);
                    ?>

                </div>
                <!-- /.box-body -->
                <div class="box-footer">

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Cancelar
                </button>
                <?= $this->Form->button(__('Salvar'), ['class' => 'btn btn-success']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>

    </div>
    <?= $this->element('Scripts/address') ?>
</div>

