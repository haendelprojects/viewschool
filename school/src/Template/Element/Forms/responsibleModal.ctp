<?php $prefix = (isset($prefix)) ? $prefix . '.' : '' ?>
<?php $text = (isset($text)) ? $text : '<i class="fa fa-plus"></i>  Adicionar Responsável' ?>
<?php $id = (isset($id)) ? '-' . $id : '' ?>


<button type="button" class="btn btn-success btn-xs" data-toggle="modal"
        data-target="#modal-responsible<?= $id ?>"><?= $text ?>
</button>

<!-- Modal -->
<div id="modal-responsible<?= $id ?>" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <?= $this->Form->create($data, array('url' => $url, 'role' => 'form', 'align' => [
                'sm' => [
                    'left' => 6,
                    'middle' => 6,
                    'right' => 12
                ],
                'md' => [
                    'left' => 3,
                    'middle' => 9,
                ]
            ])) ?>
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Informações</h4>
            </div>
            <div class="modal-body">

                <div class="box-body">
                    <?php
                    echo $this->Form->input('name', ['label' => 'Nome']);
                    echo $this->Form->input('cpf', ['label' => 'CPF', 'class' => 'cpf']);
                    echo $this->Form->input('phone', ['label' => 'Telefone', 'class' => 'phone', 'maxlength' => 15]);
                    echo $this->Form->input('cell_phone', ['label' => 'Celular', 'class' => 'cellphone', 'maxlength' => 15]);
                    echo $this->Form->input('type', ['value' => 'RESPONSAVEL', 'label' => 'Tipo', 'type' => 'hidden']);
                    echo $this->Form->input('sexo', ['label' => 'Sexo', 'options' => ['FEMININO' => 'Feminino', 'MASCULINO' => 'Masculino']]);
                    echo $this->Form->input('email', ['label' => 'E-mail']);
                    echo $this->Form->input('responsible_educational', ['label' => 'Responsável Pedagógico', 'options' => ['Não', 'Sim']]);
                    echo $this->Form->input('responsible_financial', ['label' => 'Responsável Financeiro', 'options' => ['Não', 'Sim']]);
                    echo $this->Form->input('people_id', ['type' => 'hidden', 'value' => $people_id]);
                    ?>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Cancelar
                </button>
                <?= $this->Form->button(__('Salvar'), ['class' => 'btn btn-success']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>

    </div>


    <?php
    $this->Html->css(['AdminLTE./plugins/datepicker/datepicker3',],
        ['block' => 'css']);

    $this->Html->script(['AdminLTE./plugins/input-mask/jquery.inputmask',
        'AdminLTE./plugins/input-mask/jquery.inputmask.date.extensions',
        'AdminLTE./plugins/datepicker/bootstrap-datepicker',
        'AdminLTE./plugins/datepicker/locales/bootstrap-datepicker.pt-BR',],
        ['block' => 'script']);
    ?>
    <?php $this->start('scriptBottom'); ?>
    <script>
        $(function () {
            //Datemask mm/dd/yyyy
            $(".datepicker")
                .inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"})
                .datepicker({
                    language: 'pt_br',
                    format: 'dd/mm/yyyy'
                });

            $(".cpf").inputmask("999.999.999-99", {"placeholder": "000.000.000-00"});

            $(".cellphone").inputmask("(99) 99999-9999", {"placeholder": "(00) 00000-0000"});
            $(".phone").inputmask("(99) 9999-9999", {"placeholder": "(00) 0000-0000"});
        });
    </script>
    <?php $this->end(); ?>


</div>

