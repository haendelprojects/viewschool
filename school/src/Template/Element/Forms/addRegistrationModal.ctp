<?php $prefix = (isset($prefix)) ? $prefix . '.' : '' ?>

<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-registration">
    <i class="fa fa-plus"></i> Matrícula de Aluno
</button>

<!-- Modal -->
<div id="modal-registration" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <?= $this->Form->create($this->Util->entityForm('Registrations'), array('url' => ['controller' => 'Registrations', 'action' => 'addByModal'], 'role' => 'form', 'align' => [
                'sm' => [
                    'left' => 6,
                    'middle' => 6,
                    'right' => 12
                ],
                'md' => [
                    'left' => 3,
                    'middle' => 9,
                ]
            ])) ?>
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal">&times;
                </button>
                <h4 class="modal-title">Nova Matricula</h4>
            </div>
            <div class="modal-body">
                <div class="box-body">
                    <?php
                    echo $this->Form->input('people_id', ['options' => $this->Util->listOptions('Peoples', ['name' => 'asc'], ['type' => 'ALUNO']), 'label' => 'Aluno', 'empty' => true]);
                    echo $this->Form->input('value_material', ['label' => 'Taxa de Material', 'required' => true]);
                    echo $this->Form->input('value_monthly', ['label' => 'Valor da Mensalidade', 'required' => true]);
                    echo $this->Form->input('value_registration', ['label' => 'Valor da Matrícula', 'required' => true]);

                    echo $this->Form->input('discount_value', ['label' => 'Desconto até o vencimento', 'required' => true]);
                    echo $this->Form->input('discount_type', ['label' => 'Tipo de desconto', 'options' => ['R$' => 'R$', '%' => '%']]);
                    echo $this->Form->input('observation', ['label' => 'Observação']);

                    echo $this->Form->input('class_id', ['value' => $class_id, 'type' => 'hidden']);

                    ?>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Cancelar
                </button>
                <?= $this->Form->button(__('Salvar'), ['class' => 'btn btn-success']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>

    </div>
</div>

