<script>
    var inputState = document.getElementById('state');
    var inputCountry = document.getElementById('country');
    var inputCity = document.getElementById('city');


    if(inputCountry) {

        inputCountry.addEventListener('change', function (e) {
            getJSON('<?= $this->Url->build('/rest/addresses/states/') ?>' + e.target.value + '.json', function (err, data) {
                removeStates();
                removeCities();
                addOptions(data.states, inputState);
            });
        });
    }


    if(inputState) {

        inputState.addEventListener('change', function (e) {
            getJSON('<?= $this->Url->build('/rest/addresses/cities/') ?>' + e.target.value + '.json', function (err, data) {
                removeCities();
                addOptions(data.cities, inputCity);
            });
        });
    }

    var removeStates = function () {
        var inputState = document.getElementById('state');
        for (var i = inputState.options.length - 1; i >= 0; i--) {
            inputState.remove(i);
        }
    }

    var addOptions = function (data, input) {
        var all = input.getAttribute('data-option-not-all');

        if (!all) {
            var opt = document.createElement('option');
            opt.value = '';
            opt.innerHTML = 'Todos';
            input.appendChild(opt);
        }

        for (var i = 0; i < data.length; i++) {
            var opt = document.createElement('option');
            opt.value = data[i].id;
            opt.innerHTML = data[i].name;
            input.appendChild(opt);
        }
    }

    var removeCities = function () {
        var inputCity = document.getElementById('city');
        for (var i = inputCity.options.length - 1; i >= 0; i--) {
            inputCity.remove(i);
        }
    }

    var getJSON = function (url, callback) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', url, true);
        xhr.responseType = 'json';
        xhr.onload = function () {
            var status = xhr.status;
            if (status === 200) {
                callback(null, xhr.response);
            } else {
                callback(status, xhr.response);
            }
        };
        xhr.send();
    };
</script>