<?php if (!empty($data)): ?>
    <table id="table-address" class="table table-hover datatable">
        <thead>
        <tr>
            <th>CNPJ</th>
            <th>Nome</th>
            <th>Cidade</th>
            <th>Estado</th>
            <th>Ativo</th>
            <th>
                <?php echo __('Ações'); ?>
            </th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data as $addresses): ?>
            <tr>

                <td>
                    <?= h($addresses->CNPJ) ?>
                </td>

                <td>
                    <?= h($addresses->title) ?>
                </td>
                <td>
                    <?= h($addresses->city) ?>
                </td>

                <td>
                    <?= h($addresses->state) ?>
                </td>

                <td>
                    <?= ($addresses->active == 1) ? 'Sim' : 'Não' ?>
                </td>

                <td class="actions">
                    <?= $this->Html->link(__('Detalhe'), ['controller' => 'Addresses', 'action' => 'view', $addresses->id], ['class' => 'btn btn-info btn-xs']) ?>
                    <?= $this->Html->link(__('Editar'), ['controller' => 'Addresses', 'action' => 'edit', $addresses->id], ['class' => 'btn btn-warning btn-xs']) ?>
                </td>
            </tr>
        <?php endforeach; ?>

        </tbody>
    </table>

<?php else: ?>
    <table id="table-user" class="table table-hover datatable">
        <thead>
        <tr>
            <th>CNPJ</th>
            <th>Nome</th>
            <th>Cidade</th>
            <th> Estado</th>
            <th>Active</th>
            <th>
                <?php echo __('Ações'); ?>
            </th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Não há dados</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        </tbody>
    </table>
<?php endif; ?>

