<section class="content-header">
    <h1>
        <?php echo __('Frequência'); ?>
    </h1>
    <ol class="breadcrumb">
        <li>

        </li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <i class="fa fa-info"></i>
                    <h3 class="box-title"><?php echo __('Informação'); ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt><?= __('Disciplina') ?></dt>
                        <dd>
                            <?= $lesson->has('discipline') ? $lesson->discipline->name : '' ?>
                        </dd>
                        <dt><?= __('Assunto') ?></dt>
                        <dd>
                            <?= h($lesson->title) ?>
                        </dd>

                        <dt><?= __('Dia') ?></dt>
                        <dd>
                            <?= h($lesson->day->format('d/m/Y')) ?>
                        </dd>


                        <dt><?= __('Assunto') ?></dt>
                        <dd>
                            <?= $this->Text->autoParagraph(h($lesson->description)); ?>
                        </dd>
                    </dl>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- ./col -->
    </div>
    <!-- div -->

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <i class="fa fa-share-alt"></i>
                    <h3 class="box-title"><?= __('Lista de Frequência') ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">

                    <?php if (!empty($lesson->frequencies)): ?>

                        <table class="table table-hover">
                            <tbody>
                            <tr>
                                <th>Aluno</th>
                                <th>

                                </th>
                                <th>
                                    <?php echo __(''); ?>
                                </th>
                            </tr>

                            <?php foreach ($lesson->frequencies as $frequencies): ?>
                                <tr>
                                    <td>
                                        <?= h($frequencies->people->name) ?>
                                    </td>
                                    <td>
                                        <?= h($frequencies->status) ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>

                            </tbody>
                        </table>

                    <?php endif; ?>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
