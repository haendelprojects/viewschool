<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Informações
    </h1>
</section>

<!-- END: Subheader -->
<section class="content">
    <div class="row">
        <div class="col-md-8">
            <div class="box box-default">
                <div class="box-body">
                    <div id="calendar"></div>
                </div>
            </div>
        </div>
        <div class="col-md-4">

            <?php if ($this->User->isSchool()): ?>
                <!--begin::Total Profit-->
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h4 class="box-title">
                            Total de Alunos
                        </h4>
                        <br>
                        <span class="m-widget24__desc">Alunos ViewSchool</span>
                        <span class="m-widget24__stats m--font-brand"><?= $students ?></span>
                    </div>
                </div>
                <!--end::Total Profit-->
            <?php endif; ?>
            <?php if ($this->User->isSchool()): ?>
                <!--begin::Total Profit-->
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h4 class="box-title">
                            Total de Professores
                        </h4>
                        <br>
                        <span class="m-widget24__desc">Professores ViewSchool</span>
                        <span class="m-widget24__stats m--font-brand"><?= $teachers ?></span>

                    </div>
                </div>
                <!--end::Total Profit-->
            <?php endif; ?>
            <?php if ($this->User->isSchool()): ?>
                <!--begin::Total Profit-->
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h4 class="box-title">
                            Total de Turmas
                        </h4>
                        <br>
                        <span class="m-widget24__desc">Turmas ViewSchool</span>
                        <span class="m-widget24__stats m--font-brand"><?= $class ?></span>
                        <br><bR><Br>
                    </div>
                </div>
                <!--end::Total Profit-->
            <?php endif; ?>
            <?php if ($this->User->isTeacher()): ?>
                <!--begin::Total Profit-->
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h4 class="box-title">
                            Total de Disciplinas
                        </h4>
                    </div>
                    <div class="box-body">
                        <span class="m-widget24__desc">Disciplinas trabalhadas</span>
                        <span class="m-widget24__stats m--font-brand"><?= $disciplines ?></span>
                    </div>
                </div>
                <!--end::Total Profit-->
            <?php endif; ?>


            <div class="box box-default">
                <div class="box-header">
                    <h2 class="box-title with-border">
                        Avisos
                    </h2>
                </div>
                <div class="box-body">
                    <ul class="timeline">
                        <?php foreach ($comments as $comment): ?>
                            <li>
                                <i class="fa fa-envelope bg-blue"></i>

                                <div class="timeline-item">
                                    <span class="time"><i class="fa fa-clock-o"></i> <?= $comment->created ?></span>

                                    <h3 class="timeline-header"><a
                                                href="#"><?= $comment->title ?></a></h3>

                                    <div class="timeline-body">
                                        <?= $comment->text ?>
                                    </div>
                                </div>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<?php
echo $this->Html->css('AdminLTE./plugins/fullcalendar/fullcalendar.min', ['block' => 'css']);
echo $this->Html->css('AdminLTE./plugins/fullcalendar/fullcalendar.print', ['block' => 'css', 'media' => 'print']);

echo $this->Html->script('https://code.jquery.com/ui/1.11.4/jquery-ui.min.js');
echo $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js');
echo $this->Html->script('AdminLTE./plugins/fullcalendar/fullcalendar.min');
?>

<?php $this->start('scriptBottom'); ?>
<script>
    $(function () {

        /* initialize the external events
         -----------------------------------------------------------------*/
        function ini_events(ele) {
            ele.each(function () {

                // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
                // it doesn't need to have a start or end
                var eventObject = {
                    title: $.trim($(this).text()) // use the element's text as the event title
                };

                // store the Event Object in the DOM element so we can get to it later
                $(this).data('eventObject', eventObject);

                // make the event draggable using jQuery UI
                $(this).draggable({
                    zIndex: 1070,
                    revert: true, // will cause the event to go back to its
                    revertDuration: 0  //  original position after the drag
                });

            });
        }

        ini_events($('#external-events div.external-event'));

        /* initialize the calendar
         -----------------------------------------------------------------*/
        //Date for the calendar events (dummy data)
        var date = new Date();
        var d = date.getDate(),
            m = date.getMonth(),
            y = date.getFullYear();
        $('#calendar').fullCalendar({
            ignoreTimezone: false,
            monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
            dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sabado'],
            dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],

            columnFormat: {
                month: 'ddd',
                week: 'ddd d',
                day: ''
            },
            axisFormat: 'H:mm',
            timeFormat: {
                '': 'H:mm',
                agenda: 'H:mm{ - H:mm}'
            },
            buttonText: {
                prev: "Anterior",
                next: "Próximo",
                today: "Hoje",
                month: "Mês",
                week: "Semana",
                day: "Dia"
            }
            ,
            //Random default events
            events: [
                <?php foreach($calendars as $c) : ?>
                {
                    title: '<?= $c->title ?>',
                    start: '<?= $c->start ?>',
                    end: '<?= $c->end?>',
                    url: '<?= $c->url ?>',
                    backgroundColor: '<?= $c->background ?>', //Primary (light-blue)
                    borderColor: '<?= $c->background ?>' //Primary (light-blue)
                },
                <?php endforeach; ?>
            ],
            editable: true,
            droppable: true, // this allows things to be dropped onto the calendar !!!
            drop: function (date, allDay) { // this function is called when something is dropped

                // retrieve the dropped element's stored Event Object
                var originalEventObject = $(this).data('eventObject');

                // we need to copy it, so that multiple events don't have a reference to the same object
                var copiedEventObject = $.extend({}, originalEventObject);

                // assign it the date that was reported
                copiedEventObject.start = date;
                copiedEventObject.allDay = allDay;
                copiedEventObject.backgroundColor = $(this).css("background-color");
                copiedEventObject.borderColor = $(this).css("border-color");

                // render the event on the calendar
                // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

                // is the "remove after drop" checkbox checked?
                if ($('#drop-remove').is(':checked')) {
                    // if so, remove the element from the "Draggable Events" list
                    $(this).remove();
                }

            }
        });

        /* ADDING EVENTS */
        var currColor = "#3c8dbc"; //Red by default
        //Color chooser button
        var colorChooser = $("#color-chooser-btn");
        $("#color-chooser > li > a").click(function (e) {
            e.preventDefault();
            //Save color
            currColor = $(this).css("color");
            //Add color effect to button
            $('#add-new-event').css({"background-color": currColor, "border-color": currColor});
        });
        $("#add-new-event").click(function (e) {
            e.preventDefault();
            //Get value and make sure it is not null
            var val = $("#new-event").val();
            if (val.length == 0) {
                return;
            }

            //Create events
            var event = $("<div />");
            event.css({
                "background-color": currColor,
                "border-color": currColor,
                "color": "#fff"
            }).addClass("external-event");
            event.html(val);
            $('#external-events').prepend(event);

            //Add draggable funtionality
            ini_events(event);

            //Remove event from text input
            $("#new-event").val("");
        });
    });
</script>
<?php $this->end(); ?>
