<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Lista de Avaliações
        <div class="pull-right"><?= $this->Html->link(__('Nova Avaliação'), ['action' => 'add'], ['class' => 'btn btn-success btn-xs']) ?></div>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">

                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th><?= $this->Paginator->sort('class_id', 'Classe') ?></th>
                            <th><?= $this->Paginator->sort('discipline_id', 'Disciplina') ?></th>
                            <th><?= $this->Paginator->sort('title', 'Assunto') ?></th>
                            <th><?= $this->Paginator->sort('day', 'Dia') ?></th>
                            <th><?= __('Ações') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($lessons as $lesson): ?>
                            <tr>
                                <td><?= $lesson->clas->course->name ?> <?= $lesson->clas->name ?></td>
                                <td><?= $lesson->has('discipline') ? $this->Html->link($lesson->discipline->name, ['controller' => 'Disciplines', 'action' => 'view', $lesson->discipline->id]) : '' ?></td>
                                <td><?= h($lesson->title) ?></td>
                                <td><?= h($lesson->day->format('d/m/Y')) ?></td>
                                <td class="actions" style="white-space:nowrap">
                                    <?= $this->Html->link(__('Detalhes'), ['action' => 'view', $lesson->id], ['class' => 'btn btn-info btn-xs']) ?>
                                    <?= $this->Form->postLink(__('Remover'), ['action' => 'delete', $lesson->id], ['confirm' => __('Confirma a remoção?'), 'class' => 'btn btn-danger btn-xs']) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right">
                        <?php echo $this->Paginator->numbers(); ?>
                    </ul>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
<!-- /.content -->
