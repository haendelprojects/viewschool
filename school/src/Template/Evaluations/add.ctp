<section class="content-header">
    <h1>
        Critérios Avaliativos
        <small><?= __('Adicionar') ?></small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <?= $this->Html->link('<i class="fa fa-arrow-left"></i> ' . __('Voltar'), ['action' => 'index'], ['escape' => false]) ?>
        </li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <?= $this->Form->create($evaluation, array('role' => 'form', 'align' => $this->Util->alignForm())) ?>
                <div class="box-body">
                    <?php
                    echo $this->Form->input('name', ['label' => 'Nome', 'required' => true]);
                    echo $this->Form->input('type_attendance', ['label' => 'Tipo de Frequência', 'required' => true, 'options' => ['HORA-AULA' => 'Hora Aula', 'DIAS-LETIVO' => 'Dias Letivo']]);
                    echo $this->Form->input('evaluation_attendance', ['label' => 'O Aluno é avaliado pelo percentual de frequência?', 'required' => true, 'onchange' => 'showDisplay(this, "percent-attendance")', 'id' => 'evaluation-attendance']);
                    ?>

                    <div id="percent-attendance" style="display: none;">
                        <?= $this->Form->input('percent_attendance', ['label' => 'Percentual de frequência para aprovação', 'id' => 'e', 'value' => 70]); ?>
                    </div>


                    <?php

                    echo $this->Form->input('evaluation_period', ['onchange' => 'setQuantityPeriod(this)', 'id' => 'evaluation_period', 'empty' => 'Selecione', 'label' => 'Periodo de Avaliação', 'required' => true, 'options' => [
                        'BIMESTRE' => 'Bimestre', 'TRIMESTRE' => 'Trimestre', 'SEMESTRE' => 'Semestre', 'MODULAR' => 'Modular', 'Unidade' => 'Unidade'
                    ]]);

                    echo $this->Form->input('quantity_period', ['id' => 'quantity_period', 'empty' => 'Selecione', 'label' => 'Quantidade de períodos', 'required' => true, 'options' => [1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10]]);
                    echo $this->Form->input('type', ['label' => 'Tipos de avaliações', 'options' => ['NOTA' => 'Nota', 'CONCEITO' => 'Conceito'], 'required' => true]);
                    ?>


                    <div class="panel panel-info" id="box-nota">
                        <div class="panel-heading">Parâmetros de Notas e configurações</div>
                        <div class="panel-body">
                            <fieldset>

                                <?= $this->Form->input('calculate_total_year', ['empty' => 'Selecione', 'label' => 'Cálculo do total de notas anual', 'required' => true, 'options' => ['Soma' => 'Soma', 'Média' => 'Média']]); ?>

                                <?= $this->Form->input('calculate_final_note', ['empty' => 'Selecione', 'label' => 'Cálculo da nota final (total de notas anual + recuperaeção', 'required' => true,
                                    'options' => ['Soma' => 'Soma', 'Anual' => 'Anual', 'Maior Valor' => 'Maior Valor',
                                        'Nota Final = Nota da recuperação, quando houver' => 'Nota Final = Nota da recuperação, quando houver',
                                        '(Média Anual * 7 + Recuperação * 3)/10' => '(Média Anual * 7 + Recuperação * 3)/10'
                                    ]]); ?>

                                <?= $this->Form->input('annual_recovery', ['onchange' => 'showDisplay(this, "anual_recuperation_minimum_note")', 'label' => 'Possui recuperação final?', 'required' => true, 'type' => 'checkbox']); ?>

                                <?= $this->Form->input('semi_annual_recovery', ['onchange' => 'showDisplay(this, "period_recuperation_minimum_note")', 'label' => 'Possui recuperação semestral?', 'required' => true, 'type' => 'checkbox']); ?>
                            </fieldset>

                            <fieldset>
                                <legend>Notas mínimas e máximas</legend>
                                <div id="nota-push"></div>
                            </fieldset>

                            <div id="anual_recuperation_minimum_note" style="display: none;">
                                <?= $this->Form->input('anual_recuperation_minimum_note', ['id' => '', 'label' => 'Nota mínima para recuperação final', 'required' => true]); ?>
                            </div>

                            <div id="period_recuperation_minimum_note" style="display: none;">
                                <?= $this->Form->input('period_recuperation_minimum_note', ['label' => 'Nota mínima para recuperação do périodo', 'required' => true]); ?>
                            </div>

                            <?= $this->Form->input('period_minimum_note', ['label' => 'Nota mínima por périodo', 'required' => true]); ?>
                            <?= $this->Form->input('annual_minimum_note', ['label' => 'Nota mínima total', 'required' => true]); ?>
                        </div>
                    </div>


                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <?= $this->Form->button(__('Cadastrar'), ['class' => 'btn btn-success']) ?>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</section>

<?php $this->start('scriptBottom'); ?>
<script>

    /**
     * Mostrar campo
     * */
    function showDisplay(e, target) {
        var input = document.getElementById(target);
        input.style.display = (input.style.display == 'none') ? 'block' : 'none';
    }

    /**
     * Setar a quantidade de periodos de acordo com o periodo de avaliação
     * @param e
     */
    function setQuantityPeriod(e) {
        var arr = {
            'BIMESTRE': 4, 'TRIMESTRE': 3, 'SEMESTRE': 2, 'MODULAR': 1, 'UNIDADE': 1
        }

        document.getElementById('quantity_period').value = arr[e.value];
    }
</script>
<?php $this->end(); ?>
