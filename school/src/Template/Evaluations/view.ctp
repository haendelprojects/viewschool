<section class="content-header">
  <h1>
    <?php echo __('Evaluation'); ?>
  </h1>
  <ol class="breadcrumb">
    <li>
    <?= $this->Html->link('<i class="fa fa-dashboard"></i> ' . __('Back'), ['action' => 'index'], ['escape' => false])?>
    </li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-md-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <i class="fa fa-info"></i>
                <h3 class="box-title"><?php echo __('Information'); ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <dl class="dl-horizontal">
                                                                                                                <dt><?= __('Name') ?></dt>
                                        <dd>
                                            <?= h($evaluation->name) ?>
                                        </dd>
                                                                                                                                                            <dt><?= __('Type Attendance') ?></dt>
                                        <dd>
                                            <?= h($evaluation->type_attendance) ?>
                                        </dd>
                                                                                                                                                            <dt><?= __('Evaluation Period') ?></dt>
                                        <dd>
                                            <?= h($evaluation->evaluation_period) ?>
                                        </dd>
                                                                                                                                                            <dt><?= __('Type') ?></dt>
                                        <dd>
                                            <?= h($evaluation->type) ?>
                                        </dd>
                                                                                                                                                    <dt><?= __('Branch') ?></dt>
                                <dd>
                                    <?= $evaluation->has('branch') ? $evaluation->branch->name : '' ?>
                                </dd>
                                                                                                
                                            
                                                                                                                                            
                                                                                                                                                                                                
                                                                        <dt><?= __('Evaluation Attendance') ?></dt>
                            <dd>
                            <?= $evaluation->evaluation_attendance ? __('Yes') : __('No'); ?>
                            </dd>
                                                                    
                                    </dl>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- ./col -->
</div>
<!-- div -->

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <i class="fa fa-share-alt"></i>
                    <h3 class="box-title"><?= __('Related {0}', ['Class']) ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">

                <?php if (!empty($evaluation->class)): ?>

                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                                                    
                                    <th>
                                    Id
                                    </th>
                                        
                                                                                                                                            
                                    <th>
                                    Course Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Branch Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Organization Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Init Date
                                    </th>
                                        
                                                                    
                                    <th>
                                    End Date
                                    </th>
                                        
                                                                    
                                    <th>
                                    Period
                                    </th>
                                        
                                                                    
                                    <th>
                                    Name
                                    </th>
                                        
                                                                    
                                    <th>
                                    Wallet Name
                                    </th>
                                        
                                                                    
                                    <th>
                                    Status
                                    </th>
                                        
                                                                    
                                    <th>
                                    Class Time
                                    </th>
                                        
                                                                    
                                    <th>
                                    School Year
                                    </th>
                                        
                                                                    
                                    <th>
                                    Evaluation Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    People Id
                                    </th>
                                        
                                                                    
                                <th>
                                    <?php echo __(''); ?>
                                </th>
                            </tr>

                            <?php foreach ($evaluation->class as $class): ?>
                                <tr>
                                                                        
                                    <td>
                                    <?= h($class->id) ?>
                                    </td>
                                                                                                                                                
                                    <td>
                                    <?= h($class->course_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($class->branch_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($class->organization_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($class->init_date) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($class->end_date) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($class->period) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($class->name) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($class->wallet_name) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($class->status) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($class->class_time) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($class->school_year) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($class->evaluation_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($class->people_id) ?>
                                    </td>
                                    
                                                                        <td class="actions">
                                    <?= $this->Html->link(__('View'), ['controller' => 'Class', 'action' => 'view', $class->id], ['class'=>'btn btn-info btn-xs']) ?>

                                    <?= $this->Html->link(__('Edit'), ['controller' => 'Class', 'action' => 'edit', $class->id], ['class'=>'btn btn-warning btn-xs']) ?>

                                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Class', 'action' => 'delete', $class->id], ['confirm' => __('Are you sure you want to delete # {0}?', $class->id), 'class'=>'btn btn-danger btn-xs']) ?>    
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                                    
                        </tbody>
                    </table>

                <?php endif; ?>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
