<section class="content-header">
    <h1>
        Adicionar Evento
    </h1>
    <ol class="breadcrumb">
        <li>

        </li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">

                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <?= $this->Form->create($calendar, array('role' => 'form')) ?>
                <div class="box-body">
                    <?php
                    echo $this->Form->input('title', ['label' => 'Titulo', 'placeholder' => 'Titulo do Evento']);
                    echo $this->Form->input('text', ['label' => 'Descrição', 'type' => 'textarea', 'class' => 'textarea']);
                    echo $this->Form->input('start_dp', ['type' => 'text', 'label' => 'Inicio', 'placeholder' => 'Data inicial do Evento', 'class' => 'datepicker']);
                    echo $this->Form->input('end_dp', ['type' => 'text', 'label' => 'Fim', 'placeholder' => 'Data Final do Evento', 'class' => 'datepicker']);
                    echo $this->Form->input('background', ['label' => 'Cor do evento', 'type' => 'color', 'placeholder' => 'Cor do evento no calendário']);
                    ?>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <?= $this->Form->button(__('Salvar')) ?>
                    </div>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
</section>




