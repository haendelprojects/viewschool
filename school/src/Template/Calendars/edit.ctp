<section class="content-header">
    <h1>
        Calendário
        <small><?= __('Editar') ?></small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <?= $this->Html->link('<i class="fa fa-arrow-left"></i> ' . __('Voltar'), ['action' => 'index'], ['escape' => false]) ?>
        </li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><?= __('Informações') ?></h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <?= $this->Form->create($calendar, array('role' => 'form')) ?>
                <div class="box-body">
                    <?php
                    echo $this->Form->input('title', ['label' => 'Titulo', 'placeholder' => 'Titulo do Evento']);
                    echo $this->Form->input('text', ['label' => 'Descrição', 'type' => 'textarea', 'class' => 'textarea']);
                    echo $this->Form->input('start_dp', [ 'value'=> $calendar->start->format('d/m/Y'),'type' => 'text', 'label' => 'Inicio', 'placeholder' => 'Data inicial do Evento', 'class' => 'datepicker']);
                    echo $this->Form->input('end_dp', [ 'value'=> $calendar->end->format('d/m/Y'),'type' => 'text', 'label' => 'Fim', 'placeholder' => 'Data Final do Evento', 'class' => 'datepicker']);
                    echo $this->Form->input('background', ['label' => 'Cor do evento no calendário', 'type' => 'color', 'placeholder' => 'Cor do evento no calendário']);
                    ?>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <?= $this->Form->button(__('Salvar')) ?>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</section>

