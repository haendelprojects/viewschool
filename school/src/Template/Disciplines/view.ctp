<section class="content-header">
    <h1>
        <?php echo __('Disciplina'); ?>
    </h1>
    <ol class="breadcrumb">
        <li>
            <?= $this->Html->link('<i class="fa fa-arrow-left"></i> ' . __('Volar'), ['action' => 'index'], ['escape' => false]) ?>
        </li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <i class="fa fa-info"></i>
                    <h3 class="box-title"><?php echo __('Informação'); ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt><?= __('Name') ?></dt>
                        <dd>
                            <?= h($discipline->name) ?>
                        </dd>
                    </dl>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- ./col -->
    </div>
    <!-- div -->

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <i class="fa fa-list"></i>
                    <h3 class="box-title"><?= __('Turmas Relacionadas') ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">

                    <?php if (!empty($discipline->class_discipline)): ?>

                        <table class="table table-hover">
                            <tbody>
                            <tr>
                                <th>Turma</th>
                                <th>Professor</th>
                                <th><?php echo __(''); ?></th>
                            </tr>

                            <?php foreach ($discipline->class_discipline as $classDiscipline): ?>
                                <tr>
                                    <td>
                                        <?= h($classDiscipline->class_id) ?>
                                    </td>
                                    <td>
                                        <?= h($classDiscipline->people_id) ?>
                                    </td>

                                    <td class="actions">
                                        <?= $this->Html->link(__('Detalhes'), ['controller' => 'ClassDiscipline', 'action' => 'view', $classDiscipline->id], ['class' => 'btn btn-info btn-xs']) ?>
                                        <?= $this->Html->link(__('Editar'), ['controller' => 'ClassDiscipline', 'action' => 'edit', $classDiscipline->id], ['class' => 'btn btn-warning btn-xs']) ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>

                            </tbody>
                        </table>

                    <?php endif; ?>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
