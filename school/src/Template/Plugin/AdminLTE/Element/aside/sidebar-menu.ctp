<?php

use Cake\Core\Configure;

$file = Configure::read('Theme.folder') . DS . 'src' . DS . 'Template' . DS . 'Element' . DS . 'aside' . DS . 'sidebar-menu.ctp';
if (file_exists($file)) {
    ob_start();
    include_once $file;
    echo ob_get_clean();
} else {
    ?>
    <ul class="sidebar-menu">
        <li class="header">Navegação</li>

        <?php if ($this->User->isSchool()): ?>
            <?= $this->element('Menus/school'); ?>
        <?php endif; ?>


        <?php if ($this->User->isTeacher()): ?>
            <?= $this->element('Menus/teacher'); ?>
        <?php endif; ?>


        <?php if ($this->User->isStudent()): ?>
            <?= $this->element('Menus/student'); ?>
        <?php endif; ?>



    </ul>
<?php } ?>
