<div class="alert alert-danger  alert-popup alert-dismissible">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
    <strong><i class="icon fa fa-check"></i> <?= __('Erro') ?>!</strong>
    <?= h($message) ?>
</div>