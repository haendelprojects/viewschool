<section class="content-header">
  <h1>
    <?php echo __('Calculate'); ?>
  </h1>
  <ol class="breadcrumb">
    <li>
    <?= $this->Html->link('<i class="fa fa-dashboard"></i> ' . __('Back'), ['action' => 'index'], ['escape' => false])?>
    </li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-md-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <i class="fa fa-info"></i>
                <h3 class="box-title"><?php echo __('Information'); ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <dl class="dl-horizontal">
                                                                                                                <dt><?= __('Sum Yearly') ?></dt>
                                        <dd>
                                            <?= h($calculate->sum_yearly) ?>
                                        </dd>
                                                                                                                                                            <dt><?= __('Calculatecol') ?></dt>
                                        <dd>
                                            <?= h($calculate->calculatecol) ?>
                                        </dd>
                                                                                                                                    
                                            
                                                                                                                                            
                                                                                                                                                                                                
                                                                        <dt><?= __('Recovery Final') ?></dt>
                            <dd>
                            <?= $calculate->recovery_final ? __('Yes') : __('No'); ?>
                            </dd>
                                                    <dt><?= __('Recovery Semi Annual') ?></dt>
                            <dd>
                            <?= $calculate->recovery_semi_annual ? __('Yes') : __('No'); ?>
                            </dd>
                                                                    
                                    </dl>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- ./col -->
</div>
<!-- div -->

</section>
