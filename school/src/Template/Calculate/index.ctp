<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Calculate
    <div class="pull-right"><?= $this->Html->link(__('New'), ['action' => 'add'], ['class'=>'btn btn-success btn-xs']) ?></div>
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title"><?= __('List of') ?> Calculate</h3>
          <div class="box-tools">
            <form action="<?php echo $this->Url->build(); ?>" method="POST">
              <div class="input-group input-group-sm"  style="width: 180px;">
                <input type="text" name="search" class="form-control" placeholder="<?= __('Fill in to start search') ?>">
                <span class="input-group-btn">
                <button class="btn btn-info btn-flat" type="submit"><?= __('Filter') ?></button>
                </span>
              </div>
            </form>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
          <table class="table table-hover">
            <thead>
              <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('sum_yearly') ?></th>
                <th><?= $this->Paginator->sort('recovery_final') ?></th>
                <th><?= $this->Paginator->sort('recovery_semi_annual') ?></th>
                <th><?= $this->Paginator->sort('calculatecol') ?></th>
                <th><?= __('') ?></th>
              </tr>
            </thead>
            <tbody>
            <?php foreach ($calculate as $calculate): ?>
              <tr>
                <td><?= $this->Number->format($calculate->id) ?></td>
                <td><?= h($calculate->sum_yearly) ?></td>
                <td><?= h($calculate->recovery_final) ?></td>
                <td><?= h($calculate->recovery_semi_annual) ?></td>
                <td><?= h($calculate->calculatecol) ?></td>
                <td class="actions" style="white-space:nowrap">
                  <?= $this->Html->link(__('View'), ['action' => 'view', $calculate->id], ['class'=>'btn btn-info btn-xs']) ?>
                  <?= $this->Html->link(__('Edit'), ['action' => 'edit', $calculate->id], ['class'=>'btn btn-warning btn-xs']) ?>
                  <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $calculate->id], ['confirm' => __('Confirma a remoção?'), 'class'=>'btn btn-danger btn-xs']) ?>
                </td>
              </tr>
            <?php endforeach; ?>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
          <ul class="pagination pagination-sm no-margin pull-right">
            <?php echo $this->Paginator->numbers(); ?>
          </ul>
        </div>
      </div>
      <!-- /.box -->
    </div>
  </div>
</section>
<!-- /.content -->
