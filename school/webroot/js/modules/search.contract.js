/**
 * Created by Haendel on 02/09/2016.
 */

Module('SEARCH.Contract', function (Client) {

    Client.fn.initialize = function (container) {
        this.searchInput = container.find('.search-contract');

        // this.url = "http://localhost/admin-web/ajax/getUser.json";
        this.url = "/contracts/actives.json";

        this.addEventListener();
    };

    Client.fn.addEventListener = function () {
        this.searchInput.select2({
            ajax: {
                url: this.url,
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page,
                        corporate: $(this).attr('data-corporate-id')
                    };
                },
                processResults: function (data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;

                    return {
                        results: data.data.users,
                        pagination: {
                            more: (params.page * 30) < data.data.pagination.count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }, // let our custom formatter work
            minimumInputLength: 1,
            templateResult: formatRepo, // omitted for brevity, see the source of this page
            templateSelection: formatRepoSelection // omitted for brevity, see the source of this page

        });
    }

    function formatRepo(repo) {
        // debugger;
        if (repo.first_name) {
            return (repo.name + " (" + repo.fund.credit_balance + " )");
        } else {
            return repo.text;
        }

    }

    function formatRepoSelection(repo) {
        // debugger;
        if (repo.first_name) {
            return (repo.name + " " + repo.fund.credit_balance);
        } else {
            return repo.text;
        }
    }
});
