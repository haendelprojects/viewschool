/**
 * Created by Haendel on 22/08/2016.
 */
Module('REGION.ListCities', function (ListCities) {

    ListCities.fn.initialize = function (container) {
        this.container = container;
        // Array holding selected row IDs
        this.rows_selected = [];
        this.table = container.find('#list-cities');
        this.tableBody = container.find('#list-cities tbody');
        this.checkAll = container.find('#check-select-all');
        this.form = container.find('#form-region');

        this.addEventListeners();
    }

    ListCities.fn.addEventListeners = function () {
        this.table
            .on('click', 'span', this.onTableClick.bind(this));

        this.tableBody
            .on('change', 'input[type="checkbox"]', this.onTableBodyChange.bind(this))
            .on('click', 'span', this.onTableBodyClick.bind(this));

        this.checkAll
            .on('click', this.onCheckAllClick.bind(this));

        this.form
            .on('submit', this.onFormSubmit.bind(this));

    }

    ListCities.fn.onTableClick = function (event) {
        this.container.DataTable({
            'ajax': this.container.attr('data-url') + '/cities/get-list/' + this.container.attr('data-region-id') + '.json',
            'columnDefs': [{
                'targets': 0,
                'searchable': false,
                'orderable': false,
                'className': 'dt-body-center',
                'render': function (data, type, full, meta) {
                    return '<input type="checkbox" name="id[]" ' + ((data) ? "checked" : "") + ' value="'
                        + $('<div/>').text(data).html() + '">';
                }
            }],
            'order': [1, 'asc']
        });
    }

    ListCities.fn.onTableBodyChange = function (event) {
        //If checkbox is not checked
        if (!this.checked) {
            var el = $('#example-select-all').get(0);
            // If "Select all" control is checked and has 'indeterminate' property
            if (el && el.checked && ('indeterminate' in el)) {
                // Set visual state of "Select all" control
                // as 'indeterminate'
                el.indeterminate = true;
            }
        }
    }

    ListCities.fn.onTableBodyClick = function (event) {
        var data = table.row($(this).parents('tr')).data();
    }

    ListCities.fn.onCheckAllClick = function (event) {
        // Check/uncheck all checkboxes in the table
        var rows = table.rows({'search': 'applied'}).nodes();
        $('input[type="checkbox"]', rows).prop('checked', this.checked);
    }

    ListCities.fn.onFormSubmit = function (event) {
        // Iterate over all selected checkboxes
        $.each(this.rows_selected, function (index, rowId) {
            // Create a hidden element
            this.form.append(
                $('<input>')
                    .attr('type', 'hidden')
                    .attr('name', 'regions._id[]')
                    .val(rowId)
            );
        });
    }

});

