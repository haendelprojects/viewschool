/**
 * Created by Haendel on 25/08/2016.
 */

Module('MASKS.create', function (Masks) {

    var MaskBehavior = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    };

    var Options = {
        onKeyPress: function (val, e, field, options) {
            field.mask(MaskBehavior.apply({}, arguments), options);
        }
    };


    Masks.fn.initialize = function (container) {
        this.container = container;
        this.cell_phone = container.find('.mask-cell-phone');
        this.zipcode = container.find('.zipcode');
        this.cnpj = container.find('.mask-cnpj');
        this.cpf = container.find('.mask-cpf');
        this.addEventListener();
    }

    /**
     * ADicionar eventos
     */
    Masks.fn.addEventListener = function () {
        this.cell_phone.mask(
            MaskBehavior, Options
        )

        this.zipcode.mask('00000-000');
        this.cnpj.mask('00.000.000/0000-00');
        this.cpf.mask('000.000.000-00');
        this.zipcode.mask('00000-000');
    }
    /**
     * 
     */
});
