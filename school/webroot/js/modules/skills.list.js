/**
 * Created by Haendel on 25/08/2016.
 */
(function () {

    function formatRepo(repo) {
        return '<span class="ti ti-plus"></span> ' + repo.name || repo.text;
    }

    function formatRepoSelection(repo) {
        return '<span data-info="">' + repo.name + '</span>' || repo.text;
    }

    $(".js-data-example-ajax").select2({
        ajax: {
            url: "/skills/ajax-list.json",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                console.info(data);
                return {
                    results: data.data.skills,
                    pagination: {
                        more: (params.page * 30) < data.data.pagination.count_page
                    }
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        }, // let our custom formatter work
        minimumInputLength: 1,
        templateResult: formatRepo, // omitted for brevity, see the source of this page
        templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
    });

    $('.js-data-example-ajax').on("select2:select", function (e) {
        var html = '<div class="skills-' + e.params.id + '">' + e.params.data.name + '<span class="remove-skill btn btn-danger" data-skill-id="' + e.params.id + '"><span class="ti ti-trash"></span></span></div>'
        $('.list-choice-skills').append(html);

        $('.remove-skill').on('click', function () {
            $('.skills-' + $(this).attr('data-skill-id')).remove();
        });
    });
})();
