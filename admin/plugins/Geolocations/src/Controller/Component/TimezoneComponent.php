<?php
/**
 * Created by PhpStorm.
 * User: Haendel
 * Date: 24/10/2016
 * Time: 16:44
 */

namespace GeoLocations\Controller\Component;


use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
use GeoLocations\Model\Entity\Address;

class TimezoneComponent extends Component
{
    private $Cities;
    private $States;
    private $Addresses;
    private $Countries;

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->Cities = TableRegistry::get('Cities');
        $this->States = TableRegistry::get('States');
        $this->Addresses = TableRegistry::get('Addresses');
        $this->Countries = TableRegistry::get('Countries');

    }

    public function getDateTime($city, $state, $type_return = 'array', \DateTime $date = null)
    {
        $city = $this->Cities->find('all')
            ->contain(['States'])
            ->where(['Cities.name' => $city, 'States.abbrv' => $state])
            ->first();

        if (!$date) {
            $date = new \DateTime();
        }


        if ($type_return == 'array') {
            $date = $date->setTimezone(new \DateTimeZone($city->timezone));

            return [
                'year' => $date->format('Y'),
                'month' => $date->format('m'),
                'day' => $date->format('d'),
                'minute' => $date->format('i'),
                'hour' => $date->format('H')
            ];

        } elseif ($type_return == 'datetime') {
            return $date->setTimezone(new \DateTimeZone($city->timezone));
        }
    }

    public function getDateTimeAddress(Address $address, $type_return = 'array', \DateTime $date = null)
    {

        if (!$date) {
            $date = new \DateTime();
        }

        if ($type_return == 'array') {
            $date = $date->setTimezone(new \DateTimeZone($address->o_city->timezone));

            return [
                'year' => $date->format('Y'),
                'month' => $date->format('m'),
                'day' => $date->format('d'),
                'minute' => $date->format('i'),
                'hour' => $date->format('H')
            ];

        } elseif ($type_return == 'datetime') {
            return $date->setTimezone(new \DateTimeZone($address->o_city->timezone));
        }
    }

    public function getDateTimeAddressId($address_id, $type_return = 'array', \DateTime $date = null)
    {

        $address = $this->Addresses->find('all')->where(['Addresses.id' => $address_id])->contain(['OCities'])->first();

        if (!$date) {
            $date = new \DateTime();
        }

        if ($type_return == 'array') {
            $date = $date->setTimezone(new \DateTimeZone($address->O_city->timezone));

            return [
                'year' => $date->format('Y'),
                'month' => $date->format('m'),
                'day' => $date->format('d'),
                'minute' => $date->format('i'),
                'hour' => $date->format('H')
            ];

        } elseif ($type_return == 'datetime') {
            return $date->setTimezone(new \DateTimeZone($address->o_city->timezone));
        }
    }

    public function getDateTimeCityId($city_id, $type_return = 'array', \DateTime $date = null)
    {

        $city = $this->Cities->find('all')->where(['id' => $city_id])->first();

        if (!$date) {
            $date = new \DateTime();
        }
        if ($type_return == 'array') {
            $date = $date->setTimezone(new \DateTimeZone($city->timezone));
            return [
                'year' => $date->format('Y'),
                'month' => $date->format('m'),
                'day' => $date->format('d'),
                'minute' => $date->format('i'),
                'hour' => $date->format('H')
            ];

        } elseif ($type_return == 'datetime') {
            return $date->setTimezone(new \DateTimeZone($city->timezone));
        }
    }

    public function setTimeZone($address_id, $type_return = 'array', \DateTime $date = null)
    {
        $address = $this->Addresses->find('all')->where(['Addresses.id' => $address_id])->contain(['OCities'])->first();
        date_default_timezone_set($address->o_city->timezone);
    }
}