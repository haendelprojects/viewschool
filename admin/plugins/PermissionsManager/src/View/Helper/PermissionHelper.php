<?php

/**
 * CakePHP 3.x - Acl Manager
 *
 * PHP version 5
 *
 * Class AclHelper
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @category CakePHP3
 *
 * @package  AclManager\View\Helper
 *
 * @author Ivan Amat <dev@ivanamat.es>
 * @copyright Copyright 2016, Iván Amat
 * @license MIT http://opensource.org/licenses/MIT
 * @link https://github.com/ivanamat/cakephp3-aclmanager
 */

namespace PermissionsManager\View\Helper;
use Cake\View\Helper;

class PermissionHelper extends Helper
{

    public $helpers = ['Html'];
    private $User = [];

    public $actions = [
        'index' => 'Listagem',
        'view' => 'Visualizar',
        'add' => 'Adicionar',
        'edit' => 'Editar',
        'delete' => 'Deletar',
        'login' => 'Logar',
        'logout' => 'Logout',
        'active' => 'Ativar',
        'addPoints' => 'Adicionar Aditivos',
        'display' => 'Display',
        'Permissions' => 'Permissões',
        'Roles' => 'Regras'
    ];

    public function initialize(array $config)
    {
        parent::initialize($config);

        $session = new \Cake\Network\Session;
        $this->User = $session->read('Auth.User');
    }

    public function check($all, $action)
    {
        foreach ($all as $one) {
            if ($one->id == $action->id) {
                return true;
            }
        }

        return false;
    }

    public function getName($val)
    {
        if (isset($this->actions[$val])) {
            return $this->actions[$val];
        } else {
            return '';
        }
    }
}