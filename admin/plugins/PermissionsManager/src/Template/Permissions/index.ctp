<section class="content-header">
    <h1>
        <?php echo __('Controle de Permissão'); ?>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <i class="fa fa-info"></i>
                    <h3 class="box-title"><?php echo __('Detalhes'); ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="columns large-4">
                        <h3><?php echo __('Gerenciamento'); ?></h3>
                        <ul class="options">
                            <li><?php //echo $this->Html->link(__('Gerenciar Regras'), ['controller' => 'Permissions', 'action' => 'Permissions']); ?></li>
                        </ul>
                    </div>
                    <div class="columns large-4">
                        <h3><?php echo __('Atualizar'); ?></h3>
                        <ul class="options">
                            <li><?php echo $this->Html->link(__('Atualizar Componentes de Acesso'), ['controller' => 'Permissions', 'action' => 'UpdateModules']); ?></li>
                            <li><?php echo $this->Html->link(__('Atualizar Regras de Acesso'), ['controller' => 'Roles', 'action' => 'index']); ?></li>
                        </ul>
                    </div>
                    <div class="columns large-4">
                        <h3><?php echo __('Apagar e restaurar'); ?></h3>
                        <ul class="options">
                            <li><?php //echo $this->Html->link(__('Revogar permissões'), ['controller' => 'Permissions', 'action' => 'RevokePerms'], ['confirm' => __('Do you really want to revoke all permissions? This will remove all above assigned permissions and set defaults. Only first item of last ARO will have access to panel.')]); ?></li>
                            <!--                            <li>-->
                            <?php //echo $this->Html->link(__('Drop ACOs and AROs'), ['controller' => 'Permissions', 'action' => 'drop'], ['confirm' => __('Do you really want delete ACOs and AROs? This will remove all above assigned permissions.')]); ?><!--</li>-->
                            <li><?php //echo $this->Html->link(__('Atualizar componentes e regras de acesso'), ['controller' => 'Permissions', 'action' => 'defaults'], ['confirm' => __('Do you want restore defaults? This will remove all above assigned permissions. Only first item of last ARO will have access to panel.')]); ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
