<style>

    legend {
        font-size: 16px;
        width: inherit;
        border: none;
        padding: 0 20px;
        margin: 20px 0 0;
    }

    fieldset {
        padding: .35em .625em .75em;
        margin: 0 2px;
        border: 1px solid #dedede;
    }
</style>

<section class="content-header">
    <h1>
        Regras
        <small><?= __('Adicionar') ?></small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <?= $this->Html->link('<i class="fa fa-dashboard"></i> ' . __('Voltar'), ['action' => 'index'], ['escape' => false]) ?>
        </li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><?= __('Detalhes') ?></h3>
                </div>

                <!-- /.box-header -->
                <!-- form start -->
                <?= $this->Form->create($role, array('role' => 'form')) ?>
                <div class="box-body">
                    <?php
                    echo $this->Form->input('name', ['label' => 'Nome', 'class' => 'form-control']);
                    //                    echo $this->Form->input('modules._ids', ['options' => $modules, 'label' => 'Modulos', 'multiple' => 'checkbox']);
                    ?>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><?= __('Permissões') ?></h3>
                </div>
                <div class="box-body">

                    <?php foreach ($modules as $controller): ?>
                        <?php foreach ($controller->children as $module): ?>
                            <fieldset>
                                <legend><?= $module->alias ?></legend>
                                <div class="row">
                                    <?php foreach ($module->children as $key => $action): ?>
                                        <div class="col-md-2">
                                            <?= $this->Form->input('modules.' . $action->id, ['label' => $action->alias, 'type' => 'checkbox']); ?>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </fieldset>
                        <?php endforeach; ?>
                    <?php endforeach; ?>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <?= $this->Form->button(__('Save')) ?>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</section>

