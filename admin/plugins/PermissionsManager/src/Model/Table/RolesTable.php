<?php
namespace PermissionsManager\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Roles Model
 *
 * @property \PermissionsManager\Model\Table\UsersTable|\Cake\ORM\Association\HasMany $Users
 * @property \PermissionsManager\Model\Table\ModulesTable|\Cake\ORM\Association\BelongsToMany $Modules
 *
 * @method \PermissionsManager\Model\Entity\Role get($primaryKey, $options = [])
 * @method \PermissionsManager\Model\Entity\Role newEntity($data = null, array $options = [])
 * @method \PermissionsManager\Model\Entity\Role[] newEntities(array $data, array $options = [])
 * @method \PermissionsManager\Model\Entity\Role|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \PermissionsManager\Model\Entity\Role patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \PermissionsManager\Model\Entity\Role[] patchEntities($entities, array $data, array $options = [])
 * @method \PermissionsManager\Model\Entity\Role findOrCreate($search, callable $callback = null, $options = [])
 */
class RolesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('roles');
        $this->setDisplayField('alias');
        $this->setPrimaryKey('id');

        $this->hasMany('Users', [
            'foreignKey' => 'role_id'
        ]);
        $this->belongsToMany('Modules', [
            'foreignKey' => 'role_id',
            'targetForeignKey' => 'module_id',
            'joinTable' => 'roles_modules'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('name');

        $validator
            ->allowEmpty('alias');

        return $validator;
    }
}
