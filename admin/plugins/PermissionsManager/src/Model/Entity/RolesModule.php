<?php
namespace PermissionsManager\Model\Entity;

use Cake\ORM\Entity;

/**
 * RolesModule Entity
 *
 * @property int $id
 * @property int $role_id
 * @property int $module_id
 * @property string $_create
 * @property string $_read
 * @property string $_update
 * @property string $_delete
 *
 * @property \PermissionsManager\Model\Entity\Role $role
 * @property \PermissionsManager\Model\Entity\Module $module
 */
class RolesModule extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
