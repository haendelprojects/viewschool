<?php

/**
 * CakePHP 3.x - Permission Manager
 *
 * PHP version 5
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @category CakePHP3
 *
 * @author   Ivan Amat <dev@ivanamat.es>
 * @copyright     Copyright 2016, Iván Amat
 * @license  MIT http://opensource.org/licenses/MIT
 * @link     https://github.com/ivanamat/cakephp3-aclmanager
 *
 */

use Cake\Routing\Router;

Router::connect(
    'PermissionsManager',
    ['plugin' => 'PermissionsManager', 'controller' => 'Permissions', 'action' => 'index']
);

Router::connect(
    'PermissionsManager/:action/*',
    ['plugin' => 'PermissionsManager', 'controller' => 'Permissions']
);

Router::connect(
    'PermissionsManager/Roles/:action/*',
    ['plugin' => 'PermissionsManager', 'controller' => 'Roles']
);
