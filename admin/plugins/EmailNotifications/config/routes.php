<?php
use Cake\Routing\Router;

Router::plugin(
    'EmailNotifications',
    ['path' => '/email-notifications'],
    function ($routes) {
        $routes->fallbacks('DashedRoute');
    }
);