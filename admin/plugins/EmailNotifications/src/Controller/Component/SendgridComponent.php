<?php
/**
 * Created by PhpStorm.
 * User: Haendel
 * Date: 01/06/2016
 * Time: 21:31
 */

namespace EmailNotifications\Controller\Component;


use Cake\Controller\Component;
use Cake\Network\Http\Client;

/**
 * Class SendgridComponent
 * @package EmailNotifications\Controller\Component
 * @property Client $Http
 * @property MailDockerComponent $MailDocker
 */
class SendgridComponent extends Component
{
    private $to, $from, $subject, $substitutions, $content, $template, $Http;
    private $apiKey = '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';
    /**
     * Modelos de template aceitaveis
     * @var array
     */
    private $templates = [
        'cadaster' => '',
        'activated' => ''
    ];

    public function setKey($key)
    {
        $this->apiKey = $key;

        $this->Http = new Client([
            'headers' => [
                'Authorization' => 'Bearer ' . $this->apiKey
            ]
        ]);

        return $this;
    }

    public function addTemplate($name, $value)
    {
        $this->templates[$name] = $value;
        return $this;
    }

    /**
     * @param string $email
     * @param string $name
     * * @return $this
     */
    public function setTo($email = '', $name = '')
    {
        $this->to = [
            "email" => $email,
            "name" => $name
        ];

        return $this;
    }

    /**
     * @param string $email
     * @param string $name
     * * @return $this
     */
    public function setFrom($email = '', $name = '')
    {
        $this->from = [
            "email" => $email,
            "name" => $name
        ];

        return $this;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setSubject($value = '')
    {
        $this->subject = $value;

        return $this;
    }

    /**
     * @param array $array
     * @return $this
     */
    public function setSubstitutions($array = [])
    {
        $this->substitutions = $array;

        return $this;
    }

    /**
     * @param string $type
     * @param string $value
     * @return $this
     */
    public function setContent($type = 'text/html', $value = 'Não tem Suporte a Html.')
    {
        $this->content = [
            'type' => $type,
            'value' => $value
        ];

        return $this;
    }

    /**
     * @param $value
     * @return $this
     */
    public function setTemplateId($value)
    {
        $this->template = $value;

        return $this;
    }

    /**
     * @param $value
     * @return $this
     */
    public function setTemplate($value)
    {
        $this->template = $this->templates[$value];

        return $this;
    }

    public function getSendInfos()
    {
        $data = [
            "personalizations" => [
                [
                    "to" => [
                        $this->to
                    ],
                    "subject" => $this->subject,
                    "substitutions" => $this->substitutions
                ]
            ],
            "from" => $this->from,
            "content" => [$this->content],
            "template_id" => $this->template
        ];

        return json_encode($data);
    }

    public function send()
    {
        return $this->Http->post(
            'https://api.sendgrid.com/v3/mail/send',
            $this->getSendInfos(),
            ['type' => 'json']
        );
    }

    public function newCadaster($user)
    {


    }
}