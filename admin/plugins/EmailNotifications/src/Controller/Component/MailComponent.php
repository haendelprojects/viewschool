<?php

namespace EmailNotifications\Controller\Component;

use App\Model\Entity\User;
use App\Model\Entity\Occurrence;
use App\Model\Entity\Service;
use App\Model\Entity\Contract;
use App\Model\Entity\Address;
use App\Model\Entity\Fund;
use Cake\Controller\Component;
use Cake\Network\Http\Client;
use Cake\Routing\Router;
use Cake\Utility\Security;
use Composer\DependencyResolver\Transaction;

/**
 * Description of MailComponent
 * @author Erick
 * @property \SendGrid $Sender
 * @property Client $Http
 * @property SendgridComponent $Sendgrid
 * @property MaildockerComponent $MailDocker
 *
 * @property SendgridComponent $Transition
 */
class MailComponent extends Component
{
    private $Transition;
    private $Http;
    private $email_enterprise, $name_enterprise;

    public $components = [
        'EmailNotifications.Sendgrid',
        'EmailNotifications.Sendgrid'
    ];

    public function initialize(array $config)
    {
        parent::initialize($config);
        //$this->Http = new Client();

        $this->email_enterprise = 'noreply@viewschool.com.br';
        $this->name_enterprise = 'ViewSchool';

        $this->Transition = $this->Sendgrid;
        /**
         * Criando aplicação MailDocker
         */
        $this->Transition
            ->setKey('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');

    }

    public function message($title, $text, $user)
    {
        $this->Transition
            ->setTo($user->username, $user->first_name)
            ->setFrom($this->email_enterprise, $this->name_enterprise)
            ->setSubject($title)
            ->setContent()
            ->setSubstitutions(['-text-' => $text, 'text' => $text])
            ->setTemplate('message')
            ->send();
    }

    /**
     * Email para criar uma nova senha
     * @param User $user
     */
    public function resetPassword(User $user)
    {
        $this->Transition
            ->setTo($user->username, $user->people->name)
            ->setFrom($this->email_enterprise, $this->name_enterprise)
            ->setSubject(__('Esqueçeu a senha? '))
            ->setContent()
            ->setTemplateId('f3ceef8f-21dc-4106-b1d1-e05cbc2f15f9')
            ->setSubstitutions(['token' => 'https://demo.viewschool.com.br/users/new-password/' . $user->reset_password_token, 'user_name' => $user->people->name])
            ->send();
    }

}

