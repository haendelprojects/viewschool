<?php
namespace Flow\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
use Flow\Model\Table\ExtractsTable;

/**
 * Created by PhpStorm.
 * User: erick
 * Date: 14/08/2017
 * Time: 14:48
 * @property ExtractsTable $Extracts
 */
class ExtractComponent extends Component
{
    private $Extracts;
    private $Contracts;

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->Extracts = TableRegistry::get('Extracts');
        $this->Contracts = TableRegistry::get('Contracts');
    }

    private function _calculateBalanceRecurrent($contract)
    {
        $funds_months = $contract->fund->funds_months;
        $limit = ceil($contract->fund->points_days_validity / 30);
        $count = count($funds_months);
        $balance = [];

        for ($i = 0; $i < $count; $i++) {
            $balance[$i] = $funds_months[$i]->spare_points - $funds_months[$i]->stored_points_expense;
            $funds_months[$i]->spare_points = 0;
        }

        for ($i = 0; $i < $count; $i++) {
            $funds_months[$i]->spare_points = $balance[$i];

            for ($j = 1; $j < $limit; $j++) {
                if (($i - $j) >= 0) {
                    $funds_months[$i]->spare_points += $balance[$i - $j];
                }
            }
        }
        return $funds_months;
    }

    private function _balance($id)
    {
        $contract = $this->Contracts->get($id, ['contain' => ['Funds.FundsMonths']]);
        $now = new \DateTime();
        $month = '';

        if ($contract->fund->type == 'RECURRENT') {
            $contract->fund->funds_months = $this->_calculateBalanceRecurrent($contract);
            foreach ($contract->fund->funds_months as $funds_month) {
                if ($funds_month->initial_date <= $now && $funds_month->final_date >= $now) {
                    $month = $funds_month;
                }
            }
        } else {
            foreach ($contract->fund->funds_months as $funds_month) {
                if ($funds_month->initial_date <= $now && $funds_month->final_date >= $now) {
                    $month = $funds_month;
                }
            }
        }

        if (!$month) {
            $month = $this->Contracts->Funds->FundsMonths
                ->find('all')
                ->where(['fund_id' => $contract->fund->id])->last();
        }
        return $month['spare_points'];
    }

    /**
     * Novo contrato
     * @param $contract
     */
    public function newContract($contract)
    {
        $extract = $this->Extracts->newEntity();
        $extract = $this->Extracts->patchEntity($extract, [
            'contract_id' => $contract->id,
            'enterprise_id' => $contract->enterprise_id,
            'income' => $this->_balance($contract->id),
            'balance' => $this->_balance($contract->id),
            'description' => 'NEW_CONTRACT',
            'date' => new \DateTime(),
            'active' => true
        ]);
        $this->Extracts->save($extract);
    }

    /**
     * Desativar contrato
     * @param $contract_id
     * @param $enterprise_id
     * @param $text
     */
    public function deactiveContract($contract_id, $enterprise_id, $text)
    {
        $extract = $this->Extracts->newEntity();
        $extract = $this->Extracts->patchEntity($extract, [
            'contract_id' => $contract_id,
            'enterprise_id' => $enterprise_id,
            'expense' => $this->_balance($contract_id),
            'balance' => 0,
            'description' => $text,
            'date' => new \DateTime(),
            'active' => true
        ]);
        $this->Extracts->save($extract);
    }

    /**
     * Ativar contrato
     * @param $contract_id
     * @param $enterprise_id
     * @param $text
     */
    public function activeContract($contract_id, $enterprise_id, $text)
    {
        $extract = $this->Extracts->newEntity();
        $extract = $this->Extracts->patchEntity($extract, [
            'contract_id' => $contract_id,
            'enterprise_id' => $enterprise_id,
            'income' => $this->_balance($contract_id),
            'balance' => $this->_balance($contract_id),
            'description' => $text,
            'date' => new \DateTime(),
            'active' => true
        ]);
        $this->Extracts->save($extract);
    }

    /**
     * Adicionar pontos extras ao contrato
     * @param $contract_id
     * @param $fund_month
     * @param $val
     */
    public function additive($contract_id, $fund_month, $enterprise_id, $val)
    {
        $extract = $this->Extracts->newEntity();
        $extract = $this->Extracts->patchEntity($extract, [
            'fund_month_id' => $fund_month,
            'contract_id' => $contract_id,
            'income' => $val,
            'enterprise_id' => $enterprise_id,
            'balance' => $this->_balance($contract_id),
            'description' => 'ADDITIVE',
            'date' => new \DateTime(),
            'active' => true
        ]);
        $this->Extracts->save($extract);
    }

    public function detract($contract_id, $fund_month, $enterprise_id, $val)
    {
        $extract = $this->Extracts->newEntity();
        $extract = $this->Extracts->patchEntity($extract, [
            'fund_month_id' => $fund_month,
            'contract_id' => $contract_id,
            'expense' => $val,
            'enterprise_id' => $enterprise_id,
            'balance' => $this->_balance($contract_id),
            'description' => 'DETRACT',
            'date' => new \DateTime(),
            'active' => true
        ]);
        $this->Extracts->save($extract);
    }


    /**
     * Criação de um novo chamado
     * @param $occurrence_id
     * @param $contract_id
     * @param $expense
     */
    public function newOccurrence($occurrence_id, $contract_id, $enterprise_id, $expense)
    {
        $extract = $this->Extracts->newEntity();
        $extract = $this->Extracts->patchEntity($extract, [
            'occurrence_id' => $occurrence_id,
            'contract_id' => $contract_id,
            'expense' => $expense,
            'balance' => $this->_balance($contract_id),
            'enterprise_id' => $enterprise_id,
            'description' => 'OPEN_OCCURRENCE',
            'date' => new \DateTime(),
            'active' => true
        ]);
        $this->Extracts->save($extract);
    }

    /**
     * Chamado cancelado
     * @param $occurrence_id
     * @param $contract_id
     * @param $val
     */
    public function canceledOccurrence($occurrence_id, $contract_id, $enterprise_id, $val)
    {
        $extract = $this->Extracts->newEntity();
        $extract = $this->Extracts->patchEntity($extract, [
            'occurrence_id' => $occurrence_id,
            'contract_id' => $contract_id,
            'income' => $val,
            'balance' => $val + $this->_balance($contract_id),
            'description' => 'CANCELED_OCCURRENCE',
            'date' => new \DateTime(),
            'active' => true,
            'enterprise_id' => $enterprise_id
        ]);
        $this->Extracts->save($extract);
    }


    /**
     * Chamado cancelado
     * @param $occurrence_id
     * @param $contract_id
     * @param $val
     */
    public function invalidOccurrence($occurrence_id, $contract_id, $enterprise_id, $val)
    {
        $extract = $this->Extracts->newEntity();
        $extract = $this->Extracts->patchEntity($extract, [
            'occurrence_id' => $occurrence_id,
            'contract_id' => $contract_id,
            'income' => $val,
            'balance' => $val + $this->_balance($contract_id),
            'description' => 'INVALID_OCCURRENCE',
            'date' => new \DateTime(),
            'active' => true,
            'enterprise_id' => $enterprise_id
        ]);
        $this->Extracts->save($extract);
    }

    public function finishOccurrence($occurrence_id, $contract_id, $enterprise_id, $val)
    {
        $extract = $this->Extracts->newEntity();
        $extract = $this->Extracts->patchEntity($extract, [
            'occurrence_id' => $occurrence_id,
            'contract_id' => $contract_id,
            'enterprise_id' => $enterprise_id,
            'expense' => $val,
            'description' => 'FINISHED_OCCURRENCE',
            'date' => new \DateTime(),
            'active' => true,
            'balance' => $this->_balance($contract_id)
        ]);
        $this->Extracts->save($extract);
    }


    public function finishContract($contract_id, $enterprise_id)
    {

        $extract = $this->Extracts->newEntity();
        $extract = $this->Extracts->patchEntity($extract, [
            'contract_id' => $contract_id,
            'description' => 'FINISH_CONTRACT',
            'date' => new \DateTime(),
            'balance' => 0,
            'active' => true,
            'enterprise_id' => $enterprise_id,
        ]);

        $this->Extracts->save($extract);
    }

    public function refillContract($contract_id, $enterprise_id, $val, $balance)
    {

        $extract = $this->Extracts->newEntity();
        $extract = $this->Extracts->patchEntity($extract, [
            'contract_id' => $contract_id,
            'description' => 'REFILL_CONTRACT',
            'date' => new \DateTime(),
            'income' => $val,
            'balance' => (int)$balance,
            'active' => true,
            'enterprise_id' => $enterprise_id,

        ]);

        $this->Extracts->save($extract);
    }

}