<?php
/**
 * Created by PhpStorm.
 * User: Haendel
 * Date: 18/08/2016
 * Time: 15:46
 */

namespace Flow\Controller\Component;


use App\Model\Table\ContractsTable;
use Cake\Controller\Component;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;

/**
 * Class CheckAvailableComponent
 * @package Flow\Controller\Component
 * @property ContractsTable $Contracts
 * @property Component\AuthComponent Auth
 */
class CheckAvailableComponent extends Component
{
    private $Contracts;
    private $ServicesContracts;
    private $ContractActive;
    private $Addresses;

    private $WeekDays = [
        'sunday',
        'monday',
        'tuesday',
        'wednesday',
        'thursday',
        'friday',
        'saturday',
    ];
    private $erros = [];
    private $DateTimeAvailable = false;


    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->Contracts = TableRegistry::get('Contracts');
        $this->Addresses = TableRegistry::get('Addresses');
        $this->ServicesContracts = TableRegistry::get('ServicesContracts');
        $this->components = ['Auth'];
    }

    /**
     * @return array
     */
    public function getErros()
    {
        return $this->erros;
    }

    /**
     * @param array $erros
     */
    public function setErros($erros)
    {
        $this->erros[] = $erros;
    }

    /**
     * @return boolean
     */
    public function getDateTimeAvailable()
    {
        return $this->DateTimeAvailable;
    }

    /**
     * @param boolean $DateTimeAvailable
     */
    public function setDateTimeAvailable($DateTimeAvailable)
    {
        $this->DateTimeAvailable = $DateTimeAvailable;
    }

    public function dateTime(\DateTime $date, $type = null)
    {
        //Verificar se é possivel abrir o chamado para o horario
        $schedule_time = $date;
        $date_now = new \DateTime('now');

        //debug($schedule_time);
        //debug($date_now);

        //Diferença de tempo
        $diff = $date_now->diff($schedule_time);
        $days = $diff->d;
        $hours = $diff->h;
        $months = $diff->m;
        $years = $diff->y;

        if($type == 'CORPORATE'){
            $weekday = $schedule_time->format('w');
            if ($this->ContractActive->{$this->WeekDays[$weekday]}) {
                if ($hours >= 3 || $days >= 1 || $months >= 1) {
                    if ($months <= 3 && $years == 0) {
                        if (($date->format('m') == $date_now->format('m')) && ($date->format('d') < $date_now->format('d'))) {
                            $this->DateTimeAvailable = false;
                            $this->erros[] = 'Horário inválido! Agende com mais de 3 horas de diferença!';
                        } else {
                            $this->DateTimeAvailable = true;
                        }
                    } else {
                        $this->DateTimeAvailable = false;
                        $this->erros[] = 'Não agendamos com mais de 3 meses!';
                    }
                } else {
                    $this->DateTimeAvailable = false;
                    $this->erros[] = 'Fora do horario de atendimento previsto em contrato!';
                }
            } else {
                $this->DateTimeAvailable = false;
                $this->erros[] = 'Dia da semana não disponivel em contrato!';
            }
        } else {
            if ($hours >= 3 || $days >= 1 || $months >= 1) {
                if ($months <= 3 && $years == 0) {
                    if (($date->format('m') == $date_now->format('m')) && ($date->format('d') < $date_now->format('d'))) {
                        $this->DateTimeAvailable = false;
                        $this->erros[] = 'Horário inválido! Agende com mais de 3 horas de diferença!';
                    } else {
                        $this->DateTimeAvailable = true;
                    }
                } else {
                    $this->DateTimeAvailable = false;
                    $this->erros[] = 'Não agendamos com mais de 3 meses!';
                }
            } else {
                $this->DateTimeAvailable = false;
                $this->erros[] = 'Fora do horario de atendimento!';
            }
        }
        return $this;
    }

    public function contract($contract_id)
    {
        $this->ContractActive = $this->Contracts
            ->find('all')
            ->contain(['Services'])
            ->where(['id' => $contract_id])
            ->first();
        return $this;
    }
}