<?php
/**
 * Created by PhpStorm.
 * User: Haendel
 * Date: 26/09/2016
 * Time: 15:22
 */

namespace Flow\Controller\Component;


use Cake\Controller\Component;
use Cake\Routing\Router;

/**
 * Usamos o pagarme como plataforma de compra
 * Class PaymentB2bComponent
 * @package App\Controller\Component
 */
class PaymentB2bComponent extends Component
{

    public function initialize(array $config)
    {
        parent::initialize($config);
        \PagarMe::$api_key = "ak_test_ZbQaLvUHmzeFRiRJDsvyMAgp5OwJv5";
    }


    /**
     * Criando plano para o contrato especifico
     * @param $price
     * @param $charges
     * @param $name
     * @param int $days
     * @return array
     * @throws \PagarMe_Exception
     */
    public function createPlan($price, $name, $charges = null, $days = 30)
    {
        $plan = new \PagarMe_Plan([
            'amount' => $price * 100,
            'days' => $days,
            'carges' => $charges,
            'name' => $name
        ]);

        return $plan->create();
    }

    public function createSubscription($plan, $name, $email, $contract)
    {
        $sub = new \PagarMe_Subscription([
            'plan' => $plan,
            'payment_method' => "boleto",
            'customer' => [
                'email' => $email,
                'name' => $name
            ],
            'postback_url' => Router::url('/', true) . 'contracts/alterStatus/' . $contract->id,
        ]);
        return $sub->create();
    }

    public function newContract($contract, $enterprise)
    {
        $plan = $this->createPlan($contract->fund->contract_period_value, $contract->name);
        $this->createSubscription($plan, $enterprise->fantasy_name, $enterprise->email, $contract);
    }


}