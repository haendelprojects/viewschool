<?php
/**
 * Created by PhpStorm.
 * User: Haendel
 * Date: 18/08/2016
 * Time: 15:42
 */

namespace Flow\Controller\Component;


use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

class PointsComponent extends Component
{
    private $FundsMonths;
    private $Funds;
    private $Cities;
    private $Occurrences;
    private $Coupons;
    private $Contracts;
    private $ServicesContracts;

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->FundsMonths = TableRegistry::get('FundsMonths');
        $this->Funds = TableRegistry::get('Funds');
        $this->Cities = TableRegistry::get('Cities');
        $this->Occurrences = TableRegistry::get('Occurrences');
        $this->Coupons = TableRegistry::get('Coupons');
        $this->Contracts = TableRegistry::get('Contracts');
        $this->ServicesContracts = TableRegistry::get('ServicesContracts');
    }

    private function _getReturn($success, $message)
    {
        return [
            'success' => $success,
            'message' => $message
        ];
    }


    /**
     * Calcular valores da balance de contrato recorrente
     * @param $contract
     * @return mixed
     */
    public function calculateBalanceRecurrent($contract)
    {
        $funds_months = $contract->fund->funds_months;
        $limit = ceil($contract->fund->points_days_validity / 30);
        $count = count($funds_months);
        $balance = [];

        for ($i = 0; $i < $count; $i++) {
            $balance[$i] = $funds_months[$i]->spare_points - $funds_months[$i]->stored_points_expense;
            $funds_months[$i]->spare_points = 0;
        }

        for ($i = 0; $i < $count; $i++) {
            $funds_months[$i]->spare_points = $balance[$i];

            for ($j = 1; $j < $limit; $j++) {
                if (($i - $j) >= 0) {
                    $funds_months[$i]->spare_points += $balance[$i - $j];
                }
            }
        }
        return $funds_months;
    }

    public function getPoints($service_id = null, $enterprise_id = null, $contract_id = null, $state_id = null, $city_id = null)
    {
        $conditions1 = [
            'Contracts.id' => $contract_id,
            'enterprise_id' => $enterprise_id,
        ];

        $contract = $this->Contracts
            ->find('all')
            ->where($conditions1)
            ->contain(['Funds'])
            ->first();


        if ($contract) {
            $conditions = [
                'active' => true,
                'service_id' => $service_id,
                'contract_id' => $contract->id
            ];

            $servicesContract = $this->ServicesContracts
                ->find('all')
                ->where($conditions)
                ->first();

            if ($servicesContract) {

                $specialist_price = $this->getSpecialistPrice($state_id, $city_id);

                if ($specialist_price) {

                    return [
                        'service_contract_id' => $servicesContract->id,
                        'occurrence_base_price' => $servicesContract->occurrence_base_points,
                        'occurrence_base_hours' => $servicesContract->occurrence_base_hours,
                        'occurrence_extra_price' => $servicesContract->occurrence_extra_points,
                        'occurrence_extra_hours' => $servicesContract->occurrence_extra_hours,
                        'specialist_base_price' => $specialist_price['specialist_base_price'],
                        'specialist_total_price' => $specialist_price['specialist_base_price'],
                        'specialist_extra_price' => $specialist_price['specialist_extra_price'],
                        'occurrence_total_price' => $servicesContract->occurrence_base_points,
                        'unique_point_value' => $contract->fund->unique_point_value,
                        'currency' => 'POINTS'
                    ];
                }
            }
        }
        return false;
    }

    public function getSpecialistPrice($state_id, $city_id)
    {
        if ((is_numeric($state_id)) && (is_numeric($city_id))) {
            $city = $this->Cities
                ->find('all')
                ->where([
                    'States.id' => $state_id,
                    'Cities.id' => $city_id
                ])
                ->contain([
                    'States', 'Regions'
                ])
                ->first();
        } else if ((is_numeric($state_id)) && !(is_numeric($city_id))) {
            $city = $this->Cities
                ->find('all')
                ->where([
                    'States.id' => $state_id,
                    'Cities.name' => $city_id
                ])
                ->contain([
                    'States', 'Regions'
                ])
                ->first();
        } else if (!(is_numeric($state_id)) && (is_numeric($city_id))) {
            $city = $this->Cities
                ->find('all')
                ->where([
                    'States.abbrv' => $state_id,
                    'Cities.id' => $city_id
                ])
                ->contain([
                    'States', 'Regions'
                ])
                ->first();
        } else {
            $city = $this->Cities
                ->find('all')
                ->where([
                    'States.abbrv' => $state_id,
                    'Cities.name' => $city_id
                ])
                ->contain([
                    'States', 'Regions'
                ])
                ->first();
        }


        if ($city) {
            if ($city->region) {
                return [
                    'specialist_base_price' => $city->region->specialist_base_price,
                    'specialist_extra_price' => $city->region->specialist_extra_price,
                ];
            }
        }

        return false;
    }


    /**
     * Buscar lista de contratos
     * @param $enterprise_id
     * @return mixed
     */
    public function listContracts($enterprise_id)
    {
        $contracts = $this->Contracts
            ->find('all')
            ->where([
                'enterprise_id' => $enterprise_id,
                'active' => true
            ]);

        return $contracts;
    }


    /**
     * Buscar informações do contrato primario
     */
    public function getPrimaryContract($enterprise_id)
    {
        if ($this->existPrimaryContractValid($enterprise_id)) {
            return $this->Contracts
                ->find('all')
                ->where([
                    'enterprise_id' => $enterprise_id,
                    'type' => 'primary',
                    'active' => true
                ])
                ->first();
        }

        return false;
    }

    /**
     * Buscar informações de um contrato especifico secundario
     * @param $id
     */
    public function getSecondaryContract($id)
    {
        $contract = $this->Contracts
            ->find('all')
            ->where([
                'Contracts.id' => $id,
                'type' => 'secondary',
                'active' => true
            ])
            ->first();

        return ($contract);
    }

    /**
     * Buscar lista de contratos secundarios
     */
    public function listSecondaryContracts($enterprise_id)
    {
        $contracts = $this->Contracts
            ->find('all')
            ->where([
                'enterprise_id' => $enterprise_id,
                'type' => 'secondary',
                'active' => true
            ]);

        return $contracts;
    }

    /**
     * Verificar se existe um contrato primario valido
     * @param $enterprise_id
     * @return bool
     */
    public function existPrimaryContractValid($enterprise_id)
    {
        $contract = $this->Contracts
            ->find('all')
            ->where([
                'enterprise_id' => $enterprise_id,
                'type' => 'primary',
                'active' => true
            ])
            ->first();

        return ($contract) ? true : false;
    }

    /**
     * Opções para o input de tipos de contrato
     * @param $enterprise_id
     * @return array
     */
    public function optionsTypesContract($enterprise_id)
    {
        if ($this->existPrimaryContractValid($enterprise_id)) {
            return ['secondary' => 'Secundário'];
        } else {
            return ['primary' => 'Primário', 'secondary' => 'Secundário'];
        }
    }

    /**
     * Retirar pontos ao abrir chamado
     * @param $occurrence
     * @return bool
     */
    public function withdrawCreateOccurrencePoints($occurrence)
    {
        $contract = $this->Contracts
            ->find('all')
            ->contain(['Funds', 'ServicesContracts', 'Funds.FundsMonths'])
            ->where([
                'Contracts.id' => $occurrence->contract_id
            ])
            ->first();

        return $this->_saveDepositPoints($contract);
    }

    /**
     * Retirar pontos ao finalizar o chamado
     * @param $occurrence
     * @return bool
     */
    public function withdrawFinishOccurrencePoints($occurrence)
    {
        $contract = $this->Contracts
            ->find('all')
            ->contain(['Funds', 'Funds.FundsMonths'])
            ->where([
                'Contracts.id' => $occurrence->contract_id,
            ])
            ->first();

        if ($contract) {
            return $this->_saveDepositPoints($contract);
        }
    }

    /**
     * Depositar os pontos retirados, caso o chamado seja cancelado
     * @param $occurrence
     * @return bool
     */
    public function depositCanceledOccurrencePoints($occurrence)
    {
        $contract = $this->Contracts
            ->find('all')
            ->contain(['Funds', 'Funds.FundsMonths'])
            ->where([
                'Contracts.id' => $occurrence->contract_id,
            ])
            ->first();

        return $this->_saveDepositPoints($contract);
    }

    /**
     * Se há pontos disponivies para uso no contrato
     * @param $contract
     * @return bool
     */
    public function availableBalanceContract($contract)
    {
        if ($contract->fund->total_points_quantity < $contract->fund->credit_balance) {
            return true;
        } else {
            return false;
        }
    }

    public function _saveDepositPoints($contract)
    {
        if (!($contract->fund->funds_months)) {

            $this->create_funds_months($contract);

            $contract = $this->Contracts
                ->find('all')
                ->where([
                    'Contracts.id' => $contract->id
                ])
                ->contain(['Funds', 'Funds.FundsMonths'])
                ->first();
        }
        $this->insert_queue($contract);
        $this->credit_balance($contract);
    }

    public function create_funds_months($contract)
    {
        $validity = $contract->fund->points_days_validity;
        if ($validity == null || $validity == 0) {
            $validity = 30;
        }
        $date = $contract->initial_date;
        $initial_date = clone $date;
        $final_date = clone $date->modify('+1 months');
        $final_date = clone $final_date->modify('-1 days');
        $stored_points_validity = clone $date;
        $stored_points_validity = $stored_points_validity->modify('+ ' . $validity . ' days');

        if ($contract->fund->funds_months ==  null) {
            if ($contract->fund->type == 'RECURRENT') {
                while ($initial_date < $contract->final_date) {

                    $data = [
                        'stored_points' => 0,
                        'fund_id' => $contract->fund->id,
                        'initial_date' => $initial_date,
                        'final_date' => $final_date,
                        'stored_points_validity' => $stored_points_validity,
                        'stored_points_expense' => 0,
                        'total_points_quantity' => $contract->fund->month_points,// + $extra_points,
                        'total_expense' => 0,
                        'spare_points' => $contract->fund->month_points,// + $extra_points,
                        'owed_points' => 0,
                        'active' => true,
                        'depleted' => false
                    ];

                    if ($validity == 0 || !$validity) {
                        $validity = 30;
                    }

                    $date = clone $final_date;
                    $initial_date = clone $date;
                    $initial_date = clone $initial_date->modify('+1 days');
                    $final_date = clone $date->modify('+1 months');
                    $stored_points_validity = clone $initial_date;
                    $stored_points_validity = $stored_points_validity->modify('+ ' . $validity . ' days');

                    $fund_month = $this->FundsMonths->newEntity();

                    $fund_month = $this->FundsMonths->patchEntity($fund_month, $data);

                    $this->FundsMonths->save($fund_month);

                    //$extra_points = 0;
                }
            } else {
                while ($initial_date < $contract->final_date) {

                    $data = [
                        'stored_points' => 0,
                        'fund_id' => $contract->fund->id,
                        'initial_date' => $initial_date,
                        'final_date' => $final_date,
                        'stored_points_expense' => 0,
                        'total_points_quantity' => $contract->fund->total_points_quantity,// + $extra_points,
                        'total_expense' => 0,
                        'spare_points' => $contract->fund->total_points_quantity,// + $extra_points,
                        'owed_points' => 0,
                        'active' => true,
                        'depleted' => false
                    ];

                    $date = clone $final_date;
                    $initial_date = clone $date;
                    $initial_date = clone $initial_date->modify('+1 days');
                    $final_date = clone $date->modify('+1 months');

                    $fund_month = $this->FundsMonths->newEntity();

                    $fund_month = $this->FundsMonths->patchEntity($fund_month, $data);

                    $this->FundsMonths->save($fund_month);
                }
            }
        }
        return true;
    }


    public function insert_queue($contract)
    {
        if ($contract->fund->type == 'RECURRENT') {
            $this->recurrent_queue($contract);
        } else {
            $this->detached_queue($contract);
        }
        $this->credit_balance($contract);
    }

    public function _expired($validity)
    {
        $now = Time::now();

        if ($now > $validity) {
            return 1;
        } else {
            return 0;
        }
    }

    public function credit_balance($contract)
    {
        $time = Time::now();
        $month_points = 0;

        $occurrences = $this->Occurrences->find();
        $expense = $occurrences
            ->find('all')
            ->where([
                'status IN' => [
                    'OPENED', 'ACCEPTED', 'GOING', 'ON_SERVICE', 'AWAITING_PAYMENT', 'PAYED_SPECIALIST', 'FINISHED', 'UNPRODUCTIVE'
                ],
                'Occurrences.contract_id' => $contract->id,
                'Occurrences.active' => true
            ])
            ->contain(['Prices'])
            ->select(['total' => $occurrences->func()->sum('Prices.occurrence_total_price')])
            ->first();

        $credit_balance = $contract->fund->total_points_quantity - $expense->total;

        $fund_month = $this->Funds->FundsMonths
            ->find('all')
            ->where([
                'depleted' => false,
                'fund_id' => $contract->fund->id
            ])
            ->first();

        if($fund_month) {
            if ($fund_month->initial_date <= $time) {
                if ($fund_month) {
                    if ($time <= $fund_month->final_date) {
                        $month_points = $fund_month->spare_points;
                    } else {
                        $month_points = $fund_month->stored_points - $fund_month->stored_points_expense;
                    }
                }
            } else {
                $month_points = 0;
            }
        }

        if($credit_balance <= 0) {

            $data = [
                'active' => false,
                'total_expense' => $expense->total,
                'credit_balance' => $credit_balance,
                'month_points' => $month_points
            ];

            $d = [
                'active' => 0
            ];

            $contract = $this->Contracts->patchEntity($contract, $d);
            $this->Contracts->save($contract);

        } else {

            $data = [
                'total_expense' => $expense->total,
                'credit_balance' => $credit_balance,
                'month_points' => $month_points
            ];
        }

        $fund = $this->Funds->patchEntity($contract->fund, $data);

        if (!$this->Funds->save($fund)) {
            debug($fund->errors());
        }

        return true;
    }

    public function recurrent_queue($contract)
    {
        $funds_months = $this->Funds->FundsMonths
            ->find('all')
            ->where([
                'fund_id' => $contract->fund->id,
                'active' => true
            ]);

        $stored_points_expense = 0;
        $owed_points = 0;
        $initial_date = clone $contract->initial_date->modify('-1 day');

        foreach ($funds_months as $fund_month) {
            if ($initial_date <= $fund_month->final_date) {
                if($initial_date <= $contract->initial_date) {
                    $occurrences = $this->Occurrences->find();
                    $occurrences = $occurrences
                        ->find('all')
                        ->where([
                            'status IN' => [
                                'OPENED', 'ACCEPTED', 'GOING', 'ON_SERVICE', 'AWAITING_PAYMENT', 'PAYED_SPECIALIST', 'FINISHED', 'UNPRODUCTIVE'
                            ],
                            'Occurrences.active' => true,
                            'Occurrences.contract_id' => $contract->id,
                            'Occurrences.created <=' => $fund_month->final_date
                        ])
                        ->contain(['Prices'])
                        ->select(['total' => $occurrences->func()->sum('Prices.occurrence_total_price')])
                        ->first();

                } else if ($initial_date >= $fund_month->initial_date && ($fund_month->final_date < $contract->final_date) && $initial_date > $contract->initial_date) {
                    $occurrences = $this->Occurrences->find();
                    $occurrences = $occurrences
                        ->find('all')
                        ->where([
                            'status IN' => [
                                'OPENED', 'ACCEPTED', 'GOING', 'ON_SERVICE', 'AWAITING_PAYMENT', 'PAYED_SPECIALIST', 'FINISHED', 'UNPRODUCTIVE'
                            ],
                            'Occurrences.active' => true,
                            'Occurrences.contract_id' => $contract->id,
                            'Occurrences.created >' => $initial_date,
                            'Occurrences.created <=' => $fund_month->final_date
                        ])
                        ->contain(['Prices'])
                        ->select(['total' => $occurrences->func()->sum('Prices.occurrence_total_price')])
                        ->first();

                } else if ($fund_month->final_date >= $contract->final_date) {
                    $occurrences = $this->Occurrences->find();
                    $occurrences = $occurrences
                        ->find('all')
                        ->where([
                            'status IN' => [
                                'OPENED', 'ACCEPTED', 'GOING', 'ON_SERVICE', 'AWAITING_PAYMENT', 'PAYED_SPECIALIST', 'FINISHED', 'UNPRODUCTIVE'
                            ],
                            'Occurrences.active' => true,
                            'Occurrences.contract_id' => $contract->id,
                            'Occurrences.created >' => $initial_date
                        ])
                        ->contain(['Prices'])
                        ->select(['total' => $occurrences->func()->sum('Prices.occurrence_total_price')])
                        ->first();

                } else {
                    $occurrences = $this->Occurrences->find();
                    $occurrences = $occurrences
                        ->find('all')
                        ->where([
                            'status IN' => [
                                'OPENED', 'ACCEPTED', 'GOING', 'ON_SERVICE', 'AWAITING_PAYMENT', 'PAYED_SPECIALIST', 'FINISHED', 'UNPRODUCTIVE'
                            ],
                            'Occurrences.active' => true,
                            'Occurrences.contract_id' => $contract->id,
                            'Occurrences.created >' => $initial_date,
                            'Occurrences.created <=' => $fund_month->final_date
                        ])
                        ->contain(['Prices'])
                        ->select(['total' => $occurrences->func()->sum('Prices.occurrence_total_price')])
                        ->first();
                }

                $total_expense = $occurrences->total + $owed_points;
                $spare_points = ($fund_month->total_points_quantity + $fund_month->additive_points) - $total_expense;
                if ($spare_points <= 0) {
                    $depleted = true;
                    $owed_points = abs($spare_points);
                    $spare_points = 0;
                    $stored_points = 0;
                    $total_expense = $fund_month->total_points_quantity + $fund_month->additive_points;
                    $stored_points_expense = 0;
                    $initial_date = clone $fund_month->final_date;
                } else {
                    $stored_points = $spare_points;
                    $depleted = false;

                    $occurrences = $this->Occurrences->find();
                    $occurrences = $occurrences
                        ->find('all')
                        ->where([
                            'status IN' => [
                                'OPENED', 'ACCEPTED', 'GOING', 'ON_SERVICE', 'AWAITING_PAYMENT', 'PAYED_SPECIALIST', 'FINISHED', 'UNPRODUCTIVE'
                            ],
                            'Occurrences.active' => true,
                            'Occurrences.contract_id' => $contract->id,
                            'Occurrences.created >' => $fund_month->final_date,
                            'Occurrences.created <=' => $fund_month->stored_points_validity
                        ])
                        ->contain(['Prices'])
                        ->select(['total' => $occurrences->func()->sum('Prices.occurrence_total_price')])
                        ->first();

                    $stored_points_expense = $occurrences->total;

                    $occurrences = $this->Occurrences->find();
                    $occurrence = $occurrences
                        ->find('all')
                        ->where([
                            'status IN' => [
                                'OPENED', 'ACCEPTED', 'GOING', 'ON_SERVICE', 'AWAITING_PAYMENT', 'PAYED_SPECIALIST', 'FINISHED', 'UNPRODUCTIVE'
                            ],
                            'Occurrences.active' => true,
                            'Occurrences.contract_id' => $contract->id,
                            'Occurrences.created >' => $fund_month->final_date,
                            'Occurrences.created <=' => $fund_month->stored_points_validity
                        ])
                        ->contain(['Prices'])
                        ->select(['id', 'created'])
                        ->last();

                    if($occurrence) {
                        $initial_date = $occurrence->created;
                    } else {
                        $initial_date = clone $fund_month->stored_points_validity;
                    }

                    if ($spare_points - $stored_points_expense <= 0) {
                        $owed_points = abs($spare_points - $stored_points_expense);
                        $stored_points_expense = $spare_points;
                        $depleted = true;
                    } else {
                        $owed_points = 0;
                        $depleted =  false;
                    }
                }
            } else {
                $total_expense = $owed_points;
                $spare_points = ($fund_month->total_points_quantity + $fund_month->additive_points) - $total_expense;
                if ($spare_points <= 0) {
                    $depleted = true;
                    $owed_points = abs($spare_points);
                    $spare_points = 0;
                    $stored_points = 0;
                    $total_expense = $fund_month->total_points_quantity + $fund_month->additive_points;
                    $stored_points_expense = 0;
                } else {
                    $stored_points = $spare_points;
                    $depleted = false;

                    $occurrences = $this->Occurrences->find();
                    $occurrences = $occurrences
                        ->find('all')
                        ->where([
                            'status IN' => [
                                'OPENED', 'ACCEPTED', 'GOING', 'ON_SERVICE', 'AWAITING_PAYMENT', 'PAYED_SPECIALIST', 'FINISHED', 'UNPRODUCTIVE'
                            ],
                            'Occurrences.active' => true,
                            'Occurrences.contract_id' => $contract->id,
                            'Occurrences.created >' => $initial_date,
                            'Occurrences.created <=' => $fund_month->stored_points_validity
                        ])
                        ->contain(['Prices'])
                        ->select(['total' => $occurrences->func()->sum('Prices.occurrence_total_price')])
                        ->first();

                    $stored_points_expense = $occurrences->total;

                    $occurrences = $this->Occurrences->find();
                    $occurrence = $occurrences
                        ->find('all')
                        ->where([
                            'status IN' => [
                                'OPENED', 'ACCEPTED', 'GOING', 'ON_SERVICE', 'AWAITING_PAYMENT', 'PAYED_SPECIALIST', 'FINISHED', 'UNPRODUCTIVE'
                            ],
                            'Occurrences.active' => true,
                            'Occurrences.contract_id' => $contract->id,
                            'Occurrences.created >' => $initial_date,
                            'Occurrences.created <=' => $fund_month->stored_points_validity
                        ])
                        ->contain(['Prices'])
                        ->select(['id', 'created'])
                        ->last();

                    if($occurrence) {
                        $initial_date = $occurrence->created;
                    } else {
                        $initial_date = clone $fund_month->stored_points_validity;
                    }

                    if ($spare_points - $stored_points_expense <= 0) {
                        $owed_points = abs($spare_points - $stored_points_expense);
                        $stored_points_expense = $spare_points;
                        $depleted = true;
                    } else {
                        $owed_points = 0;
                        $depleted = false;
                    }
                }
            }
            if ($depleted == 0) {
                $depleted = $this->_expired($fund_month->stored_points_validity);
            }
            $data = [
                'total_expense' => (int)$total_expense,
                'stored_points' => (int)$stored_points,
                'spare_points' => (int)$spare_points,
                'owed_points' => (int)$owed_points,
                'stored_points_expense' => (int)$stored_points_expense,
                'depleted' => $depleted
            ];

            $fund_month = $this->Funds->FundsMonths
                ->patchEntity($fund_month, $data);

            if (!$this->Funds->FundsMonths->save($fund_month)) {
                debug($fund_month->errors());
            }
            $stored_points_expense = 0;
        }

        return true;
    }

    public function detached_queue($contract)
    {
        $funds_months = $this->Funds->FundsMonths
            ->find('all')
            ->where([
                'fund_id' => $contract->fund->id,
                'active' => true
            ]);

        $depleted = false;
        $owed_points = 0;
        $stored_points = 0;

        $total_points_quantity = $contract->fund->total_points_quantity;

        foreach ($funds_months as $fund_month) {

            $initial_date = $fund_month->initial_date->modify('-1 days');
            $final_date = $fund_month->final_date->modify('+1 days');

            if($initial_date > $contract->initial_date) {
                $occurrences = $this->Occurrences->find();
                $occurrences = $occurrences
                    ->find('all')
                    ->where([
                        'status IN' => [
                            'OPENED', 'ACCEPTED', 'GOING', 'ON_SERVICE', 'AWAITING_PAYMENT', 'PAYED_SPECIALIST', 'FINISHED', 'UNPRODUCTIVE'
                        ],
                        'Occurrences.active' => true,
                        'Occurrences.contract_id' => $contract->id,
                        'Occurrences.created >=' => $fund_month->initial_date,
                        'Occurrences.created <' => $final_date
                    ])
                    ->contain(['Prices'])
                    ->select(['total' => $occurrences->func()->sum('Prices.occurrence_total_price')])
                    ->first();
            } else {
                $occurrences = $this->Occurrences->find();
                $occurrences = $occurrences
                    ->find('all')
                    ->where([
                        'status IN' => [
                            'OPENED', 'ACCEPTED', 'GOING', 'ON_SERVICE', 'AWAITING_PAYMENT', 'PAYED_SPECIALIST', 'FINISHED', 'UNPRODUCTIVE'
                        ],
                        'Occurrences.active' => true,
                        'Occurrences.contract_id' => $contract->id,
                        'Occurrences.created <' => $final_date
                    ])
                    ->contain(['Prices'])
                    ->select(['total' => $occurrences->func()->sum('Prices.occurrence_total_price')])
                    ->first();
            }

            $total_expense = $occurrences->total;

            $spare_points = $total_points_quantity + $fund_month->additive_points - $total_expense;

            if ($spare_points < 0) {
                $owed_points = abs($spare_points);
                $depleted = true;
            }

            if ($depleted == 0) {
                $depleted = $this->_expired($contract->final_date);
            }

            $data = [
                'total_points_quantity' => (int)$total_points_quantity,
                'owed_points' => (int)$owed_points,
                'spare_points' => (int)$spare_points,
                'stored_points' => 0,
                'total_expense' => (int)$total_expense,
                'depleted' => $depleted
            ];

            $fund_month = $this->Funds->FundsMonths
                ->patchEntity($fund_month, $data);

            $this->Funds->FundsMonths->save($fund_month);

            $total_points_quantity = $spare_points;
        }
    }

}