# AppNotifications plugin for CakePHP

## Installation

You can install this plugin into your CakePHP application using [composer](http://getcomposer.org).

The recommended way to install composer packages is:

```
require": {
        "findup/cakephp-notifications" : "*"
    },
     "repositories": [
    {
      "type": "package",
      "package": {
        "name": "findup/cakephp-notifications",
        "version": "0.1.0",
        "type": "cakephp-plugin",
        "source": {
          "url": "git@gitlab.findup.com.br:Libs/cakephp-notifications.git",
          "type": "git",
          "reference": "master"
        }
      }
    }
  ]


  composer require findup-cakephp-notifications
```