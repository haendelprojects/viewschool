<?php
namespace App\Model\Table;

use App\Model\Entity\AppNotification;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AppNotifications Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Models
 * @property \Cake\ORM\Association\BelongsTo $Users
 */
class AppNotificationsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('app_notifications');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Models', [
            'foreignKey' => 'model_id'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('type');

        $validator
            ->add('sent', 'valid', ['rule' => 'boolean'])
            ->allowEmpty('sent');

        $validator
            ->allowEmpty('model');

        $validator
            ->add('updated_at', 'valid', ['rule' => 'datetime'])
            ->allowEmpty('updated_at');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['model_id'], 'Models'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        return $rules;
    }
}
