<?php
/**
 * Created by PhpStorm.
 * User: erick
 * Date: 04/09/2017
 * Time: 11:06
 */

namespace Rest\Controller;


use App\Controller\AppController;
use App\Model\Table\ContractsTable;

/**
 * Class ContractsController
 * @package Rest\Controller
 *
 * @property ContractsTable $Contracts
 */
class ContractsController extends AppController
{
    public function initialize()
    {
        parent::initialize();


        $this->loadComponent('RequestHandler');

        if ($this->Auth->user()) {
            $this->Auth->allow(['corporate']);
        }

        $this->loadModel('Contracts');
    }

    public function corporate($id, $active = true)
    {
        $contracts = $this->Contracts->find('all')->where(['enterprise_id' => $id])->where(['active' => true])->select(['name', 'id', 'enterprise_id']);
        $this->set([
            'data' => $contracts,
            '_serialize' => ['data']
        ]);
    }


}