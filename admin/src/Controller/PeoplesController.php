<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Peoples Controller
 *
 * @property \App\Model\Table\PeoplesTable $Peoples
 *
 * @method \App\Model\Entity\People[] paginate($object = null, array $settings = [])
 */
class PeoplesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Addresses', 'Users', 'Branchs']
        ];
        $peoples = $this->paginate($this->Peoples->find('all')
            ->where(['Peoples.school_id' => $this->Auth->user('school_id')]));

        $this->set(compact('peoples'));
        $this->set('_serialize', ['peoples']);
    }

    /**
     * View method
     *
     * @param string|null $id People id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $people = $this->Peoples->get($id, [
            'contain' => ['Addresses', 'Users', 'Branchs', 'Class', 'ClassDiscipline', 'Comments', 'Frequencies', 'Registrations'],
            'conditions' => [
                'Peoples.school_id' => $this->Auth->user('school_id')
            ]
        ]);

        $this->set('people', $people);
        $this->set('_serialize', ['people']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($id)
    {
        $people = $this->Peoples->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['school_id'] = $id;
            $this->request->data['user'] = [
                'username' => $this->request->data['email'],
                'password' => $this->request->getData('user.password'),
                'access_school' => true,
                'school_id' => $id
            ];

            $people = $this->Peoples->patchEntity($people, $this->request->data, ['asssociated' => ['Addresses', 'Users']]);
            if ($this->Peoples->save($people)) {
                $this->Flash->success(__('Sucesso'));
                $this->redirect(['controller' => 'schools', 'action' => 'view', $id]);
            } else {
                $this->Flash->error(__('Falha, tente novamente'));
            }
        }
        $this->set(compact('people', 'addresses', 'users', 'branchs'));
        $this->set('_serialize', ['people']);
    }

    /**
     * Edit method
     *
     * @param string|null $id People id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $people = $this->Peoples->get($id, [
            'contain' => [],
            'school_id' => $this->Auth->user('school_id')
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $people = $this->Peoples->patchEntity($people, $this->request->data);
            if ($this->Peoples->save($people)) {
                $this->Flash->success(__('Sucesso'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Falha, tente novamente'));
            }
        }
        $addresses = $this->Peoples->Addresses->find('list', ['limit' => 200])->where(['school_id' => $this->Auth->user('school_id')]);
        $users = $this->Peoples->Users->find('list', ['limit' => 200])->where(['school_id' => $this->Auth->user('school_id')]);
        $branchs = $this->Peoples->Branchs->find('list', ['limit' => 200])->where(['school_id' => $this->Auth->user('school_id')]);
        $this->set(compact('people', 'addresses', 'users', 'branchs'));
        $this->set('_serialize', ['people']);
    }


    public function responsible($id = null)
    {
        if ($id) {
            $people = $this->Peoples->newEntity();
            if ($this->request->is('post')) {
                $this->request->data['school_id'] = $this->Auth->user('school_id');
                $people = $this->Peoples->patchEntity($people, $this->request->data);
                if ($this->Peoples->save($people)) {
                    $this->Flash->success(__('Salvo com sucesso.'));
                } else {
                    $this->Flash->error(__('Falha, tente novamente'));
                }
            }
        }
        $this->redirect($this->referer());
    }


    /**
     * Delete method
     *
     * @param string|null $id People id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $people = $this->Peoples->get($id, [
            'school_id' => $this->Auth->user('school_id')
        ]);
        if ($this->Peoples->delete($people)) {
            $this->Flash->success(__('Sucesso'));
        } else {
            $this->Flash->error(__('Falha, tente novamente'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
