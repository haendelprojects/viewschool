<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Frequencies Controller
 *
 * @property \App\Model\Table\FrequenciesTable $Frequencies
 *
 * @method \App\Model\Entity\Frequency[] paginate($object = null, array $settings = [])
 */
class FrequenciesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Peoples', 'ClassDiscipline']
        ];
        $frequencies = $this->paginate($this->Frequencies->find('all')
            ->where(['Frequencies.school_id' => $this->Auth->user('school_id')]));

        $this->set(compact('frequencies'));
        $this->set('_serialize', ['frequencies']);
    }

    /**
     * View method
     *
     * @param string|null $id Frequency id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $frequency = $this->Frequencies->get($id, [
            'contain' => ['Peoples', 'ClassDiscipline'],
            'conditions' => [
                'Frequencies.school_id' => $this->Auth->user('school_id')
            ]
        ]);

        $this->set('frequency', $frequency);
        $this->set('_serialize', ['frequency']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $frequency = $this->Frequencies->newEntity();
        if ($this->request->is('post')) {
            $frequency = $this->Frequencies->patchEntity($frequency, $this->request->data);
            if ($this->Frequencies->save($frequency)) {
                $this->Flash->success(__('The {0} has been saved.', 'Frequency'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Frequency'));
            }
        }
        $peoples = $this->Frequencies->Peoples->find('list', ['limit' => 200]);
        $classDiscipline = $this->Frequencies->ClassDiscipline->find('list', ['limit' => 200]);
        $this->set(compact('frequency', 'peoples', 'classDiscipline'));
        $this->set('_serialize', ['frequency']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Frequency id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $frequency = $this->Frequencies->get($id, [
            'contain' => [],
            'conditions' => [
                'school_id' => $this->Auth->user('school_id')
            ]
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $frequency = $this->Frequencies->patchEntity($frequency, $this->request->data);
            if ($this->Frequencies->save($frequency)) {
                $this->Flash->success(__('Sucesso'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Falha, tente novamente'));
            }
        }
        $peoples = $this->Frequencies->Peoples->find('list', ['limit' => 200])->where(['school_id' => $this->Auth->user('school_id')]);
        $classDiscipline = $this->Frequencies->ClassDiscipline->find('list', ['limit' => 200])->where(['school_id' => $this->Auth->user('school_id')]);
        $this->set(compact('frequency', 'peoples', 'classDiscipline'));
        $this->set('_serialize', ['frequency']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Frequency id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $frequency = $this->Frequencies->get($id, [
            'school_id' => $this->Auth->user('school_id')
        ]);
        if ($this->Frequencies->delete($frequency)) {
            $this->Flash->success(__('Sucesso'));
        } else {
            $this->Flash->error(__('Falha, tente novamente'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
