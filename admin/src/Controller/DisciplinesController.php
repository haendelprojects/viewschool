<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Disciplines Controller
 *
 * @property \App\Model\Table\DisciplinesTable $Disciplines
 *
 * @method \App\Model\Entity\Discipline[] paginate($object = null, array $settings = [])
 */
class DisciplinesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => []
        ];
        $disciplines = $this->paginate($this->Disciplines->find('all')
            ->where(['Disciplines.school_id' => $this->Auth->user('school_id')]));

        $this->set(compact('disciplines'));
        $this->set('_serialize', ['disciplines']);
    }

    /**
     * View method
     *
     * @param string|null $id Discipline id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $discipline = $this->Disciplines->get($id, [
            'contain' => ['ClassDiscipline'],
            'conditions' => [
                'Disciplines.school_id' => $this->Auth->user('school_id')
            ]
        ]);

        $this->set('discipline', $discipline);
        $this->set('_serialize', ['discipline']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $discipline = $this->Disciplines->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['branch_id'] = $this->Auth->user('branch_id');
            $this->request->data['school_id'] = $this->Auth->user('school_id');

            $discipline = $this->Disciplines->patchEntity($discipline, $this->request->data);
            if ($this->Disciplines->save($discipline)) {
                $this->Flash->success(__('Salvo com sucesso'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Falha, tente novamente'));
            }
        }
        $this->set(compact('discipline', 'branchs'));
        $this->set('_serialize', ['discipline']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Discipline id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $discipline = $this->Disciplines->get($id, [
            'contain' => [],
            'school_id' => $this->Auth->user('school_id')
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $discipline = $this->Disciplines->patchEntity($discipline, $this->request->data);
            if ($this->Disciplines->save($discipline)) {
                $this->Flash->success(__('Salvo com sucesso'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Falha, tente novamente'));
            }
        }
        $this->set(compact('discipline', 'branchs'));
        $this->set('_serialize', ['discipline']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Discipline id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $discipline = $this->Disciplines->get($id, [
            'conditions' => ['school_id' => $this->Auth->user('school_id')]
        ]);
        if ($this->Disciplines->delete($discipline)) {
            $this->Flash->success(__('Sucesso.'));
        } else {
            $this->Flash->error(__('Falha'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
