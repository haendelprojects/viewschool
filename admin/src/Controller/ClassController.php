<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Class Controller
 *
 * @property \App\Model\Table\ClassTable $Class
 *
 * @method \App\Model\Entity\Clas[] paginate($object = null, array $settings = [])
 */
class ClassController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Courses', 'Organizations', 'Evaluations', 'Peoples']
        ];
        $class = $this->paginate($this->Class->find('all')->where(['Class.school_id' => $this->Auth->user('school_id')]));

        $this->set(compact('class'));
        $this->set('_serialize', ['class']);
    }

    /**
     * View method
     *
     * @param string|null $id Clas id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $clas = $this->Class->get($id, [
            'contain' => ['Courses', 'Registrations.Peoples', 'Evaluations',
                'Peoples', 'ClassDiscipline.Disciplines', 'ClassDiscipline.Peoples', 'ClassDiscipline.Times'],
            'conditions' => [
                'Class.school_id' => $this->Auth->user('school_id')
            ]
        ]);

        $this->set('clas', $clas);
        $this->set('_serialize', ['clas']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $clas = $this->Class->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['branch_id'] = $this->Auth->user('branch_id');
            $this->request->data['school_id'] = $this->Auth->user('school_id');

            $clas = $this->Class->patchEntity($clas, $this->request->data);
            if ($this->Class->save($clas)) {
                $this->Flash->success(__('Turma salva com sucesso'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Falha, tente novamente'));
            }
        }
        $courses = $this->Class->Courses->find('list', ['limit' => 200])->where(['conditions' => [
            'school_id' => $this->Auth->user('school_id')
        ]]);
        $organizations = $this->Class->Organizations->find('list', ['limit' => 200])->where(['conditions' => [
            'school_id' => $this->Auth->user('school_id')
        ]]);
        $evaluations = $this->Class->Evaluations->find('list', ['limit' => 200])->where(['conditions' => [
            'school_id' => $this->Auth->user('school_id')
        ]]);
        $peoples = $this->Class->Peoples->find('list', ['limit' => 200])->where(['type' => 'PROFESSOR', 'school_id' => $this->Auth->user('school_id')]);
        $this->set(compact('clas', 'courses', 'branchs', 'organizations', 'evaluations', 'peoples'));
        $this->set('_serialize', ['clas']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Clas id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $clas = $this->Class->get($id, [
            'contain' => [],
            'conditions' => [
                'Class.school_id' => $this->Auth->user('school_id')
            ]
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $clas = $this->Class->patchEntity($clas, $this->request->data);
            if ($this->Class->save($clas)) {
                $this->Flash->success(__('The {0} has been saved.', 'Clas'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Clas'));
            }
        }
        $courses = $this->Class->Courses->find('list', ['limit' => 200])->where(['conditions' => [
            'school_id' => $this->Auth->user('school_id')
        ]]);
        $organizations = $this->Class->Organizations->find('list', ['limit' => 200])->where(['conditions' => [
            'school_id' => $this->Auth->user('school_id')
        ]]);
        $evaluations = $this->Class->Evaluations->find('list', ['limit' => 200])->where(['conditions' => [
            'school_id' => $this->Auth->user('school_id')
        ]]);
        $peoples = $this->Class->Peoples->find('list', ['limit' => 200])->where(['conditions' => [
            'school_id' => $this->Auth->user('school_id')
        ]]);
        $this->set(compact('clas', 'courses', 'branchs', 'organizations', 'evaluations', 'peoples'));
        $this->set('_serialize', ['clas']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Clas id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $clas = $this->Class->get($id, [
            'conditions' => [
                'Class.school_id' => $this->Auth->user('school_id')
            ]
        ]);
        if ($this->Class->delete($clas)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Clas'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Clas'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
