<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;

/**
 * Calendars Controller
 *
 * @property \App\Model\Table\CalendarsTable $Calendars
 *
 * @method \App\Model\Entity\Calendar[] paginate($object = null, array $settings = [])
 */
class CalendarsController extends AppController
{

    public function initialize()
    {
        parent::initialize(); // TODO: Change the autogenerated stub

        $this->loadComponent('Search.Prg', [
            'actions' => ['index']
        ]);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Class', 'Courses', 'Branchs']
        ];
        $calendars = $this->paginate($this->Calendars
            ->find('search', ['search' => $this->request->getQuery()])
            ->where([
                'Calendars.school_id' => $this->Auth->user('school_id')
            ]));

        $calendar = $this->Calendars->newEntity();

        $this->set(compact('calendar', 'calendars', 'users', 'class', 'courses', 'branchs'));
        $this->set('isSearch', $this->Calendars->isSearch());
        $this->set('_serialize', ['calendars']);
    }

    public function calendar()
    {
        $calendars = $this->Calendars->find('all')->where([
            'school_id' => $this->Auth->user('school_id')
        ]);
        $calendar = $this->Calendars->newEntity();
//        $users = $this->Calendars->Users->find('list', ['limit' => 200]);
//        $class = $this->Calendars->Class->find('list', ['limit' => 200]);
//        $courses = $this->Calendars->Courses->find('list', ['limit' => 200]);
        $this->set(compact('calendar', 'calendars', 'users', 'class', 'courses', 'branchs'));
        $this->set('_serialize', ['calendars']);
    }

    /**
     * View method
     *
     * @param string|null $id Calendar id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $calendar = $this->Calendars->get($id, [
            'contain' => ['Users', 'Class', 'Courses', 'Branchs'],
            'conditions' => [
                'Calendars.school_id' => $this->Auth->user('school_id')
            ]
        ]);

        $this->set('calendar', $calendar);
        $this->set('_serialize', ['calendar']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $calendar = $this->Calendars->newEntity();
        if ($this->request->is('post')) {

            $this->request->data['branch_id'] = $this->Auth->user('branch_id');
            $this->request->data['school_id'] = $this->Auth->user('school_id');
            $this->request->data['user_id'] = $this->Auth->user('id');
            $this->request->data['start'] = \DateTime::createFromFormat('d/m/Y H:i', $this->request->data['start_dp'] . ' 01:00');
            $this->request->data['end'] = \DateTime::createFromFormat('d/m/Y H:i', $this->request->data['end_dp'] . ' 23:59');


            $calendar = $this->Calendars->patchEntity($calendar, $this->request->data);
            if ($this->Calendars->save($calendar)) {
                $this->Flash->success(__('Salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Falha, tente novamente.'));
            }
        }
        $users = $this->Calendars->Users->find('list', ['limit' => 200])->where(['conditions' => $this->Auth->user('school_id')]);
        $class = $this->Calendars->Class->find('list', ['limit' => 200])->where(['conditions' => $this->Auth->user('school_id')]);
        $courses = $this->Calendars->Courses->find('list', ['limit' => 200])->where(['conditions' => $this->Auth->user('school_id')]);
        $this->set(compact('calendar', 'users', 'class', 'courses', 'branchs'));
        $this->set('_serialize', ['calendar']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Calendar id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $calendar = $this->Calendars->get($id, [
            'contain' => [],
            'conditions' => [
                'school_id' => $this->Auth->user('school_id')
            ]
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $calendar = $this->Calendars->patchEntity($calendar, $this->request->data);
            if ($this->Calendars->save($calendar)) {
                $this->Flash->success(__('Salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Falha, tente novamente.'));
            }
        }
        $users = $this->Calendars->Users->find('list', ['limit' => 200])->where(['school_id' => $this->Auth->user('school_id')]);
        $class = $this->Calendars->Class->find('list', ['limit' => 200])->where(['school_id' => $this->Auth->user('school_id')]);
        $courses = $this->Calendars->Courses->find('list', ['limit' => 200])->where(['school_id' => $this->Auth->user('school_id')]);
        $branchs = $this->Calendars->Branchs->find('list', ['limit' => 200])->where(['school_id' => $this->Auth->user('school_id')]);
        $this->set(compact('calendar', 'users', 'class', 'courses', 'branchs'));
        $this->set('_serialize', ['calendar']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Calendar id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $calendar = $this->Calendars->get($id, [
            'conditions' => [
                'school_id' => $this->Auth->user('school_id')
            ]
        ]);
        if ($this->Calendars->delete($calendar)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Calendar'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Calendar'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
