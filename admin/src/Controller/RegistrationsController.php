<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Registrations Controller
 *
 * @property \App\Model\Table\RegistrationsTable $Registrations
 *
 * @method \App\Model\Entity\Registration[] paginate($object = null, array $settings = [])
 */
class RegistrationsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Peoples', 'Class', 'Branchs']
        ];
        $registrations = $this->paginate($this->Registrations->find('all')->where(['Registrations.school_id' => $this->Auth->user('school_id')]));

        $this->set(compact('registrations'));
        $this->set('_serialize', ['registrations']);
    }

    public function my()
    {
        $registration = $this->Registrations->find('all')
            ->contain(['Peoples', 'Class', 'Class.Lessons.Frequencies' => function ($q) {
                return $q->where(['people_id' => $this->Auth->user('people.id')]);
            }, 'Class.Courses', 'Class.Registrations.Peoples', 'Class.Evaluations',
                'Class.Peoples', 'Class.ClassDiscipline.Disciplines', 'Class.ClassDiscipline.Peoples', 'Class.ClassDiscipline.Times'])
            ->where([
                'Registrations.people_id' => $this->Auth->user('people.id'),
                'Registrations.school_id' => $this->Auth->user('school_id')
            ])
            ->first();

        if (!$registration) {
            $this->redirect(['controller' => 'dashboard']);
        }

        $clas = $registration->clas;
        $this->set(compact('registration', 'clas'));
    }


    /**
     * View method
     *
     * @param string|null $id Registration id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $registration = $this->Registrations->get($id, [
            'contain' => ['Peoples', 'Class', 'Branchs'],
            'conditions' => [
                'Registrations.school_id' => $this->Auth->user('school_id')
            ]
        ]);

        $this->set('registration', $registration);
        $this->set('_serialize', ['registration']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $registration = $this->Registrations->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['school_id'] = $this->Auth->user('school_id');
            $registration = $this->Registrations->patchEntity($registration, $this->request->data);
            if ($this->Registrations->save($registration)) {
                $this->Flash->success(__('Sucesso'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Falha, tente novamente'));
            }
        }
        $peoples = $this->Registrations->Peoples->find('list', ['limit' => 200])->where(['school_id' => $this->Auth->user('school_id')]);
        $class = $this->Registrations->Class->find('list', ['limit' => 200])->where(['school_id' => $this->Auth->user('school_id')]);
        $branchs = $this->Registrations->Branchs->find('list', ['limit' => 200])->where(['school_id' => $this->Auth->user('school_id')]);
        $this->set(compact('registration', 'peoples', 'class', 'branchs'));
        $this->set('_serialize', ['registration']);
    }

    public function addByModal()
    {
        $registration = $this->Registrations->newEntity();
        if ($this->request->is('post')) {

            $this->request->data['branchs_id'] = $this->Auth->user('branch_id');
            $this->request->data['school_id'] = $this->Auth->user('school_id');

            $registration = $this->Registrations->patchEntity($registration, $this->request->data);
            if ($this->Registrations->save($registration)) {
                $this->Flash->success(__('Matrícula Efetuada com sucesso.'));
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('Falha, tente novamente.'));
            }
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Registration id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $registration = $this->Registrations->get($id, [
            'contain' => [],
            'conditions' => [
                'school_id' => $this->Auth->user('school_id')
            ]
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $registration = $this->Registrations->patchEntity($registration, $this->request->data);
            if ($this->Registrations->save($registration)) {
                $this->Flash->success(__('Sucesso'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Falha, tente novamente'));
            }
        }
        $peoples = $this->Registrations->Peoples->find('list', ['limit' => 200])->where(['school_id' => $this->Auth->user('school_id')]);
        $class = $this->Registrations->Class->find('list', ['limit' => 200])->where(['school_id' => $this->Auth->user('school_id')]);
        $branchs = $this->Registrations->Branchs->find('list', ['limit' => 200])->where(['school_id' => $this->Auth->user('school_id')]);
        $this->set(compact('registration', 'peoples', 'class', 'branchs'));
        $this->set('_serialize', ['registration']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Registration id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $registration = $this->Registrations->get($id, [
            'conditions' => [
                'school_id' => $this->Auth->user('school_id')
            ]
        ]);
        if ($this->Registrations->delete($registration)) {
            $this->Flash->success(__('Sucesso'));
        } else {
            $this->Flash->error(__('Falha, tente novamente'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function getStudentsClass($class, $exam = false)
    {
        $registrations = $this->Registrations->find('all')
            ->where(['class_id' => $class, 'Registrations.school_id' => $this->Auth->user('school_id')])->contain(['Peoples']);
        $this->set(compact('registrations', 'exam'));
        $this->viewBuilder()->layout('ajax');
    }
}
