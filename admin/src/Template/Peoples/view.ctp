<section class="content-header">
  <h1>
    <?php echo __('People'); ?>
  </h1>
  <ol class="breadcrumb">
    <li>
    <?= $this->Html->link('<i class="fa fa-dashboard"></i> ' . __('Back'), ['action' => 'index'], ['escape' => false])?>
    </li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-md-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <i class="fa fa-info"></i>
                <h3 class="box-title"><?php echo __('Information'); ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <dl class="dl-horizontal">
                                                                                                                <dt><?= __('Name') ?></dt>
                                        <dd>
                                            <?= h($people->name) ?>
                                        </dd>
                                                                                                                                                            <dt><?= __('Cpf') ?></dt>
                                        <dd>
                                            <?= h($people->cpf) ?>
                                        </dd>
                                                                                                                                                            <dt><?= __('Phone') ?></dt>
                                        <dd>
                                            <?= h($people->phone) ?>
                                        </dd>
                                                                                                                                                            <dt><?= __('Cell Phone') ?></dt>
                                        <dd>
                                            <?= h($people->cell_phone) ?>
                                        </dd>
                                                                                                                                                            <dt><?= __('Type') ?></dt>
                                        <dd>
                                            <?= h($people->type) ?>
                                        </dd>
                                                                                                                                                            <dt><?= __('Sexo') ?></dt>
                                        <dd>
                                            <?= h($people->sexo) ?>
                                        </dd>
                                                                                                                                                            <dt><?= __('Foto') ?></dt>
                                        <dd>
                                            <?= h($people->foto) ?>
                                        </dd>
                                                                                                                                                            <dt><?= __('Father Name') ?></dt>
                                        <dd>
                                            <?= h($people->father_name) ?>
                                        </dd>
                                                                                                                                                            <dt><?= __('Mother Name') ?></dt>
                                        <dd>
                                            <?= h($people->mother_name) ?>
                                        </dd>
                                                                                                                                                            <dt><?= __('Email') ?></dt>
                                        <dd>
                                            <?= h($people->email) ?>
                                        </dd>
                                                                                                                                                    <dt><?= __('Address') ?></dt>
                                <dd>
                                    <?= $people->has('address') ? $people->address->id : '' ?>
                                </dd>
                                                                                                                <dt><?= __('User') ?></dt>
                                <dd>
                                    <?= $people->has('user') ? $people->user->id : '' ?>
                                </dd>
                                                                                                                <dt><?= __('Branch') ?></dt>
                                <dd>
                                    <?= $people->has('branch') ? $people->branch->name : '' ?>
                                </dd>
                                                                                                
                                            
                                                                                                                                            
                                                                                                                                                                                                                <dt><?= __('Birth') ?></dt>
                                <dd>
                                    <?= h($people->birth) ?>
                                </dd>
                                                                                                    
                                            
                                    </dl>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- ./col -->
</div>
<!-- div -->

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <i class="fa fa-share-alt"></i>
                    <h3 class="box-title"><?= __('Related {0}', ['Class']) ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">

                <?php if (!empty($people->class)): ?>

                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                                                    
                                    <th>
                                    Id
                                    </th>
                                        
                                                                                                                                            
                                    <th>
                                    Course Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Branch Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Organization Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Init Date
                                    </th>
                                        
                                                                    
                                    <th>
                                    End Date
                                    </th>
                                        
                                                                    
                                    <th>
                                    Period
                                    </th>
                                        
                                                                    
                                    <th>
                                    Name
                                    </th>
                                        
                                                                    
                                    <th>
                                    Wallet Name
                                    </th>
                                        
                                                                    
                                    <th>
                                    Status
                                    </th>
                                        
                                                                    
                                    <th>
                                    Class Time
                                    </th>
                                        
                                                                    
                                    <th>
                                    School Year
                                    </th>
                                        
                                                                    
                                    <th>
                                    Evaluation Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    People Id
                                    </th>
                                        
                                                                    
                                <th>
                                    <?php echo __('Actions'); ?>
                                </th>
                            </tr>

                            <?php foreach ($people->class as $class): ?>
                                <tr>
                                                                        
                                    <td>
                                    <?= h($class->id) ?>
                                    </td>
                                                                                                                                                
                                    <td>
                                    <?= h($class->course_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($class->branch_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($class->organization_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($class->init_date) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($class->end_date) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($class->period) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($class->name) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($class->wallet_name) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($class->status) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($class->class_time) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($class->school_year) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($class->evaluation_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($class->people_id) ?>
                                    </td>
                                    
                                                                        <td class="actions">
                                    <?= $this->Html->link(__('View'), ['controller' => 'Class', 'action' => 'view', $class->id], ['class'=>'btn btn-info btn-xs']) ?>

                                    <?= $this->Html->link(__('Edit'), ['controller' => 'Class', 'action' => 'edit', $class->id], ['class'=>'btn btn-warning btn-xs']) ?>

                                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Class', 'action' => 'delete', $class->id], ['confirm' => __('Are you sure you want to delete # {0}?', $class->id), 'class'=>'btn btn-danger btn-xs']) ?>    
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                                    
                        </tbody>
                    </table>

                <?php endif; ?>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <i class="fa fa-share-alt"></i>
                    <h3 class="box-title"><?= __('Related {0}', ['Class Discipline']) ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">

                <?php if (!empty($people->class_discipline)): ?>

                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                                                    
                                    <th>
                                    Id
                                    </th>
                                        
                                                                                                                                            
                                    <th>
                                    Class Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Dicipline Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    People Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Branch Id
                                    </th>
                                        
                                                                    
                                <th>
                                    <?php echo __('Actions'); ?>
                                </th>
                            </tr>

                            <?php foreach ($people->class_discipline as $classDiscipline): ?>
                                <tr>
                                                                        
                                    <td>
                                    <?= h($classDiscipline->id) ?>
                                    </td>
                                                                                                                                                
                                    <td>
                                    <?= h($classDiscipline->class_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($classDiscipline->dicipline_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($classDiscipline->people_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($classDiscipline->branch_id) ?>
                                    </td>
                                    
                                                                        <td class="actions">
                                    <?= $this->Html->link(__('View'), ['controller' => 'ClassDiscipline', 'action' => 'view', $classDiscipline->id], ['class'=>'btn btn-info btn-xs']) ?>

                                    <?= $this->Html->link(__('Edit'), ['controller' => 'ClassDiscipline', 'action' => 'edit', $classDiscipline->id], ['class'=>'btn btn-warning btn-xs']) ?>

                                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ClassDiscipline', 'action' => 'delete', $classDiscipline->id], ['confirm' => __('Are you sure you want to delete # {0}?', $classDiscipline->id), 'class'=>'btn btn-danger btn-xs']) ?>    
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                                    
                        </tbody>
                    </table>

                <?php endif; ?>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <i class="fa fa-share-alt"></i>
                    <h3 class="box-title"><?= __('Related {0}', ['Comments']) ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">

                <?php if (!empty($people->comments)): ?>

                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                                                    
                                    <th>
                                    Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Text
                                    </th>
                                        
                                                                    
                                    <th>
                                    Title
                                    </th>
                                        
                                                                                                                                            
                                    <th>
                                    User Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    People Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Class Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Branch Id
                                    </th>
                                        
                                                                    
                                <th>
                                    <?php echo __('Actions'); ?>
                                </th>
                            </tr>

                            <?php foreach ($people->comments as $comments): ?>
                                <tr>
                                                                        
                                    <td>
                                    <?= h($comments->id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($comments->text) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($comments->title) ?>
                                    </td>
                                                                                                                                                
                                    <td>
                                    <?= h($comments->user_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($comments->people_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($comments->class_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($comments->branch_id) ?>
                                    </td>
                                    
                                                                        <td class="actions">
                                    <?= $this->Html->link(__('View'), ['controller' => 'Comments', 'action' => 'view', $comments->id], ['class'=>'btn btn-info btn-xs']) ?>

                                    <?= $this->Html->link(__('Edit'), ['controller' => 'Comments', 'action' => 'edit', $comments->id], ['class'=>'btn btn-warning btn-xs']) ?>

                                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Comments', 'action' => 'delete', $comments->id], ['confirm' => __('Are you sure you want to delete # {0}?', $comments->id), 'class'=>'btn btn-danger btn-xs']) ?>    
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                                    
                        </tbody>
                    </table>

                <?php endif; ?>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <i class="fa fa-share-alt"></i>
                    <h3 class="box-title"><?= __('Related {0}', ['Frequencies']) ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">

                <?php if (!empty($people->frequencies)): ?>

                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                                                    
                                    <th>
                                    Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Status
                                    </th>
                                        
                                                                    
                                    <th>
                                    People Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Class Discipline Id
                                    </th>
                                        
                                                                                                                                            
                                <th>
                                    <?php echo __('Actions'); ?>
                                </th>
                            </tr>

                            <?php foreach ($people->frequencies as $frequencies): ?>
                                <tr>
                                                                        
                                    <td>
                                    <?= h($frequencies->id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($frequencies->status) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($frequencies->people_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($frequencies->class_discipline_id) ?>
                                    </td>
                                                                                                            
                                                                        <td class="actions">
                                    <?= $this->Html->link(__('View'), ['controller' => 'Frequencies', 'action' => 'view', $frequencies->id], ['class'=>'btn btn-info btn-xs']) ?>

                                    <?= $this->Html->link(__('Edit'), ['controller' => 'Frequencies', 'action' => 'edit', $frequencies->id], ['class'=>'btn btn-warning btn-xs']) ?>

                                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Frequencies', 'action' => 'delete', $frequencies->id], ['confirm' => __('Are you sure you want to delete # {0}?', $frequencies->id), 'class'=>'btn btn-danger btn-xs']) ?>    
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                                    
                        </tbody>
                    </table>

                <?php endif; ?>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <i class="fa fa-share-alt"></i>
                    <h3 class="box-title"><?= __('Related {0}', ['Registrations']) ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">

                <?php if (!empty($people->registrations)): ?>

                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                                                    
                                    <th>
                                    Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Year
                                    </th>
                                        
                                                                    
                                    <th>
                                    Value Material
                                    </th>
                                        
                                                                    
                                    <th>
                                    Value Monthly
                                    </th>
                                        
                                                                    
                                    <th>
                                    Value Registration
                                    </th>
                                        
                                                                                                                                            
                                    <th>
                                    Discount Type
                                    </th>
                                        
                                                                    
                                    <th>
                                    Discount Value
                                    </th>
                                        
                                                                    
                                    <th>
                                    Observation
                                    </th>
                                        
                                                                    
                                    <th>
                                    People Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Class Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Branchs Id
                                    </th>
                                        
                                                                    
                                <th>
                                    <?php echo __('Actions'); ?>
                                </th>
                            </tr>

                            <?php foreach ($people->registrations as $registrations): ?>
                                <tr>
                                                                        
                                    <td>
                                    <?= h($registrations->id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($registrations->year) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($registrations->value_material) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($registrations->value_monthly) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($registrations->value_registration) ?>
                                    </td>
                                                                                                                                                
                                    <td>
                                    <?= h($registrations->discount_type) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($registrations->discount_value) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($registrations->observation) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($registrations->people_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($registrations->class_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($registrations->branchs_id) ?>
                                    </td>
                                    
                                                                        <td class="actions">
                                    <?= $this->Html->link(__('View'), ['controller' => 'Registrations', 'action' => 'view', $registrations->id], ['class'=>'btn btn-info btn-xs']) ?>

                                    <?= $this->Html->link(__('Edit'), ['controller' => 'Registrations', 'action' => 'edit', $registrations->id], ['class'=>'btn btn-warning btn-xs']) ?>

                                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Registrations', 'action' => 'delete', $registrations->id], ['confirm' => __('Are you sure you want to delete # {0}?', $registrations->id), 'class'=>'btn btn-danger btn-xs']) ?>    
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                                    
                        </tbody>
                    </table>

                <?php endif; ?>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
