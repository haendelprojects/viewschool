<div class="card">
    <div class="card-body ">
        <form method="POST" id="validate-form">
            <div class="text-center align-center">Por favor digite e confirme sua nova senha.</div>
            <hr>
            <div class="form-group has-feedback">
                <?= $this->Form->input('password', ['id' => 'password', 'label' => false, 'placeholder' => __('Senha')]); ?>
                <?= $this->Form->input('confirm_password', ['id' => 'password', 'label' => false, 'placeholder' => __('Confirmar Senha'), 'type' => 'password']); ?>
            </div>
            <div>
                <button class="btn btn-primary btn-block btn-center" type="submit"><?= __('Salvar') ?></button>
            </div>
            <hr>
            <div>
                <a href="<?php echo $this->Url->build(array('controller' => 'users', 'action' => 'login')); ?>"
                   class="btn btn-default btn-block btn-center" style="background: #D2D6DE">Voltar</a>
            </div>
            <!-- /.col -->

        </form>
    </div>
</div>
