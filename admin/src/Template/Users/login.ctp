<form accept-charset="utf-8" action="<?php echo $this->Url->build(array('controller' => 'users', 'action' => 'login')); ?>" method="post">
    <h4>Sistema Interno</h4>
    <div class="form-group has-feedback">
        <input type="email" class="form-control" placeholder="E-mail" name="username">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
    </div>
    <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" name="password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
    </div>
    <div class="row">
        <div class="col-xs-8">
            <div class="checkbox icheck">
                <label>
                    <input type="checkbox"> Lembrar-me
                </label>
            </div>

            <div>
                <a href="/users/passwordRecovery">Esqueceu a senha?</a>
            </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Entrar</button>
        </div>
        <!-- /.col -->
    </div>
</form>
