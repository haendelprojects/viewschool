<section class="content-header">
    <h1>
        <?php echo __('Avaliação'); ?>
    </h1>
    <ol class="breadcrumb">
        <li>
            <?= $this->Html->link('<i class="fa fa-dashboard"></i> ' . __('Voltar'), ['action' => 'index'], ['escape' => false]) ?>
        </li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <i class="fa fa-info"></i>
                    <h3 class="box-title"><?php echo __('Informações'); ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt><?= __('Disciplina') ?></dt>
                        <dd>
                            <?= $lesson->has('discipline') ? $lesson->discipline->name : '' ?>
                        </dd>
                        <dt><?= __('Classe') ?></dt>
                        <dd>
                            <?= $lesson->clas->course->name ?> <?= $lesson->clas->name ?>
                        </dd>

                        <dt><?= __('Data') ?></dt>
                        <dd>
                            <?= h($lesson->day) ?>
                        </dd>

                        <dt><?= __('Assunto') ?></dt>
                        <dd>
                            <?= h($lesson->title) ?>
                        </dd>

                        <dt><?= __('Descrição') ?></dt>
                        <dd>
                            <?= $this->Text->autoParagraph(h($lesson->description)); ?>
                        </dd>
                    </dl>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- ./col -->
    </div>
    <!-- div -->

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <i class="fa fa-share-alt"></i>
                    <h3 class="box-title"><?= __('Notas dos Alunos') ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">

                    <?php if (!empty($lesson->frequencies)): ?>

                    <table class="table table-hover">
                        <tbody>
                        <tr>
                            <th>Aluno</th>
                            <th>
                                Presença
                            </th>
                            <th>
                                Nota
                            </th>
                        </tr>

                        <?php foreach ($lesson->frequencies as $frequencies): ?>
                        <tr>
                            <td>
                                <?= h($frequencies->people->name) ?>
                            </td>
                            <td>
                                <?= h($frequencies->status) ?>
                            </td>

                            <td>
                                <?= number_format($frequencies->note, 2) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>

                        </tbody>
                    </table>

                    <?php endif; ?>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
