<section class="content-header">
    <h1>
        Avaliação
        <small><?= __('Adicionar') ?></small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <?= $this->Html->link('<i class="fa fa-dashboard"></i> ' . __('Voltar'), ['action' => 'index'], ['escape' => false]) ?>
        </li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><?= __('Informações') ?></h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <?= $this->Form->create($lesson, array('role' => 'form')) ?>
                <div class="box-body">

                    <?php
                    echo $this->Form->input('class_id', ['label' => 'Turma', 'id' => 'input-class', 'empty' => 'Definir Turma', 'required' => true]);
                    ?>

                    <div class="select-discipline">
                        <?= $this->Form->input('discipline_id', ['options' => [], 'label' => "Disciplina",'required' => true]); ?>
                    </div>

                    <?php
                    echo $this->Form->input('title', ['label' => 'Assunto da Prova','required' => true]);
                    echo $this->Form->input('description', ['label' => 'Descrição da Prova']);
                    echo $this->Form->input('day_base', ['empty' => true, 'label' => 'Dia da Prova', 'default' => '', 'class' => 'datepicker form-control', 'type' => 'text', 'required' => true]);
                    ?>

                    <div class="students">

                    </div>
                </div>


                <!-- /.box-body -->
                <div class="box-footer">
                    <?= $this->Form->button(__('Cadastrar'), ['class' => 'btn-success']) ?>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</section>

<?php
$this->Html->css([
    'AdminLTE./plugins/datepicker/datepicker3',
],
    ['block' => 'css']);

$this->Html->script([
    'AdminLTE./plugins/input-mask/jquery.inputmask',
    'AdminLTE./plugins/input-mask/jquery.inputmask.date.extensions',
    'AdminLTE./plugins/datepicker/bootstrap-datepicker',
    'AdminLTE./plugins/datepicker/locales/bootstrap-datepicker.pt-BR',
],
    ['block' => 'script']);
?>
<?php $this->start('scriptBottom'); ?>
<script>
    $(function () {
        //Datemask mm/dd/yyyy
        $(".datepicker")
            .inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"})
            .datepicker({
                language: 'pt',
                format: 'dd/mm/yyyy'
            });

        $('#input-class').on('change', function () {
            $.get('/classDiscipline/getDisciplines/' + $(this).val(), function (res) {
                $('.select-discipline').html(res);
            });

            $.get('/registrations/getStudentsClass/' + $(this).val() + '/1', function (res) {
                $('.students').html(res);
            })
        });
    });
</script>
<?php $this->end(); ?>
        