<table class="table-bordered table">
    <tr>
        <th>Aluno</th>
        <th>Presença</th>
        <?php if ($exam): ?>
            <th>Nota</th>
        <?php endif; ?>
    </tr>
    <?php foreach ($registrations as $registration): ?>
        <tr>
            <td><?= $registration->people->name ?></td>
            <td>
                <?= $this->Form->input('frequencies.' . $registration->id . '.people_id', ['value' => $registration->people->id, 'type' => 'hidden', 'label' => false]) ?>
                <?= $this->Form->input('frequencies.' . $registration->id . '.status', ['options' => ['Presente' => 'Presente', 'Faltou' => 'Faltou', 'Falta Justificada' => 'Falta Justificada'], 'label' => false, 'required' => true]) ?>
            </td>
            <?php if ($exam): ?>
                <td>
                    <?= $this->Form->input('frequencies.' . $registration->id . '.note', ['label' => false, 'required' => true]) ?>
                </td>
            <?php endif; ?>
        </tr>
    <?php endforeach ?>
</table>