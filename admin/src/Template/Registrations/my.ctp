<section class="content-header">
    <h1>
        <?php echo __('Turma'); ?>
    </h1>
    <ol class="breadcrumb">
        <li>
            <?= $this->Html->link('<i class="fa fa-arrow-left"></i> ' . __('Voltar'), ['action' => 'index'], ['escape' => false]) ?>
        </li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">


            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#activity" data-toggle="tab"><i class="fa fa-info"></i>Informações</a>
                    </li>
                    <li><a href="#disc" data-toggle="tab"><i class="fa fa-book"></i> Disciplinas</a></li>
                    <li><a href="#grid" data-toggle="tab"><i class="fa fa-calendar"></i> Grade Horário</a></li>
                    <li><a href="#frequence" data-toggle="tab"><i class="fa fa-list"></i> Frequência</a></li>
                    <li><a href="#note" data-toggle="tab"><i class="fa fa-sticky-note-o"></i> Notas</a></li>
                </ul>
                <div class="tab-content">
                    <div class=" active tab-pane" id="activity">
                        <?php if ($this->User->isSchool()): ?>
                            <?= $this->Html->link('<i class="fa fa-pencil"></i> ' . __('Editar'), ['action' => 'edit', $clas->id], ['escape' => false, 'class' => 'pull-right btn btn-warning btn-xs']) ?>
                        <?php endif; ?>

                        <dl class="dl-horizontal">
                            <dt><?= __('Curso') ?></dt>
                            <dd>
                                <td><?= $clas->course->name ?> <?= h($clas->name) ?></td>
                            </dd>

                            <dt><?= __('Periodo') ?></dt>
                            <dd>
                                <?= h($clas->period) ?>
                            </dd>

                            <dt><?= __('Status') ?></dt>
                            <dd>
                                <?= h($clas->status) ?>
                            </dd>

                            <dt><?= __('Professor Responsável') ?></dt>
                            <dd>
                                <td><?= $clas->people->name ?></td>
                            </dd>

                            <dt><?= __('Ano Letivo') ?></dt>
                            <dd>
                                <?= h($clas->school_year) ?>
                            </dd>
                        </dl>
                    </div>
                    <div class="tab-pane" id="disc">
                        <div class="">
                            <?php if ($this->User->isSchool()): ?>
                                <?= $this->element('Forms/addDisciplineModal', ['class_id' => $clas->id]); ?>
                            <?php endif; ?>
                            <br>
                            <br>

                            <table class="table table-striped  table-bordered">
                                <tr>
                                    <th>Disciplina</th>
                                    <th>Professor</th>
                                    <th>Aulas</th>
                                </tr>
                                <?php foreach ($clas->class_discipline as $discipline): ?>
                                    <tr>

                                        <td><?= $discipline->discipline->name ?></td>
                                        <td><?= $discipline->people->name ?></td>
                                        <td>
                                            <?php foreach ($discipline->times as $time) : ?>
                                                <?= $time->day_week ?> -  <?= $time->init_time ?>h às <?= $time->end_time ?>h
                                            <?php endforeach; ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane" id="grid">
                        <table class="table table-bordered">
                            <tr>
                                <th>Segunda</th>
                                <th>Terça</th>
                                <th>Quarta</th>
                                <th>Quinta</th>
                                <th>Sexta</th>
                                <th>Sábado</th>
                            </tr>

                            <tr>
                                <td>
                                    <?= $this->Util->dayDiscipline($clas->class_discipline, 'SEG'); ?>
                                </td>

                                <td>
                                    <?= $this->Util->dayDiscipline($clas->class_discipline, 'TER'); ?>
                                </td>

                                <td>
                                    <?= $this->Util->dayDiscipline($clas->class_discipline, 'QUA'); ?>
                                </td>

                                <td>
                                    <?= $this->Util->dayDiscipline($clas->class_discipline, 'QUI'); ?>
                                </td>

                                <td>
                                    <?= $this->Util->dayDiscipline($clas->class_discipline, 'SEX'); ?>
                                </td>

                                <td>
                                    <?= $this->Util->dayDiscipline($clas->class_discipline, 'SAB'); ?>
                                </td>
                            </tr>
                        </table>
                    </div>

                    <div class="tab-pane" id="frequence">
                        <div class="panel-group" id="accordion">
                            <?php foreach ($registration->clas->class_discipline as $discipline) : ?>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion"
                                               href="#collapse<?= $discipline->id ?>">
                                                <?= $discipline->discipline->name ?>

                                            </a>
                                        </h4>
                                        <?= $this->Util->countMissing($discipline->class_id, $discipline->discipline_id, $this->User->peopleId()) ?>
                                    </div>
                                    <div id="collapse<?= $discipline->id ?>" class="panel-collapse collapse out">
                                        <div class="panel-body">
                                            <?= $this->Util->tableFrequencie($discipline->class_id, $discipline->discipline_id, $this->User->peopleId()) ?>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="tab-pane" id="note">
                        <div class="panel-group" id="accordion">
                            <?php foreach ($registration->clas->class_discipline as $discipline) : ?>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion"
                                               href="#collapse-note<?= $discipline->id ?>">
                                                <?= $discipline->discipline->name ?>
                                            </a>
                                        </h4>
                                        <?= $this->Util->averageNow($discipline->class_id, $discipline->discipline_id, $this->User->peopleId()) ?>
                                    </div>
                                    <div id="collapse-note<?= $discipline->id ?>" class="panel-collapse collapse out">
                                        <div class="panel-body">
                                            <?= $this->Util->tableNotes($discipline->class_id, $discipline->discipline_id, $this->User->peopleId()) ?>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
            <!-- ./col -->
        </div>
        <!-- div -->

</section>
