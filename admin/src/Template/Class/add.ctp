<section class="content-header">
    <h1>
        Turma
        <small><?= __('Adicionar') ?></small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <?= $this->Html->link('<i class="fa fa-arrow-left"></i> ' . __('Voltar'), ['action' => 'index'], ['escape' => false]) ?>
        </li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><?= __('Informações') ?></h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <?= $this->Form->create($clas, array('role' => 'form', 'align' => [
                    'sm' => [
                        'left' => 6,
                        'middle' => 6,
                        'right' => 12
                    ],
                    'md' => [
                        'left' => 2,
                        'middle' => 10,
                    ]
                ])) ?>
                <div class="box-body">
                    <?php
                    echo $this->Form->input('course_id', ['options' => $courses, 'label' => 'Curso/Série']);
                    echo $this->Form->input('name', ['label' => 'Nome <small>Ex.: A</small>', 'escape' => false, 'placeholder' => 'Informe o nome da turma', 'required' => 'required']);
                    echo $this->Form->input('init_date', ['label' => 'Data de Inicio', 'empty' => true, 'default' => '', 'class' => 'datepicker form-control', 'type' => 'text', 'required' => 'required']);
                    echo $this->Form->input('end_date', ['label' => 'Data Final', 'empty' => true, 'default' => '', 'class' => 'datepicker form-control', 'type' => 'text', 'required' => 'required']);
                    echo $this->Form->input('period', ['label' => 'Periodo', 'options' => ['MANHA' => 'Manhã', 'TARDE' => 'Tarde', 'NOITE' => 'Noite', 'required' => 'required']]);

                    echo $this->Form->input('status', ['label' => 'Status', 'options' => ['ABERTO' => 'ABERTO', 'FECHADO' => 'FECHADO', 'required' => 'required']]);
                    echo $this->Form->input('class_time', ['label' => 'Carga horária total']);
                    echo $this->Form->input('school_year', ['label' => 'Ano Letivo', 'options' => $this->Util->years(), 'required' => 'required']);
                    echo $this->Form->input('evaluation_id', ['label' => 'Critério de Avaliação', 'options' => $evaluations, 'empty' => true, 'required' => 'required']);
                    echo $this->Form->input('people_id', ['label' => 'Professor Responsável', 'options' => $peoples, 'empty' => true, 'required' => 'required']);
                    ?>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <?= $this->Form->button(__('Cadastrar')) ?>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</section>

<?php
$this->Html->css([
    'AdminLTE./plugins/datepicker/datepicker3',
],
    ['block' => 'css']);

$this->Html->script([
    'AdminLTE./plugins/input-mask/jquery.inputmask',
    'AdminLTE./plugins/input-mask/jquery.inputmask.date.extensions',
    'AdminLTE./plugins/datepicker/bootstrap-datepicker',
    'AdminLTE./plugins/datepicker/locales/bootstrap-datepicker.pt-BR',
],
    ['block' => 'script']);
?>
<?php $this->start('scriptBottom'); ?>
<script>
    $(function () {
        //Datemask mm/dd/yyyy
        $(".datepicker")
            .inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"})
            .datepicker({
                language: 'en',
                format: 'dd/mm/yyyy'
            });
    });
</script>
<?php $this->end(); ?>