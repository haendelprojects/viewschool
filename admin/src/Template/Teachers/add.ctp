<section class="content-header">
    <h1>
        Professor
        <small><?= __('adicionar') ?></small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <?= $this->Html->link('<i class="fa fa-arrow-left"></i> ' . __('Voltar'), ['action' => 'index'], ['escape' => false]) ?>
        </li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <?= $this->Form->create($people, array('role' => 'form', 'align' => [
        'sm' => [
            'left' => 6,
            'middle' => 6,
            'right' => 12
        ],
        'md' => [
            'left' => 2,
            'middle' => 10,
        ]
    ])) ?>
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><?= __('Detalhes') ?></h3>
                </div>
                <div class="box-body">
                    <?php
                    echo $this->Form->input('name', ['label' => 'Nome']);
                    echo $this->Form->input('cpf', ['label' => 'CPF', 'class' => 'cpf']);
                    echo $this->Form->input('phone', ['label' => 'Telefone', 'class' => 'phone', 'maxlength' => 15]);
                    echo $this->Form->input('cell_phone', ['label' => 'Celular', 'class' => 'cellphone', 'maxlength' => 15]);
                    echo $this->Form->input('birth', ['label' => 'Nascimento', 'empty' => true, 'default' => '', 'class' => 'datepicker form-control', 'type' => 'text']);
                    echo $this->Form->input('type', ['value' => 'PROFESSOR', 'label' => 'Tipo', 'type' => 'hidden']);
                    echo $this->Form->input('sexo', ['label' => 'Sexo', 'options' => ['FEMININO' => 'Feminino', 'MASCULINO' => 'Masculino']]);
                    echo $this->Form->input('email', ['label' => 'E-mail']);
                    ?>
                </div>
            </div>

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><?= __('Endereço') ?></h3>
                </div>

                <div class="box-body">
                    <?php
                    echo $this->Form->input('address.zipcode', ['label' => 'CEP']);
                    echo $this->Form->input('address.street', ['label' => 'Endereço']);
                    echo $this->Form->input('address.number', ['label' => 'Número']);
                    echo $this->Form->input('address.neighborhood', ['label' => 'Bairro']);
                    echo $this->Form->input('address.city', ['label' => 'Cidade']);
                    echo $this->Form->input('address.state', ['label' => 'Estado']);
                    echo $this->Form->input('address.complement', ['label' => 'Complemento']);
                    ?>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <?= $this->Form->button(__('Cadastrar')) ?>
                </div>
            </div>
        </div>
    </div>

    <?= $this->Form->end() ?>
</section>

<?php
$this->Html->css(['AdminLTE./plugins/datepicker/datepicker3',],
    ['block' => 'css']);

$this->Html->script(['AdminLTE./plugins/input-mask/jquery.inputmask',
    'AdminLTE./plugins/input-mask/jquery.inputmask.date.extensions',
    'AdminLTE./plugins/datepicker/bootstrap-datepicker',
    'AdminLTE./plugins/datepicker/locales/bootstrap-datepicker.pt-BR',],
    ['block' => 'script']);
?>
<?php $this->start('scriptBottom'); ?>
<script>
    $(function () {
        //Datemask mm/dd/yyyy
        $(".datepicker")
            .inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"})
            .datepicker({
                language: 'pt_br',
                format: 'dd/mm/yyyy'
            });

        $(".cpf").inputmask("999.999.999-99", {"placeholder": "000.000.000-00"});

        $(".cellphone").inputmask("(99) 99999-9999", {"placeholder": "(00) 00000-0000"});
        $(".phone").inputmask("(99) 9999-9999", {"placeholder": "(00) 0000-0000"});
    });
</script>
<?php $this->end(); ?>
        