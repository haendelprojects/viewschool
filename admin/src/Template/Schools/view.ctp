<section class="content-header">
    <h1>
        <?php echo __('Escola'); ?>
    </h1>
    <ol class="breadcrumb">

    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <i class="fa fa-info"></i>
                    <h3 class="box-title"><?php echo __('Informação'); ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt><?= __('Escola') ?></dt>
                        <dd>
                            <?= h($school->name) ?>
                        </dd>
                        <dt><?= __('CNPJ') ?></dt>
                        <dd>
                            <?= h($school->cnpj) ?>
                        </dd>


                    </dl>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- ./col -->
    </div>
    <!-- div -->

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <i class="fa fa-share-alt"></i>
                    <h3 class="box-title"><?= __('Usuários Internos') ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <div>
                        <div class="pull-right"><?= $this->Html->link(__('Cadastrar usuário da escola'), ['controller' => 'peoples', 'action' => 'add', $school->id], ['class' => 'btn btn-success btn-xs']) ?></div>

                    </div>

                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th><?= $this->Paginator->sort('name', 'Nome') ?></th>
                            <th><?= $this->Paginator->sort('cpf', 'CPF') ?></th>
                            <th><?= $this->Paginator->sort('phone', 'Telefone') ?></th>
                            <th><?= $this->Paginator->sort('cell_phone', 'Celular') ?></th>
                            <th><?= __('Ações') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($peoples as $people): ?>
                            <tr>
                                <td><?= h($people->name) ?></td>
                                <td><?= h($people->cpf) ?></td>
                                <td><?= h($people->phone) ?></td>
                                <td><?= h($people->cell_phone) ?></td>
                                <td class="actions" style="white-space:nowrap">
                                    <?= $this->Html->link(__('Editar'), ['controller' => 'peoples', 'action' => 'edit', $people->id], ['class' => 'btn btn-warning btn-xs']) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
