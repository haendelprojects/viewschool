<?php if (!empty($data)): ?>
    <table id="table-user" class="table table-hover datatable">
        <thead>
        <tr>
            <th>Email</th>
            <th>Nome</th>
            <th>Ações</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data as $user): ?>
            <tr>
                <td><?= h($user->username) ?></td>
                <td><?= h($user->first_name) ?> <?= h($user->last_name) ?></td>
                <td class="actions" style="white-space:nowrap">
                    <?= $this->Html->link(__('Detalhes'), ['controller' => 'users', 'action' => 'view', $user->id], ['class' => 'btn btn-info btn-xs']) ?>
                    <?= $this->Html->link(__('Editar'), ['controller' => 'users', 'action' => 'edit', $user->id], ['class' => 'btn btn-warning btn-xs']) ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
<?php else: ?>
    <table id="table-user" class="table table-hover datatable">
        <thead>
        <tr>
            <th>Email</th>
            <th>Nome</th>
            <th>Ações</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Não há dados</td>
            <td></td>
            <td></td>
        </tr>
        </tbody>
    </table>
<?php endif; ?>
