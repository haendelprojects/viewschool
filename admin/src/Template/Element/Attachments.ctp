<?php $type = (isset($type)) ? $type : 'all'; ?>

    <ul class="mailbox-attachments clearfix row">
        <?php if (!empty($data)): ?>
            <?php foreach ($data as $file): ?>
                <?php if (($type == $file->type) || $type == 'all'): ?>
                    <li class="item col-md-4">
                        <div>
                            <?= $this->Util->iconPreview($file); ?>
                            <div class="mailbox-attachment-info">
                                <a href="<?= h($file->filepath) ?>" target="_blank" class="mailbox-attachment-name">
                                    <i class="fa fa-paperclip"></i>
                                    <?= substr($file->description, 0, 12) ?> <?= substr($file->name, 0, 12) ?>
                                </a>
                                <span class="mailbox-attachment-size">
                      <a download="<?= h($file->filepath) ?>" href="<?= h($file->filepath) ?>" target="_blank"
                         class="btn btn-default btn-xs"><i class="fa fa-cloud-download"></i></a>
                            <a id='btn_exclude' class='btn btn-danger btn-xs'
                               href="javascript:void(0)"
                               type="button"
                               onclick="clickDeleteDocument(<?= h($file->id) ?>,'<?= h($file->firebase_ref) ?>')">
                                <i class="fa fa-trash"></i>
                            </a>

                                    <?php if (!$file->file_type) : ?>
                                        <a id='btn_exclude' class='btn btn-danger btn-xs'
                                           href="javascript:void(0)"
                                           type="button"
                                           onclick="setTypeDocument(<?= h($file->id) ?>,'<?= h($file->firebase_ref) ?>' , '<?= ($file->file_type) ? ($file->file_type) : false ?>')">
                                <i class="fa fa-refresh"></i>
                            </a>
                                    <?php endif; ?>
                    </span>
                            </div>
                        </div>
                    </li>
                <?php endif; ?>
            <?php endforeach; ?>
        <?php endif; ?>
    </ul>


<?php $this->start('scriptBottom'); ?>

    <script>
        var auth = firebase.auth();
        var storageRef = firebase.storage().ref();  //Reference to the firebase storage

        function setTypeDocument(documentId, firebaseFileRef, type) {
            console.log('passou');
            var file = storageRef.child(firebaseFileRef);
            if (!type) {
                file.getMetadata().then(function (metadata) {
                    // Metadata now contains the metadata for 'images/forest.jpg'
                    $.ajax({
                        url: '<?= $url['edit'] ?>' + documentId + '.json',
                        type: 'PUT',
                        data: {'file_type': metadata.contentType},
                        dataType: "json",
                        success: function (r) {
                            location.reload()
                        }
                    });
                });
            }
        }

        function clickDeleteDocument(documentId, firebaseFileRef) {
            if (confirm('Deseja realmente apagar este documento?')) {
                try {
                    storageRef.child(firebaseFileRef).delete().then(function () {
                        console.log('Delete success');
                    }).catch(function (error) {
                        console.log('Delete fail ' + exception);
                    });
                } catch (err) {
                    console.log('Falha ao encontrar o caminho do arquivo no firebase ' + err);
                } finally {
                    $.ajax({
                        url: '<?= $url['delete'] ?>' + documentId + '.json',
                        type: 'DELETE',
                        dataType: "json",
                        success: function (r) {
                            location.reload()
                        }
                    });
                }
            }
        }

        //Loga anonimamente no firebase ao abrir a pagina
        window.onload = function () {
            auth.onAuthStateChanged(function (user) {
                if (user) {
                    console.log('Anonymous user signed-in.', user);
                } else {
                    console.log('There was no anonymous session. Creating a new anonymous user.');
                    // Sign the user in anonymously since accessing Storage requires the user to be authorized.
                    auth.signInAnonymously();
                }
            });
        }

    </script>

<?php $this->end(); ?>