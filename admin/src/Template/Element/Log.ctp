<?php foreach ($logs as $log): ?>
    <div class="post">
        <div class="user-block">
            <?php echo $this->Html->image('user1-128x128.jpg', array('class' => 'img-circle img-bordered-sm', 'alt' => 'User Image')); ?>
            <span class="username">
                                      <?php if ($log->user) : ?>
                                          <a href="<?= $this->Url->build('/users/view/' . $log->user->id) ?>"><?= $log->user->first_name ?> <?= $log->user->last_name ?></a>
                                      <?php endif ?>
                                    </span>
            <span class="description">alterado em - <?= $log->created ?></span>
        </div>
        <!-- /.user-block -->
        <div>
            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                <tbody>
                <?php foreach ($log->audit_deltas as $audits) : ?>
                    <tr>
                        <td width="20%" class="activity-name">
                            <?= ucfirst($audits->property_name) ?>
                        </td>
                        <td width="40%" class="activity-old-val">
                            <?= ucfirst($audits->old_value) ?>
                        </td>
                        <td width="40%" class="activity-new-val">
                            <?= ucfirst($audits->new_value) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>

                </tbody>
            </table>
        </div>
    </div>
<?php endforeach; ?>