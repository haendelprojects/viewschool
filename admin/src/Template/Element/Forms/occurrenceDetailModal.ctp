<?php $prefix = (isset($prefix)) ? $prefix . '.' : '' ?>

<button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#modal-address">
    <i class="fa fa-pencil"></i> Editar
</button>

<!-- Modal -->
<div id="modal-address" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <?= $this->Form->create($occurrence, array('url' => $url, 'role' => 'form', 'align' => [
                'sm' => [
                    'left' => 6,
                    'middle' => 6,
                    'right' => 12
                ],
                'md' => [
                    'left' => 3,
                    'middle' => 9,
                ]
            ])) ?>
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Editar Detalhes</h4>
            </div>
            <div class="modal-body">

                <div class="box-body">
                    <?php
                    echo $this->Form->input('Occurrences.responsible_user_id', ['label' => 'Responsavel', 'options' => $this->ListConfiguration->getResponsibles()]);
                    echo $this->Form->input('Occurrences.backoffice_work', ['label' => 'Esforço do backoffice', 'options' => ['Não', 'Sim']]);
                    echo $this->Form->input('Occurrences.service_id', ['label' => 'Responsavel', 'options' => $this->ListConfiguration->getServices()]);
                    echo $this->Form->input('Occurrences.first_service', ['label' => 'Resolvido na 1ª visita', 'options' => ['Não', 'Sim']]);
                    echo $this->Form->input('Occurrences.description', ['label' => 'Descrição']);
                    echo $this->Form->input('address.zipcode', ['label' => 'CEP', 'required' => true]);
                    echo $this->Form->input('address.street', ['class' => 'form-control', 'label' => 'Rua', 'required' => true]);
                    echo $this->Form->input('address.number', ['class' => 'form-control', 'label' => 'Número']);
                    echo $this->Form->input('address.complement', ['class' => 'form-control', 'label' => 'Complemento']);
                    echo $this->Form->input('address.neighborhood', ['class' => 'form-control', 'label' => 'Bairro', 'required' => true]);
                    echo $this->Form->input('address.country_id', ['options' => $this->Address->getCountries($occurrence->address->country_id ? $occurrence->address->country_id : 31), 'empty' => true, 'class' => 'form-control', 'label' => 'Pais', 'required' => true, 'id' => 'country']);

                    echo $this->Form->input('address.state_id', ['options' => $this->Address->getStates($occurrence->address->country_id ? $occurrence->address->country_id : 31), 'empty' => true, 'class' => 'form-control', 'label' => 'Estado', 'required' => true, 'id' => 'state']);
                    echo $this->Form->input('address.city_id', ['options' => $this->Address->getCities($occurrence->address->state_id), 'empty' => true, 'class' => 'form-control', 'label' => 'Cidade', 'required' => true, 'id' => 'city']);

                    echo $this->Form->input('address.lat', ['class' => 'form-control', 'label' => 'Latitude', 'required' => true]);
                    echo $this->Form->input('address.lng', ['class' => 'form-control', 'label' => 'Longitude', 'required' => true]);
                    ?>

                </div>
                <!-- /.box-body -->
                <div class="box-footer">

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Cancelar
                </button>
                <?= $this->Form->button(__('Salvar'), ['class' => 'btn btn-success']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>

    </div>
    <?= $this->element('Scripts/address') ?>
</div>

