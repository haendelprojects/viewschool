<?php $prefix = (isset($prefix)) ? $prefix . '.' : '' ?>

<button type="button" class="btn btn-info btn-xs " data-toggle="modal" data-target="#modal-recalculate">
    <i class="fa fa-pencil"></i> Recalcular
</button>

<!-- Modal -->
<div id="modal-recalculate" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <?= $this->Form->create($occurrence, array('url' => $url, 'role' => 'form', 'align' => [
                'sm' => [
                    'left' => 6,
                    'middle' => 6,
                    'right' => 12
                ],
                'md' => [
                    'left' => 4,
                    'middle' => 8,
                ]
            ])) ?>
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Recalcular</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <h4>Receita</h4>
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th></th>
                                <th><?= __('Valores Atuais') ?></th>
                                <th><?= __('Valores Novos') ?></th>
                            </tr>
                            </thead>
                            <tbody>


                            <tr>
                                <th><?= __('Tempo Base') ?>:</th>
                                <td>
                                    <?= $occurrence->price->occurrence_base_price ?> <?= ($occurrence->price->currency == 'POINTS') ? 'UPs' : 'R$' ?>
                                    (<?= $occurrence->price->occurrence_base_hours ?> h)
                                </td>
                                <td>
                                    <?= $price['occurrence_base_price'] ?> <?= ($occurrence->price->currency == 'POINTS') ? 'UPs' : 'R$' ?>
                                    (<?= $price['occurrence_base_hours'] ?> h)

                                    <?= $this->Form->input('price.occurrence_base_price', ['type' => 'hidden', 'value' => $price['occurrence_base_price']]); ?>
                                    <?= $this->Form->input('price.occurrence_base_hours', ['type' => 'hidden', 'value' => $price['occurrence_base_hours']]); ?>
                                </td>
                            </tr>

                            <tr>
                                <th><?= __('Tempo Extra') ?>:</th>
                                <td>
                                    <?= ($occurrence->price->occurrence_extra_price_quantity) ? $occurrence->price->occurrence_extra_price_quantity : 0 ?>
                                    <?= ($occurrence->price->currency == 'POINTS') ? 'UPs' : 'R$' ?>

                                    (<?= ($occurrence->price->occurrence_extra_hours_quantity) ? $occurrence->price->occurrence_extra_hours_quantity * $occurrence->price->occurrence_extra_hours : 0 ?>
                                    h)
                                </td>
                                <td>

                                    <?= $price['occurrence_extra_hours_quantity'] * $price['occurrence_extra_price'] ?>
                                    UP
                                    (<?= $price['occurrence_extra_hours_quantity'] * $price['occurrence_extra_hours'] ?>
                                    h)

                                    <?= $this->Form->input('price.occurrence_extra_hours_quantity', ['type' => 'hidden', 'value' => $price['occurrence_extra_hours_quantity']]); ?>
                                    <?= $this->Form->input('price.occurrence_extra_price_quantity', ['type' => 'hidden', 'value' => $price['occurrence_extra_hours_quantity'] * $price['occurrence_extra_price']]); ?>
                                    <?= $this->Form->input('price.occurrence_extra_price', ['type' => 'hidden', 'value' => $price['occurrence_extra_price']]); ?>
                                </td>
                            </tr>

                            <tr>
                                <th><?= __('Tempo Total') ?>:</th>
                                <td>
                                    <?= $occurrence->price->occurrence_total_price ?> <?= ($occurrence->price->currency == 'POINTS') ? 'UPs' : 'R$' ?>
                                    (<?= $occurrence->price->occurrence_total_hours ?> h)
                                </td>
                                <td>
                                    <?= $price['occurrence_total_price'] ?> <?= ($occurrence->price->currency == 'POINTS') ? 'UPs' : 'R$' ?>
                                    (<?= $price['occurrence_total_hours'] ?> h)

                                    <?= $this->Form->input('price.occurrence_total_hours', ['type' => 'hidden', 'value' => $price['occurrence_total_hours']]); ?>
                                    <?= $this->Form->input('price.occurrence_total_price', ['type' => 'hidden', 'value' => $price['occurrence_total_price']]); ?>
                                </td>
                            </tr>

                            </tbody>
                        </table>
                    </div>

                    <div class="col-md-12">
                        <h4>Despesa</h4>
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th></th>
                                <th><?= __('Valores Atuais') ?></th>
                                <th><?= __('Valores Novos') ?></th>
                            </tr>
                            </thead>
                            <tbody>

                            <tr>
                                <th><?= __('Tempo Base') ?>:</th>
                                <td>
                                    R$ <?= $occurrence->price->specialist_base_price ?>,00
                                    (<?= $price['specialist_base_hour'] ?> h)
                                </td>
                                <td>
                                    R$ <?= $price['specialist_base_price'] ?>,00
                                    (<?= $price['specialist_base_hour'] ?> h)

                                    <?= $this->Form->input('price.specialist_base_price', ['type' => 'hidden', 'value' => $price['specialist_base_price']]); ?>
                                </td>
                            </tr>

                            <tr>
                                <th><?= __('Tempo Extra') ?>:</th>
                                <td>
                                    R$ <?= $occurrence->price->specialist_extra_price_quantity ?>,00
                                    (<?= ($price['specialist_total_hours'] - $price['specialist_base_hour']) < 0 ? 0 : ($price['specialist_total_hours'] - $price['specialist_base_hour']) ?>
                                    h)
                                </td>
                                <td>
                                    R$ <?= ($price['specialist_extra_price_quantity']) ?>,00
                                    (<?= ($price['specialist_total_hours'] - $price['specialist_base_hour']) < 0 ? 0 : ($price['specialist_total_hours'] - $price['specialist_base_hour']) ?>
                                    h)

                                    <?= $this->Form->input('price.specialist_extra_price_quantity', ['type' => 'hidden', 'value' => $price['specialist_extra_price_quantity']]); ?>
                                </td>
                            </tr>

                            <tr>
                                <th><?= __('Tempo Total') ?>:</th>
                                <td>
                                    R$ <?= $occurrence->price->specialist_total_price ?>,00
                                    (<?= ($price['specialist_total_hours']) ?> h)
                                </td>
                                <td>
                                    R$ <?= $price['specialist_total_price'] ?>,00
                                    (<?= ($price['specialist_total_hours']) ?> h)

                                    <?= $this->Form->input('price.specialist_total_price', ['type' => 'hidden', 'value' => $price['specialist_total_price']]); ?>
                                </td>
                            </tr>

                            </tbody>
                        </table>
                    </div>

                </div>


                <?php
                echo $this->Form->input('price.occurrence_additional_costs', ['type' => 'number', 'label' => __('Valor Extra'), 'placeholder' => '0']);
                ?>

                <?php
                echo $this->Form->input('price.occurrence_displacement_costs', ['type' => 'number', 'label' => __('Valor do Deslocamento'), 'placeholder' => '0']); ?>

                <?php echo $this->Form->input('price.occurrence_tools_costs', ['type' => 'number', 'label' => __('Valor do Ferramental'), 'placeholder' => '0']); ?>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Cancelar
                </button>
                <?= $this->Form->button(__('Salvar'), ['class' => 'btn btn-success']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>

    </div>
    <?= $this->element('Scripts/address') ?>
</div>

