<?php $prefix = (isset($prefix)) ? $prefix . '.' : '' ?>

<button type="button" class="btn btn-warning btn-xs pull-right" data-toggle="modal" data-target="#modal-procedure">
    <i class="fa fa-pencil"></i> Editar
</button>

<!-- Modal -->
<div id="modal-procedure" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <?= $this->Form->create($occurrence, array('url' => $url, 'role' => 'form', 'align' => [

            ])) ?>
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Editar Detalhes do Procedimento</h4>
            </div>
            <div class="modal-body">

                <div class="box-body">
                    <?php
                    echo $this->Form->input('checkin_date', ['label' => 'Horário do Checkin']);
                    echo $this->Form->input('checkout_date', ['label' => 'Horário do Checkout']);
                    echo $this->Form->input('solution_description', ['label' => 'Solução do Técnico']);
                    ?>

                </div>
                <!-- /.box-body -->
                <div class="box-footer">

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Cancelar
                </button>
                <?= $this->Form->button(__('Salvar'), ['class' => 'btn btn-success']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>

    </div>
    <?= $this->element('Scripts/address') ?>
</div>

