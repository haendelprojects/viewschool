<?php $prefix = (isset($prefix)) ? $prefix . '.' : '' ?>

<button type="button" class="btn btn-warning btn-xs pull-right" data-toggle="modal" data-target="#modal-edit-client">
    <i class="fa fa-pencil"></i> Editar
</button>

<!-- Modal -->
<div id="modal-edit-client" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <?= $this->Form->create($occurrence, array('url' => $url, 'role' => 'form', 'align' => [
                'sm' => [
                    'left' => 6,
                    'middle' => 6,
                    'right' => 12
                ],
                'md' => [
                    'left' => 3,
                    'middle' => 9,
                ]
            ])) ?>
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Editar Cliente</h4>
            </div>
            <div class="modal-body">

                <div class="box-body">
                    <?php
                    echo $this->Form->input('corporate_id', ['label' => 'Empresa', 'options' => $this->ListConfiguration->getCorporates(), 'empty' => true, 'disabled']);
                    echo $this->Form->input('contract_id', ['label' => 'Contrato', 'options' => $this->ListConfiguration->getContracts($occurrence->corporate_id)]);
                    ?>

                    <div class="form-group select">
                        <label class="control-label col-sm-6 col-md-3" for="contract-id">Cliente</label>
                        <div class="col-sm-6 col-md-9">
                            <select style="width: 100%;" class="search-client form-control" name="client_user_id"
                                    required
                                    data-corporate-id="<?= $occurrence->corporate_id ?>">
                                <?php if ($occurrence->client): ?>
                                    <option value="<?= $occurrence->client->id ?>"
                                            selected="selected"><?= $occurrence->client->first_name . ' ' . $occurrence->client->last_name ?></option>
                                <?php else: ?>
                                    <option value="" selected="selected">Escolha o cliente</option>
                                <?php endif; ?>
                            </select>

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Cancelar
                </button>
                <?= $this->Form->button(__('Salvar'), ['class' => 'btn btn-success']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>

    </div>
    <?php
    $this->Html->css(['AdminLTE./plugins/select2/select2.min',], ['block' => 'css']);
    $this->Html->script(['AdminLTE./plugins/select2/select2.full.min', 'modules/search.client'], ['block' => 'script']);
    ?>
    <?= $this->element('Scripts/address') ?>
    <?= $this->element('Scripts/contractSelect') ?>
</div>

