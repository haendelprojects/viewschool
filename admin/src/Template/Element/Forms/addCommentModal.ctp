<?php $prefix = (isset($prefix)) ? $prefix . '.' : '' ?>


<button type="button" class="btn btn-success btn-xs pull-right" data-toggle="modal"
        data-target="#modal-comment-add">Adicionar Comentário
</button>

<!-- Modal -->
<div id="modal-comment-add" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <?= $this->Form->create(null, array('url' => $url, 'role' => 'form', 'align' => [
                'sm' => [
                    'left' => 6,
                    'middle' => 6,
                    'right' => 12
                ],
                'md' => [
                    'left' => 3,
                    'middle' => 9,
                ]
            ])) ?>
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Novo comentário</h4>
            </div>
            <div class="modal-body">

                <div class="box-body">
                    <?php
                    echo $this->Form->input('occurrence_id', ['label' => 'Texto', 'value' => $occurrence->id, 'type' => 'hidden']);
                    echo $this->Form->input('text', ['label' => 'Texto', 'required' => true, 'type' => 'textarea']);
                    ?>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Cancelar
                </button>
                <?= $this->Form->button(__('Salvar'), ['class' => 'btn btn-success']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>

    </div>
    <?= $this->element('Scripts/address') ?>
</div>

