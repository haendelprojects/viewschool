<?php $prefix = (isset($prefix)) ? $prefix . '.' : '' ?>


<button type="button" class="btn btn-warning btn-xs" data-toggle="modal"
        data-target="#modal-address"><i class="fa fa-pencil"></i> Editar Informaçoes
</button>

<!-- Modal -->
<div id="modal-address" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <?= $this->Form->create($data, array('url' => $url, 'role' => 'form', 'align' => [
                'sm' => [
                    'left' => 6,
                    'middle' => 6,
                    'right' => 12
                ],
                'md' => [
                    'left' => 3,
                    'middle' => 9,
                ]
            ])) ?>
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Editar Informações</h4>
            </div>
            <div class="modal-body">

                <div class="box-body">
                    <?php
                    echo $this->Form->input('name', ['label' => 'Nome']);
                    echo $this->Form->input('cpf', ['label' => 'CPF']);
                    echo $this->Form->input('phone', ['label' => 'Telefone']);
                    echo $this->Form->input('cell_phone', ['label' => 'Celular']);
                    echo $this->Form->input('birth', ['label' => 'Nascimento', 'empty' => true, 'default' => '', 'class' => 'datepicker form-control', 'type' => 'text']);
                    echo $this->Form->input('type', ['value' => 'ALUNO', 'label' => 'Tipo', 'type' => 'hidden']);
                    echo $this->Form->input('sexo', ['label' => 'Sexo', 'options' => ['FEMININO' => 'Feminino', 'MASCULINO' => 'Masculino']]);
                    echo $this->Form->input('father_name', ['label' => 'Nome do Pai']);
                    echo $this->Form->input('mother_name', ['label' => 'Nome da Mãe']);
                    echo $this->Form->input('email', ['label' => 'E-mail','required'=>true]);
                    ?>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Cancelar
                </button>
                <?= $this->Form->button(__('Salvar'), ['class' => 'btn btn-success']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>

    </div>

    <?php
    $this->Html->css(['AdminLTE./plugins/datepicker/datepicker3',],
        ['block' => 'css']);

    $this->Html->script(['AdminLTE./plugins/input-mask/jquery.inputmask',
        'AdminLTE./plugins/input-mask/jquery.inputmask.date.extensions',
        'AdminLTE./plugins/datepicker/bootstrap-datepicker',
        'AdminLTE./plugins/datepicker/locales/bootstrap-datepicker.pt-BR',],
        ['block' => 'script']);
    ?>
    <?php $this->start('scriptBottom'); ?>
    <script>
        $(function () {
            //Datemask mm/dd/yyyy
            $(".datepicker")
                .inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"})
                .datepicker({
                    language: 'en',
                    format: 'mm/dd/yyyy'
                });
        });
    </script>
    <?php $this->end(); ?>

</div>

