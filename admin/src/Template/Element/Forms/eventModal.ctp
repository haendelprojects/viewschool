<?php $prefix = (isset($prefix)) ? $prefix . '.' : '' ?>
<?php $text = (isset($text)) ? $text : '<i class="fa fa-plus"></i>  Adicionar Evento' ?>
<?php $id = (isset($id)) ? '-' . $id : '' ?>


<a href="javascript:void(0)" data-toggle="modal"
   data-target="#modal-event<?= $id ?>"><?= $text ?>
</a>


<!-- Modal -->
<div id="modal-event<?= $id ?>" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <?= $this->Form->create($data, array('url' => $url, 'role' => 'form', 'align' => [
                'sm' => [
                    'left' => 6,
                    'middle' => 6,
                    'right' => 12
                ],
                'md' => [
                    'left' => 3,
                    'middle' => 9,
                ]
            ])) ?>
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal">&times;
                </button>
                <h4 class="modal-title">Evento</h4>
            </div>
            <div class="modal-body">
                <?php
                echo $this->Form->input('title', ['label' => 'Titulo', 'placeholder' => 'Titulo do Evento']);
                echo $this->Form->input('text', ['label' => 'Descrição', 'type' => 'textarea', 'class' => 'textarea']);
                echo $this->Form->input('start', ['type' => 'text', 'label' => 'Inicio', 'placeholder' => 'Data inicial do Evento', 'class' => 'datepicker']);
                echo $this->Form->input('end', ['type' => 'text', 'label' => 'Fim', 'placeholder' => 'Data Final do Evento', 'class' => 'datepicker']);
                echo $this->Form->input('background', ['label' => 'Cor do evento', 'type' => 'color', 'placeholder' => 'Cor do evento no calendário']);
                ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Cancelar
                </button>
                <?= $this->Form->button(__('Salvar'), ['class' => 'btn btn-success']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>

    <?php
    $this->Html->css(['AdminLTE./plugins/datepicker/datepicker3',],
        ['block' => 'css']);

    $this->Html->script(['AdminLTE./plugins/input-mask/jquery.inputmask',
        'AdminLTE./plugins/input-mask/jquery.inputmask.date.extensions',
        'AdminLTE./plugins/datepicker/bootstrap-datepicker',
        'AdminLTE./plugins/datepicker/locales/bootstrap-datepicker.pt-BR',],
        ['block' => 'script']);
    ?>
    <?php $this->start('scriptBottom'); ?>
    <script>
        $(function () {
            //Datemask mm/dd/yyyy
            $(".datepicker")
                .inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"})
                .datepicker({
                    language: 'pt_br',
                    format: 'dd/mm/yyyy'
                });

            $(".cpf").inputmask("999.999.999-99", {"placeholder": "000.000.000-00"});

            $(".cellphone").inputmask("(99) 99999-9999", {"placeholder": "(00) 00000-0000"});
            $(".phone").inputmask("(99) 9999-9999", {"placeholder": "(00) 0000-0000"});
        });
    </script>
    <?php $this->end(); ?>

</div>

