
<li>
    <?= $this->Html->link('<i class="fa fa-home"></i> Escolas', ['controller' => 'schools', 'action' => 'index', 'plugin' => false], ['escape' => false]); ?>
</li>

<li>
    <?= $this->Html->link('<i class="fa fa-cog"></i> Usuários', ['controller' => 'users', 'action' => 'index', 'plugin' => false], ['escape' => false]); ?>
</li>

