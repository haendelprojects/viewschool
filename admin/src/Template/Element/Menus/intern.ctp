<div id="m_aside_left" class="m-grid__item m-aside-left ">
    <!-- BEGIN: Aside Menu -->
    <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-light m-aside-menu--submenu-skin-light "
         data-menu-vertical="true" data-menu-scrollable="false" data-menu-dropdown-timeout="500">
        <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
            <?php foreach ($lists as $list) : ?>

                <li class="m-menu__section">
                    <h4 class="m-menu__section-text"><?= $list['title'] ?> </h4>
                    <i class="m-menu__section-icon flaticon-more-v3"></i>
                </li>

                <?php foreach ($list['itens'] as $item) : ?>
                    <li class="m-menu__item <?= ($item['active']) ? 'm-menu__item--active' : '' ?>" aria-haspopup="true"
                        data-redirect="true">
                        <a href="<?= $this->Url->build($item['url']) ?>"
                           class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                <span></span>
                            </i>
                            <span class="m-menu__link-text"><?= $item['text'] ?></span>
                        </a>
                    </li>
                <?php endforeach; ?>
            <?php endforeach; ?>
        </ul>
    </div>
    <!-- END: Aside Menu -->
</div>