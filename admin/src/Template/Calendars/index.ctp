<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Calendário
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-3">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h4 class="box-title"></h4>
                </div>
                <div class="box-body">

                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <?= $this->Html->link('<i class="fa fa-calendar"> </i> Visualizar no calendário', ['action' => 'calendar'], ['escape' => false]); ?>
                        </li>
                        <li class="list-group-item">
                            <?= $this->element('Forms/eventModal', ['data' => $calendar, 'url' => ['action' => 'add']]) ?>
                        </li>
                    </ul>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <div class="box">

                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th><?= $this->Paginator->sort('title', 'Evento') ?></th>
                            <th><?= $this->Paginator->sort('start', 'Inicia dia') ?></th>
                            <th><?= $this->Paginator->sort('end', 'Finaliza') ?></th>
                            <th><?= __('') ?></th>
                        </tr>

                        <tr>
                            <?= $this->Form->create(null, ['valueSources' => ['query']]); ?>

                            <th>
                                <?= $this->Form->input('title', ['label' => false, 'class' => 'form-control', 'placeholder' => ('Evento')]); ?>
                            </th>
                            <th>

                            </th>
                            <th>

                            </th>
                            <th>
                                <div class="form-group select">
                                    <?php
                                    echo $this->Form->button('<i class="fa fa-filter"></i>', ['type' => 'submit', 'class' => 'btn btn-default', ' escape' => false]);
                                    if ($isSearch) {
                                        echo $this->Html->link('<i class="fa fa-trash"></i>', ['action' => 'index'], ['class' => 'btn btn-danger ', 'style' => 'margin-left: 10px', 'escape' => false]);
                                    }
                                    ?>
                                </div>
                            </th>
                            <?= $this->Form->end(); ?>
                        </tr>

                        </thead>
                        <tbody>
                        <?php foreach ($calendars as $calendar): ?>
                            <tr>
                                <td><?= h($calendar->title) ?></td>
                                <td><?= $this->Util->convertDateTimezone($calendar->start) ?></td>
                                <td><?= $this->Util->convertDateTimezone($calendar->end) ?></td>
                                <td class="actions" style="white-space:nowrap">
                                    <?= $this->Html->link(__('Detalhes'), ['action' => 'view', $calendar->id], ['class' => 'btn btn-info btn-xs']) ?>
                                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $calendar->id], ['class' => 'btn btn-warning btn-xs']) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right">
                        <?php echo $this->Paginator->numbers(); ?>
                    </ul>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
<!-- /.content -->
