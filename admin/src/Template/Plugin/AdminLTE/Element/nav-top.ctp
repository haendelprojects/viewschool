<?php
use Cake\Core\Configure;

$file = Configure::read('Theme.folder') . DS . 'src' . DS . 'Template' . DS . 'Element' . DS . 'nav-top.ctp';

if (file_exists($file)) {
    ob_start();
    include_once $file;
    echo ob_get_clean();
} else {
    ?>
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

                <li class="dropdown user user-menu">
                    <?php if ($this->User->isTeacher()): ?>
                        <a href="#"
                           class=" btn btn-outline-metal m-btn  m-btn--icon m-btn--pill bg-green">
                            <span class="hidden-xs">Sistema do Professor</span>
                        </a>
                    <?php endif; ?>

                    <?php if ($this->User->isSchool()): ?>
                        <a href="#"
                           class=" btn btn-outline-metal m-btn  m-btn--icon m-btn--pill  bg-blue">
                            <span class="hidden-xs">Sistema Administrativo</span>
                        </a>
                    <?php endif; ?>
                    <?php if ($this->User->isStudent()): ?>
                        <a href="#"
                           class=" btn btn-outline-metal m-btn  m-btn--icon m-btn--pill">
                            <span class="hidden-xs">Sistema Pais e Alunos</span>
                        </a>
                    <?php endif; ?>
                </li>

                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">

                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <?php echo $this->Html->image('user2-160x160.jpg', array('class' => 'user-image', 'alt' => 'User Image')); ?>
                        <span class="hidden-xs"> <?= $Auth['username'] ?></span>
                    </a>

                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <?php echo $this->Html->image('user2-160x160.jpg', array('class' => 'img-circle', 'alt' => 'User Image')); ?>

                            <p>
                                <?= $Auth['name'] ?>
                                <small>Membro desde <?= $Auth['created'] ?></small>
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">Perfil</a>
                            </div>
                            <div class="pull-right">
                                <a href="/users/logout" class="btn btn-default btn-flat">Sair</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
    <?php
}
?>
