<div class="alert alert-info alert-popup alert-dismissible">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
    <strong><i class="icon fa fa-check"></i> <?= __('Info') ?>!</strong>
    <?= h($message) ?>
</div>