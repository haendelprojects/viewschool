<section class="content-header">
    <h1>
        Cursos
        <small><?= __('Novo') ?></small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <?= $this->Html->link('<i class="fa fa-arrow-left"></i> ' . __('Voltar'), ['action' => 'index'], ['escape' => false]) ?>
        </li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><?= __('Informações') ?></h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <?= $this->Form->create($course, array('role' => 'form', 'align' => [
                    'sm' => [
                        'left' => 6,
                        'middle' => 6,
                        'right' => 12
                    ],
                    'md' => [
                        'left' => 2,
                        'middle' => 10,
                    ]
                ])) ?>
                <div class="box-body">
                    <?php
                    echo $this->Form->input('name', ['label' => 'Nome']);
                    echo $this->Form->input('nivel', ['options' => [
                        'INFANTIL' => 'Infantil',
                        'FUNDAMENTAL' => 'Fundamental',
                        'TECNICO' => 'Técnico',
                        'MEDIO' => 'Médio',
                        'OUTRO' => 'Outro'
                    ]]);

                    echo $this->Form->input('active', ['options' => [1 => 'Sim', 0 => 'Não'], 'label' => 'Este cursos está ativo?']);
                    echo $this->Form->input('workload', ['label' => 'Carga horária total do curso']);
                    ?>


                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <?= $this->Form->button(__('Cadastrar')) ?>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</section>

