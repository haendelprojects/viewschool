<?php
/**
 * Created by PhpStorm.
 * User: Haendel
 * Date: 14/09/2016
 * Time: 09:46
 */

namespace App\View\Helper;


use Cake\View\Helper;

class ProgressTrackerHelper extends Helper
{

    public $listStatus = [
        'OPENED' => 'Buscando Técnico',
        'ACCEPTED' => 'Aceito',
        'GOING' => 'Saiu para Atendimento',
        'ON_SERVICE' => 'Em Atendimento',
        'AWAITING_PAYMENT' => 'Espera de Pagamento',
        'PAYED_SPECIALIST' => 'Pagamento para Tècnico',
        'FINISHED' => 'Validado',
    ];

    public $listStatusCoporate = [
        'OPENED' => 'Buscando Técnico',
        'ACCEPTED' => 'Aceito',
        'GOING' => 'Saiu para Atendimento',
        'ON_SERVICE' => 'Em Atendimento',
        'AWAITING_PAYMENT' => 'Espera de Validação',
        'PAYED_SPECIALIST' => 'Pagamento para Tècnico',
        'FINISHED' => 'Validado',
    ];

    public function status($now, $corporate_id = null)
    {

//
//        <
//        li ><a href = "#0" > Aberto</a ></li >
//                                <li ><a href = "#0" > Aceito</a ></li >
//                                <li class="current" ><em > Deslocamento</em ></li >
//                                <li ><em > Em Serviço </em ></li >
//                                <li ><em > Validação do Cliente </em ></li >
//                                <li ><em > Validação da Operação </em ></li >
//                                <li ><em > Pagamento técnico </em ></li >
//                                <li ><em > Finalizado</em ></li >

        if ($now != "CANCELED" && $now != "UNPRODUCTIVE" && $now != "INVALID") {
            $html = "   <ol class=\"cd-breadcrumb triangle custom-icons\">";

            $list = ($corporate_id == null) ? $this->listStatus : $this->listStatusCoporate;

            foreach ($list as $key => $value) {
                $class = ($now == $key) ? "current" : '';
                $html .= "<li class='$class'><a href=\"#0\">$value</a></li>";
            }
            $html .= "</ol> ";
            return $html;
        } else {

        }
    }

}