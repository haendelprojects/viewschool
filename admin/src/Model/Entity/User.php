<?php
namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

/**
 * User Entity
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property string $first_name
 * @property string $last_name
 * @property bool $new_photo
 * @property string $reset_password_token
 * @property \Cake\I18n\FrozenTime $reset_password_token_created_at
 * @property string $hash_password
 * @property int $user_reference_id
 * @property bool $accept_demands
 * @property bool $moderator
 * @property int $role_id
 * @property int $notification_id
 * @property int $enterprise_id
 * @property bool $access_client
 * @property bool $access_admin
 * @property bool $access_specialist
 * @property bool $must_change_password
 * @property string $hash_invite
 * @property string $lat
 * @property string $lng
 * @property string $facebook_pass
 * @property string $facebook_id
 * @property string $uid_user
 * @property string $uid_geolocation
 * @property bool $black_list
 * @property string $timezone
 * @property bool $active
 * @property bool $full_record
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property float $rating
 * @property bool $available
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Role $role
 * @property \App\Model\Entity\Notification $notification
 * @property \App\Model\Entity\Enterprise $enterprise
 * @property \App\Model\Entity\Facebook $facebook
 * @property \App\Model\Entity\Availability[] $availabilities
 * @property \App\Model\Entity\Comment[] $comments
 * @property \App\Model\Entity\Configuration[] $configurations
 * @property \App\Model\Entity\DisabledReason[] $disabled_reasons
 * @property \App\Model\Entity\FavoriteSpecialist[] $favorite_specialists
 * @property \App\Model\Entity\Historic[] $historics
 * @property \App\Model\Entity\Individual[] $individuals
 * @property \App\Model\Entity\Media[] $medias
 * @property \App\Model\Entity\Message[] $messages
 * @property \App\Model\Entity\ProposedOccurrence[] $proposed_occurrences
 * @property \App\Model\Entity\Test[] $tests
 * @property \App\Model\Entity\Transportation[] $transportations
 */
class User extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    /**
     * Fields that are excluded from JSON an array versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];

    protected function _setBirthdate($value)
    {
        return Date::createFromFormat('d/m/Y', $value);
    }

    protected function _setPassword($password)
    {
        return (new DefaultPasswordHasher())->hash($password);
    }

}
