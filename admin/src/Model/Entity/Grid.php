<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Grid Entity
 *
 * @property int $id
 * @property string $week_day
 * @property \Cake\I18n\FrozenTime $init_hour
 * @property \Cake\I18n\FrozenTime $end_hour
 * @property int $class_id
 * @property int $dicipline_id
 *
 * @property \App\Model\Entity\Clas $clas
 * @property \App\Model\Entity\Dicipline $dicipline
 */
class Grid extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
