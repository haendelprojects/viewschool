<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * People Entity
 *
 * @property int $id
 * @property string $name
 * @property string $cpf
 * @property string $phone
 * @property string $cell_phone
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenDate $birth
 * @property string $type
 * @property string $sexo
 * @property string $foto
 * @property string $father_name
 * @property string $mother_name
 * @property string $email
 * @property int $address_id
 * @property int $users_id
 * @property int $branch_id
 *
 * @property \App\Model\Entity\Address $address
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Branch $branch
 * @property \App\Model\Entity\Clas[] $class
 * @property \App\Model\Entity\ClassDiscipline[] $class_discipline
 * @property \App\Model\Entity\Comment[] $comments
 * @property \App\Model\Entity\Frequency[] $frequencies
 * @property \App\Model\Entity\Registration[] $registrations
 */
class People extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    protected function _setBirth($value)
    {
        return \DateTime::createFromFormat('d/m/Y', $value);
    }

    protected function _setRg($rg)
    {
        if ($rg) {
            return preg_replace("/\D+/", "", $rg);
        } else {
            return null;
        }

    }

    protected function _setCpf($cpf)
    {
        if ($cpf) {
            return preg_replace("/\D+/", "", $cpf);
        } else {
            return null;
        }

    }

    protected function _setCellPhone($value)
    {
        if ($value) {
            return preg_replace("/\D+/", "", $value);
        } else {
            return null;
        }

    }

    protected function _setPhone($value)
    {
        if ($value) {
            return preg_replace("/\D+/", "", $value);
        } else {
            return null;
        }

    }
}
