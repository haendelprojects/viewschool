<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Calculate Model
 *
 * @method \App\Model\Entity\Calculate get($primaryKey, $options = [])
 * @method \App\Model\Entity\Calculate newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Calculate[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Calculate|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Calculate patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Calculate[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Calculate findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CalculateTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('calculate');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('sum_yearly')
            ->allowEmpty('sum_yearly');

        $validator
            ->boolean('recovery_final')
            ->allowEmpty('recovery_final');

        $validator
            ->boolean('recovery_semi_annual')
            ->allowEmpty('recovery_semi_annual');

        $validator
            ->scalar('calculatecol')
            ->allowEmpty('calculatecol');

        return $validator;
    }
}
