<?php
namespace App\Test\TestCase\Controller;

use App\Controller\CiusuariosController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\CiusuariosController Test Case
 */
class CiusuariosControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ciusuarios',
        'app.clinicas',
        'app.cdusuarios',
        'app.cdusuarios_clinicas',
        'app.exames',
        'app.pusuarios',
        'app.convenios',
        'app.prontuarios',
        'app.servicos',
        'app.anexos',
        'app.ciusuarios_clinicas'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
