<?php
namespace App\Test\TestCase\Controller;

use App\Controller\ConveniosController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\ConveniosController Test Case
 */
class ConveniosControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.convenios',
        'app.exames',
        'app.pusuarios',
        'app.prontuarios',
        'app.clinicas',
        'app.cdusuarios',
        'app.cdusuarios_clinicas',
        'app.ciusuarios',
        'app.ciusuarios_clinicas',
        'app.servicos',
        'app.anexos'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
