<?php
namespace App\Test\TestCase\Controller;

use App\Controller\ServicosController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\ServicosController Test Case
 */
class ServicosControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.servicos',
        'app.exames',
        'app.pusuarios',
        'app.convenios',
        'app.prontuarios',
        'app.clinicas',
        'app.cdusuarios',
        'app.cdusuarios_clinicas',
        'app.ciusuarios',
        'app.ciusuarios_clinicas',
        'app.anexos'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
