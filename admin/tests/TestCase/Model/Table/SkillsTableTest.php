<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SkillsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SkillsTable Test Case
 */
class SkillsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SkillsTable
     */
    public $Skills;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.skills',
        'app.questions',
        'app.tests',
        'app.tests_questions',
        'app.answers',
        'app.individuals',
        'app.users',
        'app.individuals_tests',
        'app.addresses',
        'app.cities',
        'app.states',
        'app.regions',
        'app.prices',
        'app.services_contracts',
        'app.contracts',
        'app.enterprises',
        'app.disabled_reasons',
        'app.extracts',
        'app.funds',
        'app.funds_months',
        'app.occurrences',
        'app.external_os',
        'app.coupons',
        'app.services',
        'app.contracts_tools',
        'app.tools',
        'app.services_skills',
        'app.comments',
        'app.historics',
        'app.medias',
        'app.ratings',
        'app.reviews',
        'app.ratings_reviews',
        'app.phones',
        'app.service_providers',
        'app.credits',
        'app.account_statements',
        'app.payment_methods',
        'app.holidays',
        'app.countries',
        'app.proposed_occurrences',
        'app.documents',
        'app.individuals_auths',
        'app.auths',
        'app.socialmedias',
        'app.links',
        'app.individuals_skills'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Skills') ? [] : ['className' => SkillsTable::class];
        $this->Skills = TableRegistry::get('Skills', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Skills);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
