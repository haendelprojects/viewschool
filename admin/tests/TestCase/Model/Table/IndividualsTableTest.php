<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\IndividualsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\IndividualsTable Test Case
 */
class IndividualsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\IndividualsTable
     */
    public $Individuals;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.individuals',
        'app.users',
        'app.individuals_tests',
        'app.addresses',
        'app.cities',
        'app.states',
        'app.regions',
        'app.holidays',
        'app.countries',
        'app.proposed_occurrences',
        'app.enterprises',
        'app.contracts',
        'app.contracts_tools',
        'app.tools',
        'app.services',
        'app.credits',
        'app.account_statements',
        'app.extracts',
        'app.funds',
        'app.funds_months',
        'app.occurrences',
        'app.services_contracts',
        'app.disabled_reasons',
        'app.phones',
        'app.documents',
        'app.individuals_auths',
        'app.links',
        'app.skills',
        'app.individuals_skills',
        'app.tests'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Individuals') ? [] : ['className' => IndividualsTable::class];
        $this->Individuals = TableRegistry::get('Individuals', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Individuals);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
