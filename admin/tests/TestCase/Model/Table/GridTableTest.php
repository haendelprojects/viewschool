<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\GridTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\GridTable Test Case
 */
class GridTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\GridTable
     */
    public $Grid;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.grid',
        'app.class',
        'app.courses',
        'app.branchs',
        'app.addresses',
        'app.peoples',
        'app.schools',
        'app.calendars',
        'app.users',
        'app.class_discipline',
        'app.dicipline',
        'app.frequencies',
        'app.comments',
        'app.evaluations',
        'app.organizations'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Grid') ? [] : ['className' => GridTable::class];
        $this->Grid = TableRegistry::get('Grid', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Grid);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
