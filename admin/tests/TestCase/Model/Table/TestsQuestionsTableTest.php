<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TestsQuestionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TestsQuestionsTable Test Case
 */
class TestsQuestionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TestsQuestionsTable
     */
    public $TestsQuestions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tests_questions',
        'app.tests',
        'app.users',
        'app.skills',
        'app.questions',
        'app.answers',
        'app.individuals',
        'app.individuals_tests',
        'app.addresses',
        'app.cities',
        'app.states',
        'app.countries',
        'app.holidays',
        'app.regions',
        'app.prices',
        'app.services_contracts',
        'app.contracts',
        'app.enterprises',
        'app.disabled_reasons',
        'app.extracts',
        'app.funds',
        'app.funds_months',
        'app.occurrences',
        'app.external_os',
        'app.coupons',
        'app.services',
        'app.contracts_tools',
        'app.tools',
        'app.services_skills',
        'app.comments',
        'app.historics',
        'app.medias',
        'app.ratings',
        'app.reviews',
        'app.ratings_reviews',
        'app.phones',
        'app.service_providers',
        'app.credits',
        'app.account_statements',
        'app.payment_methods',
        'app.proposed_occurrences',
        'app.documents',
        'app.individuals_auths',
        'app.auths',
        'app.socialmedias',
        'app.links',
        'app.individuals_skills',
        'app.trainings'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TestsQuestions') ? [] : ['className' => TestsQuestionsTable::class];
        $this->TestsQuestions = TableRegistry::get('TestsQuestions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TestsQuestions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
