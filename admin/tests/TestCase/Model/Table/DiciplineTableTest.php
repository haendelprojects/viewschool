<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DiciplineTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DiciplineTable Test Case
 */
class DiciplineTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DiciplineTable
     */
    public $Dicipline;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.dicipline',
        'app.branchs',
        'app.addresses',
        'app.peoples',
        'app.schools',
        'app.calendars',
        'app.users',
        'app.class',
        'app.courses',
        'app.organizations',
        'app.evaluations',
        'app.class_discipline',
        'app.frequencies',
        'app.comments',
        'app.grid'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Dicipline') ? [] : ['className' => DiciplineTable::class];
        $this->Dicipline = TableRegistry::get('Dicipline', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Dicipline);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
