<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ServicesSkillsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ServicesSkillsTable Test Case
 */
class ServicesSkillsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ServicesSkillsTable
     */
    public $ServicesSkills;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.services_skills',
        'app.skills',
        'app.services',
        'app.contracts_tools',
        'app.tools',
        'app.contracts',
        'app.enterprises',
        'app.addresses',
        'app.cities',
        'app.states',
        'app.regions',
        'app.prices',
        'app.services_contracts',
        'app.payment_methods',
        'app.occurrences',
        'app.external_os',
        'app.coupons',
        'app.users',
        'app.comments',
        'app.extracts',
        'app.funds',
        'app.funds_months',
        'app.historics',
        'app.medias',
        'app.ratings',
        'app.reviews',
        'app.ratings_reviews',
        'app.holidays',
        'app.countries',
        'app.proposed_occurrences',
        'app.individuals',
        'app.individuals_tests',
        'app.tests',
        'app.documents',
        'app.individuals_auths',
        'app.auths',
        'app.socialmedias',
        'app.links',
        'app.phones',
        'app.service_providers',
        'app.individuals_skills',
        'app.disabled_reasons',
        'app.credits',
        'app.account_statements'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ServicesSkills') ? [] : ['className' => ServicesSkillsTable::class];
        $this->ServicesSkills = TableRegistry::get('ServicesSkills', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ServicesSkills);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
