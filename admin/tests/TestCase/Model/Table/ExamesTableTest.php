<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ExamesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ExamesTable Test Case
 */
class ExamesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ExamesTable
     */
    public $Exames;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.exames',
        'app.pusuarios',
        'app.prontuarios',
        'app.clinicas',
        'app.cdusuarios',
        'app.cdusuarios_clinicas',
        'app.ciusuarios',
        'app.ciusuarios_clinicas',
        'app.convenios',
        'app.servicos',
        'app.anexos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Exames') ? [] : ['className' => ExamesTable::class];
        $this->Exames = TableRegistry::get('Exames', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Exames);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
