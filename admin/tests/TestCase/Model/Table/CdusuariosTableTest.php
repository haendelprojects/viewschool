<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CdusuariosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CdusuariosTable Test Case
 */
class CdusuariosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CdusuariosTable
     */
    public $Cdusuarios;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.cdusuarios',
        'app.clinicas',
        'app.cdusuarios_clinicas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Cdusuarios') ? [] : ['className' => CdusuariosTable::class];
        $this->Cdusuarios = TableRegistry::get('Cdusuarios', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Cdusuarios);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
