<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DialoguesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DialoguesTable Test Case
 */
class DialoguesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DialoguesTable
     */
    public $Dialogues;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.dialogues'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Dialogues') ? [] : ['className' => DialoguesTable::class];
        $this->Dialogues = TableRegistry::get('Dialogues', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Dialogues);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
