<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FundsMonthsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FundsMonthsTable Test Case
 */
class FundsMonthsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FundsMonthsTable
     */
    public $FundsMonths;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.funds_months',
        'app.funds',
        'app.contracts',
        'app.enterprises',
        'app.addresses',
        'app.cities',
        'app.states',
        'app.regions',
        'app.holidays',
        'app.proposed_occurrences',
        'app.countries',
        'app.individuals',
        'app.occurrences',
        'app.disabled_reasons',
        'app.users',
        'app.extracts',
        'app.phones',
        'app.contracts_tools',
        'app.tools',
        'app.services',
        'app.credits',
        'app.account_statements',
        'app.services_contracts'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('FundsMonths') ? [] : ['className' => FundsMonthsTable::class];
        $this->FundsMonths = TableRegistry::get('FundsMonths', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FundsMonths);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
