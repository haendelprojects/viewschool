<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CiusuariosClinicasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CiusuariosClinicasTable Test Case
 */
class CiusuariosClinicasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CiusuariosClinicasTable
     */
    public $CiusuariosClinicas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ciusuarios_clinicas',
        'app.ciusuarios',
        'app.clinicas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CiusuariosClinicas') ? [] : ['className' => CiusuariosClinicasTable::class];
        $this->CiusuariosClinicas = TableRegistry::get('CiusuariosClinicas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CiusuariosClinicas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
