<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProposedOccurrencesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProposedOccurrencesTable Test Case
 */
class ProposedOccurrencesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ProposedOccurrencesTable
     */
    public $ProposedOccurrences;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.proposed_occurrences',
        'app.cities',
        'app.states',
        'app.regions',
        'app.addresses',
        'app.countries',
        'app.holidays',
        'app.individuals',
        'app.users',
        'app.individuals_tests',
        'app.tests',
        'app.documents',
        'app.individuals_auths',
        'app.auths',
        'app.socialmedias',
        'app.links',
        'app.phones',
        'app.service_providers',
        'app.enterprises',
        'app.contracts',
        'app.contracts_tools',
        'app.tools',
        'app.services',
        'app.credits',
        'app.account_statements',
        'app.extracts',
        'app.funds',
        'app.funds_months',
        'app.occurrences',
        'app.external_os',
        'app.prices',
        'app.services_contracts',
        'app.payment_methods',
        'app.coupons',
        'app.comments',
        'app.historics',
        'app.medias',
        'app.ratings',
        'app.disabled_reasons',
        'app.skills',
        'app.individuals_skills'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ProposedOccurrences') ? [] : ['className' => ProposedOccurrencesTable::class];
        $this->ProposedOccurrences = TableRegistry::get('ProposedOccurrences', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ProposedOccurrences);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
