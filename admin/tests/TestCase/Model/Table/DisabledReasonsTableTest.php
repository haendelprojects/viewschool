<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DisabledReasonsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DisabledReasonsTable Test Case
 */
class DisabledReasonsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DisabledReasonsTable
     */
    public $DisabledReasons;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.disabled_reasons',
        'app.users',
        'app.enterprises'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('DisabledReasons') ? [] : ['className' => DisabledReasonsTable::class];
        $this->DisabledReasons = TableRegistry::get('DisabledReasons', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DisabledReasons);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
