<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ExamesFixture
 *
 */
class ExamesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'nome' => ['type' => 'string', 'length' => 45, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'pUsuario_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'prontuario_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'clinica_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'convenio_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'servico_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'fk_exames_pUsuarios1_idx' => ['type' => 'index', 'columns' => ['pUsuario_id'], 'length' => []],
            'fk_exames_prontuarios1_idx' => ['type' => 'index', 'columns' => ['prontuario_id'], 'length' => []],
            'fk_exames_clinicas1_idx' => ['type' => 'index', 'columns' => ['clinica_id'], 'length' => []],
            'fk_exames_convenios1_idx' => ['type' => 'index', 'columns' => ['convenio_id'], 'length' => []],
            'fk_exames_servicos1_idx' => ['type' => 'index', 'columns' => ['servico_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_exames_clinicas1' => ['type' => 'foreign', 'columns' => ['clinica_id'], 'references' => ['clinicas', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_exames_convenios1' => ['type' => 'foreign', 'columns' => ['convenio_id'], 'references' => ['convenios', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_exames_pUsuarios1' => ['type' => 'foreign', 'columns' => ['pUsuario_id'], 'references' => ['pusuarios', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_exames_prontuarios1' => ['type' => 'foreign', 'columns' => ['prontuario_id'], 'references' => ['prontuarios', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_exames_servicos1' => ['type' => 'foreign', 'columns' => ['servico_id'], 'references' => ['servicos', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'nome' => 'Lorem ipsum dolor sit amet',
            'pUsuario_id' => 1,
            'prontuario_id' => 1,
            'clinica_id' => 1,
            'convenio_id' => 1,
            'servico_id' => 1
        ],
    ];
}
