/**
 * Created by Haendel on 24/08/2016.
 */
Module('GALLERY.Images', function (Images) {

    Images.fn.initialize = function (container) {
        container.fancybox({
            openEffect: 'none',
            closeEffect: 'none'
        });
    }
});
