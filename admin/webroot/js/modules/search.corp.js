/**
 * Created by Haendel on 02/09/2016.
 */


Module('SEARCH.Corp', function (Corp) {

    var self = this;

    Corp.fn.initialize = function (container) {
        self.searchInput = container.find('.search-corp');
        self.inputSearchClient = container.find('.search-client');
        self.inputSearchContract = container.find('.search-contract');

        self.inputSearchClientBase = container.find('.search-client-base');
        self.inputSearchContractBase = container.find('.search-contract-base');

        self.inputSearchAddress = container.find('.box-select-filiais');

        // self.url = "http://localhost/admin-web/ajax/getEnterprises.json";
        self.url = "/ajax/getEnterprises.json";


        this.addEventListener();
    };

    Corp.fn.addEventListener = function () {
        self.searchInput.select2({
            ajax: {
                url: self.url,
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;

                    return {
                        results: data.data.enterprises,
                        pagination: {
                            more: (params.page * 30) < data.data.pagination.count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }, // let our custom formatter work
            minimumInputLength: 1,
            templateResult: formatRepo, // omitted for brevity, see the source of self page
            templateSelection: formatRepoSelection // omitted for brevity, see the source of self page

        });
    }

    function formatRepo(repo) {
        return repo.name || repo.text;
    }

    function formatRepoSelection(repo) {
        if (repo.id) {
            //Pessoas
            $.ajax({
                url: "/ajax/getUser.json",
                dataType: 'json',
                data: {
                    corporate: repo.id
                }
            }).done(function (res) {
                var html = '';
                res.data.users.forEach(function (data) {
                    html += '<option value="' + data.id + '" >' + data.first_name + ' ' + data.last_name + '</option>';
                });

                self.inputSearchClientBase.html(html);
            });

            //Contrato
            $.ajax({
                url: "/contracts/actives/" + repo.id + ".json",
                dataType: 'json',
            }).done(function (res) {
                var html = '';
                res.contracts.forEach(function (data) {
                    html += '<option value="' + data.id + '" >' + data.name + ' - ' + data.type + ' (' + data.fund.credit_balance + ')</option>';
                });

                self.inputSearchContractBase.html(html);
            });

            //Contrato
            $.ajax({
                url: "/addresses/optionsAddressCorporate/ " + repo.id
            }).done(function (res) {
                self.inputSearchAddress.html(res);
            });
        }

        if (repo.name) {
            self.inputSearchClient.html('<option value="" selected="selected">Escolha o cliente</option>');
        }

        return repo.name || repo.text;
    }
});
