/**
 * Created by Haendel on 14/09/2016.
 */

Module('COMMENT.Box', function (Box) {

    var self = this;
    Box.fn.initialize = function (container) {
        self.btnAddComment = container.find('.btn-add-comment');
        self.btnSaveComment = container.find('.btn-save-comment');
        self.btnCancelComment = container.find('.btn-cancel-comment');
        self.boxAddComment = container.find('.box-add-comment');
        self.btnEditComment = container.find('.btn-edit-comment')
        self.textComment = container.find('.text-comment');
        self.tableComment = container.find('.table-comment tbody');

        self.templateTableComment = '<tr><td>${texto}</td></tr>';


        this.addEventListener();
    };


    Box.fn.addEventListener = function () {
        self.btnAddComment.on('click', this.btnAddCommentClick.bind(this));
        self.btnCancelComment.on('click', this.btnCancelCommentClick.bind(this));
        self.btnEditComment.on('click', this.btnEditCommentClick.bind(this));
        self.btnSaveComment.on('click', this.btnSaveCommentClick.bind(this));
    }

    Box.fn.btnAddCommentClick = function (e) {
        $(e.target).hide();
        self.boxAddComment.show();
    };

    Box.fn.btnCancelCommentClick = function (e) {
        self.btnAddComment.show();
        self.boxAddComment.hide();
    };

    Box.fn.btnEditCommentClick = function (e) {
        var commentId = $(e.target).attr('data-comment-id');
        $.ajax({
            type: 'GET',
            url: '/comments/edit/' + commentId
        }).done(function (res) {
            $('#editModal').remove();
            $('body').append(res);
            $('#editModal').modal();
        })
    }

    Box.fn.btnSaveCommentClick = function (e) {
        var text = self.textComment.val();
        var occurrenceId = self.boxAddComment.attr('data-occurrence-id');
        var compiled = _.template('<tr> <td> <span> <a href="/occurrences/edit/<%= id %>" title="Detalhes"> <%= user %></a> adicionou um comentário <%= date %> </span> <p> <%= text %> </p> </td> <td class="actions"> <div class="btn-group"> <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <span class="ti ti-more-alt"></span> </button> <ul class="dropdown-menu"> <li> <a href="javascript:void(0)" data-comment-id="<%= id %>" class="btn-edit-comment"><span class="ti ti-pencil "></span>Editar                                                                </a> </li> <li><form name="post_57d705649c0a7687190801" style="display:none;" method="post" action="/comments/delete/<%= id %>"><input type="hidden" name="_method" value="POST"></form><a href="#" title="Editar" onclick="if (confirm(&quot;Deseja deletar ? &quot;)) { document.post_57d705649c0a7687190801.submit(); } event.returnValue = false; return false;"><span class="ti ti-trash"></span> Exluir</a> </li> </ul> </div> </td> </tr>');

        $.ajax({
            type: 'POST',
            url: '/comments/add.json',
            data: {
                text: text,
                occurrence_id: occurrenceId
            }
        }).done(function (res) {
            self.tableComment.append(compiled({
                text: res.comment.text,
                user: res.comment.user.first_name,
                id: res.comment.id,
                date: res.comment.created
            }));

            self.btnAddComment.show();
            self.boxAddComment.hide();
            self.textComment.val('');

        }).fail(function (res) {
            console.log(res);
        })

    }

});