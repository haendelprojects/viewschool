/**
 * Created by Haendel on 25/08/2016.
 */

Module('ADDRESS.Autocomplete', function (Autocomplete) {


    Autocomplete.fn.initialize = function (container) {
        this.Zipcode = container.find('.zipcode');
        this.State = container.find('.state');
        this.City = container.find('.city');
        this.Neighborhood = container.find('.neighborhood');
        this.Street = container.find('.street');
        this.Latitude = container.find('.latitude');
        this.Longitude = container.find('.longitude');

        this.addEventListener();
    };


    Autocomplete.fn.addEventListener = function () {
        this.Zipcode.on('change', this.onZipcodeChange.bind(this));
    }
    /**
     * Auto completar o formulario de endereço com o cep
     * @param e
     */
    Autocomplete.fn.onZipcodeChange = function (e) {

        var zipcode = this.Zipcode.val();

        var self = this;

        if (zipcode.length <= 0) return;
        $.get("https://viacep.com.br/ws/" + zipcode + "/json/ ",
            function (result) {
                debugger;
                self.State.val(result.uf);
                self.City.val(result.localidade);
                self.Neighborhood.val(result.bairro);
                self.Street.val(result.logradouro);

                $.get('/addresses/selectStateName/' + result.uf , function(res){
                    $('.box-select-state').html(res);

                    $('.select-state').on('change',  function(){
                        var id = $(this).val();
                        $.get('/addresses/select/cities/' + id , function(res){
                            $('.box-select-city').html(res);
                        });
                    });

                });

                $.get('/addresses/selectStateCities/' + result.uf + '/' + result.localidade , function(res){
                    $('.box-select-city').html(res);
                });
                //getGeo(result.cep);
            });
    }

    /**
     * Buscar coordenadas no google maps
     * @param zipcode
     */
    function getGeo(zipcode) {
        var url = 'https://maps.google.com/maps/api/geocode/json?address=' + zipcode + '&sensor=false';
        $.get(url,
            function (result) {
                settings.inputLatitude.val(result.results[0].geometry.location.lat);
                settings.inputLongitude.val(result.results[0].geometry.location.lng);
            });
    }
});
