<section class="content-header">
  <h1>
    <?php echo __('Grid'); ?>
  </h1>
  <ol class="breadcrumb">
    <li>
    <?= $this->Html->link('<i class="fa fa-dashboard"></i> ' . __('Back'), ['action' => 'index'], ['escape' => false])?>
    </li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-md-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <i class="fa fa-info"></i>
                <h3 class="box-title"><?php echo __('Information'); ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <dl class="dl-horizontal">
                                                                                                                <dt><?= __('Week Day') ?></dt>
                                        <dd>
                                            <?= h($grid->week_day) ?>
                                        </dd>
                                                                                                                                                    <dt><?= __('Clas') ?></dt>
                                <dd>
                                    <?= $grid->has('clas') ? $grid->clas->name : '' ?>
                                </dd>
                                                                                                                <dt><?= __('Discipline') ?></dt>
                                <dd>
                                    <?= $grid->has('discipline') ? $grid->discipline->name : '' ?>
                                </dd>
                                                                                                
                                            
                                                                                                                                            
                                                                                                        <dt><?= __('Init Hour') ?></dt>
                                <dd>
                                    <?= h($grid->init_hour) ?>
                                </dd>
                                                                                                                    <dt><?= __('End Hour') ?></dt>
                                <dd>
                                    <?= h($grid->end_hour) ?>
                                </dd>
                                                                                                    
                                            
                                    </dl>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- ./col -->
</div>
<!-- div -->

</section>
