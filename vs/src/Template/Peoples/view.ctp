<div class="panel-header panel-header-sm">
</div>


<div class="content">
    <div class="row">
        <div class="col-md-12">

            <div class="row">
                <div class="col-md-3">

                    <!-- Profile Image -->
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">

                            </h4>
                        </div>
                        <div class="card-body">
                            <div class="box-body box-profile text-center">
                                <?php echo $this->Html->image(($people->foto) ? $people->foto : 'avatar.png', array('class' => 'profile-user-img img-responsive img-circle', 'alt' => 'User profile picture')); ?>
                                <p class="text-muted text-center mt-3"><?= $people->name ?></p>

                                <div class="mt-4">

                                    <?= (!$people->active) ? '<div class="badge badge-danger">Funcionário Inativo</div>' : '' ?>

                                    <div>
                                        <?php $text = ($people->active) ? 'Desativar' : 'Ativar' ?>
                                        <?= $this->ModalConfirm->postLink($text, ['action' => 'delete', $people->id], ['class' => 'btn btn-info', 'title' => $text, 'text' => 'Deseja prosseguir em ' . $text . ' o funcionário?']) ?>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">

                            </h4>
                        </div>

                        <div class="card-body">
                            <div class="nav-tabs-custom">
                                <ul class="nav-pills-primary nav nav-pills">
                                    <li class="nav-item"><a href="#activity" class="nav-link active" data-toggle="tab">
                                            Informações</a>
                                    </li>
                                    <li class="nav-item"><a href="#endereco" class="nav-link"
                                                            data-toggle="tab">Endereço</a>
                                    </li>
                                    <li class="nav-item"><a href="#system" class="nav-link" data-toggle="tab">
                                            Sistema</a></li>
                                </ul>
                                <div class="tab-content pt-5">
                                    <!--                                    INformações-->
                                    <div class=" active tab-pane" id="activity">
                                        <div class="row">
                                            <dl class="dl-horizontal col-md-6">
                                                <dt><?= __('Nome') ?></dt>
                                                <dd>
                                                    <?= h($people->name) ?>
                                                </dd>
                                                <dt><?= __('CPF') ?></dt>
                                                <dd>
                                                    <?= h($people->cpf) ?>
                                                </dd>
                                                <dt><?= __('Nascimento') ?></dt>
                                                <dd>
                                                    <?= h($people->birth) ?>
                                                </dd>

                                                <dt><?= __('Sexo') ?></dt>
                                                <dd>
                                                    <?= h($people->sexo) ?>
                                                </dd>
                                            </dl>

                                            <dl class="dl-horizontal col-md-6">

                                                <dt><?= __('Email') ?></dt>
                                                <dd>
                                                    <?= h($people->email) ?>
                                                </dd>

                                                <dt><?= __('Pai') ?></dt>
                                                <dd>
                                                    <?= h($people->father_name) ?>
                                                </dd>
                                                <dt><?= __('Mãe') ?></dt>
                                                <dd>
                                                    <?= h($people->mother_name) ?>
                                                </dd>
                                                <dt><?= __('Telefone') ?></dt>
                                                <dd>
                                                    <?= h($people->phone) ?>
                                                </dd>
                                                <dt><?= __('Celular') ?></dt>
                                                <dd>
                                                    <?= h($people->cell_phone) ?>
                                                </dd>


                                            </dl>
                                        </div>


                                        <?= $this->element('Forms/peopleModal', ['data' => $people, 'url' => ['action' => 'edit', $people->id]]) ?>
                                    </div>
                                    <!--                                    Endereço-->
                                    <div class=" tab-pane" id="endereco">

                                        <div class="row">
                                            <dl class="dl-horizontal col-md-6">
                                                <dt><?= __('CEP') ?></dt>
                                                <dd>
                                                    <?= h($people->address->zipcode) ?>
                                                </dd>
                                                <dt><?= __('Endereço') ?></dt>
                                                <dd>
                                                    <?= h($people->address->street) ?>
                                                    , <?= h($people->address->number) ?>
                                                </dd>
                                                <dt><?= __('Complemento') ?></dt>
                                                <dd>
                                                    <?= h($people->address->complement) ?>
                                                </dd>
                                            </dl>

                                            <dl class="dl-horizontal col-md-6">
                                                <dt><?= __('Bairro') ?></dt>
                                                <dd>
                                                    <?= h($people->address->neighborhood) ?>
                                                </dd>
                                                <dt><?= __('Cidade') ?></dt>
                                                <dd>
                                                    <?= h($people->address->city) ?>
                                                </dd>
                                                <dt><?= __('Estado') ?></dt>
                                                <dd>
                                                    <?= h($people->address->state) ?>
                                                </dd>

                                            </dl>

                                        </div>
                                        <?= $this->element('Forms/addressModal', ['data' => $people, 'prefix' => 'address', 'url' => ['action' => 'edit', $people->id]]) ?>
                                    </div>
                                    <!--                                    SISTEMA-->
                                    <div class="tab-pane" id="system">
                                        <?php if ($people->user): ?>
                                            <dl class="dl-horizontal">
                                                <dt><?= __('Usuário') ?></dt>
                                                <dd>
                                                    <?= h($people->user->username) ?>
                                                </dd>

                                                <?php if ($people->user->password == '$2y$10$wPT7z.7HyMcLkL9zg4R93OZcJyRdDWHcqHCmLDDr1Ji1mP2t2.RlS'): ?>
                                                    <dt><?= __('Criar Senha') ?></dt>
                                                    <dd>
                                                        <?= $this->element('Forms/passwordModal', ['text' => 'Gerar', 'user' => $people->user, 'url' => ['controller' => 'users', 'action' => 'password', $people->user->id]]); ?>
                                                    </dd>
                                                <?php else: ?>
                                                    <dt><?= __('Alterar Senha') ?></dt>
                                                    <dd>
                                                        <?= $this->element('Forms/passwordModal', ['text' => 'Editar', 'user' => $people->user, 'url' => ['controller' => 'users', 'action' => 'password', $people->user->id]]); ?>
                                                    </dd>
                                                <?php endif; ?>
                                            </dl>
                                        <?php else: ?>
                                            <?= $this->element('Forms/userModal', ['user' => $user, 'prefix' => 'user', 'url' => ['action' => 'edit', $people->id]]); ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
