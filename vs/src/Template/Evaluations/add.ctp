<section class="content-header">
  <h1>
    Evaluation
    <small><?= __('Add') ?></small>
  </h1>
  <ol class="breadcrumb">
    <li>
    <?= $this->Html->link('<i class="fa fa-dashboard"></i> '.__('Back'), ['action' => 'index'], ['escape' => false]) ?>
    </li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"><?= __('Form') ?></h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?= $this->Form->create($evaluation, array('role' => 'form')) ?>
          <div class="box-body">
          <?php
            echo $this->Form->input('name');
            echo $this->Form->input('type_attendance');
            echo $this->Form->input('evaluation_attendance');
            echo $this->Form->input('percent_attendance');
            echo $this->Form->input('evaluation_period');
            echo $this->Form->input('quantity_period');
            echo $this->Form->input('calculate_total_year');
            echo $this->Form->input('calculate_final_note');
            echo $this->Form->input('period_minimum_note');
            echo $this->Form->input('annual_minimum_note');
            echo $this->Form->input('period_recuperation_minimum_note');
            echo $this->Form->input('anual_recuperation_minimum_note');
            echo $this->Form->input('annual_recovery');
            echo $this->Form->input('semi_annual_recovery');
            echo $this->Form->input('type');
            echo $this->Form->input('branch_id', ['options' => $branchs]);
          ?>
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <?= $this->Form->button(__('Save')) ?>
          </div>
        <?= $this->Form->end() ?>
      </div>
    </div>
  </div>
</section>

