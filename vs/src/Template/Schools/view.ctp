<section class="content-header">
  <h1>
    <?php echo __('School'); ?>
  </h1>
  <ol class="breadcrumb">
    <li>
    <?= $this->Html->link('<i class="fa fa-dashboard"></i> ' . __('Back'), ['action' => 'index'], ['escape' => false])?>
    </li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-md-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <i class="fa fa-info"></i>
                <h3 class="box-title"><?php echo __('Information'); ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <dl class="dl-horizontal">
                                                                                                                <dt><?= __('Name') ?></dt>
                                        <dd>
                                            <?= h($school->name) ?>
                                        </dd>
                                                                                                                                                            <dt><?= __('Cnpj') ?></dt>
                                        <dd>
                                            <?= h($school->cnpj) ?>
                                        </dd>
                                                                                                                                                            <dt><?= __('Color') ?></dt>
                                        <dd>
                                            <?= h($school->color) ?>
                                        </dd>
                                                                                                                                    
                                            
                                                                                                                                            
                                                                                                                                                                                                
                                            
                                    </dl>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- ./col -->
</div>
<!-- div -->

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <i class="fa fa-share-alt"></i>
                    <h3 class="box-title"><?= __('Related {0}', ['Branchs']) ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">

                <?php if (!empty($school->branchs)): ?>

                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                                                    
                                    <th>
                                    Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Name
                                    </th>
                                        
                                                                                                                                            
                                    <th>
                                    Cnpj
                                    </th>
                                        
                                                                    
                                    <th>
                                    Address Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Phone
                                    </th>
                                        
                                                                    
                                    <th>
                                    Email
                                    </th>
                                        
                                                                    
                                    <th>
                                    School Id
                                    </th>
                                        
                                                                    
                                <th>
                                    <?php echo __('Actions'); ?>
                                </th>
                            </tr>

                            <?php foreach ($school->branchs as $branchs): ?>
                                <tr>
                                                                        
                                    <td>
                                    <?= h($branchs->id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($branchs->name) ?>
                                    </td>
                                                                                                                                                
                                    <td>
                                    <?= h($branchs->cnpj) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($branchs->address_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($branchs->phone) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($branchs->email) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($branchs->school_id) ?>
                                    </td>
                                    
                                                                        <td class="actions">
                                    <?= $this->Html->link(__('View'), ['controller' => 'Branchs', 'action' => 'view', $branchs->id], ['class'=>'btn btn-info btn-xs']) ?>

                                    <?= $this->Html->link(__('Edit'), ['controller' => 'Branchs', 'action' => 'edit', $branchs->id], ['class'=>'btn btn-warning btn-xs']) ?>

                                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Branchs', 'action' => 'delete', $branchs->id], ['confirm' => __('Are you sure you want to delete # {0}?', $branchs->id), 'class'=>'btn btn-danger btn-xs']) ?>    
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                                    
                        </tbody>
                    </table>

                <?php endif; ?>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <i class="fa fa-share-alt"></i>
                    <h3 class="box-title"><?= __('Related {0}', ['Users']) ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">

                <?php if (!empty($school->users)): ?>

                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                                                    
                                    <th>
                                    Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Username
                                    </th>
                                        
                                                                    
                                    <th>
                                    Password
                                    </th>
                                        
                                                                                                                                            
                                    <th>
                                    School Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Active
                                    </th>
                                        
                                                                    
                                    <th>
                                    Access School
                                    </th>
                                        
                                                                    
                                    <th>
                                    Access Student
                                    </th>
                                        
                                                                    
                                    <th>
                                    Access Teacher
                                    </th>
                                        
                                                                    
                                    <th>
                                    Access Backoffice
                                    </th>
                                        
                                                                    
                                    <th>
                                    Branch Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Name
                                    </th>
                                        
                                                                    
                                <th>
                                    <?php echo __('Actions'); ?>
                                </th>
                            </tr>

                            <?php foreach ($school->users as $users): ?>
                                <tr>
                                                                        
                                    <td>
                                    <?= h($users->id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($users->username) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($users->password) ?>
                                    </td>
                                                                                                                                                
                                    <td>
                                    <?= h($users->school_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($users->active) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($users->access_school) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($users->access_student) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($users->access_teacher) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($users->access_backoffice) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($users->branch_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($users->name) ?>
                                    </td>
                                    
                                                                        <td class="actions">
                                    <?= $this->Html->link(__('View'), ['controller' => 'Users', 'action' => 'view', $users->id], ['class'=>'btn btn-info btn-xs']) ?>

                                    <?= $this->Html->link(__('Edit'), ['controller' => 'Users', 'action' => 'edit', $users->id], ['class'=>'btn btn-warning btn-xs']) ?>

                                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Users', 'action' => 'delete', $users->id], ['confirm' => __('Are you sure you want to delete # {0}?', $users->id), 'class'=>'btn btn-danger btn-xs']) ?>    
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                                    
                        </tbody>
                    </table>

                <?php endif; ?>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
