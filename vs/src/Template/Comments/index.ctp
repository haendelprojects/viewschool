<div class="panel-header panel-header-sm">
</div>


<div class="content">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">
                        <div class="float-right">
                            <?= $this->Html->link(__('Novo(a)'), ['action' => 'add'], ['class' => 'btn btn-default btn-sm ', 'escape' => false]) ?>
                        </div>
                    </h4>
                </div>


                <div class="card-body">
                    <?php if ($this->User->isSchool()) : ?>

                        <div class="box">
                            <div class="box-body table-responsive no-padding">


                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th><?= $this->Paginator->sort('title', 'Titulo') ?></th>
                                        <th><?= __('Avisos') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($comments as $comment): ?>
                                        <tr>
                                            <td><?= h($comment->title) ?></td>
                                            <td class="actions" style="white-space:nowrap">
                                                <?= $this->Html->link(__('Detalhes'), ['action' => 'view', $comment->id], ['class' => 'btn btn-info btn-xs']) ?>
                                                <?= $this->Html->link(__('Editar'), ['action' => 'edit', $comment->id], ['class' => 'btn btn-warning btn-xs']) ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>


                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer clearfix">
                                <ul class="pagination pagination-sm no-margin pull-right">
                                    <?php echo $this->Paginator->numbers(); ?>
                                </ul>
                            </div>
                        </div>


                    <?php else: ?>
                        <ul class="timeline">
                            <?php foreach ($comments as $comment): ?>
                                <li>
                                    <i class="fa fa-envelope bg-blue"></i>

                                    <div class="timeline-item">
                                        <span class="time"><i class="fa fa-clock-o"></i> <?= $comment->created ?></span>

                                        <h3 class="timeline-header"><a
                                                    href="#"><?= $comment->title ?></a> <?= $comment->user->name ?></h3>

                                        <div class="timeline-body">
                                            <?= $comment->text ?>
                                        </div>
                                    </div>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                    <!-- /.box -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.content -->
