<div class="panel-header panel-header-sm">
</div>


<div class="content">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">
                        <?= $this->Html->link('<i class="fa fa-arrow-left"></i> ' . __('Voltar'), ['action' => 'index'], ['escape' => false]) ?>
                    </h4>
                </div>


                <div class="card-body">
                    <?= $this->Form->create($comment, array('role' => 'form')) ?>
                    <div class="box-body">
                        <?php
                        echo $this->Form->input('title', ['label' => 'Titulo', 'required' => true]);
                        echo $this->Form->input('text', ['label' => 'Aviso', 'required' => true]);
                        echo $this->Form->input('people_id', ['empty' => 'Deixar este campo vazio caso seja uma mensagem para toda a escola.', 'label' => 'Aluno', 'options' => $peoples, 'required' => false]);
                        ?>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <?= $this->Form->button(__('Cadastrar')) ?>
                    </div>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</div>

