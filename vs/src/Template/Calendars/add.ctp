<div class="panel-header panel-header-sm">
</div>


<div class="content">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">

                    </h4>
                </div>


                <div class="card-body">
                    <?= $this->Form->create($calendar, array( 'role' => 'form')) ?>
                    <div class="modal-header">
                        <h5 class="modal-title">Evento</h5>
                    </div>
                    <div class="modal-body">
                        <?php
                        echo $this->Form->input('title', ['label' => 'Titulo', 'placeholder' => 'Titulo do Evento']);
                        echo $this->Form->input('text', ['label' => 'Descrição', 'type' => 'textarea', 'class' => 'textarea']);
                        echo $this->Form->input('start_dp', ['label' => 'Dia do Evento', 'empty' => true, 'default' => '', 'class' => 'datepicker form-control', 'type' => 'text']);
                        echo $this->Form->input('end_dp', ['label' => 'Fim do Evento', 'type' => 'text', 'placeholder' => 'Data Final do Evento', 'class' => 'datepicker']);
                        echo $this->Form->input('background', ['label' => 'Cor do evento', 'type' => 'color', 'placeholder' => 'Cor do evento no calendário']);
                        ?>
                    </div>
                    <div class="modal-footer">
                        <?= $this->Form->button(__('Salvar'), ['class' => 'btn btn-success']) ?>
                    </div>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</div>

