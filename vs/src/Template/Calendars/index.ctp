<div class="panel-header panel-header-sm">
</div>


<div class="content">


    <div class="row">
        <div class="col-md-3">
            <div class="card">
                <div class="card-header">
                </div>
                <div class="card-body">

                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <?= $this->Html->link('<i class="fa fa-calendar"> </i> Visualizar no calendário', ['action' => 'calendar'], ['escape' => false]); ?>
                        </li>
                        <li class="list-group-item">
                            <?= $this->Html->link('<i class="fa fa-plus"> </i> Adicionar Evento', ['action' => 'add'], ['escape' => false]); ?>
                        </li>
                    </ul>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
        <!-- /.col -->
        <div class="col-md-9">

            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">
                        <div class="float-right">
                            <?= $this->Html->link(__('Novo(a)'), ['action' => 'add'], ['class' => 'btn btn-default btn-sm ', 'escape' => false]) ?>
                        </div>
                    </h4>
                </div>


                <div class="card-body">

                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th><?= $this->Paginator->sort('title', 'Titulo') ?></th>
                            <th><?= $this->Paginator->sort('start', 'Inicia dia') ?></th>
                            <th><?= $this->Paginator->sort('end', 'Finaliza') ?></th>
                            <th><?= __('Ações') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($calendars as $calendar): ?>
                            <tr>
                                <td><?= h($calendar->title) ?></td>
                                <td><?= $this->Util->convertDateTimezone($calendar->start) ?></td>
                                <td><?= $this->Util->convertDateTimezone($calendar->end) ?></td>
                                <td class="actions" style="white-space:nowrap">
                                    <?= $this->Html->link(__('Detalhes'), ['action' => 'view', $calendar->id], ['class' => 'btn btn-info btn-xs']) ?>
                                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $calendar->id], ['class' => 'btn btn-warning btn-xs']) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right">
                        <?php echo $this->Paginator->numbers(); ?>
                    </ul>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</div>
<!-- /.content -->
