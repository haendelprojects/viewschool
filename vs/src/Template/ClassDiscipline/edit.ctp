<section class="content-header">
    <h1>
        Editar Disciplina de
        <small><?= __('Edit') ?></small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <?= $this->Html->link('<i class="fa fa-dashboard"></i> ' . __('Back'), ['action' => 'index'], ['escape' => false]) ?>
        </li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><?= __('Form') ?></h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <!-- /.box-header -->
                <!-- form start -->
                <?= $this->Form->create($classDiscipline, array('role' => 'form', 'align' => [
                    'sm' => [
                        'left' => 6,
                        'middle' => 6,
                        'right' => 12
                    ],
                    'md' => [
                        'left' => 3,
                        'middle' => 9,
                    ]
                ])) ?>
                <div class="box-body">

                    <?php
                    echo $this->Form->input('people_id', ['options' => $this->Util->listOptions('Peoples', ['name' => 'asc'], ['type' => 'PROFESSOR']), 'label' => 'Professor', 'required' => true]);
                    echo $this->Form->input('workload', ['label' => 'Carga Horária (Hrs)', 'required' => true]);
                    echo $this->Form->input('minimum_grade', ['label' => 'Nota Mínima', 'required' => true]);
                    ?>


                    <table class="table table-bordered" id="table-more-time">
                        <tr>
                            <th>Dia da Semana</th>
                            <th>Hora Inicial</th>
                            <th>Hora Final</th>
                            <th></th>
                        </tr>
                        <tr>
                            <td> <?= $this->Form->input('times.0.day_week', ['options' => ['SEG' => 'Segunda', 'TER' => 'Terça', 'QUA' => 'Quarta', 'QUI' => 'Quinta', 'SEX' => 'Sexta'], 'label' => false]); ?> </td>
                            <td> <?= $this->Form->input('times.0.init_time', ['label' => false]); ?></td>
                            <td> <?= $this->Form->input('times.0.end_time', ['label' => false]); ?></td>
                            <td></td>
                        </tr>
                    </table>

                    <a href="javascript:void(0)" class="btn btn-success btn-xs" id="more-time">Mais
                        Horário de Aula</a>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <?= $this->Form->button(__('Save')) ?>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</section>

