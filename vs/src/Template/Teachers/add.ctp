<div class="panel-header panel-header-sm">
</div>


<div class="content">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">

                    </h4>
                </div>

                <div class="card-body">
                    <?= $this->Form->create($people, array('role' => 'form')) ?>
                    <?= $this->Form->input('type', ['value' => 'PROFESSOR', 'label' => 'Tipo', 'type' => 'hidden']); ?>
                    <div class="row">
                        <!-- left column -->

                        <div class="col-md-8"> <?= $this->Form->input('name', ['label' => 'Nome']); ?> </div>
                        <div class="col-md-4"> <?= $this->Form->input('cpf', ['label' => 'CPF', 'class' => 'cpf', 'placeholder' => '000.000.000-00']); ?> </div>
                        <div class="col-md-6"> <?= $this->Form->input('phone', ['label' => 'Telefone', 'class' => 'sp_celphones', 'maxlength' => 16]); ?> </div>
                        <div class="col-md-6"> <?= $this->Form->input('cell_phone', ['label' => 'Celular', 'class' => 'sp_celphones', 'maxlength' => 16]); ?> </div>
                        <div class="col-md-6"> <?= $this->Form->input('birth', ['label' => 'Nascimento', 'empty' => true, 'default' => '', 'class' => 'datepicker form-control', 'type' => 'text']); ?> </div>
                        <div class="col-md-6"> <?= $this->Form->input('sexo', ['label' => 'Sexo', 'options' => ['FEMININO' => 'Feminino', 'MASCULINO' => 'Masculino']]); ?> </div>
                        <div class="col-md-12"> <?= $this->Form->input('email', ['label' => 'E-mail']); ?> </div>


                        <h4 class="col-md-12"><?= __('Endereço') ?></h4>


                        <div class="col-md-3"> <?= $this->Form->input('address.zipcode', ['label' => 'CEP']); ?> </div>
                        <div class="col-md-7"> <?= $this->Form->input('address.street', ['label' => 'Endereço']); ?> </div>
                        <div class="col-md-2"> <?= $this->Form->input('address.number', ['label' => 'Número']); ?> </div>
                        <div class="col-md-12"> <?= $this->Form->input('address.neighborhood', ['label' => 'Bairro']); ?> </div>
                        <div class="col-md-6"> <?= $this->Form->input('address.city', ['label' => 'Cidade']); ?> </div>
                        <div class="col-md-6"> <?= $this->Form->input('address.state', ['label' => 'Estado']); ?> </div>
                        <div class="col-md-12"> <?= $this->Form->input('address.complement', ['label' => 'Complemento']); ?> </div>


                        <div class="col-md-12">
                            <?= $this->Form->button(__('Cadastrar')) ?>
                        </div>


                    </div>
                </div>
            </div>

            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
