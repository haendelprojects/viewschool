<header class="main-header">
    <nav class="navbar navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <a href="<?php echo $this->Url->build('/'); ?>" class="navbar-brand">
                    <span><img src="/img/logo.png" alt="Marca FindUP"
                               style="height: 14px; vertical-align: baseline;"></span>
                    Find<b>UP</b></a>
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#navbar-collapse">
                    <i class="fa fa-bars"></i>
                </button>
            </div>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="#"><b><?= $this->User->getCorporateName() ?></b></a>
                    </li>
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->
                            <?php echo $this->Html->image($this->User->getPhoto(), array('class' => 'user-image', 'alt' => 'User Image')); ?>
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs"><?= $this->User->getName() ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li style="margint-top:10px;">
                                <a href="/erickhaendel" class="DashUserDropdown-userInfoLink js-nav"
                                   data-nav="view_profile" role="menuitem">
                                    <b class="fullname"><?= $this->User->getName() ?></b><br>
                                    <span><small><?= $this->User->getEmail() ?></small></span>
                                    <p class="name">
                                        <small> <b><?= $this->User->getCorporateName() ?> </b></small>
                                    </p>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="/users/profile"> <i class="fas fa-user"></i> Perfil</a>
                            </li>
                            <li>
                                <a href="/users/profile"> <i class="fas fa-briefcase"></i> Empresa</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="/users/logout">Sair</a>
                            </li>
                            <li class="divider"></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-custom-menu -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <nav class="navbar navbar-static-top navbar-dark" style="    z-index: -999; background-color: #222d32;">
        <div class="container">
            <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="">
                        <?= $this->Html->link('<i class="fas fa-home"></i> <span>Início</span>', ['controller' => 'dashboard', 'action' => 'index', 'plugin' => false], ['escape' => false]); ?>
                    </li>
                    <li class="">
                        <?= $this->Html->link('<i class="fas fa-list"></i>  <span>Ocorrências</span>', ['controller' => 'occurrences', 'action' => 'index', 'plugin' => false], ['escape' => false]); ?>
                    </li>

                    <li class="">
                        <?= $this->Html->link('<i class="fas fa-briefcase"></i>  <span>Contratos</span>', ['controller' => 'contracts', 'action' => 'index', 'plugin' => false], ['escape' => false]); ?>
                    </li>


                    <li class="">
                        <?= $this->Html->link('<i class="fas fa-chart-area"></i>  <span>Relatórios</span>', ['controller' => 'bi', 'action' => 'index', 'plugin' => false], ['escape' => false]); ?>
                    </li>


                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fas fa-cogs"></i>
                            Configurações <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li> <?= $this->Html->link('<span>Filiais</span>', ['controller' => 'addresses', 'action' => 'index', 'plugin' => false], ['escape' => false]); ?></li>
                            <li><?= $this->Html->link(' <span>Usuários</span>', ['controller' => 'clients', 'action' => 'index', 'plugin' => false], ['escape' => false]); ?></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /.container-fluid -->
    </nav>
</header>
