<div class="col-md-12">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Dados de SLA </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <div class="col-md-8">
                    <p class="text-center">
                        <strong>Histórico 12 meses</strong>
                    </p>

                    <div class="chart">
                        <!-- Sales Chart Canvas -->
                        <div class="chart" id="bar-chart" style="height: 200px;"></div>
                    </div>
                    <!-- /.chart-responsive -->
                </div>
                <!-- /.col -->
                <div class="col-md-4">
                    <p class="text-center">
                        <strong>SLA em Barra</strong>
                    </p>


                    <div class="progress-group">
                        <span class="progress-text">Dentro do Prazo em 30 dias -
                            <?= $this->Html->link('Ver <i class="fa fa-arrow-circle-right"></i>',
                                ['controller' => 'occurrences', 'action' => 'index', 'occurrenceTimeSla' => 'P1M', 'violated'=> false,'status' => ['ON_SERVICE', 'AWAITING_PAYMENT', 'AWAITING_VALIDATION', 'PENDING_PAYMENT', 'FINISHED', 'UNPRODUCTIVE', 'INVALID', 'OPERATION_VALIDATION']],
                                ['escape' => false, 'class' => 'small-box-footer', 'target' => '_blank']) ?></span>
                        <span class="progress-number"><b>
                                <?= (isset($sla['month']['ok'])) ? $sla['month']['ok'] : 0; ?></b>/
                            <?php
                            $ok = (isset($sla['month']['ok'])) ? $sla['month']['ok'] : 0;
                            $vio = (isset($sla['month']['violated'])) ? $sla['month']['violated'] : 0;
                            echo $ok + $vio; ?></span>

                        <div class="progress sm">
                            <div class="progress-bar progress-bar-green"
                                 style="width:  <?php
                                 $ok = (isset($sla['month']['ok'])) ? $sla['month']['ok'] : 0;
                                 $vio = (isset($sla['month']['violated'])) ? $sla['month']['violated'] : 0;
                                 echo number_format(($ok / ($ok + $vio)) * 100, 2); ?>%"></div>
                        </div>
                    </div>
                    <div class="progress-group">
                        <span class="progress-text">Violado em 30 dias -
                            <?= $this->Html->link('Ver <i class="fa fa-arrow-circle-right"></i>',
                                ['controller' => 'occurrences', 'action' => 'index', 'occurrenceTimeSla' => 'P1M', 'violated'=> true, 'status' => ['ON_SERVICE', 'AWAITING_PAYMENT', 'AWAITING_VALIDATION', 'PENDING_PAYMENT', 'FINISHED', 'UNPRODUCTIVE', 'INVALID', 'OPERATION_VALIDATION']],
                                ['escape' => false, 'class' => 'small-box-footer', 'target' => '_blank']) ?>
                        </span>
                        <span class="progress-number"><b>
                                <?= (isset($sla['month']['violated'])) ? $sla['month']['violated'] : 0; ?></b>/
                            <?php
                            $ok = (isset($sla['month']['ok'])) ? $sla['month']['ok'] : 0;
                            $vio = (isset($sla['month']['violated'])) ? $sla['month']['violated'] : 0;
                            echo $ok + $vio; ?></span>
                        </span>

                        <div class="progress sm">
                            <div class="progress-bar progress-bar-red"
                                 style="width:
                                 <?php
                                 $ok = (isset($sla['month']['ok'])) ? $sla['month']['ok'] : 0;
                                 $vio = (isset($sla['month']['violated'])) ? $sla['month']['violated'] : 0;
                                 echo number_format(($vio / ($ok + $vio)) * 100, 2); ?>%"></div>
                        </div>
                    </div>

                    <div class="progress-group">
                        <span class="progress-text">Dentro do Prazo em 7 dias -
                            <?= $this->Html->link('Ver <i class="fa fa-arrow-circle-right"></i>',
                                ['controller' => 'occurrences', 'action' => 'index', 'occurrenceTimeSla' => 'P7D', 'violated'=> false,'status' => ['ON_SERVICE', 'AWAITING_PAYMENT', 'AWAITING_VALIDATION', 'PENDING_PAYMENT', 'FINISHED', 'UNPRODUCTIVE', 'INVALID', 'OPERATION_VALIDATION']],
                                ['escape' => false, 'class' => 'small-box-footer', 'target' => '_blank']) ?></span>
                        <span class="progress-number"><b>
                                <?= (isset($sla['week']['ok'])) ? $sla['week']['ok'] : 0; ?></b>/
                            <?php


                            $ok = (isset($sla['week']['ok'])) ? $sla['week']['ok'] : 0;
                            $vio = (isset($sla['week']['violated'])) ? $sla['week']['violated'] : 0;


                            echo $ok + $vio; ?></span>


                        <div class="progress sm">
                            <div class="progress-bar progress-bar-green"
                                 style="width:  <?php
                                 $ok = (isset($sla['week']['ok'])) ? $sla['week']['ok'] : 0;
                                 $vio = (isset($sla['week']['violated'])) ? $sla['week']['violated'] : 0;
                                 echo number_format(($ok / ($ok + $vio)) * 100, 2); ?>%"></div>
                        </div>
                    </div>
                    <div class="progress-group">
                        <span class="progress-text">Violado em 7 dias -
                            <?= $this->Html->link('Ver <i class="fa fa-arrow-circle-right"></i>',
                                ['controller' => 'occurrences', 'action' => 'index', 'occurrenceTimeSla' => 'P7D', 'violated'=> true,'status' => ['ON_SERVICE', 'AWAITING_PAYMENT', 'AWAITING_VALIDATION', 'PENDING_PAYMENT', 'FINISHED', 'UNPRODUCTIVE', 'INVALID', 'OPERATION_VALIDATION']],
                                ['escape' => false, 'class' => 'small-box-footer', 'target' => '_blank']) ?></span>
                        <span class="progress-number"><b>
                                <?= (isset($sla['week']['violated'])) ? $sla['week']['violated'] : 0; ?></b>/
                            <?php
                            $ok = (isset($sla['week']['ok'])) ? $sla['week']['ok'] : 0;
                            $vio = (isset($sla['week']['violated'])) ? $sla['week']['violated'] : 0;
                            echo $ok + $vio; ?></span>
                        </span>


                        <div class="progress sm">
                            <div class="progress-bar progress-bar-green"
                                 style="width:  <?php
                                 $ok = (isset($sla['week']['ok'])) ? $sla['week']['ok'] : 0;
                                 $vio = (isset($sla['week']['violated'])) ? $sla['week']['violated'] : 0;
                                 echo number_format(($vio / ($ok + $vio)) * 100, 2); ?>%"></div>
                        </div>
                    </div>


                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- ./box-body -->
        <div class="box-footer">
            <div class="row">
                <div class="col-sm-3 col-xs-6">
                    <div class="description-block border-right">
                        <h5 class="description-header text-green"><?= (isset($sla['month']['ok'])) ? $sla['month']['ok'] : 0; ?></h5>
                        <span class="description-text">NO PRAZO MÊs</span>
                    </div>
                    <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                    <div class="description-block border-right">
                        <h5 class="description-header text-yellow"><?= (isset($sla['month']['violated'])) ? $sla['month']['violated'] : 0; ?></h5>
                        <span class="description-text">VIOLADO MÊS</span>
                    </div>
                    <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                    <div class="description-block border-right">
                        <h5 class="description-header">
                            <?php


                            $ok = (isset($sla['month']['ok'])) ? $sla['month']['ok'] : 0;
                            $vio = (isset($sla['month']['violated'])) ? $sla['month']['violated'] : 0;


                            echo $ok + $vio; ?>

                        </h5>
                        <span class="description-text">TOTAL</span>
                    </div>
                    <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                    <div class="description-block">
                        <h5 class="description-header">

                            <?php
                            $ok = (isset($sla['month']['ok'])) ? $sla['month']['ok'] : 0;
                            $vio = (isset($sla['month']['violated'])) ? $sla['month']['violated'] : 0;
                            echo number_format(($vio / ($ok + $vio)) * 100, 2);
                            ?>
                            %
                        </h5>
                        <span class="description-text">VIOLADO</span>
                    </div>
                    <!-- /.description-block -->
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.box-footer -->
    </div>
    <!-- /.box -->
</div>