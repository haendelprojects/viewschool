<div class="col-md-12">
    <h5>Ocorrências abertas sem técnico</h5>
</div>


<div class=" col-md-12" id="values">
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3><?= $occurrences_accepted['15'] ?></h3>
                    <p>15min</p>
                </div>
                <div class="icon">
                    <i class="ion ion-clock"></i>
                </div>
                <?= $this->Html->link('Mais informações <i class="fa fa-arrow-circle-right"></i>',
                    ['controller' => 'occurrences', 'action' => 'index', 'occurrenceTimeWaitCount' => '0-PT15M', 'status' => ['OPENED']],
                    ['escape' => false, 'class' => 'small-box-footer', 'target' => '_blank']) ?>
            </div>
            <!-- /.info-box -->
        </div>


        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="small-box <?= $this->Util->classColorMetric($occurrences_accepted['15-60'], [10, 0], [5, 9], [0, 4]) ?>">
                <div class="inner">
                    <h3><?= $occurrences_accepted['15-60'] ?></h3>
                    <p>DE 15' à 60'</p>
                </div>
                <div class="icon">
                    <i class="ion ion-clock"></i>
                </div>
                <?= $this->Html->link('Mais informações <i class="fa fa-arrow-circle-right"></i>',
                    ['controller' => 'occurrences', 'action' => 'index', 'occurrenceTimeWaitCount' => 'PT15M-PT60M', 'status' => ['OPENED']],
                    ['escape' => false, 'class' => 'small-box-footer', 'target' => '_blank']) ?>

            </div>
            <!-- /.info-box -->
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="small-box  <?= $this->Util->classColorMetric($occurrences_accepted['60-24'], [10, 0], [5, 9], [0, 4]) ?>">
                <div class="inner">
                    <h3><?= $occurrences_accepted['60-24'] ?></h3>
                    <p>De 60' à 24h</p>
                </div>
                <div class="icon">
                    <i class="ion ion-clock"></i>
                </div>
                <?= $this->Html->link('Mais informações <i class="fa fa-arrow-circle-right"></i>',
                    ['controller' => 'occurrences', 'action' => 'index', 'occurrenceTimeWaitCount' => 'PT60M-P1D', 'status' => ['OPENED']],
                    ['escape' => false, 'class' => 'small-box-footer', 'target' => '_blank']) ?>
            </div>

            <!-- /.info-box -->
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="small-box  <?php if ($occurrences_accepted['24'] > 5 && $occurrences_accepted['24'] < 10) {
                echo 'bg-yellow';
            } elseif ($occurrences_accepted['24'] > 1) {
                echo 'bg-red';
            } else {
                echo 'bg-aqua';
            }
            ?>">
                <div class="inner">
                    <h3><?= $occurrences_accepted['24'] ?></h3>
                    <p>Mais de 24h</p>
                </div>
                <div class="icon">
                    <i class="ion ion-clock"></i>
                </div>
<?= $this->Html->link('Mais informações <i class="fa fa-arrow-circle-right"></i>',
    ['controller' => 'occurrences', 'action' => 'index', 'occurrenceTimeWaitCount' => 'P1D-0', 'status' => ['OPENED']],
    ['escape' => false, 'class' => 'small-box-footer', 'target' => '_blank']) ?>
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
    </div>

</div>
