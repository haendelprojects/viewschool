<div class="col-md-12">
    <h5>Ocorrências No-show</h5>
</div>


<div class=" col-md-12" id="values">
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3><?= $occurrences_accepted['20'] ?></h3>
                    <p>20'</p>
                </div>
                <div class="icon">
                    <i class="ion ion-android-alarm-clock"></i>
                </div>
                <?= $this->Html->link('Mais informações <i class="fa fa-arrow-circle-right"></i>',
                    ['controller' => 'occurrences', 'action' => 'index', 'occurrenceTimeWaitCount' => '0-PT20M', 'status' => ['ACCEPTED']],
                    ['escape' => false, 'class' => 'small-box-footer', 'target' => '_blank']) ?>
            </div>
            <!-- /.info-box -->
        </div>


        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="small-box bg-red">
                <div class="inner">
                    <h3><?= $occurrences_accepted['20-60'] ?></h3>
                    <p>DE 20' à 60'</p>
                </div>
                <div class="icon">
                    <i class="ion ion-alert-circled"></i>
                </div>
                <?= $this->Html->link('Mais informações <i class="fa fa-arrow-circle-right"></i>',
                    ['controller' => 'occurrences', 'action' => 'index', 'occurrenceTimeWaitCount' => 'PT20M-PT60M', 'status' => ['ACCEPTED']],
                    ['escape' => false, 'class' => 'small-box-footer', 'target' => '_blank']) ?>
            </div>
            <!-- /.info-box -->
        </div>

        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="small-box  bg-red">
                <div class="inner">
                    <h3><?= $occurrences_accepted['60'] ?></h3>
                    <p>Mais de 1h</p>
                </div>
                <div class="icon">
                    <i class="ion ion-nuclear"></i>
                </div>
                <?= $this->Html->link('Mais informações <i class="fa fa-arrow-circle-right"></i>',
                    ['controller' => 'occurrences', 'action' => 'index', 'occurrenceTimeWaitCount' => 'PT60M-0', 'status' => ['ACCEPTED']],
                    ['escape' => false, 'class' => 'small-box-footer', 'target' => '_blank']) ?>
            </div>

            <!-- /.info-box -->
        </div>
    </div>

</div>
