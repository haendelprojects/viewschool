<div class="col-md-12">
    <h5>Custo e Receita</h5>
</div>

<div class=" col-md-12" id="values">
    <div class="row">

        <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="info-box bg-light-blue">
                <span class="info-box-icon"><i class="fa fa-globe"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Chamados no mês</span>
                    <span class="info-box-number"><?= $occurrences_count ?></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>

        <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="info-box bg-yellow">
                <span class="info-box-icon"><i class="fa fa-dollar"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Custo Real</span>
                    <span class="info-box-number">$ <?= number_format($occurrences_count * 68, 2) ?></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>

        <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="info-box bg-aqua">
                <span class="info-box-icon"><i class="fa fa-calendar-plus-o"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">UPs Comprados</span>
                    <span class="info-box-number"><?= $all_month['spare_points'] ?>                        </span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>

        <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="info-box bg-yellow">
                <span class="info-box-icon"><i class="fa fa-dollar"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Custo Previsto</span>
                    <span class="info-box-number">$ <?= number_format($all_month['spare_points'] * 68, 2) ?></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
    </div>

</div>