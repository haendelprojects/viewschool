<div class="col-md-6">
    <div class="row">


        <div class="col-md-6 col-sm-6 col-xs-12">


            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3><?= number_format(($qnt_reschedule / $total_occurrences) * 100, 2) ?>%</h3>
                    <p>Reagendamentos </br>Em um Total de <?= $total_occurrences ?> atendimentos</p>
                </div>
                <div class="icon">
                    <i class="ion ion-android-calendar"></i>
                </div>
<!--                <a href="#" class="small-box-footer">Mais informações <i class="fa fa-arrow-circle-right"></i></a>-->
            </div>
            <!-- /.info-box -->
        </div>

        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3><?= number_format(($qnt_rejected / $total_occurrences) * 100, 2) ?>%</h3>
                    <p>Rejeição </br>Em um Total de <?= $total_occurrences ?> atendimentos</p>
                </div>
                <div class="icon">
                    <i class="ion ion-android-cancel"></i>
                </div>
<!--                <a href="#" class="small-box-footer">Mais informações <i class="fa fa-arrow-circle-right"></i></a>-->
            </div>


            <!-- /.info-box -->
        </div>

        <div class="col-md-6">

            <div class="info-box bg-blue">
                <span class="info-box-icon"><i class="ion ion-clock"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Média Atendimento</span>
                    <span class="info-box-number"> <?= $this->Util->minutesToHours($avg_occurrence) ?></span>
                </div>
                <!-- /.info-box-content -->
            </div>
        </div>
        <div class="col-md-6">

            <div class="info-box  <?= ($avg_accepted > 60) ? 'bg-red' : 'bg-blue' ?>" data-toggle="tooltip" data-placement="left"
                 title="Média para Aceitar Chamado">
                <span class="info-box-icon"><i class="ion ion-clock"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Média para Aceitar Chamado</span>
                    <span class="info-box-number"><?= $this->Util->minutesToHours($avg_accepted) ?></span>
                </div>
                <!-- /.info-box-content -->
            </div>


        </div>
    </div>
    <!-- /.box -->
</div>