<footer class="footer">
    <div class="container-fluid">
        <nav>
            <ul>
                <li>
                    <a href="https://viewschool.com.br">
                        ViewSchool
                    </a>
                </li>
                <li>
                    <a href="https://findup.com.br/sobre">
                        Sobre Nós
                    </a>
                </li>
                <li>
                    <a href="https://findup.com.br/blog">
                        Blog
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</footer>