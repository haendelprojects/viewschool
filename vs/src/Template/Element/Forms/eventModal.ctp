<?php $prefix = (isset($prefix)) ? $prefix . '.' : '' ?>
<?php $text = (isset($text)) ? $text : '<i class="fa fa-plus"></i>  Adicionar Evento' ?>
<?php $id = (isset($id)) ? '-' . $id : '' ?>


<a href="javascript:void(0)" data-toggle="modal"
        data-target="#modal-event<?= $id ?>"><?= $text ?>
</a>


<!-- Modal -->
<div id="modal-event<?= $id ?>" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <?= $this->Form->create($data, array('url' => $url, 'role' => 'form')) ?>
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal">&times;</button>
                <h5 class="modal-title">Evento</h5>
            </div>
            <div class="modal-body">
                <?php
                echo $this->Form->input('title', ['label' => 'Titulo', 'placeholder' => 'Titulo do Evento']);
                echo $this->Form->input('text', ['label' => 'Descrição', 'type' => 'textarea', 'class' => 'textarea']);
                echo $this->Form->input('start', ['label' => 'Nascimento', 'empty' => true, 'default' => '', 'class' => 'datepicker form-control', 'type' => 'text']);
                echo $this->Form->input('end', ['label' => 'Fim','type'=>'text', 'placeholder' => 'Data Final do Evento', 'class' => 'datepicker']);
                echo $this->Form->input('background', ['label' => 'Cor do evento', 'type' => 'color', 'placeholder' => 'Cor do evento no calendário']);
                ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Cancelar
                </button>
                <?= $this->Form->button(__('Salvar'), ['class' => 'btn btn-success']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>

    </div>

    <?php
    $this->Html->css(['AdminLTE./plugins/datepicker/datepicker3',
        'AdminLTE./plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min'],
        ['block' => 'css']);

    $this->Html->script(['AdminLTE./plugins/input-mask/jquery.inputmask',
        'AdminLTE./plugins/input-mask/jquery.inputmask.date.extensions',
        'AdminLTE./plugins/datepicker/bootstrap-datepicker',
        'AdminLTE./plugins/datepicker/locales/bootstrap-datepicker.pt-BR',
        'AdminLTE./plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min',
        'https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js',
    ],
        ['block' => 'script']);
    ?>
    <?php $this->start('scriptBottom'); ?>
    <script>
        $(function () {
            //Datemask mm/dd/yyyy
            $(".datepicker")
                .inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"})
                .datepicker({
                    language: 'en',
                    format: 'mm/dd/yyyy'
                });


        });

        $(function () {
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            CKEDITOR.replace('editor1');
            //bootstrap WYSIHTML5 - text editor
            $(".textarea").wysihtml5();
        });
    </script>
    <?php $this->end(); ?>

</div>

