<?php $prefix = (isset($prefix)) ? $prefix . '.' : '' ?>


<button type="button" class="btn btn-warning btn-xs" data-toggle="modal"
        data-target="#modal-address"><i class="fa fa-pencil"></i> Editar Informaçoes
</button>

<!-- Modal -->
<div id="modal-address" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <?= $this->Form->create($data, array('url' => $url, 'role' => 'form')) ?>
            <div class="modal-header">
                <h5 class="modal-title">Editar dados</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="box-body">
                    <?php
                    echo $this->Form->input('name', ['label' => 'Nome', 'required' => true]);
                    echo $this->Form->input('cpf', ['label' => 'CPF', 'class' => 'cpf']);
                    echo $this->Form->input('phone', ['label' => 'Telefone', 'class' => 'sp_celphones']);
                    echo $this->Form->input('cell_phone', ['label' => 'Celular', 'class' => 'sp_celphones']);
                    echo $this->Form->input('birth', ['label' => 'Nascimento', 'class' => 'datepicker form-control', 'type' => 'text', 'value' => $people->birth->format('d/m/Y')]);
                    echo $this->Form->input('type', ['value' => 'ALUNO', 'label' => 'Tipo', 'type' => 'hidden']);
                    echo $this->Form->input('sexo', ['label' => 'Sexo', 'options' => ['FEMININO' => 'Feminino', 'MASCULINO' => 'Masculino']]);
                    echo $this->Form->input('father_name', ['label' => 'Nome do Pai']);
                    echo $this->Form->input('mother_name', ['label' => 'Nome da Mãe']);
                    echo $this->Form->input('email', ['label' => 'E-mail']);
                    ?>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Cancelar
                </button>
                <?= $this->Form->button(__('Salvar'), ['class' => 'btn btn-success']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>

    </div>

</div>

