<?php $prefix = (isset($prefix)) ? $prefix . '.' : '' ?>

<button type="button" class="btn btn-info btn-xs " data-toggle="modal" data-target="#modal-values">
    <i class="fa fa-plus"></i> Adicionar Disciplina a Classe
</button>

<!-- Modal -->
<div id="modal-values" class="modal fade " role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <?= $this->Form->create($this->Util->entityForm('Disciplines'), array('url' => ['controller' => 'ClassDiscipline', 'action' => 'add'], 'role' => 'form')) ?>
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal">&times;
                </button>
                <h5 class="modal-title">Nova Disciplina</h5>
            </div>
            <div class="modal-body">
                <div class="box-body">
                    <?php
                    echo $this->Form->input('class_id', ['value' => $class_id, 'type' => 'hidden']);
                    echo $this->Form->input('discipline_id', ['options' => $this->Util->listOptions(), 'label' => 'Disciplinas', 'required' => true]);
                    echo $this->Form->input('people_id', ['options' => $this->Util->listOptions('Peoples', ['name' => 'asc'], ['type' => 'PROFESSOR']), 'label' => 'Professor', 'required' => true]);
                    echo $this->Form->input('workload', ['label' => 'Carga Horária (Hrs)', 'required' => true]);
                    echo $this->Form->input('minimum_grade', ['label' => 'Nota Mínima', 'required' => true]);
                    ?>
                </div>


                <table class="table table-bordered" id="table-more-time">
                    <tr>
                        <th width="200">Dia da Semana</th>
                        <th>Hora Inicial</th>
                        <th>Hora Final</th>
                        <th></th>
                    </tr>
                    <tr>
                        <td> <?= $this->Form->input('times.0.day_week', ['options' => ['SEG' => 'Segunda', 'TER' => 'Terça', 'QUA' => 'Quarta', 'QUI' => 'Quinta', 'SEX' => 'Sexta'], 'label' => false]); ?> </td>
                        <td> <?= $this->Form->input('times.0.init_time', ['label' => false]); ?></td>
                        <td> <?= $this->Form->input('times.0.end_time', ['label' => false]); ?></td>
                        <td></td>
                    </tr>
                </table>

                <a href="javascript:void(0)" class="btn btn-success btn-xs" id="more-time">Mais
                    Horário de Aula</a>

                <!-- /.box-body -->
                <div class="box-footer">

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Cancelar
                </button>
                <?= $this->Form->button(__('Salvar'), ['class' => 'btn btn-success']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>

    </div>
</div>


<script>

        var btnMoreTime = document.getElementById('more-time');
        var countCells = 1;

        btnMoreTime.addEventListener('click', moreTimes);

        function moreTimes() {
            var table = document.getElementById('table-more-time');
            var tr = document.createElement('tr');

            tr.id = 'cell-' + countCells;

            tr.innerHTML = '' +
                '<td> <div class="form-group select"><div class="col-sm-6 col-md-9"><select name="times[' + countCells + '][day_week]" id="times-0-day-week" class="form-control"><option value="SEG">Segunda</option><option value="TER">Terça</option><option value="QUA">Quarta</option><option value="QUI">Quinta</option><option value="SEX">Sexta</option></select></div></div> </td>' +
                '<td> <div class="form-group text"><div class="col-sm-6 col-md-9"><input type="text" name="times[' + countCells + '][init_time]" id="times-0-init-time" class="form-control"></div></div></td>' +
                '<td> <div class="form-group text"><div class="col-sm-6 col-md-9"><input type="text" name="times[' + countCells + '][end_time]" id="times-0-end-time" class="form-control"></div></div></td>' +
                '<td><a href="javascript:void(0)" class="btn btn-warning btn-xs" onclick="removeElement(\'cell-' + countCells + '\')">Remover</a></td>';
            table.appendChild(tr);
            countCells++;
        }

        function removeElement(elementId) {
            // Removes an element from the document
            var element = document.getElementById(elementId);
            element.parentNode.removeChild(element);
        }


</script>


