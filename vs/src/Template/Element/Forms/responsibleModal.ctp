<?php $prefix = (isset($prefix)) ? $prefix . '.' : '' ?>
<?php $text = (isset($text)) ? $text : '<i class="fa fa-plus"></i>  Adicionar Responsável' ?>
<?php $id = (isset($id)) ? '-' . $id : '' ?>


<button type="button" class="btn btn-warning btn-xs" data-toggle="modal"
        data-target="#modal-responsible<?= $id ?>"><?= $text ?>
</button>

<!-- Modal -->
<div id="modal-responsible<?= $id ?>" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <?= $this->Form->create($data, array('url' => $url, 'role' => 'form')) ?>
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal">&times;</button>
                <h5 class="modal-title">Informações</h5>
            </div>
            <div class="modal-body">

                <div class="box-body">
                    <?php
                    echo $this->Form->input('name', ['label' => 'Nome']);
                    echo $this->Form->input('cpf', ['label' => 'CPF', 'class' => 'cpf']);
                    echo $this->Form->input('phone', ['label' => 'Telefone', 'class' => 'phone', 'maxlength' => 15]);
                    echo $this->Form->input('cell_phone', ['label' => 'Celular', 'class' => 'sp_cellphone', 'maxlength' => 15]);
                    echo $this->Form->input('type', ['value' => 'RESPONSAVEL', 'label' => 'Tipo', 'type' => 'hidden']);
                    echo $this->Form->input('sexo', ['label' => 'Sexo', 'options' => ['FEMININO' => 'Feminino', 'MASCULINO' => 'Masculino']]);
                    echo $this->Form->input('email', ['label' => 'E-mail']);
                    echo $this->Form->input('responsible_educational', ['label' => 'Responsável Pedagógico', 'options' => ['Não', 'Sim']]);
                    echo $this->Form->input('responsible_financial', ['label' => 'Responsável Financeiro', 'options' => ['Não', 'Sim']]);
                    echo $this->Form->input('people_id', ['type' => 'hidden', 'value' => $people_id]);
                    ?>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Cancelar
                </button>
                <?= $this->Form->button(__('Salvar'), ['class' => 'btn btn-success']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>

    </div>

</div>

