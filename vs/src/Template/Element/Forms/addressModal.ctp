<?php $prefix = (isset($prefix)) ? $prefix . '.' : '' ?>


<button type="button" class="btn btn-warning btn-xs" data-toggle="modal"
        data-target="#modal-addresss"><i class="fa fa-pencil"></i> Editar Endereço
</button>

<!-- Modal -->
<div id="modal-addresss" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <?= $this->Form->create($data, array('url' => $url, 'role' => 'form'
            )) ?>
            <div class="modal-header">

                <h5 class="modal-title">Editar Endereço</h5>
                <button type="button" class="close"
                        data-dismiss="modal">&times;
                </button>
            </div>
            <div class="modal-body">

                <div class="box-body">
                    <?php
                    echo $this->Form->input($prefix . 'zipcode', ['label' => 'CEP', 'required' => true]);
                    echo $this->Form->input($prefix . 'street', ['class' => 'form-control', 'label' => 'Rua', 'required' => true]);
                    echo $this->Form->input($prefix . 'number', ['class' => 'form-control', 'label' => 'Número']);
                    echo $this->Form->input($prefix . 'complement', ['class' => 'form-control', 'label' => 'Complemento']);
                    echo $this->Form->input($prefix . 'neighborhood', ['class' => 'form-control', 'label' => 'Bairro', 'required' => true]);
                    echo $this->Form->input($prefix . 'city', ['class' => 'form-control', 'label' => 'Cidade', 'required' => true]);
                    echo $this->Form->input($prefix . 'state', ['class' => 'form-control', 'label' => 'Estado', 'required' => true]);
                    ?>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Cancelar
                </button>
                <?= $this->Form->button(__('Salvar'), ['class' => 'btn btn-success']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

