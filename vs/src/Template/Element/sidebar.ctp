<div class="sidebar" data-color="blue">
    <!--
        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
    -->

    <div class="logo">
        <a href="/" class="simple-text logo-mini">
            <?= $this->Html->image('logo-white.png', ['style' => 'width>100%']); ?>
        </a>

        <a href="h/" class="simple-text logo-normal">
            ViewSchool
        </a>
        <div class="navbar-minimize">
            <button id="minimizeSidebar" class="btn btn-simple btn-icon btn-neutral btn-round">
                <i class="now-ui-icons text_align-center visible-on-sidebar-regular"></i>
                <i class="now-ui-icons design_bullet-list-67 visible-on-sidebar-mini"></i>
            </button>
        </div>
    </div>

    <div class="sidebar-wrapper">
        <div class="user">
            <div class="photo">
                <img src="<?= $this->User->getPhoto() ?>">
            </div>
            <div class="info">
                <a data-toggle="collapse" href="#collapseExample" class="collapsed">
                    <span>
                        <?= $this->User->getName(); ?>
                        <b class="caret"></b>
                    </span>
                </a>
                <div class="clearfix"></div>
                <div class="collapse" id="collapseExample">
                    <ul class="nav">
                        <li>
                            <a href="#">
                                <span class="sidebar-mini-icon">MP</span>
                                <span class="sidebar-normal">My Profile</span>
                            </a>
                        </li>
                        <li>
                            <a href="/profile/edit">
                                <span class="sidebar-mini-icon">EP</span>
                                <span class="sidebar-normal">Editar Perfil</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <ul class="nav">

            <li class="">
                <a href="/dashboard">
                    <i class="now-ui-icons business_bank"></i>
                    <p>Inicio</p>
                </a>
            </li>


            <li>
                <a data-toggle="collapse" href="#componentsExamples">
                    <i class="now-ui-icons files_paper"></i>
                    <p>Gerenciamento
                        <b class="caret"></b>
                    </p>
                </a>

                <div class="collapse" id="componentsExamples">
                    <ul class="nav">
                        <li>
                            <a href="/students">
                                <span class="sidebar-mini-icon">A</span>
                                <span class="sidebar-normal">Alunos</span>
                            </a>
                        </li>
                        <li>
                            <a href="/teachers">
                                <span class="sidebar-mini-icon">P</span>
                                <span class="sidebar-normal">Professores</span>
                            </a>
                        </li>
                        <li>
                            <a href="/courses">
                                <span class="sidebar-mini-icon">CS</span>
                                <span class="sidebar-normal">Cursos/Séries</span>
                            </a>
                        </li>
                        <li>
                            <a href="/class">
                                <span class="sidebar-mini-icon">T</span>
                                <span class="sidebar-normal">Turmas</span>
                            </a>
                        </li>
                        <li>
                            <a href="/lessons">
                                <span class="sidebar-mini-icon">AF</span>
                                <span class="sidebar-normal">Aulas e Frequências</span>
                            </a>
                        </li>
                        <li>
                            <a href="/exams">
                                <span class="sidebar-mini-icon">A</span>
                                <span class="sidebar-normal">Avaliações</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>

            <li>
                <a data-toggle="collapse" href="#att">
                    <i class="now-ui-icons loader_refresh"></i>
                    <p>Atualizações
                        <b class="caret"></b>
                    </p>
                </a>

                <div class="collapse" id="att">
                    <ul class="nav">
                        <li>
                            <a href="/calendars">
                                <span class="sidebar-mini-icon">C</span>
                                <span class="sidebar-normal">Calendário</span>
                            </a>
                        </li>
                        <li>
                            <a href="/comments">
                                <span class="sidebar-mini-icon">QA</span>
                                <span class="sidebar-normal">Quadro de Avisos</span>
                            </a>
                        </li>
                        <li>
                            <a href="/contacts">
                                <span class="sidebar-mini-icon">M</span>
                                <span class="sidebar-normal">Mensagens</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>

            <li>
                <a data-toggle="collapse" href="#conf">
                    <i class="now-ui-icons loader_gear"></i>
                    <p>Configurações
                        <b class="caret"></b>
                    </p>
                </a>

                <div class="collapse" id="conf">
                    <ul class="nav">
                        <li>
                            <a href="/peoples">
                                <span class="sidebar-mini-icon">U</span>
                                <span class="sidebar-normal">Usuários do Sistema</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</div>