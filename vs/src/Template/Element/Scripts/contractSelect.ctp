<script>
    var inputEnterprise = document.getElementById('corporate');
    var inputContract = document.getElementById('contracts');
    var inputUsers = document.getElementById('users');
    var inputFiliais = document.getElementById('filiais');

    var inputServices = document.getElementById('services');
    var inputTags = document.getElementById('tags');

    if (inputEnterprise) {
        inputEnterprise.addEventListener('change', function (e) {
            $.get('<?= $this->Url->build('/rest/contracts/corporate/') ?>' + e.target.value + '.json', function (data) {
                remove('contracts');
                addOptions(data.data, inputContract);
            });

            $.get('<?= $this->Url->build('/rest/users/getList/corporate/') ?>' + e.target.value + '.json', function (data) {
                remove('users');
                addOptions(data.data, inputUsers);
            });

            $.get('<?= $this->Url->build('/rest/addresses/filiais/') ?>' + e.target.value + '.json', function (data) {
                remove('filiais');
                addOptions(data.data, inputFiliais);
            });

        });
    }

    if (inputContract) {
        inputContract.addEventListener('change', function (e) {
            $.get('<?= $this->Url->build('/rest/services/getList/') ?>' + e.target.value + '.json', function (data) {
                remove('services');
                addOptions(data.data, inputServices);
            });
        });
    }

    if (inputServices) {
        inputServices.addEventListener('change', function (e) {
            $.get('<?= $this->Url->build('/rest/services/getTags/') ?>' + e.target.value + '.json', function (data) {
                remove('tags');
                addOptions(data.data, inputTags);
            });
        });
    }

    var remove = function (id) {
        var input = document.getElementById(id);
        for (var i = input.options.length - 1; i >= 0; i--) {
            input.remove(i);
        }
    }

    var addOptions = function (data, input) {

        var all = input.getAttribute('data-option-not-all');

        if (!all) {
            var opt = document.createElement('option');
            opt.value = '';
            opt.innerHTML = 'Todos';
            input.appendChild(opt);
        }

        // Campo default
        var opt = document.createElement('option');
        opt.value = '';
        opt.innerHTML = 'Definir';
        input.appendChild(opt);

        for (var i = 0; i < data.length; i++) {
            var opt = document.createElement('option');
            opt.value = data[i].id;
            opt.innerHTML = data[i].name;
            input.appendChild(opt);
        }
    }
</script>