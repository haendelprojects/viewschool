<div data-notify="container" class="col-xs-11 col-sm-4 alert alert-success alert-with-icon animated fadeInUp" role="alert"
     data-notify-position="top-right"
     style="display: inline-block; margin: 0px auto; position: fixed; transition: all 0.5s ease-in-out; z-index: 1060; top: 20px; right: 20px;"
     data-closing="true">
    <button type="button" aria-hidden="true" class="close" data-notify="dismiss"
            style="position: absolute; right: 10px; top: 50%; margin-top: -13px; z-index: 1062;"><i
                class="now-ui-icons ui-1_simple-remove"></i></button>
    <span data-notify="icon" class="now-ui-icons ui-1_bell-53"></span> <span data-notify="title"></span> <span
            data-notify="message">  <?= h($message) ?></span><a href="#" target="_blank" data-notify="url"></a>
</div>