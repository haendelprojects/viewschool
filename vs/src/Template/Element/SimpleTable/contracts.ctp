<?php if (!empty($data)): ?>
    <table id="table-contract" class="table table-hover datatable">
        <thead>
        <tr>
            <th>ID</th>
            <th>Nome</th>
            <th>Inicio</th>
            <th>Validade</th>
            <th>Status</th>
            <th>Ativo</th>
            <th>
                <?php echo __('Ações'); ?>
            </th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data as $contracts): ?>
            <tr>
                <td>
                    <?= h($contracts->id) ?>
                </td>
                <td>
                    <?= h($contracts->name) ?>
                </td>
                <td>
                    <?= h($contracts->initial_date) ?>
                </td>

                <td>
                    <?= h($contracts->final_date) ?>
                </td>
                <td>
                    <?= h($contracts->status) ?>
                </td>
                <td>
                    <?= ($contracts->active == 1) ? 'Sim' : 'Não' ?>
                </td>
                <td class="actions">
                    <?= $this->Html->link(__('Detalhes'), ['controller' => 'Contracts', 'action' => 'view', $contracts->id], ['class' => 'btn btn-info btn-xs']) ?>
                    <?= $this->Html->link(__('Editar'), ['controller' => 'Contracts', 'action' => 'edit', $contracts->id], ['class' => 'btn btn-warning btn-xs']) ?>
                </td>
            </tr>
        <?php endforeach; ?>

        </tbody>
    </table>
<?php else: ?>
    <table id="table-user" class="table table-hover datatable">
        <thead>
        <tr>
            <th>ID</th>
            <th>Nome</th>
            <th>Inicio</th>
            <th>Validade</th>
            <th>Status</th>
            <th>Ativo</th>
            <th>
                <?php echo __('Ações'); ?>
            </th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Não há dados</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        </tbody>
    </table>
<?php endif; ?>

