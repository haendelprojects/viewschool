<div class="panel-header panel-header-sm">
</div>


<div class="content">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">
                        <div class="float-right">

                        </div>
                    </h4>
                </div>


                <div class="card-body">
                    <?= $this->Form->create($course, array('role' => 'form')) ?>
                    <div class="box-body">
                        <?php
                        echo $this->Form->input('name', ['label' => 'Nome']);
                        echo $this->Form->input('nivel', ['options' => [
                            'INFANTIL' => 'Infantil',
                            'FUNDAMENTAL' => 'Fundamental',
                            'TECNICO' => 'Técnico',
                            'MEDIO' => 'Médio',
                            'OUTRO' => 'Outro'
                        ]]);

                        echo $this->Form->input('active', ['options' => [1 => 'Sim', 0 => 'Não'], 'label' => 'Este cursos está ativo?']);
                        echo $this->Form->input('workload', ['label' => 'Carga horária total do curso']);
                        ?>


                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <?= $this->Form->button(__('Cadastrar')) ?>
                    </div>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</div>

