<div class="panel-header panel-header-sm">
</div>


<div class="content">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">
                        <div class="float-right">
                            <?= $this->Html->link(__('Editar'), ['action' => 'edit', $course->id], ['class' => 'btn btn-alert btn-xs']) ?>
                        </div>
                    </h4>
                </div>


                <div class="card-body">
                    <dl class="dl-horizontal">
                        <dt><?= __('Nome') ?></dt>
                        <dd>
                            <?= h($course->name) ?>
                        </dd>
                        <dt><?= __('Nível') ?></dt>
                        <dd>
                            <?= h($course->nivel) ?>
                        </dd>
                        <dt><?= __('Carga Horária') ?></dt>
                        <dd>
                            <?= $course->workload ?> h
                        </dd>
                        <dt><?= __('Ativo') ?></dt>
                        <dd>
                            <?= ($course->active) ? 'Sim' : 'Não' ?>
                        </dd>

                    </dl>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- ./col -->
    </div>
</div>
