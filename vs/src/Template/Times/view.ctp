<section class="content-header">
  <h1>
    <?php echo __('Time'); ?>
  </h1>
  <ol class="breadcrumb">
    <li>
    <?= $this->Html->link('<i class="fa fa-dashboard"></i> ' . __('Back'), ['action' => 'index'], ['escape' => false])?>
    </li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-md-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <i class="fa fa-info"></i>
                <h3 class="box-title"><?php echo __('Information'); ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <dl class="dl-horizontal">
                                                                                                                <dt><?= __('Day Week') ?></dt>
                                        <dd>
                                            <?= h($time->day_week) ?>
                                        </dd>
                                                                                                                                                    <dt><?= __('Class Discipline') ?></dt>
                                <dd>
                                    <?= $time->has('class_discipline') ? $time->class_discipline->id : '' ?>
                                </dd>
                                                                                                                        <dt><?= __('Init Time') ?></dt>
                                        <dd>
                                            <?= h($time->init_time) ?>
                                        </dd>
                                                                                                                                                            <dt><?= __('End Time') ?></dt>
                                        <dd>
                                            <?= h($time->end_time) ?>
                                        </dd>
                                                                                                                                    
                                            
                                                                                                                                            
                                            
                                            
                                    </dl>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- ./col -->
</div>
<!-- div -->

</section>
