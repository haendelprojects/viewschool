<div class="panel-header panel-header-sm">
</div>


<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">
                        <div class="float-right">

                        </div>
                    </h4>
                </div>


                <div class="card-body">
                    <h3 class="box-title"><?= __('Informações') ?></h3>

                    <!-- /.box-header -->
                    <!-- form start -->
                    <?= $this->Form->create($lesson, array('role' => 'form')) ?>
                    <div class="box-body">

                        <?php

                        $clasOptions = [
                            'label' => 'Turma',
                            'id' => 'input-class-exam',
                            'empty' => 'Definir Turma',
                            'required',
                        ];

                        if ($classId) {
                            $clasOptions['value'] = $classId;
                            $clasOptions['disabled'] = true;
                        }

                        echo $this->Form->input('class_id', $clasOptions);
                        ?>

                        <div class="select-discipline">
                            <?= $this->Form->input('discipline_id', ['options' => ($classId) ? $disciplines : [], 'label' => "Disciplina", 'required' => true]); ?>
                        </div>

                        <?php
                        echo $this->Form->input('title', ['label' => 'Assunto da Prova', 'required' => true]);
                        echo $this->Form->input('description', ['label' => 'Descrição da Prova']);
                        echo $this->Form->input('day_base', ['empty' => true, 'label' => 'Dia da Prova', 'default' => '', 'class' => 'datepicker form-control', 'type' => 'text', 'required' => true]);
                        ?>

                        <div class="students">
                            <?php if ($classId): ?>

                                <table class="table-bordered table">
                                    <tbody>
                                    <tr>
                                        <th>Aluno</th>
                                        <th>Presença</th>
                                        <th>Nota</th>
                                    </tr>
                                    <?php foreach ($registrations as $key => $val): ?>
                                        <tr>
                                            <td><?= $val->people->name ?></td>
                                            <td>
                                                <input type="hidden" name="frequencies[<?= $key ?>][people_id]"
                                                       id="frequencies-1-people-id" value="<?= $val->people->id ?>">
                                                <div class="form-group select required">
                                                    <select name="frequencies[<?= $key ?>][status]" required="required"
                                                            id="frequencies-1-status" class="form-control">
                                                        <option value="Presente">Presente</option>
                                                        <option value="Faltou">Faltou</option>
                                                        <option value="Falta Justificada">Falta Justificada</option>
                                                    </select>
                                                </div>

                                            </td>
                                            <td>
                                                <div class="form-group select required">
                                                    <input name="frequencies[<?= $key ?>][note]" required="required"
                                                           id="frequencies-1-status" class="form-control"/>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            <?php endif; ?>
                        </div>
                    </div>


                    <!-- /.box-body -->
                    <div class="box-footer">
                        <?= $this->Form->button(__('Cadastrar'), ['class' => 'btn-success']) ?>
                    </div>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</div>
