<div class="panel-header panel-header-sm">
</div>


<div class="content">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">
                        Informações
                    </h4>
                </div>


                <div class="card-body">
                    <dl class="dl-horizontal">
                        <dt><?= __('Disciplina') ?></dt>
                        <dd>
                            <?= $lesson->has('discipline') ? $lesson->discipline->name : '' ?>
                        </dd>
                        <dt><?= __('Classe') ?></dt>
                        <dd>
                            <?= $lesson->clas->course->name ?> <?= $lesson->clas->name ?>
                        </dd>

                        <dt><?= __('Data') ?></dt>
                        <dd>
                            <?= h($lesson->day) ?>
                        </dd>

                        <dt><?= __('Assunto') ?></dt>
                        <dd>
                            <?= h($lesson->title) ?>
                        </dd>

                        <dt><?= __('Descrição') ?></dt>
                        <dd>
                            <?= $this->Text->autoParagraph(h($lesson->description)); ?>
                        </dd>
                    </dl>
                </div>
                <!-- /.box-body -->
            </div>
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">
                        Notas dos alunos
                    </h4>
                </div>


                <div class="card-body">
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">

                        <?php if (!empty($lesson->frequencies)): ?>

                            <table class="table table-hover">
                                <tbody>
                                <tr>
                                    <th>Aluno</th>
                                    <th>
                                        Presença
                                    </th>
                                    <th>
                                        Nota
                                    </th>
                                </tr>

                                <?php foreach ($lesson->frequencies as $frequencies): ?>
                                    <tr>
                                        <td>
                                            <?= h($frequencies->people->name) ?>
                                        </td>
                                        <td>
                                            <?= h($frequencies->status) ?>
                                        </td>

                                        <td>
                                            <?= number_format($frequencies->note, 2) ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>

                                </tbody>
                            </table>

                        <?php endif; ?>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
</div>
