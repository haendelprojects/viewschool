<div class="panel-header panel-header-sm">
</div>


<div class="content">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">
                        <div class="float-right">
                            <?= $this->Html->link(__('Novo(a)'), ['action' => 'add'], ['class' => 'btn btn-default btn-sm ', 'escape' => false]) ?>
                        </div>
                    </h4>
                </div>


                <div class="card-body">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th><?= $this->Paginator->sort('class_id', 'Classe') ?></th>
                            <th><?= $this->Paginator->sort('discipline_id', 'Disciplina') ?></th>
                            <th><?= $this->Paginator->sort('title', 'Assunto') ?></th>
                            <th><?= $this->Paginator->sort('day', 'Dia') ?></th>
                            <th><?= __('Ações') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($lessons as $lesson): ?>
                            <tr>
                                <td><?= $lesson->clas->course->name ?> <?= $lesson->clas->name ?></td>
                                <td><?= $lesson->has('discipline') ? $this->Html->link($lesson->discipline->name, ['controller' => 'Disciplines', 'action' => 'view', $lesson->discipline->id]) : '' ?></td>
                                <td><?= h($lesson->title) ?></td>
                                <td><?= h($lesson->day) ?></td>
                                <td class="actions" style="white-space:nowrap">
                                    <?= $this->Html->link(__('Detalhes'), ['action' => 'view', $lesson->id], ['class' => 'btn btn-info btn-xs']) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right">
                        <?php echo $this->Paginator->numbers(); ?>
                    </ul>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</div>
<!-- /.content -->
