<div class="panel-header panel-header-sm">
</div>


<div class="content">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">
                        Informação
                    </h4>
                </div>


                <div class="card-body">

                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <dl class="dl-horizontal">
                                    <dt><?= __('Disciplina') ?></dt>
                                    <dd>
                                        <?= $lesson->has('discipline') ? $lesson->discipline->name : '' ?>
                                    </dd>
                                    <dt><?= __('Assunto') ?></dt>
                                    <dd>
                                        <?= h($lesson->title) ?>
                                    </dd>


                                </dl>
                            </div>
                            <div class="col-md-6">
                                <dl class="dl-horizontal">
                                    <dt><?= __('Classe') ?></dt>
                                    <dd>
                                        <?= $lesson->clas->course->name ?> <?= $lesson->clas->name ?>
                                    </dd>

                                    <dt><?= __('Dia') ?></dt>
                                    <dd>
                                        <?= h($lesson->day->format('d/m/Y')) ?>
                                    </dd>
                                </dl>
                            </div>

                            <div class="col-md-12">

                                <dl class="dl-horizontal">
                                    <dt><?= __('Descrição da Aula') ?></dt>
                                    <dd>
                                        <?= $this->Text->autoParagraph(h($lesson->description)); ?>
                                    </dd>
                                </dl>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>


                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">
                            Frequência
                        </h4>
                    </div>


                    <div class="card-body">

                        <?php if (!empty($lesson->frequencies)): ?>

                            <table class="table table-hover">
                                <tbody>
                                <tr>
                                    <th>
                                        Frequência
                                    </th>


                                    <th>
                                        Aluno
                                    </th>
                                </tr>

                                <?php foreach ($lesson->frequencies as $frequencies): ?>
                                    <tr>
                                        <td>
                                            <?= h($frequencies->status) ?>
                                        </td>

                                        <td>
                                            <?= h($frequencies->people->name) ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>

                                </tbody>
                            </table>

                        <?php endif; ?>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
</div>
