<div class="panel-header panel-header-sm">
</div>


<div class="content">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">
                        <div class="float-right">

                        </div>
                    </h4>
                </div>


                <div class="card-body">
                    <?= $this->Form->create($lesson, array('role' => 'form')) ?>
                    <div class="box-body">

                        <?php

                        $clasOptions = [
                            'label' => 'Turma',
                            'id' => 'input-class',
                            'empty' => 'Definir Turma',
                            'required',
                        ];

                        if ($classId) {
                            $clasOptions['value'] = $classId;
                            $clasOptions['disabled'] = true;
                        }

                        echo $this->Form->input('class_id', $clasOptions);
                        ?>

                        <div class="select-discipline">
                            <?= $this->Form->input('discipline_id', ['options' => ($classId) ? $disciplines : [], 'label' => "Disciplina", 'required' => true]); ?>
                        </div>

                        <?php
                        echo $this->Form->input('title', ['label' => 'Assunto da Aula', 'required' => true]);
                        echo $this->Form->input('description', ['label' => 'Descrição da aula', 'required' => true]);
                        echo $this->Form->input('day_base', ['empty' => true, 'label' => 'Dia da Aula', 'default' => '', 'class' => 'datepicker form-control', 'type' => 'text', 'required' => true]);
                        ?>

                        <div class="students">
                            <?php if ($classId): ?>

                                <table class="table-bordered table">
                                    <tbody>
                                    <tr>
                                        <th>Aluno</th>
                                        <th>Presença</th>
                                    </tr>
                                    <?php foreach ($registrations as $key => $val): ?>
                                        <tr>
                                            <td><?= $val->people->name ?></td>
                                            <td>
                                                <input type="hidden" name="frequencies[<?= $key ?>][people_id]"
                                                       id="frequencies-1-people-id" value="<?= $val->people->id ?>">
                                                <div class="form-group select required">
                                                    <select name="frequencies[<?= $key ?>][status]" required="required"
                                                            id="frequencies-1-status" class="form-control">
                                                        <option value="Presente">Presente</option>
                                                        <option value="Faltou">Faltou</option>
                                                        <option value="Falta Justificada">Falta Justificada</option>
                                                    </select>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            <?php endif; ?>
                        </div>
                    </div>


                    <!-- /.box-body -->
                    <div class="box-footer">
                        <?= $this->Form->button(__('Cadastrar'), ['class' => 'btn-success']) ?>
                    </div>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</div>
