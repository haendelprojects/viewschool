<div class="panel-header panel-header-sm">
</div>


<div class="content">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">
                        <div class="float-right">

                        </div>
                    </h4>
                </div>


                <div class="card-body">
                    <?= $this->Form->create($lesson, array('role' => 'form')) ?>
                    <div class="box-body">

                        <?php
                        echo $this->Form->input('class_id', ['label' => 'Turma', 'id' => 'input-class', 'empty' => 'Definir Turma', 'required']);
                        ?>

                        <div class="select-discipline">
                            <?= $this->Form->input('discipline_id', ['options' => [], 'label' => "Disciplina", 'required']); ?>
                        </div>

                        <?php
                        echo $this->Form->input('title', ['label' => 'Assunto da Aula', 'required']);
                        echo $this->Form->input('description', ['label' => 'Descrição da aula']);
                        echo $this->Form->input('day_base', ['empty' => true, 'label' => 'Dia da Aula', 'default' => '', 'class' => 'datepicker form-control', 'type' => 'text']);
                        ?>

                        <div class="students">

                        </div>
                    </div>


                    <!-- /.box-body -->
                    <div class="box-footer">
                        <?= $this->Form->button(__('Cadastrar'), ['class' => 'btn-success']) ?>
                    </div>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</div>
