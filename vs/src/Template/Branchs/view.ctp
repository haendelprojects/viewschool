<section class="content-header">
  <h1>
    <?php echo __('Branch'); ?>
  </h1>
  <ol class="breadcrumb">
    <li>
    <?= $this->Html->link('<i class="fa fa-dashboard"></i> ' . __('Back'), ['action' => 'index'], ['escape' => false])?>
    </li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-md-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <i class="fa fa-info"></i>
                <h3 class="box-title"><?php echo __('Information'); ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <dl class="dl-horizontal">
                                                                                                                <dt><?= __('Name') ?></dt>
                                        <dd>
                                            <?= h($branch->name) ?>
                                        </dd>
                                                                                                                                                            <dt><?= __('Cnpj') ?></dt>
                                        <dd>
                                            <?= h($branch->cnpj) ?>
                                        </dd>
                                                                                                                                                    <dt><?= __('Address') ?></dt>
                                <dd>
                                    <?= $branch->has('address') ? $branch->address->id : '' ?>
                                </dd>
                                                                                                                        <dt><?= __('Phone') ?></dt>
                                        <dd>
                                            <?= h($branch->phone) ?>
                                        </dd>
                                                                                                                                                            <dt><?= __('Email') ?></dt>
                                        <dd>
                                            <?= h($branch->email) ?>
                                        </dd>
                                                                                                                                                    <dt><?= __('School') ?></dt>
                                <dd>
                                    <?= $branch->has('school') ? $branch->school->name : '' ?>
                                </dd>
                                                                                                
                                            
                                                                                                                                            
                                                                                                                                                                                                
                                            
                                    </dl>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- ./col -->
</div>
<!-- div -->

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <i class="fa fa-share-alt"></i>
                    <h3 class="box-title"><?= __('Related {0}', ['Calendars']) ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">

                <?php if (!empty($branch->calendars)): ?>

                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                                                    
                                    <th>
                                    Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Title
                                    </th>
                                        
                                                                    
                                    <th>
                                    Text
                                    </th>
                                        
                                                                                                                                            
                                    <th>
                                    Start
                                    </th>
                                        
                                                                    
                                    <th>
                                    End
                                    </th>
                                        
                                                                    
                                    <th>
                                    Background
                                    </th>
                                        
                                                                    
                                    <th>
                                    Url
                                    </th>
                                        
                                                                    
                                    <th>
                                    User Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Class Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Course Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Branch Id
                                    </th>
                                        
                                                                    
                                <th>
                                    <?php echo __('Actions'); ?>
                                </th>
                            </tr>

                            <?php foreach ($branch->calendars as $calendars): ?>
                                <tr>
                                                                        
                                    <td>
                                    <?= h($calendars->id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($calendars->title) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($calendars->text) ?>
                                    </td>
                                                                                                                                                
                                    <td>
                                    <?= h($calendars->start) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($calendars->end) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($calendars->background) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($calendars->url) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($calendars->user_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($calendars->class_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($calendars->course_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($calendars->branch_id) ?>
                                    </td>
                                    
                                                                        <td class="actions">
                                    <?= $this->Html->link(__('View'), ['controller' => 'Calendars', 'action' => 'view', $calendars->id], ['class'=>'btn btn-info btn-xs']) ?>

                                    <?= $this->Html->link(__('Edit'), ['controller' => 'Calendars', 'action' => 'edit', $calendars->id], ['class'=>'btn btn-warning btn-xs']) ?>

                                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Calendars', 'action' => 'delete', $calendars->id], ['confirm' => __('Are you sure you want to delete # {0}?', $calendars->id), 'class'=>'btn btn-danger btn-xs']) ?>    
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                                    
                        </tbody>
                    </table>

                <?php endif; ?>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <i class="fa fa-share-alt"></i>
                    <h3 class="box-title"><?= __('Related {0}', ['Class']) ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">

                <?php if (!empty($branch->class)): ?>

                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                                                    
                                    <th>
                                    Id
                                    </th>
                                        
                                                                                                                                            
                                    <th>
                                    Course Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Branch Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Organization Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Init Date
                                    </th>
                                        
                                                                    
                                    <th>
                                    End Date
                                    </th>
                                        
                                                                    
                                    <th>
                                    Period
                                    </th>
                                        
                                                                    
                                    <th>
                                    Name
                                    </th>
                                        
                                                                    
                                    <th>
                                    Wallet Name
                                    </th>
                                        
                                                                    
                                    <th>
                                    Status
                                    </th>
                                        
                                                                    
                                    <th>
                                    Class Time
                                    </th>
                                        
                                                                    
                                    <th>
                                    School Year
                                    </th>
                                        
                                                                    
                                    <th>
                                    Evaluation Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    People Id
                                    </th>
                                        
                                                                    
                                <th>
                                    <?php echo __('Actions'); ?>
                                </th>
                            </tr>

                            <?php foreach ($branch->class as $class): ?>
                                <tr>
                                                                        
                                    <td>
                                    <?= h($class->id) ?>
                                    </td>
                                                                                                                                                
                                    <td>
                                    <?= h($class->course_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($class->branch_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($class->organization_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($class->init_date) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($class->end_date) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($class->period) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($class->name) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($class->wallet_name) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($class->status) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($class->class_time) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($class->school_year) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($class->evaluation_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($class->people_id) ?>
                                    </td>
                                    
                                                                        <td class="actions">
                                    <?= $this->Html->link(__('View'), ['controller' => 'Class', 'action' => 'view', $class->id], ['class'=>'btn btn-info btn-xs']) ?>

                                    <?= $this->Html->link(__('Edit'), ['controller' => 'Class', 'action' => 'edit', $class->id], ['class'=>'btn btn-warning btn-xs']) ?>

                                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Class', 'action' => 'delete', $class->id], ['confirm' => __('Are you sure you want to delete # {0}?', $class->id), 'class'=>'btn btn-danger btn-xs']) ?>    
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                                    
                        </tbody>
                    </table>

                <?php endif; ?>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <i class="fa fa-share-alt"></i>
                    <h3 class="box-title"><?= __('Related {0}', ['Class Discipline']) ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">

                <?php if (!empty($branch->class_discipline)): ?>

                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                                                    
                                    <th>
                                    Id
                                    </th>
                                        
                                                                                                                                            
                                    <th>
                                    Class Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Discipline Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    People Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Branch Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Workload
                                    </th>
                                        
                                                                    
                                    <th>
                                    Minimum Grade
                                    </th>
                                        
                                                                    
                                <th>
                                    <?php echo __('Actions'); ?>
                                </th>
                            </tr>

                            <?php foreach ($branch->class_discipline as $classDiscipline): ?>
                                <tr>
                                                                        
                                    <td>
                                    <?= h($classDiscipline->id) ?>
                                    </td>
                                                                                                                                                
                                    <td>
                                    <?= h($classDiscipline->class_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($classDiscipline->discipline_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($classDiscipline->people_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($classDiscipline->branch_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($classDiscipline->workload) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($classDiscipline->minimum_grade) ?>
                                    </td>
                                    
                                                                        <td class="actions">
                                    <?= $this->Html->link(__('View'), ['controller' => 'ClassDiscipline', 'action' => 'view', $classDiscipline->id], ['class'=>'btn btn-info btn-xs']) ?>

                                    <?= $this->Html->link(__('Edit'), ['controller' => 'ClassDiscipline', 'action' => 'edit', $classDiscipline->id], ['class'=>'btn btn-warning btn-xs']) ?>

                                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ClassDiscipline', 'action' => 'delete', $classDiscipline->id], ['confirm' => __('Are you sure you want to delete # {0}?', $classDiscipline->id), 'class'=>'btn btn-danger btn-xs']) ?>    
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                                    
                        </tbody>
                    </table>

                <?php endif; ?>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <i class="fa fa-share-alt"></i>
                    <h3 class="box-title"><?= __('Related {0}', ['Comments']) ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">

                <?php if (!empty($branch->comments)): ?>

                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                                                    
                                    <th>
                                    Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Text
                                    </th>
                                        
                                                                    
                                    <th>
                                    Title
                                    </th>
                                        
                                                                                                                                            
                                    <th>
                                    User Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Type
                                    </th>
                                        
                                                                    
                                    <th>
                                    People Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Class Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Branch Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Read
                                    </th>
                                        
                                                                    
                                <th>
                                    <?php echo __('Actions'); ?>
                                </th>
                            </tr>

                            <?php foreach ($branch->comments as $comments): ?>
                                <tr>
                                                                        
                                    <td>
                                    <?= h($comments->id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($comments->text) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($comments->title) ?>
                                    </td>
                                                                                                                                                
                                    <td>
                                    <?= h($comments->user_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($comments->type) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($comments->people_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($comments->class_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($comments->branch_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($comments->read) ?>
                                    </td>
                                    
                                                                        <td class="actions">
                                    <?= $this->Html->link(__('View'), ['controller' => 'Comments', 'action' => 'view', $comments->id], ['class'=>'btn btn-info btn-xs']) ?>

                                    <?= $this->Html->link(__('Edit'), ['controller' => 'Comments', 'action' => 'edit', $comments->id], ['class'=>'btn btn-warning btn-xs']) ?>

                                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Comments', 'action' => 'delete', $comments->id], ['confirm' => __('Are you sure you want to delete # {0}?', $comments->id), 'class'=>'btn btn-danger btn-xs']) ?>    
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                                    
                        </tbody>
                    </table>

                <?php endif; ?>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <i class="fa fa-share-alt"></i>
                    <h3 class="box-title"><?= __('Related {0}', ['Courses']) ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">

                <?php if (!empty($branch->courses)): ?>

                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                                                    
                                    <th>
                                    Id
                                    </th>
                                        
                                                                                                                                            
                                    <th>
                                    Name
                                    </th>
                                        
                                                                    
                                    <th>
                                    Wallet Name
                                    </th>
                                        
                                                                    
                                    <th>
                                    Nivel
                                    </th>
                                        
                                                                    
                                    <th>
                                    Branch Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Active
                                    </th>
                                        
                                                                    
                                    <th>
                                    Workload
                                    </th>
                                        
                                                                    
                                <th>
                                    <?php echo __('Actions'); ?>
                                </th>
                            </tr>

                            <?php foreach ($branch->courses as $courses): ?>
                                <tr>
                                                                        
                                    <td>
                                    <?= h($courses->id) ?>
                                    </td>
                                                                                                                                                
                                    <td>
                                    <?= h($courses->name) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($courses->wallet_name) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($courses->nivel) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($courses->branch_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($courses->active) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($courses->workload) ?>
                                    </td>
                                    
                                                                        <td class="actions">
                                    <?= $this->Html->link(__('View'), ['controller' => 'Courses', 'action' => 'view', $courses->id], ['class'=>'btn btn-info btn-xs']) ?>

                                    <?= $this->Html->link(__('Edit'), ['controller' => 'Courses', 'action' => 'edit', $courses->id], ['class'=>'btn btn-warning btn-xs']) ?>

                                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Courses', 'action' => 'delete', $courses->id], ['confirm' => __('Are you sure you want to delete # {0}?', $courses->id), 'class'=>'btn btn-danger btn-xs']) ?>    
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                                    
                        </tbody>
                    </table>

                <?php endif; ?>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <i class="fa fa-share-alt"></i>
                    <h3 class="box-title"><?= __('Related {0}', ['Disciplines']) ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">

                <?php if (!empty($branch->disciplines)): ?>

                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                                                    
                                    <th>
                                    Id
                                    </th>
                                        
                                                                                                                                            
                                    <th>
                                    Name
                                    </th>
                                        
                                                                    
                                    <th>
                                    Workload
                                    </th>
                                        
                                                                    
                                    <th>
                                    Branch Id
                                    </th>
                                        
                                                                    
                                <th>
                                    <?php echo __('Actions'); ?>
                                </th>
                            </tr>

                            <?php foreach ($branch->disciplines as $disciplines): ?>
                                <tr>
                                                                        
                                    <td>
                                    <?= h($disciplines->id) ?>
                                    </td>
                                                                                                                                                
                                    <td>
                                    <?= h($disciplines->name) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($disciplines->workload) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($disciplines->branch_id) ?>
                                    </td>
                                    
                                                                        <td class="actions">
                                    <?= $this->Html->link(__('View'), ['controller' => 'Disciplines', 'action' => 'view', $disciplines->id], ['class'=>'btn btn-info btn-xs']) ?>

                                    <?= $this->Html->link(__('Edit'), ['controller' => 'Disciplines', 'action' => 'edit', $disciplines->id], ['class'=>'btn btn-warning btn-xs']) ?>

                                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Disciplines', 'action' => 'delete', $disciplines->id], ['confirm' => __('Are you sure you want to delete # {0}?', $disciplines->id), 'class'=>'btn btn-danger btn-xs']) ?>    
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                                    
                        </tbody>
                    </table>

                <?php endif; ?>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <i class="fa fa-share-alt"></i>
                    <h3 class="box-title"><?= __('Related {0}', ['Evaluations']) ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">

                <?php if (!empty($branch->evaluations)): ?>

                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                                                    
                                    <th>
                                    Id
                                    </th>
                                        
                                                                                                                                            
                                    <th>
                                    Name
                                    </th>
                                        
                                                                    
                                    <th>
                                    Type Attendance
                                    </th>
                                        
                                                                    
                                    <th>
                                    Evaluation Attendance
                                    </th>
                                        
                                                                    
                                    <th>
                                    Percent Attendance
                                    </th>
                                        
                                                                    
                                    <th>
                                    Evaluation Period
                                    </th>
                                        
                                                                    
                                    <th>
                                    Quantity Period
                                    </th>
                                        
                                                                    
                                    <th>
                                    Calculate Total Year
                                    </th>
                                        
                                                                    
                                    <th>
                                    Calculate Final Note
                                    </th>
                                        
                                                                    
                                    <th>
                                    Period Minimum Note
                                    </th>
                                        
                                                                    
                                    <th>
                                    Annual Minimum Note
                                    </th>
                                        
                                                                    
                                    <th>
                                    Period Recuperation Minimum Note
                                    </th>
                                        
                                                                    
                                    <th>
                                    Anual Recuperation Minimum Note
                                    </th>
                                        
                                                                    
                                    <th>
                                    Annual Recovery
                                    </th>
                                        
                                                                    
                                    <th>
                                    Semi Annual Recovery
                                    </th>
                                        
                                                                    
                                    <th>
                                    Type
                                    </th>
                                        
                                                                    
                                    <th>
                                    Branch Id
                                    </th>
                                        
                                                                    
                                <th>
                                    <?php echo __('Actions'); ?>
                                </th>
                            </tr>

                            <?php foreach ($branch->evaluations as $evaluations): ?>
                                <tr>
                                                                        
                                    <td>
                                    <?= h($evaluations->id) ?>
                                    </td>
                                                                                                                                                
                                    <td>
                                    <?= h($evaluations->name) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($evaluations->type_attendance) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($evaluations->evaluation_attendance) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($evaluations->percent_attendance) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($evaluations->evaluation_period) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($evaluations->quantity_period) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($evaluations->calculate_total_year) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($evaluations->calculate_final_note) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($evaluations->period_minimum_note) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($evaluations->annual_minimum_note) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($evaluations->period_recuperation_minimum_note) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($evaluations->anual_recuperation_minimum_note) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($evaluations->annual_recovery) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($evaluations->semi_annual_recovery) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($evaluations->type) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($evaluations->branch_id) ?>
                                    </td>
                                    
                                                                        <td class="actions">
                                    <?= $this->Html->link(__('View'), ['controller' => 'Evaluations', 'action' => 'view', $evaluations->id], ['class'=>'btn btn-info btn-xs']) ?>

                                    <?= $this->Html->link(__('Edit'), ['controller' => 'Evaluations', 'action' => 'edit', $evaluations->id], ['class'=>'btn btn-warning btn-xs']) ?>

                                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Evaluations', 'action' => 'delete', $evaluations->id], ['confirm' => __('Are you sure you want to delete # {0}?', $evaluations->id), 'class'=>'btn btn-danger btn-xs']) ?>    
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                                    
                        </tbody>
                    </table>

                <?php endif; ?>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <i class="fa fa-share-alt"></i>
                    <h3 class="box-title"><?= __('Related {0}', ['Organizations']) ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">

                <?php if (!empty($branch->organizations)): ?>

                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                                                    
                                    <th>
                                    Id
                                    </th>
                                        
                                                                                                                                            
                                    <th>
                                    Name
                                    </th>
                                        
                                                                    
                                    <th>
                                    Branch Id
                                    </th>
                                        
                                                                    
                                <th>
                                    <?php echo __('Actions'); ?>
                                </th>
                            </tr>

                            <?php foreach ($branch->organizations as $organizations): ?>
                                <tr>
                                                                        
                                    <td>
                                    <?= h($organizations->id) ?>
                                    </td>
                                                                                                                                                
                                    <td>
                                    <?= h($organizations->name) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($organizations->branch_id) ?>
                                    </td>
                                    
                                                                        <td class="actions">
                                    <?= $this->Html->link(__('View'), ['controller' => 'Organizations', 'action' => 'view', $organizations->id], ['class'=>'btn btn-info btn-xs']) ?>

                                    <?= $this->Html->link(__('Edit'), ['controller' => 'Organizations', 'action' => 'edit', $organizations->id], ['class'=>'btn btn-warning btn-xs']) ?>

                                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Organizations', 'action' => 'delete', $organizations->id], ['confirm' => __('Are you sure you want to delete # {0}?', $organizations->id), 'class'=>'btn btn-danger btn-xs']) ?>    
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                                    
                        </tbody>
                    </table>

                <?php endif; ?>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <i class="fa fa-share-alt"></i>
                    <h3 class="box-title"><?= __('Related {0}', ['Peoples']) ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">

                <?php if (!empty($branch->peoples)): ?>

                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                                                    
                                    <th>
                                    Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Name
                                    </th>
                                        
                                                                    
                                    <th>
                                    Cpf
                                    </th>
                                        
                                                                    
                                    <th>
                                    Phone
                                    </th>
                                        
                                                                    
                                    <th>
                                    Cell Phone
                                    </th>
                                        
                                                                                                                                            
                                    <th>
                                    Birth
                                    </th>
                                        
                                                                    
                                    <th>
                                    Type
                                    </th>
                                        
                                                                    
                                    <th>
                                    Sexo
                                    </th>
                                        
                                                                    
                                    <th>
                                    Foto
                                    </th>
                                        
                                                                    
                                    <th>
                                    Father Name
                                    </th>
                                        
                                                                    
                                    <th>
                                    Mother Name
                                    </th>
                                        
                                                                    
                                    <th>
                                    Email
                                    </th>
                                        
                                                                    
                                    <th>
                                    Address Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    User Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Branch Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    People Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Responsible Educational
                                    </th>
                                        
                                                                    
                                    <th>
                                    Responsible Financial
                                    </th>
                                        
                                                                    
                                <th>
                                    <?php echo __('Actions'); ?>
                                </th>
                            </tr>

                            <?php foreach ($branch->peoples as $peoples): ?>
                                <tr>
                                                                        
                                    <td>
                                    <?= h($peoples->id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($peoples->name) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($peoples->cpf) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($peoples->phone) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($peoples->cell_phone) ?>
                                    </td>
                                                                                                                                                
                                    <td>
                                    <?= h($peoples->birth) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($peoples->type) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($peoples->sexo) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($peoples->foto) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($peoples->father_name) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($peoples->mother_name) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($peoples->email) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($peoples->address_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($peoples->user_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($peoples->branch_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($peoples->people_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($peoples->responsible_educational) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($peoples->responsible_financial) ?>
                                    </td>
                                    
                                                                        <td class="actions">
                                    <?= $this->Html->link(__('View'), ['controller' => 'Peoples', 'action' => 'view', $peoples->id], ['class'=>'btn btn-info btn-xs']) ?>

                                    <?= $this->Html->link(__('Edit'), ['controller' => 'Peoples', 'action' => 'edit', $peoples->id], ['class'=>'btn btn-warning btn-xs']) ?>

                                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Peoples', 'action' => 'delete', $peoples->id], ['confirm' => __('Are you sure you want to delete # {0}?', $peoples->id), 'class'=>'btn btn-danger btn-xs']) ?>    
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                                    
                        </tbody>
                    </table>

                <?php endif; ?>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <i class="fa fa-share-alt"></i>
                    <h3 class="box-title"><?= __('Related {0}', ['Users']) ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">

                <?php if (!empty($branch->users)): ?>

                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                                                    
                                    <th>
                                    Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Username
                                    </th>
                                        
                                                                    
                                    <th>
                                    Password
                                    </th>
                                        
                                                                                                                                            
                                    <th>
                                    School Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Active
                                    </th>
                                        
                                                                    
                                    <th>
                                    Access School
                                    </th>
                                        
                                                                    
                                    <th>
                                    Access Student
                                    </th>
                                        
                                                                    
                                    <th>
                                    Access Teacher
                                    </th>
                                        
                                                                    
                                    <th>
                                    Access Backoffice
                                    </th>
                                        
                                                                    
                                    <th>
                                    Branch Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Name
                                    </th>
                                        
                                                                    
                                <th>
                                    <?php echo __('Actions'); ?>
                                </th>
                            </tr>

                            <?php foreach ($branch->users as $users): ?>
                                <tr>
                                                                        
                                    <td>
                                    <?= h($users->id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($users->username) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($users->password) ?>
                                    </td>
                                                                                                                                                
                                    <td>
                                    <?= h($users->school_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($users->active) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($users->access_school) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($users->access_student) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($users->access_teacher) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($users->access_backoffice) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($users->branch_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($users->name) ?>
                                    </td>
                                    
                                                                        <td class="actions">
                                    <?= $this->Html->link(__('View'), ['controller' => 'Users', 'action' => 'view', $users->id], ['class'=>'btn btn-info btn-xs']) ?>

                                    <?= $this->Html->link(__('Edit'), ['controller' => 'Users', 'action' => 'edit', $users->id], ['class'=>'btn btn-warning btn-xs']) ?>

                                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Users', 'action' => 'delete', $users->id], ['confirm' => __('Are you sure you want to delete # {0}?', $users->id), 'class'=>'btn btn-danger btn-xs']) ?>    
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                                    
                        </tbody>
                    </table>

                <?php endif; ?>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
