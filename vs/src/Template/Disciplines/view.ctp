<section class="content-header">
  <h1>
    <?php echo __('Discipline'); ?>
  </h1>
  <ol class="breadcrumb">
    <li>
    <?= $this->Html->link('<i class="fa fa-dashboard"></i> ' . __('Back'), ['action' => 'index'], ['escape' => false])?>
    </li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-md-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <i class="fa fa-info"></i>
                <h3 class="box-title"><?php echo __('Information'); ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <dl class="dl-horizontal">
                                                                                                                <dt><?= __('Name') ?></dt>
                                        <dd>
                                            <?= h($discipline->name) ?>
                                        </dd>
                                                                                                                                                            <dt><?= __('Workload') ?></dt>
                                        <dd>
                                            <?= h($discipline->workload) ?>
                                        </dd>
                                                                                                                                                    <dt><?= __('Branch') ?></dt>
                                <dd>
                                    <?= $discipline->has('branch') ? $discipline->branch->name : '' ?>
                                </dd>
                                                                                                
                                            
                                                                                                                                            
                                                                                                                                                                                                
                                            
                                    </dl>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- ./col -->
</div>
<!-- div -->

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <i class="fa fa-share-alt"></i>
                    <h3 class="box-title"><?= __('Related {0}', ['Class Discipline']) ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">

                <?php if (!empty($discipline->class_discipline)): ?>

                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                                                    
                                    <th>
                                    Id
                                    </th>
                                        
                                                                                                                                            
                                    <th>
                                    Class Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Discipline Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    People Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Branch Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Workload
                                    </th>
                                        
                                                                    
                                    <th>
                                    Minimum Grade
                                    </th>
                                        
                                                                    
                                <th>
                                    <?php echo __('Actions'); ?>
                                </th>
                            </tr>

                            <?php foreach ($discipline->class_discipline as $classDiscipline): ?>
                                <tr>
                                                                        
                                    <td>
                                    <?= h($classDiscipline->id) ?>
                                    </td>
                                                                                                                                                
                                    <td>
                                    <?= h($classDiscipline->class_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($classDiscipline->discipline_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($classDiscipline->people_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($classDiscipline->branch_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($classDiscipline->workload) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($classDiscipline->minimum_grade) ?>
                                    </td>
                                    
                                                                        <td class="actions">
                                    <?= $this->Html->link(__('View'), ['controller' => 'ClassDiscipline', 'action' => 'view', $classDiscipline->id], ['class'=>'btn btn-info btn-xs']) ?>

                                    <?= $this->Html->link(__('Edit'), ['controller' => 'ClassDiscipline', 'action' => 'edit', $classDiscipline->id], ['class'=>'btn btn-warning btn-xs']) ?>

                                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ClassDiscipline', 'action' => 'delete', $classDiscipline->id], ['confirm' => __('Are you sure you want to delete # {0}?', $classDiscipline->id), 'class'=>'btn btn-danger btn-xs']) ?>    
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                                    
                        </tbody>
                    </table>

                <?php endif; ?>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <i class="fa fa-share-alt"></i>
                    <h3 class="box-title"><?= __('Related {0}', ['Lessons']) ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">

                <?php if (!empty($discipline->lessons)): ?>

                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                                                    
                                    <th>
                                    Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Class Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Discipline Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Type
                                    </th>
                                        
                                                                                                                                            
                                    <th>
                                    Title
                                    </th>
                                        
                                                                    
                                    <th>
                                    Description
                                    </th>
                                        
                                                                    
                                    <th>
                                    Day
                                    </th>
                                        
                                                                    
                                <th>
                                    <?php echo __('Actions'); ?>
                                </th>
                            </tr>

                            <?php foreach ($discipline->lessons as $lessons): ?>
                                <tr>
                                                                        
                                    <td>
                                    <?= h($lessons->id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($lessons->class_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($lessons->discipline_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($lessons->type) ?>
                                    </td>
                                                                                                                                                
                                    <td>
                                    <?= h($lessons->title) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($lessons->description) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($lessons->day) ?>
                                    </td>
                                    
                                                                        <td class="actions">
                                    <?= $this->Html->link(__('View'), ['controller' => 'Lessons', 'action' => 'view', $lessons->id], ['class'=>'btn btn-info btn-xs']) ?>

                                    <?= $this->Html->link(__('Edit'), ['controller' => 'Lessons', 'action' => 'edit', $lessons->id], ['class'=>'btn btn-warning btn-xs']) ?>

                                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Lessons', 'action' => 'delete', $lessons->id], ['confirm' => __('Are you sure you want to delete # {0}?', $lessons->id), 'class'=>'btn btn-danger btn-xs']) ?>    
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                                    
                        </tbody>
                    </table>

                <?php endif; ?>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
