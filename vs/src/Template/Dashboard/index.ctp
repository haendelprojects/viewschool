<div class="panel-header panel-header-lg">
    <div class="chartjs-size-monitor"
         style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
        <div class="chartjs-size-monitor-expand"
             style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
            <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>
        </div>
        <div class="chartjs-size-monitor-shrink"
             style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
            <div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>
        </div>
    </div>

    <!--    <canvas id="bigDashboardChart" width="1231" height="255" class="chartjs-render-monitor"-->
    <!--            style="display: block; width: 1231px; height: 255px;"></canvas>-->

    <canvas id="idChartUps" class="chart-ups" height="100" width="1231" height="255"
            style="display: block; width: 1231px; height: 255px;"

            data-labels='<?= json_encode($ups['labels']) ?>'
            data-values='<?= json_encode($ups['values']) ?>'></canvas>


</div>


<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-stats card-raised">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="statistics">
                                <div class="info">
                                    <div class="icon icon-primary">
                                        <i class="fas fa-exclamation-triangle"></i>
                                    </div>
                                    <h3 class="info-title"><?= $countValidation ?></h3>
                                    <h6 class="stats-title">
                                        <?= $this->Html->link('Espera de Validação', ['controller' => 'occurrences', 'action' => 'index', 'status' => 'AWAITING_VALIDATION'], ['target' => '_blank']) ?>
                                    </h6>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="statistics">
                                <div class="info">
                                    <div class="icon icon-success">
                                        <i class="now-ui-icons business_money-coins"></i>
                                    </div>
                                    <h3 class="info-title">
                                        <small>$</small>
                                        3,521
                                    </h3>
                                    <h6 class="stats-title">Today Revenue</h6>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="statistics">
                                <div class="info">
                                    <div class="icon icon-info">
                                        <i class="now-ui-icons users_single-02"></i>
                                    </div>
                                    <h3 class="info-title">562</h3>
                                    <h6 class="stats-title">Customers</h6>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="statistics">
                                <div class="info">
                                    <div class="icon icon-danger">
                                        <i class="now-ui-icons objects_support-17"></i>
                                    </div>
                                    <h3 class="info-title"><?= $totalOccurrencesMonth ?> </h3>
                                    <h6 class="stats-title">Ocorrências neste mês</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- <button type="button" class="btn btn-round btn-default dropdown-toggle btn-simple" data-toggle="dropdown">
        7 days
    </button> -->

    <div class="row">
        <div class="col-md-4">
            <div class="card card-chart">
                <div class="card-header">
                    <h5 class="card-category">Contratos</h5>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table ">
                            <?php foreach ($contracts as $contract): ?>
                                <tr>
                                    <td>
                                        <?= $this->Html->link($contract->name, ['controller' => 'contracts', 'action' => 'view', $contract->id]); ?> </td>
                                    <td><?= $contract->fund->credit_balance ?> UPs</td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        <i class="now-ui-icons arrows-1_refresh-69"></i> Dados Atualizados
                    </div>
                </div>
            </div>

            <div class="card card-chart">
                <div class="card-header">
                    <h5 class="card-category">Avaliações</h5>
                </div>
                <div class="card-body">
                    <canvas id="idValidation" class="chart-validation" height="150"
                            data-labels='<?= json_encode(array_keys($ratings['byName'])) ?>'
                            data-values='<?= json_encode(array_values($ratings['byName'])) ?>'
                            data-ids='<?= json_encode(array_keys($ratings['byId'])) ?>'></canvas>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        <i class="now-ui-icons arrows-1_refresh-69"></i> Dados Atualizados
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card card-chart">
                <div class="card-header">
                    <h5 class="card-category">Top Filiais</h5>
                </div>
                <div class="card-body">
                    <div class="chart-area">
                        <!--                        <canvas id="lineChartExampleWithNumbersAndGrid"></canvas>-->

                        <canvas id="idChartOccurrencesAddress" class="chart-address-occurrences" height="100"
                                data-labels='<?= json_encode(array_keys($addressOccurrences['byName'])) ?>'
                                data-values='<?= json_encode(array_values($addressOccurrences['byName'])) ?>'
                                data-ids='<?= json_encode(array_keys($addressOccurrences['byId'])) ?>'></canvas>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        <i class="now-ui-icons arrows-1_refresh-69"></i> Just Updated
                    </div>
                </div>
            </div>

            <div class="card card-chart">
                <div class="card-header">
                    <h5 class="card-category">Serviços</h5>
                </div>
                <div class="card-body">
                    <div class="chart-area">
                        <canvas id="idChartServices" class="chart-services" height="100"
                                data-labels='<?= json_encode(array_keys($servicesOccurrence['byName'])) ?>'
                                data-values='<?= json_encode(array_values($servicesOccurrence['byName'])) ?>'
                                data-ids='<?= json_encode(array_keys($servicesOccurrence['byId'])) ?>'></canvas>
                    </div>
                </div>

                <div class="card-footer">
                    <div class="stats">
                        <i class="now-ui-icons ui-2_time-alarm"></i> Dados atualizados
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4">


            <div class="card card-chart">
                <div class="card-header">
                    <h5 class="card-category">Mapa</h5>
                </div>
                <div class="card-body">
                    <?php
                    $points = [];
                    foreach ($occurrencesMap as $occurrence) {
                        if ($occurrence->address->lat && $occurrence->address->lng) {
                            $points[] = [
                                'position' => [
                                    $occurrence->address->lat, $occurrence->address->lng
                                ],
                                'popup' => '<a href="/occurrences/view/' . $occurrence->id . '" target="_blank"> OS: ' . $occurrence->id . ', dia ' . $this->Util->convertDate($occurrence->schedule_time) . '</a>'
                            ];
                        }
                    }
                    ?>
                    <div id="mapid" class="map"
                         data-options='{ "zoom": 4, "center" : [<?= $occurrence->address->lat ?> ,  <?= $occurrence->address->lng ?>]}'
                         data-multi-markers='
                             [
                                {"markers" : <?= json_encode($points) ?>, "icon" : "/img/icon_tec.png"},
                                {"markers" : <?= json_encode($points) ?>, "icon" : "/img/icon.png"}
                             ]'
                         style="height: 525px;"></div>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        <i class="now-ui-icons ui-2_time-alarm"></i> Dados atuais
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"> Últimas ocorrências abertas</h4>

                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-shopping">
                            <thead class="">
                            <tr>
                                <th>OS</th>
                                <th>Requisitado por</th>
                                <th>Endereço</th>
                                <th>Atendimento</th>
                                <th>Status</th>
                                <th class="text-right"></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($occurrences as $occurrence): ?>
                                <tr <?= $this->Util->getValidateExpired($occurrence->historics, $occurrence->status) ?>
                                        data-toggle="tooltip" data-placement="top"
                                        title="<?= $this->Util->textOccurrenceTooltip($occurrence) ?>">
                                    <td>
                                        <?= $this->Html->link($occurrence->id, [
                                            'action' => 'view', $occurrence->id], [
                                            'escape' => false,
                                            'class' => ' ' . $this->Status->slaDanger($occurrence->schedule_time, $occurrence->checkin_date, $occurrence->status), 'title' => __('Detalhes')]) ?>
                                    </td>
                                    <td>
                                        <?= $occurrence->has('client') ? $occurrence->client->first_name : '' ?>
                                    </td>
                                    <td>
                                        <?= h($occurrence->address->title) ?>
                                        - <?= h($occurrence->address->city) ?>
                                        / <?= h($occurrence->address->state) ?>
                                    </td>
                                    <td>  <?= $this->Util->convertDateTimezone($occurrence->schedule_time, $occurrence->address->has('o_city') ? $occurrence->address->o_city->timezone : null) ?> </td>
                                    <td>   <?= $this->Status->getName($occurrence->status) ?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>

