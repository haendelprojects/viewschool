<div class="panel-header panel-header-sm">
</div>


<div class="content">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">
                        <?= $this->Html->link('<i class="fa fa-arrow-left"></i> ' . __('Voltar'), ['action' => 'index'], ['escape' => false]) ?>
                    </h4>
                </div>


                <div class="card-body">
                    <dl class="dl-horizontal">
                        <dt><?= __('Titulo') ?></dt>
                        <dd>
                            <?= h($comment->title) ?>
                        </dd>

                        <dt><?= __('Aviso') ?></dt>
                        <dd>
                            <?= $this->Text->autoParagraph(h($comment->text)); ?>
                        </dd>

                        <?php if ($comment->has('people')) : ?>
                            <dt><?= __('Aluno') ?></dt>
                            <dd>
                                <?= $comment->people->name ?>
                            </dd>
                        <?php endif; ?>

                        <?php if ($comment->has('clas')) : ?>
                            <dt><?= __('Classe') ?></dt>
                            <dd>
                                <?= $comment->has('clas') ? $comment->clas->name : '' ?>
                            </dd>
                        <?php endif; ?>
                        <?php if ($comment->has('user')) : ?>
                            <dt><?= __('Criado por') ?></dt>
                            <dd>
                                <?= $comment->has('user') ? $comment->user->username : '' ?>
                            </dd>
                        <?php endif; ?>
                    </dl>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- ./col -->
    </div>
    <!-- div -->

</div>
