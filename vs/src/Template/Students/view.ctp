<div class="panel-header panel-header-sm">
</div>


<div class="content">
    <div class="row">
        <div class="col-md-12">

            <div class="row">
                <div class="col-md-3">

                    <!-- Profile Image -->
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">

                            </h4>
                        </div>
                        <div class="card-body">
                            <div class="box-body box-profile text-center">
                                <?php echo $this->Html->image(($people->foto) ? $people->foto : 'avatar.png', array('class' => 'profile-user-img img-responsive img-circle', 'alt' => 'User profile picture')); ?>
                                <p class="text-muted text-center mt-3"><?= $people->name ?></p>
                                <div class="mt-2">

                                    <?= (!$people->active) ? '<div class="badge badge-danger">Aluno Inativo</div>' : '' ?>

                                    <div>
                                        <?php $text = ($people->active) ? 'Desativar' : 'Ativar' ?>
                                        <?= $this->ModalConfirm->postLink($text, ['action' => 'delete', $people->id], ['class' => 'btn btn-info', 'title' => $text, 'text' => 'Deseja prosseguir em ' . $text . ' o aluno?']) ?>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">

                            </h4>
                        </div>

                        <div class="card-body">
                            <div class="nav-tabs-custom">
                                <ul class="nav-pills-primary nav nav-pills">
                                    <li class="nav-item"><a href="#activity" class="nav-link active" data-toggle="tab">
                                            Informações</a>
                                    </li>
                                    <li class="nav-item"><a href="#endereco" class="nav-link"
                                                            data-toggle="tab">Endereço</a>
                                    </li>
                                    <li class="nav-item"><a href="#responsaveis" class="nav-link" data-toggle="tab">
                                            Responsáveis</a></li>
                                    <li class="nav-item"><a href="#system" class="nav-link" data-toggle="tab">
                                            Sistema</a></li>
                                </ul>
                                <div class="tab-content pt-5">
                                    <div class=" active tab-pane" id="activity">

                                        <div class="row">
                                            <dl class="dl-horizontal col-md-6">
                                                <dt><?= __('Nome') ?></dt>
                                                <dd>
                                                    <?= h($people->name) ?>
                                                </dd>
                                                <dt><?= __('CPF') ?></dt>
                                                <dd>
                                                    <?= h($people->cpf) ?>
                                                </dd>
                                                <dt><?= __('Nascimento') ?></dt>
                                                <dd>
                                                    <?= h($people->birth) ?>
                                                </dd>
                                                <dt><?= __('Sexo') ?></dt>
                                                <dd>
                                                    <?= h($people->sexo) ?>
                                                </dd>

                                            </dl>

                                            <dl class="dl-horizontal col-md-6">
                                                <dt><?= __('Pai') ?></dt>
                                                <dd>
                                                    <?= h($people->father_name) ?>
                                                </dd>
                                                <dt><?= __('Mãe') ?></dt>
                                                <dd>
                                                    <?= h($people->mother_name) ?>
                                                </dd>
                                                <dt><?= __('Telefone') ?></dt>
                                                <dd>
                                                    <?= h($people->phone) ?>
                                                </dd>
                                                <dt><?= __('Celular') ?></dt>
                                                <dd>
                                                    <?= h($people->cell_phone) ?>
                                                </dd>

                                                <dt><?= __('Email') ?></dt>
                                                <dd>
                                                    <?= h($people->email) ?>
                                                </dd>

                                            </dl>
                                        </div>


                                        <?= $this->element('Forms/peopleModal', ['data' => $people, 'url' => ['action' => 'edit', $people->id]]) ?>
                                    </div>
                                    <div class=" tab-pane" id="endereco">

                                        <div class="row">
                                            <dl class="dl-horizontal col-md-6">
                                                <dt><?= __('CEP') ?></dt>
                                                <dd>
                                                    <?= h($people->address->zipcode) ?>
                                                </dd>
                                                <dt><?= __('Endereço') ?></dt>
                                                <dd>
                                                    <?= h($people->address->street) ?>
                                                    , <?= h($people->address->number) ?>
                                                </dd>
                                                <dt><?= __('Bairro') ?></dt>
                                                <dd>
                                                    <?= h($people->address->neighborhood) ?>
                                                </dd>
                                            </dl>
                                            <dl class="dl-horizontal col-md-6">
                                                <dt><?= __('Cidade') ?></dt>
                                                <dd>
                                                    <?= h($people->address->city) ?>
                                                </dd>
                                                <dt><?= __('Estado') ?></dt>
                                                <dd>
                                                    <?= h($people->address->state) ?>
                                                </dd>
                                                <dt><?= __('Complemento') ?></dt>
                                                <dd>
                                                    <?= h($people->address->complement) ?>
                                                </dd>
                                            </dl>
                                        </div>


                                        <?= $this->element('Forms/addressModal', ['data' => $people, 'prefix' => 'address', 'url' => ['action' => 'edit', $people->id]]) ?>
                                    </div>
                                    <div class="tab-pane" id="responsaveis">

                                        <?= $this->element('Forms/responsibleModal', ['data' => $people_entity, 'people_id' => $people->id, 'url' => ['action' => 'responsible']]); ?>

                                        <br>
                                        <br>
                                        <?php if (!empty($people->responsibles)): ?>

                                            <table class="table table-hover">
                                                <tbody>
                                                <tr>
                                                    <th>Nome</th>
                                                    <th>Cpf</th>
                                                    <th>Responsavel Financeiro</th>
                                                    <th>Responsavel Pedagógico</th>
                                                    <th>Email</th>
                                                    <th>Celular</th>
                                                    <th><?php echo __('Ações'); ?></th>
                                                </tr>

                                                <?php foreach ($people->responsibles as $responsible): ?>
                                                    <tr>

                                                        <td>
                                                            <?= h($responsible->name) ?>
                                                        </td>

                                                        <td>
                                                            <?= h($responsible->cpf) ?>
                                                        </td>

                                                        <td>
                                                            <?= ($responsible->responsible_financial) ? 'Sim' : 'Não' ?>
                                                        </td>

                                                        <td>
                                                            <?= ($responsible->responsible_educational) ? 'Sim' : 'Não' ?>
                                                        </td>

                                                        <td>
                                                            <?= h($responsible->email) ?>
                                                        </td>
                                                        <td>
                                                            <?= h($responsible->cell_phone) ?>
                                                        </td>

                                                        <td class="actions">
                                                            <?= $this->element('Forms/responsibleModal',
                                                                [
                                                                    'data' => $responsible,
                                                                    'people_id' => $people->id,
                                                                    'url' => ['action' => 'responsible', $responsible->id],
                                                                    'text' => 'Editar',
                                                                    'id' => $responsible->id
                                                                ]); ?>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>

                                                </tbody>
                                            </table>

                                        <?php endif; ?>
                                    </div>
                                    <div class="tab-pane" id="system">
                                        <?php if ($people->user): ?>
                                            <div class="alert alert-info">
                                                Este usuário deve ser usado para acesso ao sistema, divulga-lo para o
                                                responsavel.
                                            </div>
                                            <dl class="dl-horizontal">
                                                <dt><?= __('Usuário') ?></dt>
                                                <dd>
                                                    <?= h($people->user->username) ?>
                                                </dd>

                                                <?php if ($people->user->password == '$2y$10$wPT7z.7HyMcLkL9zg4R93OZcJyRdDWHcqHCmLDDr1Ji1mP2t2.RlS'): ?>
                                                    <dt><?= __('Criar Senha') ?></dt>
                                                    <dd>
                                                        <?= $this->element('Forms/passwordModal', ['text' => 'Gerar', 'user' => $people->user, 'url' => ['controller' => 'users', 'action' => 'password', $people->user->id]]); ?>
                                                    </dd>
                                                <?php else: ?>
                                                    <dt><?= __('Alterar Senha') ?></dt>
                                                    <dd>
                                                        <?= $this->element('Forms/passwordModal', ['text' => 'Editar', 'user' => $people->user, 'url' => ['controller' => 'users', 'action' => 'password', $people->user->id]]); ?>
                                                    </dd>
                                                <?php endif; ?>
                                            </dl>
                                        <?php else: ?>
                                            <?= $this->element('Forms/userModal', ['user' => $user, 'prefix' => 'user', 'url' => ['action' => 'edit', $people->id]]); ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
