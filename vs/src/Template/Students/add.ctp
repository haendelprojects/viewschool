<div class="panel-header panel-header-sm">
</div>


<div class="content">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">

                    </h4>
                </div>


                <div class="card-body">
                    <?= $this->Form->create($people, array('role' => 'form')) ?>
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-12">

                            <strong class="box-title"><?= __('Detalhes') ?></strong>
                            <hr>
                            <?php
                            echo $this->Form->input('name', ['label' => 'Nome']);
                            echo $this->Form->input('cpf', ['label' => 'CPF', 'class' => 'cpf']);
                            echo $this->Form->input('phone', ['label' => 'Telefone', 'class' => 'phone', 'maxlength' => 15]);
                            echo $this->Form->input('cell_phone', ['label' => 'Celular', 'class' => 'sp_celphones', 'maxlength' => 15]);
                            echo $this->Form->input('birth', ['label' => 'Nascimento', 'empty' => true, 'default' => '', 'class' => 'datepicker form-control', 'type' => 'text']);
                            echo $this->Form->input('type', ['value' => 'ALUNO', 'label' => 'Tipo', 'type' => 'hidden']);
                            echo $this->Form->input('sexo', ['label' => 'Sexo', 'options' => ['FEMININO' => 'Feminino', 'MASCULINO' => 'Masculino']]);
                            echo $this->Form->input('father_name', ['label' => 'Nome do Pai']);
                            echo $this->Form->input('mother_name', ['label' => 'Nome da Mãe']);
                            echo $this->Form->input('email', ['label' => 'E-mail']);
                            ?>

                            <strong class="box-title"><?= __('Endereço') ?></strong>

                            <hr>
                            <?php
                            echo $this->Form->input('address.zipcode', ['label' => 'CEP']);
                            echo $this->Form->input('address.street', ['label' => 'Endereço']);
                            echo $this->Form->input('address.number', ['label' => 'Número']);
                            echo $this->Form->input('address.neighborhood', ['label' => 'Bairro']);
                            echo $this->Form->input('address.city', ['label' => 'Cidade']);
                            echo $this->Form->input('address.state', ['label' => 'Estado']);
                            echo $this->Form->input('address.complement', ['label' => 'Complemento']);
                            ?>

                            <!-- /.box-body -->

                            <?= $this->Form->button(__('Cadastrar')) ?>

                        </div>
                    </div>
                </div>

                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>

        