<form class="form" method="POST" action="/users/login">
    <div class="card card-login card-plain">

        <div class="card-header ">
            <div class="logo-container">
                <img src="/img/logo-azul.png" alt="">
            </div>
        </div>

        <div class="card-body ">
            <div class="input-group no-border form-control-lg">
                <span class="input-group-addon">
                    <i class="now-ui-icons users_circle-08"></i>
                </span>
                <input type="text" class="form-control" name="username" placeholder="Usuário">
            </div>

            <div class="input-group no-border form-control-lg">
                <span class="input-group-addon">
                    <i class="now-ui-icons text_caps-small"></i>
                </span>
                <input type="password" placeholder="Senha" name="password" class="form-control">
            </div>

            <button type="submit" class="btn btn-primary btn-round btn-lg btn-block">Acessar</button>
        </div>

        <div class="card-footer ">

            <div class="pull-left">
                <h6><a href="#pablo" class="link footer-link">Esqueçeu a senha?</a></h6>
            </div>

            <div class="pull-right">
                <h6><a href="#pablo" class="link footer-link">Precisa de Ajuda?</a></h6>
            </div>
        </div>

    </div>
</form>
