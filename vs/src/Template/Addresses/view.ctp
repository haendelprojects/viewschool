<section class="content-header">
  <h1>
    <?php echo __('Address'); ?>
  </h1>
  <ol class="breadcrumb">
    <li>
    <?= $this->Html->link('<i class="fa fa-dashboard"></i> ' . __('Back'), ['action' => 'index'], ['escape' => false])?>
    </li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-md-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <i class="fa fa-info"></i>
                <h3 class="box-title"><?php echo __('Information'); ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <dl class="dl-horizontal">
                                                                                                                <dt><?= __('Street') ?></dt>
                                        <dd>
                                            <?= h($address->street) ?>
                                        </dd>
                                                                                                                                                            <dt><?= __('Number') ?></dt>
                                        <dd>
                                            <?= h($address->number) ?>
                                        </dd>
                                                                                                                                                            <dt><?= __('Neighborhood') ?></dt>
                                        <dd>
                                            <?= h($address->neighborhood) ?>
                                        </dd>
                                                                                                                                                            <dt><?= __('City') ?></dt>
                                        <dd>
                                            <?= h($address->city) ?>
                                        </dd>
                                                                                                                                                            <dt><?= __('State') ?></dt>
                                        <dd>
                                            <?= h($address->state) ?>
                                        </dd>
                                                                                                                                                            <dt><?= __('Complement') ?></dt>
                                        <dd>
                                            <?= h($address->complement) ?>
                                        </dd>
                                                                                                                                    
                                            
                                                                                                                                            
                                                                                                                                                                                                
                                            
                                    </dl>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- ./col -->
</div>
<!-- div -->

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <i class="fa fa-share-alt"></i>
                    <h3 class="box-title"><?= __('Related {0}', ['Branchs']) ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">

                <?php if (!empty($address->branchs)): ?>

                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                                                    
                                    <th>
                                    Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Name
                                    </th>
                                        
                                                                                                                                            
                                    <th>
                                    Cnpj
                                    </th>
                                        
                                                                    
                                    <th>
                                    Address Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Phone
                                    </th>
                                        
                                                                    
                                    <th>
                                    Email
                                    </th>
                                        
                                                                    
                                    <th>
                                    School Id
                                    </th>
                                        
                                                                    
                                <th>
                                    <?php echo __('Actions'); ?>
                                </th>
                            </tr>

                            <?php foreach ($address->branchs as $branchs): ?>
                                <tr>
                                                                        
                                    <td>
                                    <?= h($branchs->id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($branchs->name) ?>
                                    </td>
                                                                                                                                                
                                    <td>
                                    <?= h($branchs->cnpj) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($branchs->address_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($branchs->phone) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($branchs->email) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($branchs->school_id) ?>
                                    </td>
                                    
                                                                        <td class="actions">
                                    <?= $this->Html->link(__('View'), ['controller' => 'Branchs', 'action' => 'view', $branchs->id], ['class'=>'btn btn-info btn-xs']) ?>

                                    <?= $this->Html->link(__('Edit'), ['controller' => 'Branchs', 'action' => 'edit', $branchs->id], ['class'=>'btn btn-warning btn-xs']) ?>

                                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Branchs', 'action' => 'delete', $branchs->id], ['confirm' => __('Are you sure you want to delete # {0}?', $branchs->id), 'class'=>'btn btn-danger btn-xs']) ?>    
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                                    
                        </tbody>
                    </table>

                <?php endif; ?>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <i class="fa fa-share-alt"></i>
                    <h3 class="box-title"><?= __('Related {0}', ['Peoples']) ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">

                <?php if (!empty($address->peoples)): ?>

                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                                                    
                                    <th>
                                    Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Name
                                    </th>
                                        
                                                                    
                                    <th>
                                    Cpf
                                    </th>
                                        
                                                                    
                                    <th>
                                    Phone
                                    </th>
                                        
                                                                    
                                    <th>
                                    Cell Phone
                                    </th>
                                        
                                                                                                                                            
                                    <th>
                                    Birth
                                    </th>
                                        
                                                                    
                                    <th>
                                    Type
                                    </th>
                                        
                                                                    
                                    <th>
                                    Sexo
                                    </th>
                                        
                                                                    
                                    <th>
                                    Foto
                                    </th>
                                        
                                                                    
                                    <th>
                                    Father Name
                                    </th>
                                        
                                                                    
                                    <th>
                                    Mother Name
                                    </th>
                                        
                                                                    
                                    <th>
                                    Email
                                    </th>
                                        
                                                                    
                                    <th>
                                    Address Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    User Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Branch Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    People Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Responsible Educational
                                    </th>
                                        
                                                                    
                                    <th>
                                    Responsible Financial
                                    </th>
                                        
                                                                    
                                <th>
                                    <?php echo __('Actions'); ?>
                                </th>
                            </tr>

                            <?php foreach ($address->peoples as $peoples): ?>
                                <tr>
                                                                        
                                    <td>
                                    <?= h($peoples->id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($peoples->name) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($peoples->cpf) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($peoples->phone) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($peoples->cell_phone) ?>
                                    </td>
                                                                                                                                                
                                    <td>
                                    <?= h($peoples->birth) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($peoples->type) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($peoples->sexo) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($peoples->foto) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($peoples->father_name) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($peoples->mother_name) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($peoples->email) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($peoples->address_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($peoples->user_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($peoples->branch_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($peoples->people_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($peoples->responsible_educational) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($peoples->responsible_financial) ?>
                                    </td>
                                    
                                                                        <td class="actions">
                                    <?= $this->Html->link(__('View'), ['controller' => 'Peoples', 'action' => 'view', $peoples->id], ['class'=>'btn btn-info btn-xs']) ?>

                                    <?= $this->Html->link(__('Edit'), ['controller' => 'Peoples', 'action' => 'edit', $peoples->id], ['class'=>'btn btn-warning btn-xs']) ?>

                                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Peoples', 'action' => 'delete', $peoples->id], ['confirm' => __('Are you sure you want to delete # {0}?', $peoples->id), 'class'=>'btn btn-danger btn-xs']) ?>    
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                                    
                        </tbody>
                    </table>

                <?php endif; ?>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
