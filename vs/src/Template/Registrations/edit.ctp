<div class="panel-header panel-header-sm">
</div>


<div class="content">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">

                    </h4>
                </div>


                <div class="card-body">
                    <!-- /.box-header -->
                    <!-- form start -->
                    <?= $this->Form->create($registration, array('role' => 'form')) ?>
                    <div class="box-body">
                        <div class="box-body">
                            <?php
                            echo $this->Form->input('people_id', ['options' => $this->Util->listOptions('Peoples', ['name' => 'asc'], ['type' => 'ALUNO']), 'label' => 'Aluno', 'disabled' => true]);
                            echo $this->Form->input('people_id', ['options' => $this->Util->listOptions('Class', ['name' => 'asc'], []), 'label' => 'Classe', 'disabled' => true]);
                            echo $this->Form->input('value_material', ['label' => 'Taxa de Material', 'required' => true]);
                            echo $this->Form->input('value_monthly', ['label' => 'Valor da Mensalidade', 'required' => true]);
                            echo $this->Form->input('value_registration', ['label' => 'Valor da Matrícula', 'required' => true]);

                            echo $this->Form->input('discount_value', ['label' => 'Desconto até o vencimento', 'required' => true]);
                            echo $this->Form->input('discount_type', ['label' => 'Tipo de desconto', 'options' => ['R$' => 'R$', '%' => '%']]);
                            echo $this->Form->input('observation', ['label' => 'Observação']);
                            ?>

                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <?= $this->Form->button(__('Salvar'), ['class' => 'btn btn-success']) ?>
                    </div>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</div>


