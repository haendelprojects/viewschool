<div class="panel-header panel-header-sm">
</div>


<div class="content">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">
                        <div class="float-right">

                        </div>
                    </h4>
                </div>


                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-solid">
                                <div class="box-header with-border">
                                    <i class="fa fa-info"></i>
                                    <h3 class="box-title"><?php echo __('Information'); ?></h3>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <dl class="dl-horizontal">
                                        <dt><?= __('Year') ?></dt>
                                        <dd>
                                            <?= h($registration->year) ?>
                                        </dd>
                                        <dt><?= __('Discount Type') ?></dt>
                                        <dd>
                                            <?= h($registration->discount_type) ?>
                                        </dd>
                                        <dt><?= __('People') ?></dt>
                                        <dd>
                                            <?= $registration->has('people') ? $registration->people->name : '' ?>
                                        </dd>
                                        <dt><?= __('Clas') ?></dt>
                                        <dd>
                                            <?= $registration->has('clas') ? $registration->clas->name : '' ?>
                                        </dd>
                                        <dt><?= __('Branch') ?></dt>
                                        <dd>
                                            <?= $registration->has('branch') ? $registration->branch->name : '' ?>
                                        </dd>


                                        <dt><?= __('Value Material') ?></dt>
                                        <dd>
                                            <?= $this->Number->format($registration->value_material) ?>
                                        </dd>
                                        <dt><?= __('Value Monthly') ?></dt>
                                        <dd>
                                            <?= $this->Number->format($registration->value_monthly) ?>
                                        </dd>
                                        <dt><?= __('Value Registration') ?></dt>
                                        <dd>
                                            <?= $this->Number->format($registration->value_registration) ?>
                                        </dd>
                                        <dt><?= __('Discount Value') ?></dt>
                                        <dd>
                                            <?= $this->Number->format($registration->discount_value) ?>
                                        </dd>


                                        <dt><?= __('Observation') ?></dt>
                                        <dd>
                                            <?= $this->Text->autoParagraph(h($registration->observation)); ?>
                                        </dd>
                                    </dl>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                        <!-- ./col -->
                    </div>
                    <!-- div -->

                </div>
            </div>
        </div>
    </div>
</div>
