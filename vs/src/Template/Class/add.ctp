<div class="panel-header panel-header-sm">
</div>


<div class="content">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">
                        <div class="float-right">

                        </div>
                    </h4>
                </div>


                <div class="card-body">
                    <h5 class="box-title"><?= __('Informações') ?></h5>
                    <hr>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <?= $this->Form->create($clas, array('role' => 'form')) ?>
                    <div class="box-body">
                        <?php
                        echo $this->Form->input('course_id', ['options' => $courses, 'label' => 'Curso/Série']);
                        echo $this->Form->input('name', ['label' => 'Nome <small>Ex.: A</small>', 'escape' => false, 'placeholder' => 'Informe o nome da turma', 'required' => 'required']);
                        echo $this->Form->input('init_date', ['label' => 'Data de Inicio', 'empty' => true, 'default' => '', 'class' => 'datepicker form-control', 'type' => 'text', 'required' => 'required']);
                        echo $this->Form->input('end_date', ['label' => 'Data Final', 'empty' => true, 'default' => '', 'class' => 'datepicker form-control', 'type' => 'text', 'required' => 'required']);
                        echo $this->Form->input('period', ['label' => 'Periodo', 'options' => ['MANHA' => 'Manhã', 'TARDE' => 'Tarde', 'NOITE' => 'Noite', 'required' => 'required']]);

                        echo $this->Form->input('status', ['label' => 'Status', 'options' => ['ABERTO' => 'ABERTO', 'FECHADO' => 'FECHADO'], 'required' => 'required']);
                        echo $this->Form->input('class_time', ['label' => 'Carga horária total']);
                        echo $this->Form->input('school_year', ['label' => 'Ano Letivo', 'options' => $this->Util->years(), 'required' => 'required']);
                        echo $this->Form->input('evaluation_id', ['label' => 'Critério de Avaliação', 'options' => $evaluations, 'empty' => true, 'required' => 'required']);
                        echo $this->Form->input('people_id', ['label' => 'Professor Responsável', 'options' => $peoples, 'empty' => true, 'required' => 'required']);
                        ?>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <?= $this->Form->button(__('Cadastrar')) ?>
                    </div>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</div>