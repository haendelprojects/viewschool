<div class="panel-header panel-header-sm">
</div>
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">
                        <div class="float-right">
                        </div>
                    </h4>
                </div>
                <div class="card-body">
                    <div class="nav-tabs-custom">
                        <ul class="nav-pills-primary nav nav-pills">
                            <li class=" nav-item"><a href="#activity" class="nav-link active" data-toggle="tab">Informações</a>
                            </li>
                            <li class="nav-item"><a href="#disc" class="nav-link" data-toggle="tab">Disciplinas</a></li>
                            <li class="nav-item"><a href="#grid" class="nav-link" data-toggle="tab">Grade de Horário</a></li>
                            <li class="nav-item"><a href="#students" class="nav-link" data-toggle="tab">Alunos
                                    Matriculados</a></li>
                            <li class="nav-item"><a href="#freq" class="nav-link" data-toggle="tab">Aulas e
                                    Frequência</a></li>
                            <li class="nav-item"><a href="#ava" class="nav-link" data-toggle="tab">Avaliações</a></li>

                        </ul>
                        <div class="tab-content pt-5">
                            <div class=" active tab-pane" id="activity">

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-3"><strong><?= __('Curso') ?></strong></div>
                                            <div class="col-md-9">
                                                <td><?= $clas->has('course') ? $this->Html->link($clas->course->name, ['controller' => 'Courses', 'action' => 'view', $clas->course->id]) : '' ?></td>
                                            </div>
                                            <div class="col-md-3"><strong><?= __('Nome') ?></strong></div>
                                            <div class="col-md-9">
                                                <?= h($clas->name) ?>
                                            </div>
                                            <div class="col-md-3"><strong><?= __('Unidade') ?></strong></div>
                                            <div class="col-md-9">
                                                <?= $clas->has('branch') ? $clas->branch->name : '' ?>
                                            </div>

                                            <div class="col-md-3"><strong><?= __('Periodo') ?></strong></div>
                                            <div class="col-md-9">
                                                <?= h($clas->period) ?>
                                            </div>
                                            <div class="col-md-3"><strong><?= __('Status') ?></strong></div>
                                            <div class="col-md-9">
                                                <?= h($clas->status) ?>
                                            </div>

                                            <div class="col-md-3"><strong><?= __('Professor Responsável') ?></strong>
                                            </div>
                                            <div class="col-md-9">
                                                <td><?= $clas->has('people') ? $this->Html->link($clas->people->name, ['controller' => 'Peoples', 'action' => 'view', $clas->people->id]) : '' ?></td>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-3"><strong><?= __('Carga Horária') ?></strong></div>
                                            <div class="col-md-9">
                                                <?= h($clas->class_time) ?>h
                                            </div>

                                            <div class="col-md-3"><strong><?= __('Ano Letivo') ?></strong></div>
                                            <div class="col-md-9">
                                                <?= h($clas->school_year) ?>
                                            </div>

                                            <div class="col-md-3"><strong><?= __('Data de inicio') ?></strong></div>
                                            <div class="col-md-9">
                                                <?= h($clas->init_date) ?>
                                            </div>
                                            <div class="col-md-3"><strong><?= __('Data Final') ?></strong></div>
                                            <div class="col-md-9">
                                                <?= h($clas->end_date) ?>
                                            </div>

                                            <div class="col-md-3"><strong><?= __('Critério Avaliativo') ?></strong>
                                            </div>
                                            <div class="col-md-9">
                                                <td><?= $clas->has('evaluation') ? $this->Html->link($clas->evaluation->name, ['controller' => 'Evaluations', 'action' => 'view', $clas->evaluation->id]) : '' ?></td>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <?= $this->Html->link('<i class="fa fa-pencil"></i> ' . __('Editar'), ['action' => 'edit', $clas->id], ['escape' => false, 'class' => 'pull-right btn btn-warning btn-xs']) ?>
                            </div>
                            <div class="tab-pane" id="disc">
                                <div class="">
                                    <table class="table table-striped  table-bordered">
                                        <tr>
                                            <th>Disciplina</th>
                                            <th>Professor</th>
                                            <th>Nota Mínima</th>
                                            <th>Carga Horária</th>
                                            <th>Aulas</th>
                                            <th></th>
                                        </tr>
                                        <?php foreach ($clas->class_discipline as $discipline): ?>
                                            <tr>

                                                <td><?= $discipline->discipline->name ?></td>
                                                <td><?= $discipline->people->name ?></td>
                                                <td><?= $discipline->minimum_grade ?></td>
                                                <td><?= $discipline->workload ?> h</td>
                                                <td>
                                                    <?php foreach ($discipline->times as $time) : ?>
                                                        <?= $time->day_week ?> -  <?= $time->init_time ?>h às <?= $time->end_time ?>h
                                                    <?php endforeach; ?>
                                                </td>
                                                <td class="actions" style="white-space:nowrap">
                                                    <?php if ($this->User->isSchool()): ?>
                                                        <?= $this->element('Forms/editDisciplineModal', ['discipline' => $discipline]); ?>
                                                        <?= $this->Form->postLink(__('Remover'), ['controller' => 'ClassDiscipline', 'action' => 'delete', $discipline->id], ['confirm' => __('Confirma a exclusão da disciplina para a classe?'), 'class' => 'btn btn-danger btn-xs']) ?>
                                                    <?php endif ?>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </table>

                                    <?= $this->element('Forms/addDisciplineModal', ['class_id' => $clas->id]); ?>
                                </div>
                            </div>
                            <div class="tab-pane" id="students">


                                <table class="table table-striped  table-bordered">
                                    <tr>
                                        <th>Aluno</th>
                                        <th></th>
                                    </tr>
                                    <?php foreach ($clas->registrations as $registrate): ?>
                                        <tr>

                                            <td><?= $registrate->people->name ?></td>
                                            <td class="actions" style="white-space:nowrap">
                                                <?php if ($this->User->isSchool()): ?>
                                                    <?= $this->Html->link(__('Editar'), ['controller' => 'registrations', 'action' => 'edit', $registrate->id], ['class' => 'btn btn-warning btn-xs']) ?>
                                                <?php endif ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </table>

                                <?= $this->element('Forms/addRegistrationModal', ['class_id' => $clas->id]); ?>

                            </div>
                            <div class="tab-pane" id="freq">
                                <div>
                                    <?= $this->Html->link(__('Nova Aula'), ['controller' => 'lessons', 'action' => 'add', $clas->id], ['class' => 'btn btn-default btn-sm ', 'escape' => false]) ?>
                                </div>
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th><?= $this->Paginator->sort('discipline_id', 'Disciplina') ?></th>
                                        <th><?= $this->Paginator->sort('title', 'Aula') ?></th>
                                        <th><?= $this->Paginator->sort('day', 'Dia') ?></th>
                                        <th><?= __('Ações') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($lessons as $lesson): ?>
                                        <tr>
                                            <td><?= $lesson->has('discipline') ? $this->Html->link($lesson->discipline->name, ['controller' => 'Disciplines', 'action' => 'view', $lesson->discipline->id]) : '' ?></td>
                                            <td><?= h($lesson->title) ?></td>
                                            <td><?= h($lesson->day->format('d/m/Y')) ?></td>
                                            <td class="actions" style="white-space:nowrap">
                                                <?= $this->Html->link(__('Detalhes'), ['action' => 'view', $lesson->id], ['class' => 'btn btn-info btn-xs']) ?>
                                                <?= $this->Html->link(__('Editar'), ['action' => 'edit', $lesson->id], ['class' => 'btn btn-warning btn-xs']) ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane" id="ava">
                                <div>
                                    <?= $this->Html->link(__('Nova Avaliação'), ['controller' => 'exams', 'action' => 'add', $clas->id], ['class' => 'btn btn-default btn-sm ', 'escape' => false]) ?>
                                </div>
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th><?= $this->Paginator->sort('discipline_id', 'Disciplina') ?></th>
                                        <th><?= $this->Paginator->sort('title', 'Aula') ?></th>
                                        <th><?= $this->Paginator->sort('day', 'Dia') ?></th>
                                        <th><?= __('Ações') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($ava as $lesson): ?>
                                        <tr>
                                            <td><?= $lesson->has('discipline') ? $this->Html->link($lesson->discipline->name, ['controller' => 'Disciplines', 'action' => 'view', $lesson->discipline->id]) : '' ?></td>
                                            <td><?= h($lesson->title) ?></td>
                                            <td><?= h($lesson->day->format('d/m/Y')) ?></td>
                                            <td class="actions" style="white-space:nowrap">
                                                <?= $this->Html->link(__('Detalhes'), ['action' => 'view', $lesson->id], ['class' => 'btn btn-info btn-xs']) ?>
                                                <?= $this->Html->link(__('Editar'), ['action' => 'edit', $lesson->id], ['class' => 'btn btn-warning btn-xs']) ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane" id="grid">
                                <table class="table table-bordered">
                                    <tr>
                                        <th>Segunda</th>
                                        <th>Terça</th>
                                        <th>Quarta</th>
                                        <th>Quinta</th>
                                        <th>Sexta</th>
                                        <th>Sábado</th>
                                    </tr>

                                    <tr>
                                        <td>
                                            <?= $this->Util->dayDiscipline($clas->class_discipline, 'SEG'); ?>
                                        </td>

                                        <td>
                                            <?= $this->Util->dayDiscipline($clas->class_discipline, 'TER'); ?>
                                        </td>

                                        <td>
                                            <?= $this->Util->dayDiscipline($clas->class_discipline, 'QUA'); ?>
                                        </td>

                                        <td>
                                            <?= $this->Util->dayDiscipline($clas->class_discipline, 'QUI'); ?>
                                        </td>

                                        <td>
                                            <?= $this->Util->dayDiscipline($clas->class_discipline, 'SEX'); ?>
                                        </td>

                                        <td>
                                            <?= $this->Util->dayDiscipline($clas->class_discipline, 'SAB'); ?>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <!-- ./col -->
                </div>
                <!-- div -->

            </div>
        </div>
    </div>
</div>
