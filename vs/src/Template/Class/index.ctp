<div class="panel-header panel-header-sm">
</div>


<div class="content">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">
                        <div class="float-right">
                            <?= $this->Html->link(__('Novo(a)'), ['action' => 'add'], ['class' => 'btn btn-default btn-sm ', 'escape' => false]) ?>
                        </div>
                    </h4>
                </div>


                <div class="card-body">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th><?= $this->Paginator->sort('course_id', 'Curso') ?></th>
                            <th><?= $this->Paginator->sort('name', 'Nome') ?></th>
                            <th><?= $this->Paginator->sort('school_year', 'Ano') ?></th>
                            <th><?= __('Ações') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($class as $clas): ?>
                            <tr>
                                <td><?= $clas->has('course') ? $this->Html->link($clas->course->name, ['controller' => 'Courses', 'action' => 'view', $clas->course->id]) : '' ?></td>
                                <td><?= h($clas->name) ?></td>
                                <td><?= h($clas->school_year) ?></td>
                                <td class="actions" style="white-space:nowrap">
                                    <?= $this->Html->link(__('Detalhe'), ['action' => 'view', $clas->id], ['class' => 'btn btn-info btn-xs']) ?>

                                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $clas->id], ['class' => 'btn btn-warning btn-xs']) ?>

                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right">
                        <?php echo $this->Paginator->numbers(); ?>
                    </ul>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</div>
<!-- /.content -->
