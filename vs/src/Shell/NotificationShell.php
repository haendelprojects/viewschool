<?php
/**
 * Created by PhpStorm.
 * User: Ananda
 * Date: 25/08/2016
 * Time: 22:11
 */

namespace App\Shell;


use Notifications\Controller\Component\ClientNotificationComponent;
use Bake\Shell\Task\ComponentTask;
use Cake\Console\Shell;
use Cake\Controller\ComponentRegistry;
use Cake\Core\App;

class NotificationShell extends Shell
{

    public $ClientNotification = null;

    public function initialize()
    {
        parent::initialize();

        $this->loadModel('Occurrences');
        $this->ClientNotification = new ClientNotificationComponent(new ComponentRegistry());
    }

    /**
     * Enviar notificação de chamados abertos
     */
    public function broadcast()
    {
        $conditions = [
            'status' => 'OPENED',
            'specialist_user_id is' => null
        ];

        $occurrences = $this->Occurrences
            ->find('all')
            ->where($conditions)
            ->contain([
                'Addresses'
            ]);

        foreach ($occurrences as $occurrence) {
            if ($occurrence->address->lat && $occurrence->address->lng) {
                $this->ClientNotification->open($occurrence->id, 70000, $occurrence->address->lat, $occurrence->address->lng);
            }
        }
        exit();
    }

    /**
     * Lembrar o técnico que ele tem atendimento para daqui a alguns minutos
     * @param $interval
     * @param null $precondition
     */
    public function rememberOccurrence($interval, $precondition = null)
    {
        $conditions = [
            'status' => 'ACCEPTED',
            'specialist_user_id IS NOT' => null,
            'schedule_time BETWEEN  NOW() AND DATE_ADD(NOW(), INTERVAL ' . $interval . ')'
        ];

        if ($precondition == null) {
            $conditions['service_alert IS'] = $precondition;
        } else {
            $conditions['service_alert'] = $precondition;
        }


        $occurrences = $this->Occurrences
            ->find('all')
            ->where($conditions)
            ->contain([
                'Addresses'
            ]);

        $this->out($occurrences->toArray());
    }
}