<?php
namespace App\Shell;

use App\Model\Table\ContractsTable;
use App\Model\Table\FundsTable;
use App\Model\Table\OccurrencesTable;
use Cake\Console\Shell;
use Cake\I18n\Time;

/**
 * Contract shell command.
 * @property ContractsTable $Contracts
 * @property FundsTable $Funds
 * @property OccurrencesTable $Occurrences
 */
class ContractShell extends Shell
{
    public function initialize()
    {
        parent::initialize();

        $this->loadModel('Contracts');
        $this->loadModel('Funds');
        $this->loadModel('Occurrences');
    }

    public function contractsValidity()
    {
        $time = Time::now();
        $contracts = $this->Contracts
            ->find('all')
            ->where([
                'Funds.type IN' => [
                    'RECURRENT', 'FREE', 'DETACHED'
                ]
            ])
            ->contain([
                'Funds'
            ]);

        foreach ($contracts as $contract) {
            if ($contract->fund->type != 'RECURRENT') {
                if($contract->final_date <= $time) {
                    $data['active'] = $this->checkCreditBalance($contract);
                } else {
                    $data = [
                        'active' => false
                    ];
                }
            } else {
                $fund_month = $this->Funds->FundsMonths
                    ->find('all')
                    ->where([
                        'fund_id' => $contract->fund->id
                    ])
                    ->last();
                if ($fund_month->stored_points_validity >= $time)
                {
                    $data['active'] = $this->checkCreditBalance($contract);
                } else {

                    $data = [
                        'active' => false
                    ];
                }
            }
            $contract = $this->Contracts
                ->patchEntity($contract, $data);

            if(!($this->Contracts->save($contract))){
                debug($contract->errors());
            }
        }
    }

    public function checkCreditBalance($contract) {
        if($contract->fund->credit_balance > 0){
            return true;
        } else {
            return false;
        }
    }

    public function refill()
    {
        $day = Time::now();

        $day = $day->day;
        echo($day);

        $conditions = [
            'Funds.type' => 'RECURRENT'
        ];

        $contracts = $this->Contracts
            ->find('all')
            ->where($conditions)
            ->contain(['Funds', 'Funds.FundsMonths']);

        foreach ($contracts as $contract) {
            $this->insert_queue($contract);
        }
    }

    public function insert_queue($contract)
    {
        if ($contract->fund->type == 'RECURRENT') {
            $this->recurrent_queue($contract);
        } else {
            $this->detached_queue($contract);
        }
        $this->credit_balance($contract);
    }

    public function _expired($validity)
    {
        $now = Time::now();

        if ($now > $validity) {
            return 1;
        } else {
            return 0;
        }
    }

    public function credit_balance($contract)
    {
        $time = Time::now();
        $month_points = 0;

        $occurrences = $this->Occurrences->find();
        $expense = $occurrences
            ->find('all')
            ->where([
                'status NOT IN' => [
                    'INVALID', 'CANCELED'
                ],
                'Occurrences.contract_id' => $contract->id,
                'Occurrences.active' => true
            ])
            ->contain(['Prices'])
            ->select(['total' => $occurrences->func()->sum('Prices.occurrence_total_price')])
            ->first();

        $credit_balance = $contract->fund->total_points_quantity - $expense->total;

        if($credit_balance <= 0) {

            $data = [
                'active' => false,
                'total_expense' => $expense->total,
                'credit_balance' => $credit_balance,
                'month_points' => $month_points
            ];

            $d = [
                'active' => 0
            ];

            $contract = $this->Contracts->patchEntity($contract, $d);
            $this->Contracts->save($contract);

        } else {
            $fund_month = $this->fund_month($contract->fund->id);
            if($fund_month) {
                if ($contract->fund->type == 'RECURRENT') {
                    $month_points = $fund_month->accumulated_points;
                } else {
                    $month_points = $fund_month->spare_points;
                }
            } else {
                $month_points = 0;
            }

            $data = [
                'total_expense' => $expense->total,
                'credit_balance' => $credit_balance,
                'month_points' => $month_points
            ];
        }

        $fund = $this->Funds->patchEntity($contract->fund, $data);

        if (!$this->Funds->save($fund)) {
            debug($fund->errors());
        }

        return true;
    }

    public function recurrent_queue($contract)
    {
        $funds_months = $this->Funds->FundsMonths
            ->find('all')
            ->where([
                'fund_id' => $contract->fund->id,
                'active' => true
            ]);

        $stored_points_expense = 0;
        $owed_points = 0;
        $initial_date = clone $contract->initial_date->modify('-1 day');

        foreach ($funds_months as $fund_month) {
            if ($initial_date <= $fund_month->final_date) {
                if($initial_date <= $contract->initial_date) {
                    $occurrences = $this->Occurrences->find();
                    $occurrences = $occurrences
                        ->find('all')
                        ->where([
                            'status NOT IN' => [
                                'INVALID', 'CANCELED'
                            ],
                            'Occurrences.active' => true,
                            'Occurrences.contract_id' => $contract->id,
                            'Occurrences.created <=' => $fund_month->final_date
                        ])
                        ->contain(['Prices'])
                        ->select(['total' => $occurrences->func()->sum('Prices.occurrence_total_price')])
                        ->first();

                } else if ($initial_date >= $fund_month->initial_date && $fund_month->final_date < $contract->final_date && $initial_date > $contract->initial_date) {
                    $occurrences = $this->Occurrences->find();
                    $occurrences = $occurrences
                        ->find('all')
                        ->where([
                            'status NOT IN' => [
                                'INVALID', 'CANCELED'
                            ],
                            'Occurrences.active' => true,
                            'Occurrences.contract_id' => $contract->id,
                            'Occurrences.created >' => $initial_date,
                            'Occurrences.created <=' => $fund_month->final_date
                        ])
                        ->contain(['Prices'])
                        ->select(['total' => $occurrences->func()->sum('Prices.occurrence_total_price')])
                        ->first();

                } else if ($fund_month->final_date >= $contract->final_date) {
                    $occurrences = $this->Occurrences->find();
                    $occurrences = $occurrences
                        ->find('all')
                        ->where([
                            'status NOT IN' => [
                                'INVALID', 'CANCELED'
                            ],
                            'Occurrences.active' => true,
                            'Occurrences.contract_id' => $contract->id,
                            'Occurrences.created >' => $initial_date
                        ])
                        ->contain(['Prices'])
                        ->select(['total' => $occurrences->func()->sum('Prices.occurrence_total_price')])
                        ->first();

                } else {
                    $occurrences = $this->Occurrences->find();
                    $occurrences = $occurrences
                        ->find('all')
                        ->where([
                            'status NOT IN' => [
                                'INVALID', 'CANCELED'
                            ],
                            'Occurrences.active' => true,
                            'Occurrences.contract_id' => $contract->id,
                            'Occurrences.created >' => $initial_date,
                            'Occurrences.created <=' => $fund_month->final_date
                        ])
                        ->contain(['Prices'])
                        ->select(['total' => $occurrences->func()->sum('Prices.occurrence_total_price')])
                        ->first();
                }

                $total_expense = $occurrences->total + $owed_points;
                $spare_points = ($fund_month->total_points_quantity + $fund_month->additive_points) - $total_expense;
                if ($spare_points <= 0) {
                    $depleted = true;
                    $owed_points = abs($spare_points);
                    $spare_points = 0;
                    $stored_points = 0;
                    $total_expense = $fund_month->total_points_quantity + $fund_month->additive_points;
                    $stored_points_expense = 0;
                    $initial_date = clone $fund_month->final_date;
                } else {
                    $stored_points = $spare_points;
                    $depleted = false;

                    $occurrences = $this->Occurrences->find();
                    $occurrences = $occurrences
                        ->find('all')
                        ->where([
                            'status NOT IN' => [
                                'INVALID', 'CANCELED'
                            ],
                            'Occurrences.active' => true,
                            'Occurrences.contract_id' => $contract->id,
                            'Occurrences.created >' => $fund_month->final_date,
                            'Occurrences.created <=' => $fund_month->stored_points_validity
                        ])
                        ->contain(['Prices'])
                        ->select(['total' => $occurrences->func()->sum('Prices.occurrence_total_price')])
                        ->first();

                    $stored_points_expense = $occurrences->total;

                    $occurrences = $this->Occurrences->find();
                    $occurrence = $occurrences
                        ->find('all')
                        ->where([
                            'status NOT IN' => [
                                'INVALID', 'CANCELED'
                            ],
                            'Occurrences.active' => true,
                            'Occurrences.contract_id' => $contract->id,
                            'Occurrences.created >' => $fund_month->final_date,
                            'Occurrences.created <=' => $fund_month->stored_points_validity
                        ])
                        ->contain(['Prices'])
                        ->select(['id', 'created'])
                        ->last();

                    if($occurrence) {
                        $initial_date = $occurrence->created;
                    } else {
                        $initial_date = clone $fund_month->stored_points_validity;
                    }

                    if ($spare_points - $stored_points_expense <= 0) {
                        $owed_points = abs($spare_points - $stored_points_expense);
                        $stored_points_expense = $spare_points;
                        $depleted = true;
                    } else {
                        $owed_points = 0;
                        $depleted =  false;
                    }
                }
            } else {
                $total_expense = $owed_points;
                $spare_points = ($fund_month->total_points_quantity + $fund_month->additive_points) - $total_expense;
                if ($spare_points <= 0) {
                    $depleted = true;
                    $owed_points = abs($spare_points);
                    $spare_points = 0;
                    $stored_points = 0;
                    $total_expense = $fund_month->total_points_quantity + $fund_month->additive_points;
                    $stored_points_expense = 0;
                } else {
                    $stored_points = $spare_points;
                    $depleted = false;

                    $occurrences = $this->Occurrences->find();
                    $occurrences = $occurrences
                        ->find('all')
                        ->where([
                            'status NOT IN' => [
                                'INVALID', 'CANCELED'
                            ],
                            'Occurrences.active' => true,
                            'Occurrences.contract_id' => $contract->id,
                            'Occurrences.created >' => $initial_date,
                            'Occurrences.created <=' => $fund_month->stored_points_validity
                        ])
                        ->contain(['Prices'])
                        ->select(['total' => $occurrences->func()->sum('Prices.occurrence_total_price')])
                        ->first();

                    $stored_points_expense = $occurrences->total;

                    $occurrences = $this->Occurrences->find();
                    $occurrence = $occurrences
                        ->find('all')
                        ->where([
                            'status NOT IN' => [
                                'INVALID', 'CANCELED'
                            ],
                            'Occurrences.active' => true,
                            'Occurrences.contract_id' => $contract->id,
                            'Occurrences.created >' => $initial_date,
                            'Occurrences.created <=' => $fund_month->stored_points_validity
                        ])
                        ->contain(['Prices'])
                        ->select(['id', 'created'])
                        ->last();

                    if($occurrence) {
                        $initial_date = $occurrence->created;
                    } else {
                        $initial_date = clone $fund_month->stored_points_validity;
                    }

                    if ($spare_points - $stored_points_expense <= 0) {
                        $owed_points = abs($spare_points - $stored_points_expense);
                        $stored_points_expense = $spare_points;
                        $depleted = true;
                    } else {
                        $owed_points = 0;
                        $depleted = false;
                    }
                }
            }
            if ($depleted == 0) {
                $depleted = $this->_expired($fund_month->stored_points_validity);
            }
            $data = [
                'total_expense' => (int)$total_expense,
                'stored_points' => (int)$stored_points,
                'spare_points' => (int)$spare_points,
                'owed_points' => (int)$owed_points,
                'stored_points_expense' => (int)$stored_points_expense,
                'depleted' => $depleted
            ];

            $fund_month = $this->Funds->FundsMonths
                ->patchEntity($fund_month, $data);

            if (!$this->Funds->FundsMonths->save($fund_month)) {
                debug($fund_month->errors());
            }
            $stored_points_expense = 0;
        }
        $this->_calculateBalanceRecurrent($contract->id);

        return true;
    }

    private function _calculateBalanceRecurrent($contract_id)
    {
        $contract = $this->Contracts
            ->find('all')
            ->where([
                'Contracts.id' => $contract_id
            ])
            ->contain(['Funds', 'Funds.FundsMonths'])
            ->first();

        $funds_months = $contract->fund->funds_months;
        $limit = ceil($contract->fund->points_days_validity / 30);
        $count = count($funds_months);
        $balance = [];

        for ($i = 0; $i < $count; $i++) {
            $balance[$i] = $funds_months[$i]->spare_points - $funds_months[$i]->stored_points_expense;
        }

        for ($i = 0; $i < $count; $i++) {
            $funds_months[$i]->accumulated_points = $balance[$i];

            for ($j = 1; $j < $limit; $j++) {
                if (($i - $j) >= 0) {
                    $funds_months[$i]->accumulated_points += $balance[$i - $j];
                }
            }
        }
        foreach($funds_months as $fund_month) {
            $data = [
                'accumulated_points' => $fund_month->accumulated_points
            ];

            $data2 = [
                'month_points' => $fund_month->accumulated_points
            ];

            $fund_month = $this->Funds->FundsMonths
                ->patchEntity($fund_month, $data);

            if (!$this->Funds->FundsMonths->save($fund_month)) {
                debug($fund_month->errors());
            }

            $fund = $this->Funds
                ->patchEntity($contract->fund, $data2);

            $this->Funds->save($fund);
        }
    }

    public function detached_queue($contract)
    {
        $funds_months = $this->Funds->FundsMonths
            ->find('all')
            ->where([
                'fund_id' => $contract->fund->id,
                'active' => true
            ]);

        $depleted = false;
        $owed_points = 0;
        $stored_points = 0;

        foreach ($funds_months as $fund_month) {

            if ($fund_month->initial_date != $contract->initial_date) {

                $initial_date = clone $fund_month->initial_date->modify('-1 days');
                $final_date = clone $fund_month->final_date->modify('+1 days');

                $occurrences = $this->Occurrences->find();
                $occurrences = $occurrences
                    ->find('all')
                    ->where([
                        'status NOT IN' => [
                            'INVALID', 'CANCELED'
                        ],
                        'Occurrences.contract_id' => $contract->id,
                        'Occurrences.created >' => $initial_date,
                        'Occurrences.created <' => $final_date
                    ])
                    ->contain(['Prices'])
                    ->select(['total' => $occurrences->func()->sum('Prices.occurrence_total_price'), 'qtd' => $occurrences->count()])
                    ->first();

            } else {

                $total_points_quantity = $fund_month->total_points_quantity;

                $occurrences = $this->Occurrences->find();
                $occurrences = $occurrences
                    ->find('all')
                    ->where([
                        'status NOT IN' => [
                            'INVALID', 'CANCELED'
                        ],
                        'Occurrences.contract_id' => $contract->id,
                        'Occurrences.created <=' => $fund_month->final_date
                    ])
                    ->contain(['Prices'])
                    ->select(['total' => $occurrences->func()->sum('Prices.occurrence_total_price'), 'qtd' => $occurrences->count()])
                    ->first();
            }

            $total_expense = $occurrences->total;

            $spare_points = $total_points_quantity + $fund_month->additive_points - $total_expense;

            if ($spare_points < 0) {
                $owed_points = abs($spare_points);
                $depleted = true;
            }

            if ($depleted == 0) {
                $depleted = $this->_expired($contract->final_date);
            }

            $data = [
                'total_points_quantity' => (int)$total_points_quantity,
                'owed_points' => (int)$owed_points,
                'spare_points' => (int)$spare_points,
                'stored_points' => 0,
                'total_expense' => (int)$total_expense,
                'depleted' => $depleted
            ];

            $fund_month = $this->Funds->FundsMonths
                ->patchEntity($fund_month, $data);

            $this->Funds->FundsMonths->save($fund_month);

            $total_points_quantity = $spare_points;
        }
    }
}
