<?php
/**
 * Created by PhpStorm.
 * User: Flaviano (@flavindias)
 * Date: 18/10/2016
 * Time: 12:01
 */

namespace App\Shell;

use Bake\Shell\Task\ComponentTask;
use Twilio\Rest\Client;
use Cake\Console\Shell;
use Cake\I18n\Time;
use Cake\Controller\ComponentRegistry;
use Cake\Core\App;

class SmsShell extends Shell
{
	public $client = null;
	public $twilioNumber = null;
    public $advice = null;
    public $production = false;

	public function initialize()
    {
        parent::initialize();

        $this->loadModel('Occurrences');
        $this->loadModel('Phones');

        if ($this->production){
            $sid = "AC2f18b5ba81e1b63aa4df0f7aa35f7224"; // Sua SID do www.twilio.com/console
            $token = "128a99074c1dcaef5c44d7a79880d45b"; // Seu Token Auth do www.twilio.com/console
            $this->twilioNumber = '+13346058286'; // Número previamente cadastrado/comprado no twilio.com/console
        }
        else{
            $sid = "AC2f18b5ba81e1b63aa4df0f7aa35f7224"; // Sua SID do www.twilio.com/console
            $token = "128a99074c1dcaef5c44d7a79880d45b"; // Seu Token Auth do www.twilio.com/console
            $this->twilioNumber = '+13346058286'; // Número previamente cadastrado/comprado no twilio.com/console
        }

        $this->advice = 'Olá Técnico FindUP, nós sentimos sua falta, existem atendimentos disponíveis na sua localidade. Para aceitá-los baixe agora o App: https://goo.gl/GcqXYQ';
		$this->client = new Client($sid, $token);

    }

	/*
	*	Função para o envio de SMS onde recebe como parametros o numero do telefone de origem (Número pré cadastrado no Twilio), o número de destino e o corpo da mensagem. 
	*/
	public function sendSMS($bodyText, $to)
	{
		$message = $this->client->messages->create(
  			$to, // Enviar para esse número +5511988887777
  			array(
				'from' => $this->twilioNumber,
				'body' => $this->advice
  			)
		);

		print $message->sid;
	}


	public function unsubscrible($number)
    {
        $number = str_replace('+55', '', $number);

        $phone = $this->Phones
            ->find('all')
            ->where([
                'number' => $number
            ])
            ->first();

        $body = $_REQUEST['Body'];

        if($body == 'SAIR' || $body == 'CANCELAR')
        {
            $data = [
                'subscriber' => false
            ];

            $phone = $this->Phones->patchEntity($phone, $data);

            $this->Phones->save($phone);
        }
	}

    public function broadcast()
    {
        $conditions = [
            'status' => 'OPENED',
            'specialist_user_id is' => null,
            'radius' => 30
        ];

        $occurrences = $this->Occurrences
            ->find('all')
            ->where($conditions);

        foreach ($occurrences as $occurrence) {
            if ($occurrence->address->lat && $occurrence->address->lng) {
                if ($occurrence->count_notifications_radius < 5) {
                    $data = [
                        'count_notifications_radius' => $occurrence->count_notifications_radius + 1
                    ];
                    $this->auxBroadcast($occurrence, $data);
                } else {
                    if ($occurrence->radius < 70) {
                        $data = [
                            'radius' => $occurrence->radius + 5,
                            'count_notifications_radius' => 0
                        ];
                        $this->auxBroadcast($occurrence, $data);
                    } else {
                        $data = [
                            'count_notifications_radius' => 0
                        ];
                        $this->auxBroadcast($occurrence, $data);
                        if ($occurrence->has('address')) {
                            if ($occurrence->address->lat && $occurrence->address->lng) {
                                if ($occurrence->count_notifications_radius < 5) {
                                    $data = [
                                        'count_notifications_radius' => $occurrence->count_notifications_radius + 1
                                    ];
                                    $this->auxBroadcast($occurrence, $data);
                                } else {
                                    if ($occurrence->radius < 70) {
                                        $data = [
                                            'radius' => $occurrence->radius + 5,
                                            'count_notifications_radius' => 0
                                        ];
                                        $this->auxBroadcast($occurrence, $data);
                                    } else {
                                        $data = [
                                            'count_notifications_radius' => 0
                                        ];
                                        $this->auxBroadcast($occurrence, $data);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } exit();
    }

    private function auxBroadcast($occurrence) {
        $specialists = $this->Occurrences->Specialists
            ->find('distance', [
                'lat' => $occurrence->address->lat,
                'lng' => $occurrence->address->lng,
                'distance' => $occurrence->radius])

            ->where(['access_specialist' => true])
            ->contain([
                'Individuals'
            ]);

        foreach($specialists as $specialist) {
//            $this->ClientNotification->open($specialist->id, $occurrence->id);
            $now = Time::now();
            $diff = $occurrence->created->diff($now);
            if($diff->i>7){
                $condtitions = [
                    'individual_id' => $specialist->individuals[0]->id,
                    'subscriber' => true,
                    'active' => true
                ];

                $phone = $this->Phones
                    ->find('all')
                    ->where($condtitions)
                    ->first();
                if (phone){
                    $user_phone = $phone->DDI . $phone->number;
                    if ($user_phone){
                        $this->sendSMS($this->advice, $user_phone);
                    }
                }




            }
        }
    }


}