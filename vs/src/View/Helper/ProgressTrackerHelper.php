<?php
/**
 * Created by PhpStorm.
 * User: Haendel
 * Date: 14/09/2016
 * Time: 09:46
 */

namespace App\View\Helper;


use Cake\View\Helper;

class ProgressTrackerHelper extends Helper
{

    public $listStatus = [
        'OPENED' => 'Buscando Técnico',
        'ACCEPTED' => 'Aceito',
        'GOING' => 'Saiu para Atendimento',
        'ON_SERVICE' => 'Em Atendimento',
        'AWAITING_VALIDATION' => 'Espera de Pagamento',
        'OPERATION_VALIDATION' => 'Validação da Operação',
        'PAYED_SPECIALIST' => 'Pagamento para Técnico',
        'FINISHED' => 'Validado',
    ];

    public $listStatusCoporate = [
        'OPENED' => 'Buscando Técnico',
        'ACCEPTED' => 'Aceito',
        'GOING' => 'Saiu para Atendimento',
        'ON_SERVICE' => 'Em Atendimento',
        'AWAITING_VALIDATION' => 'Espera de Validação',
        'OPERATION_VALIDATION' => 'Validação da FindUP',
        'FINISHED' => 'Finalizado',
    ];

    public function convertToGroupStatus($status){
        if (in_array($status, ['PENDING_PAYMENT', 'PAYED_SPECIALIST'])) {
            return 'OPERATION_VALIDATION';
        } else {
            return $status;
        }
    }

    public function status($now, $corporate_id = null)
    {
        if ($now != "CANCELED" && $now != "UNPRODUCTIVE" && $now != "INVALID") {
            $html = "    <ul class=\"nav nav-wizard\">";

            $list = ($corporate_id == null) ? $this->listStatus : $this->listStatusCoporate;

            $active = false;

            $now = $this->convertToGroupStatus($now);

            foreach ($list as $key => $value) {
                if (($now == $key)) {
                    $class = "active";
                    $active = true;
                } else {
                    $class = ($active) ? 'disabled' : 'complete';
                }

                $classCol = 'col-xs-1';

                if ($key == 'OPENED') {
                    $classCol = 'col-xs-2';
                }

                if ($key == 'FINISHED') {
                    $classCol = 'col-xs-3';
                }

                if ($key == 'ON_SERVICE') {
                    $classCol = 'col-xs-2';
                }

                $html .= "<li class=\" $classCol $class\">
                  <a href=\"#\">$value</a>
                  </li>";
            }
            $html .= "</ul> ";
            return $html;
        } else {

        }
    }

}