<?php
/**
 * Created by PhpStorm.
 * User: Ananda Almeida
 * Date: 17/01/2018
 * Time: 14:11
 */

namespace App\View\Helper;


use Cake\Utility\Text;
use Cake\View\Helper;

class ModalConfirmHelper extends Helper
{
    public $options = [
        'class' => '',
        'title' => '',
        'text'=>'',
    ];

    public $label = '';
    public $url = '';
    public $uuid = '';

    public $helpers = ['Html', 'Form'];

    public function postLink($label, $url, $options)
    {
        $this->options = array_merge($this->options, $options);
        $this->label = $label;
        $this->url = $url;
        $this->uuid = Text::uuid();

        return $this->_link() . $this->_modal();
    }

    private function _link()
    {
        return "<a href='#' data-toggle='modal' data-target='#modal-confirm-{$this->uuid}' data-placement='top' title='{$this->options['title']}' class='{$this->options['class']}'>{$this->label}</a>";
    }

    private function _modal()
    {
        return "
            <!-- Modal -->
            <div class=\"modal fade\" id=\"modal-confirm-{$this->uuid}\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\">
              <div class=\"modal-dialog\" role=\"document\">
                <div class=\"modal-content\">
                  {$this->Form->create(null, ['url'=> $this->url])}
                  <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                    <h5 class=\"modal-title\" id=\"myModalLabel\">{$this->options['title']}</h5>
                  </div>
                  <div class=\"modal-body\">
                    {$this->options['text']} 
                  </div>
                  <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancelar</button>
                    <button type=\"submit\"  class=\"btn btn-success\">Confirmar</button>
                  </div>
                  {$this->Form->end()}
                </div>
              </div>
            </div>";
    }
}