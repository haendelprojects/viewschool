<?php
/**
 * Created by PhpStorm.
 * User: Haendel
 * Date: 05/10/2016
 * Time: 11:01
 */

namespace App\View\Helper;


use Cake\Chronos\Date;
use Cake\ORM\TableRegistry;
use Cake\View\Helper;

class UtilHelper extends Helper
{
    private $Strings = [
        'unpaid' => 'Pagamento Pendente',
        'paid' => 'Pagamento Regular',
        'awaiting_paid' => 'Espera de Pagamento',
        'finished' => 'Finaliado'
    ];


    /**
     * Strings de chaves do banco, tornado forma legivel para o usuario
     * @param $key
     * @return mixed|string
     */
    public function strings($key)
    {
        if (isset($this->Strings[$key])) {
            return $this->Strings[$key];
        } else {
            return $key;
        }
    }


    public function years()
    {
        return [
            date("Y") => date("Y"),
            date("Y") + 1 => date("Y") + 1,
            date("Y") + 2 => date("Y") + 2,
        ];
    }

    public function iconPreview($file)
    {
        if (in_array($file->file_type, ['image/png', 'image/jpg', 'image/jpeg', 'PNG', 'png', 'jpg'])) {
            return "<span class='mailbox-attachment-icon has-img' style=''><img src='$file->filepath' alt='Attachment'></span>";
        } elseif (in_array($file->file_type, ['application/pdf'])) {
            return '<span class="mailbox-attachment-icon"><i class="fa fa-file-pdf-o"></i></span>';
        } else {
            return '<span class="mailbox-attachment-icon"><i class="fa fa-image"></i></span>';
        }
    }

    /**
     * Gerar lista de times zone
     * @return array
     */
    public function generate_timezone_list()
    {
        static $regions = array(
            \DateTimeZone::AFRICA,
            \DateTimeZone::AMERICA,
            \DateTimeZone::ANTARCTICA,
            \DateTimeZone::ASIA,
            \DateTimeZone::ATLANTIC,
            \DateTimeZone::AUSTRALIA,
            \DateTimeZone::EUROPE,
            \DateTimeZone::INDIAN,
            \DateTimeZone::PACIFIC,
        );
        $timezones = array();
        foreach ($regions as $region) {
            $timezones = array_merge($timezones, \DateTimeZone::listIdentifiers($region));
        }
        $timezone_offsets = array();
        foreach ($timezones as $timezone) {
            $tz = new \DateTimeZone($timezone);
            $timezone_offsets[$timezone] = $tz->getOffset(new \DateTime);
        }
        // sort timezone by offset
        asort($timezone_offsets);
        $timezone_list = array();
        foreach ($timezone_offsets as $timezone => $offset) {
            $offset_prefix = $offset < 0 ? '-' : '+';
            $offset_formatted = gmdate('H:i', abs($offset));

            $pretty_offset = "UTC${offset_prefix}${offset_formatted}";

            $timezone_list[$timezone] = "(${pretty_offset}) $timezone";
        }
        return $timezone_list;
    }

    /**
     * Converter para o formato br e setar a timezone
     * @param $date
     * @param string $timezone
     * @return mixed
     */
    public function convertDateTimezone($date, $timezone = 'America/Sao_Paulo', $type = 'full')
    {
        if ($type == 'full') {
            $format = 'd/m/Y H:i';
        } elseif ($type == 'date') {
            $format = 'd/m/Y';
        } elseif ($type == 'time') {
            $format = 'H:i';
        } else {
            $format = 'd/m/Y H:i';
        }

        if ($date) {
            if ($timezone) {
                return $date->setTimezone(new \DateTimeZone($timezone))->format($format);
            } else {
                return $date->setTimezone(new \DateTimeZone('America/Sao_Paulo'))->format($format);
            }
        } else {
            return '';
        }
    }

    public function minutesToHours($minutes)
    {
        $h = floor($minutes / 60) ? floor($minutes / 60) . ' h' : '';
        $m = $minutes % 60 ? $minutes % 60 . ' m' : '';
        return $h && $m ? $h . ' ' . $m : $h . $m;
    }

    function alignForm()
    {
        return [
            'sm' => [
                'left' => 6,
                'middle' => 6,
                'right' => 12
            ],
            'md' => [
                'left' => 2,
                'middle' => 10,
            ]
        ];
    }


    /**
     * Listas de informações para options
     * @return $this
     */
    public function listOptions($table = 'Disciplines', $order = ['name' => 'asc'], $where = [])
    {
        $dis = TableRegistry::get($table);

        return $dis->find('list')->order($order)->where($where);
    }

    public function listTeachers($table = 'Disciplines', $order = ['name' => 'asc'])
    {
        $dis = TableRegistry::get($table);

        return $dis->find('list')->order($order);
    }

    public function entityForm($table = 'Disciplines')
    {
        $dis = TableRegistry::get($table);

        return $dis->newEntity();
    }

    public function dayDiscipline($disciplines, $day)
    {
        foreach ($disciplines as $discipline):
            foreach ($discipline->times as $time) :
                if ($time->day_week == $day) {
                    return "{$discipline->discipline->name} - $time->init_time h às  $time->end_time h";
                }
            endforeach;
        endforeach;
    }

    /**
     * Contagem de faltas do aluno
     * @param $class
     * @param $discipline
     * @param $people_id
     * @return string
     */
    public function countMissing($class, $discipline)
    {
        $User = $this->request->session()->read('Auth.User');

        $Frequencies = TableRegistry::get('Frequencies');
        $f = $Frequencies->find('all')
            ->where([
                'people_id' => $User['people']['id'],
                'Lessons.class_id' => $class,
                'Lessons.discipline_id' => $discipline,
                'status' => 'Faltou',
                'Lessons.type' => 'Aula'
            ])
            ->contain(['Lessons'])
            ->count();

        return "<span class='label label-danger'>$f faltas</span>";
    }

    /**
     * Lista da frequencia
     * @param $class
     * @param $discipline
     * @return string
     */
    public function tableFrequencie($class, $discipline)
    {
        $out = "<table class='table table-bordered'>";

        $User = $this->request->session()->read('Auth.User');

        $Frequencies = TableRegistry::get('Frequencies');
        $f = $Frequencies->find('all')
            ->where([
                'people_id' => $User['people']['id'],
                'Lessons.class_id' => $class,
                'Lessons.discipline_id' => $discipline,
                'Lessons.type' => 'Aula'
            ])
            ->contain(['Lessons']);

        foreach ($f as $a) {
            $bg = '';
            if ($a->status == 'Faltou') {
                $bg = 'label-danger';
            } elseif ($a->status == 'Presente') {
                $bg = 'label-success';
            } else {
                $bg = 'label-info';
            }

            $out .= "<tr><td>{$a->lesson->title}</td> <td>{$a->lesson->day}</td> <td><span class='label $bg'>{$a->status}</span></td> </tr>";
        }

        return "$out</table>";
    }

    /**
     * Contagem de faltas do aluno
     * @param $class
     * @param $discipline
     * @param $people_id
     * @return string
     */
    public function averageNow($class, $discipline)
    {
        $User = $this->request->session()->read('Auth.User');

        $Frequencies = TableRegistry::get('Frequencies');
        $f = $Frequencies->find('all');

        $a = $f->select(['average' => $f->func()->avg('note')])
            ->where([
                'people_id' => $User['people']['id'],
                'Lessons.class_id' => $class,
                'Lessons.discipline_id' => $discipline,
                'Lessons.type' => 'Avaliação'
            ])
            ->contain(['Lessons'])
            ->first();

        $bg = '';
        if ($a->average < 7) {
            $bg = 'label-danger';
        } else {
            $bg = 'label-success';
        }


        return "<span class='label $bg'>{$a->average} média atual</span>";
    }

    /**
     * Lista da frequencia
     * @param $class
     * @param $discipline
     * @return string
     */
    public function tableNotes($class, $discipline)
    {
        $out = "<table class='table table-bordered'>";

        $User = $this->request->session()->read('Auth.User');

        $Frequencies = TableRegistry::get('Frequencies');
        $f = $Frequencies->find('all')
            ->where([
                'people_id' => $User['people']['id'],
                'Lessons.class_id' => $class,
                'Lessons.discipline_id' => $discipline,
                'Lessons.type' => 'Avaliação'
            ])
            ->contain(['Lessons']);

        foreach ($f as $a) {
            $bg = '';
            if ($a->note < 7) {
                $bg = 'label-danger';
            } else {
                $bg = 'label-success';
            }

            $out .= "<tr><td>{$a->lesson->title}</td> <td>{$a->lesson->day}</td> <td><span class='label $bg'>{$a->note}</span></td> </tr>";
        }

        return "$out</table>";
    }

}