<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;

/**
 * Peoples Controller
 *
 * @property \App\Model\Table\PeoplesTable $Peoples
 *
 * @method \App\Model\Entity\People[] paginate($object = null, array $settings = [])
 */
class StudentsController extends AppController
{

    public function initialize()
    {
        parent::initialize(); // TODO: Change the autogenerated stub

        $this->loadModel('Peoples');

        $this->loadComponent('Search.Prg', [
            'actions' => ['index']
        ]);

        $this->set('titlePage', 'Alunos');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => []
        ];
        $query = $this->Peoples->find('search', ['search' => $this->request->getQuery()])->where([$this->Auth->user('school_id'), 'type' => 'ALUNO']);
        $peoples = $this->paginate($query);
        $this->set(compact('peoples'));
        $this->set('isSearch', $this->Peoples->isSearch());
        $this->set('_serialize', ['peoples']);
    }

    /**
     * View method
     *
     * @param string|null $id People id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $people = $this->Peoples->get($id, [
            'contain' => ['Addresses', 'Users', 'Responsibles']
        ]);

        $user = $this->Peoples->Users->newEntity();
        $people_entity = $this->Peoples->newEntity();

        $this->set('people', $people);
        $this->set('user', $user);
        $this->set('people_entity', $people_entity);
        $this->set('_serialize', ['people']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $people = $this->Peoples->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['school_id'] = $this->Auth->user('school_id');
            $this->request->data['user'] = [
                'username' => strtolower(str_replace(' ', '', $this->request->getData('name'))),
                'password' => 'generate',
                'access_student' => true,
                'school_id' => $this->Auth->user('school_id')
            ];

            $people = $this->Peoples->patchEntity($people, $this->request->data, ['asssociated' => ['Addresses', 'Users']]);
            if ($this->Peoples->save($people)) {
                $this->Flash->success(__('Estudante salvo com suceso.'));
                return $this->redirect(['action' => 'view', $people->id]);
            } else {
                $this->Flash->error(__('Falha, tente novamente.'));
            }
        }

        $this->set(compact('people', 'addresses', 'users', 'branchs'));
        $this->set('_serialize', ['people']);
    }


    public function responsible($id = null)
    {
        if ($id == null) {
            $people = $this->Peoples->newEntity();
            $this->request->data['school_id'] = $this->Auth->user('school_id');
            $people = $this->Peoples->patchEntity($people, $this->request->data, ['asssociated' => ['Addresses']]);
            if ($this->Peoples->save($people)) {
                $this->Flash->success(__('Estudante salvo com suceso.'));
            } else {
                $this->Flash->error(__('Falha, tente novamente.'));
            }
        } else {
            $people = $this->Peoples->get($id, [
                'contain' => []
            ]);
            if ($this->request->is(['patch', 'post', 'put'])) {
                $people = $this->Peoples->patchEntity($people, $this->request->data, ['associated' => ['Addresses']]);
                if ($this->Peoples->save($people)) {
                    $this->Flash->success(__('Salvo com sucesso'));
                } else {
                    $this->Flash->error(__('Falha, tente novamente'));
                }
            }
        }
        $this->redirect($this->referer());
    }

    /**
     * Edit method
     *
     * @param string|null $id People id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $people = $this->Peoples->get($id, [
            'contain' => ['Addresses']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $people = $this->Peoples->patchEntity($people, $this->request->data, ['associated' => ['Addresses']]);
            if ($this->Peoples->save($people)) {
                $this->Flash->success(__('Salvo com sucesso'));
                $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('Falha, tente novamente'));
            }
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id People id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $people = $this->Peoples->get($id);
        if ($this->Peoples->delete($people)) {
            $this->Flash->success(__('The {0} has been deleted.', 'People'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'People'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
