<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Grid Controller
 *
 * @property \App\Model\Table\GridTable $Grid
 *
 * @method \App\Model\Entity\Grid[] paginate($object = null, array $settings = [])
 */
class GridController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Class', 'Disciplines']
        ];
        $grid = $this->paginate($this->Grid);

        $this->set(compact('grid'));
        $this->set('_serialize', ['grid']);
    }

    /**
     * View method
     *
     * @param string|null $id Grid id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $grid = $this->Grid->get($id, [
            'contain' => ['Class', 'Disciplines']
        ]);

        $this->set('grid', $grid);
        $this->set('_serialize', ['grid']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $grid = $this->Grid->newEntity();
        if ($this->request->is('post')) {
            $grid = $this->Grid->patchEntity($grid, $this->request->data);
            if ($this->Grid->save($grid)) {
                $this->Flash->success(__('The {0} has been saved.', 'Grid'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Grid'));
            }
        }
        $class = $this->Grid->Class->find('list', ['limit' => 200]);
        $disciplines = $this->Grid->Disciplines->find('list', ['limit' => 200]);
        $this->set(compact('grid', 'class', 'disciplines'));
        $this->set('_serialize', ['grid']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Grid id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $grid = $this->Grid->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $grid = $this->Grid->patchEntity($grid, $this->request->data);
            if ($this->Grid->save($grid)) {
                $this->Flash->success(__('The {0} has been saved.', 'Grid'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Grid'));
            }
        }
        $class = $this->Grid->Class->find('list', ['limit' => 200]);
        $disciplines = $this->Grid->Disciplines->find('list', ['limit' => 200]);
        $this->set(compact('grid', 'class', 'disciplines'));
        $this->set('_serialize', ['grid']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Grid id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $grid = $this->Grid->get($id);
        if ($this->Grid->delete($grid)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Grid'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Grid'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
