<?php
/**
 * Created by PhpStorm.
 * User: Ananda Almeida
 * Date: 28/11/2017
 * Time: 14:56
 */

namespace App\Controller\Component;


use Cake\Controller\Component;

class UtilComponent extends Component
{

    private $status = [
        'OPENED' => 'Buscando Técnico',
        'ACCEPTED' => 'Aceito',
        'GOING' => 'Saiu para Atendimento',
        'ON_SERVICE' => 'Em Atendimento',
        'AWAITING_PAYMENT' => 'Aguardando Pagamento B2C',
        'AWAITING_VALIDATION' => 'Espera de Validação do cliente',
        'PENDING_PAYMENT' => 'Técnico Pendente',
        'PAYED_SPECIALIST' => 'Técnico Pendente',
        'FINISHED' => 'Finalizado',
        'CANCELED' => 'Cancelado',
        'UNPRODUCTIVE' => 'Improdutivo',
        'INVALID' => 'Não Validado',
        'REJECTED' => 'Rejeitado',
        'RESCHEDULE' => 'Reagendado',
        'RESCHEDULED' => 'Reagendado',
        'OPERATION_VALIDATION' => 'Validação da Operação',
    ];


    public function getName($status)
    {
        return ($status) ? $this->status[$status] : '';
    }


    /**
     * Converter para o formato br e setar a timezone
     * @param $date
     * @param string $timezone
     * @return mixed
     */
    public function convertDateTimezone($date, $timezone = 'America/Sao_Paulo', $type = 'full')
    {
        if ($type == 'full') {
            $format = 'd/m/Y H:i';
        } elseif ($type == 'date') {
            $format = 'd/m/Y';
        } elseif ($type == 'time') {
            $format = 'H:i';
        } else {
            $format = 'd/m/Y H:i';
        }

        if ($date) {
            if ($timezone) {
                return $date->setTimezone(new \DateTimeZone($timezone))->format($format);
            } else {
                return $date->setTimezone(new \DateTimeZone('America/Sao_Paulo'))->format($format);
            }
        } else {
            return '';
        }
    }

    /**
     * Diferença em minutos entre duas data
     * @param $checkin
     * @param $checkout
     * @return float|int
     */
    public function diffDates($checkin, $checkout)
    {
        if ($checkin && $checkout) {
            $diff = strtotime($checkout->format('Y-m-d H:i')) - strtotime($checkin->format('Y-m-d H:i'));
            // Minutos
            return floor($diff / (60));;
        } else {
            return 0;
        }
    }

    /**
     * VErificar se o sla está violado, até 15 minutos de atraso não é violado
     * @param $schedule
     * @param $checkin
     * @return bool
     */
    public function slaViolated($schedule, $checkin)
    {
        if ($schedule && $checkin) {
            return ($schedule->add(new \DateInterval('PT15M')) < $checkin);
        } else {
            return false;
        }
    }
}