<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Calculate Controller
 *
 * @property \App\Model\Table\CalculateTable $Calculate
 *
 * @method \App\Model\Entity\Calculate[] paginate($object = null, array $settings = [])
 */
class CalculateController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $calculate = $this->paginate($this->Calculate);

        $this->set(compact('calculate'));
        $this->set('_serialize', ['calculate']);
    }

    /**
     * View method
     *
     * @param string|null $id Calculate id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $calculate = $this->Calculate->get($id, [
            'contain' => []
        ]);

        $this->set('calculate', $calculate);
        $this->set('_serialize', ['calculate']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $calculate = $this->Calculate->newEntity();
        if ($this->request->is('post')) {
            $calculate = $this->Calculate->patchEntity($calculate, $this->request->data);
            if ($this->Calculate->save($calculate)) {
                $this->Flash->success(__('The {0} has been saved.', 'Calculate'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Calculate'));
            }
        }
        $this->set(compact('calculate'));
        $this->set('_serialize', ['calculate']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Calculate id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $calculate = $this->Calculate->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $calculate = $this->Calculate->patchEntity($calculate, $this->request->data);
            if ($this->Calculate->save($calculate)) {
                $this->Flash->success(__('The {0} has been saved.', 'Calculate'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Calculate'));
            }
        }
        $this->set(compact('calculate'));
        $this->set('_serialize', ['calculate']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Calculate id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $calculate = $this->Calculate->get($id);
        if ($this->Calculate->delete($calculate)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Calculate'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Calculate'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
