<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Branchs Controller
 *
 * @property \App\Model\Table\BranchsTable $Branchs
 *
 * @method \App\Model\Entity\Branch[] paginate($object = null, array $settings = [])
 */
class BranchsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Addresses', 'Schools']
        ];
        $branchs = $this->paginate($this->Branchs);

        $this->set(compact('branchs'));
        $this->set('_serialize', ['branchs']);
    }

    /**
     * View method
     *
     * @param string|null $id Branch id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $branch = $this->Branchs->get($id, [
            'contain' => ['Addresses', 'Schools', 'Calendars', 'Class', 'ClassDiscipline', 'Comments', 'Courses', 'Disciplines', 'Evaluations', 'Organizations', 'Peoples', 'Users']
        ]);

        $this->set('branch', $branch);
        $this->set('_serialize', ['branch']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $branch = $this->Branchs->newEntity();
        if ($this->request->is('post')) {
            $branch = $this->Branchs->patchEntity($branch, $this->request->data);
            if ($this->Branchs->save($branch)) {
                $this->Flash->success(__('The {0} has been saved.', 'Branch'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Branch'));
            }
        }
        $addresses = $this->Branchs->Addresses->find('list', ['limit' => 200]);
        $schools = $this->Branchs->Schools->find('list', ['limit' => 200]);
        $this->set(compact('branch', 'addresses', 'schools'));
        $this->set('_serialize', ['branch']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Branch id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $branch = $this->Branchs->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $branch = $this->Branchs->patchEntity($branch, $this->request->data);
            if ($this->Branchs->save($branch)) {
                $this->Flash->success(__('The {0} has been saved.', 'Branch'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Branch'));
            }
        }
        $addresses = $this->Branchs->Addresses->find('list', ['limit' => 200]);
        $schools = $this->Branchs->Schools->find('list', ['limit' => 200]);
        $this->set(compact('branch', 'addresses', 'schools'));
        $this->set('_serialize', ['branch']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Branch id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $branch = $this->Branchs->get($id);
        if ($this->Branchs->delete($branch)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Branch'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Branch'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
