<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * ClassDiscipline Controller
 *
 * @property \App\Model\Table\ClassDisciplineTable $ClassDiscipline
 *
 * @method \App\Model\Entity\ClassDiscipline[] paginate($object = null, array $settings = [])
 */
class ClassDisciplineController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Class', 'Peoples', 'Branchs']
        ];
        $classDiscipline = $this->paginate($this->ClassDiscipline);

        $this->set(compact('classDiscipline'));
        $this->set('_serialize', ['classDiscipline']);
    }

    /**
     * View method
     *
     * @param string|null $id Class Discipline id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $classDiscipline = $this->ClassDiscipline->get($id, [
            'contain' => ['Class', 'Dicipline', 'Peoples', 'Branchs', 'Frequencies']
        ]);

        $this->set('classDiscipline', $classDiscipline);
        $this->set('_serialize', ['classDiscipline']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $classDiscipline = $this->ClassDiscipline->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['school_id'] = $this->Auth->user('school_id');
            $classDiscipline = $this->ClassDiscipline->patchEntity($classDiscipline, $this->request->data);
            if ($this->ClassDiscipline->save($classDiscipline)) {
                $this->Flash->success(__('Salvo com sucesso'));
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('Falha, tente novamente'));
            }
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Class Discipline id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $classDiscipline = $this->ClassDiscipline->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $classDiscipline = $this->ClassDiscipline->patchEntity($classDiscipline, $this->request->data);
            if ($this->ClassDiscipline->save($classDiscipline)) {
                $this->Flash->success(__('The {0} has been saved.', 'Class Discipline'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Class Discipline'));
            }
        }
        $peoples = $this->ClassDiscipline->Peoples->find('list', ['limit' => 200]);
        $this->set(compact('classDiscipline', 'class', 'dicipline', 'peoples', 'branchs'));
        $this->set('_serialize', ['classDiscipline']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Class Discipline id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $classDiscipline = $this->ClassDiscipline->get($id);
        if ($this->ClassDiscipline->delete($classDiscipline)) {
            $this->Flash->success(__('Removido com sucesso.'));
        } else {
            $this->Flash->error(__('Falha, tente novamente'));
        }

        return $this->redirect($this->referer());
    }

    public function getDisciplines($class){
        $class = $this->ClassDiscipline->find('all')->where(['class_id' => $class])->contain(['Disciplines']);

        $disciplines = [];


        foreach($class as $cl){
            $disciplines[$cl->discipline->id] = $cl->discipline->name;
        }

        $this->set(compact('disciplines'));

        $this->viewBuilder()->layout('ajax');
    }
}
