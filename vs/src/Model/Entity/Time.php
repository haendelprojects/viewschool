<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Time Entity
 *
 * @property int $id
 * @property string $day_week
 * @property int $class_discipline_id
 * @property string $init_time
 * @property string $end_time
 *
 * @property \App\Model\Entity\ClassDiscipline $class_discipline
 */
class Time extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
