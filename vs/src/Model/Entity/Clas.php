<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Clas Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $course_id
 * @property int $branch_id
 * @property int $organization_id
 * @property \Cake\I18n\FrozenDate $init_date
 * @property \Cake\I18n\FrozenDate $end_date
 * @property string $period
 * @property string $name
 * @property string $wallet_name
 * @property string $status
 * @property string $class_time
 * @property string $school_year
 * @property int $evaluation_id
 * @property int $people_id
 *
 * @property \App\Model\Entity\Course $course
 * @property \App\Model\Entity\Branch $branch
 * @property \App\Model\Entity\Organization $organization
 * @property \App\Model\Entity\Evaluation $evaluation
 * @property \App\Model\Entity\People $people
 */
class Clas extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
