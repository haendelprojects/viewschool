<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Branch Entity
 *
 * @property int $id
 * @property string $name
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property string $cnpj
 * @property int $address_id
 * @property string $phone
 * @property string $email
 * @property int $school_id
 *
 * @property \App\Model\Entity\Address $address
 * @property \App\Model\Entity\School $school
 * @property \App\Model\Entity\Calendar[] $calendars
 * @property \App\Model\Entity\Clas[] $class
 * @property \App\Model\Entity\ClassDiscipline[] $class_discipline
 * @property \App\Model\Entity\Comment[] $comments
 * @property \App\Model\Entity\Course[] $courses
 * @property \App\Model\Entity\Discipline[] $disciplines
 * @property \App\Model\Entity\Evaluation[] $evaluations
 * @property \App\Model\Entity\Organization[] $organizations
 * @property \App\Model\Entity\People[] $peoples
 * @property \App\Model\Entity\User[] $users
 */
class Branch extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
