<?php
namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\I18n\Date;
use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $school_id
 * @property int $active
 * @property bool $access_school
 * @property bool $access_student
 * @property bool $access_teacher
 * @property bool $access_backoffice
 * @property int $branch_id
 * @property string $name
 *
 * @property \App\Model\Entity\School $school
 * @property \App\Model\Entity\Branch $branch
 * @property \App\Model\Entity\Calendar[] $calendars
 * @property \App\Model\Entity\Comment[] $comments
 * @property \App\Model\Entity\People[] $peoples
 */
class User extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];


    protected function _setBirthdate($value)
    {
        return \DateTime::createFromFormat('d/m/Y', $value);
    }

    protected function _setPassword($password)
    {
        return (new DefaultPasswordHasher)->hash($password);
    }
}
