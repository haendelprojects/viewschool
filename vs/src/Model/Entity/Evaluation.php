<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Evaluation Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property string $name
 * @property string $type_attendance
 * @property bool $evaluation_attendance
 * @property int $percent_attendance
 * @property string $evaluation_period
 * @property string $quantity_period
 * @property string $calculate_total_year
 * @property string $calculate_final_note
 * @property string $period_minimum_note
 * @property string $annual_minimum_note
 * @property string $period_recuperation_minimum_note
 * @property string $anual_recuperation_minimum_note
 * @property bool $annual_recovery
 * @property bool $semi_annual_recovery
 * @property string $type
 * @property int $branch_id
 *
 * @property \App\Model\Entity\Branch $branch
 * @property \App\Model\Entity\Clas[] $class
 */
class Evaluation extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
