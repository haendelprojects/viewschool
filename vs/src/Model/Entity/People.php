<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * People Entity
 *
 * @property int $id
 * @property string $name
 * @property string $cpf
 * @property string $phone
 * @property string $cell_phone
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenDate $birth
 * @property string $type
 * @property string $sexo
 * @property string $foto
 * @property string $father_name
 * @property string $mother_name
 * @property string $email
 * @property int $address_id
 * @property int $user_id
 * @property int $branch_id
 * @property int $people_id
 * @property int $responsible_educational
 * @property int $responsible_financial
 *
 * @property \App\Model\Entity\Address $address
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Branch $branch
 * @property \App\Model\Entity\People[] $peoples
 * @property \App\Model\Entity\Clas[] $class
 * @property \App\Model\Entity\ClassDiscipline[] $class_discipline
 * @property \App\Model\Entity\Comment[] $comments
 * @property \App\Model\Entity\Frequency[] $frequencies
 * @property \App\Model\Entity\Registration[] $registrations
 */
class People extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    /**
     * Converter data para formato americando ao setar o dado
     * @param $data
     * @return string
     */
    protected function _setBirth($data)
    {
        $data = explode('/', $data);
        return $data[2] . '-' . $data[1] . '-' . $data[0];
    }

    protected function _getBrith()
    {
        if ($this->_properties['birth']) return $this->_properties['birth']->format('d/m/Y');
    }

    protected function _setPhone($data)
    {
        return preg_replace("/[^0-9]/", "", $data);
    }

    protected function _setCellPhone($data)
    {
        return preg_replace("/[^0-9]/", "", $data);
    }

    protected function _setCpf($data)
    {
        return preg_replace("/[^0-9]/", "", $data);
    }
}
