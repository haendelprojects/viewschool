<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Peoples Model
 *
 * @property \App\Model\Table\AddressesTable|\Cake\ORM\Association\BelongsTo $Addresses
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\BranchsTable|\Cake\ORM\Association\BelongsTo $Branchs
 * @property \App\Model\Table\ClassTable|\Cake\ORM\Association\HasMany $Class
 * @property \App\Model\Table\ClassDisciplineTable|\Cake\ORM\Association\HasMany $ClassDiscipline
 * @property \App\Model\Table\CommentsTable|\Cake\ORM\Association\HasMany $Comments
 * @property \App\Model\Table\FrequenciesTable|\Cake\ORM\Association\HasMany $Frequencies
 * @property \App\Model\Table\RegistrationsTable|\Cake\ORM\Association\HasMany $Registrations
 *
 * @method \App\Model\Entity\People get($primaryKey, $options = [])
 * @method \App\Model\Entity\People newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\People[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\People|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\People patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\People[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\People findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PeoplesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('peoples');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Addresses', [
            'foreignKey' => 'address_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->belongsTo('Branchs', [
            'foreignKey' => 'branch_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Class', [
            'foreignKey' => 'people_id'
        ]);

        $this->hasMany('Responsibles', [
            'className' => 'Peoples',
            'foreignKey' => 'people_id'
        ]);

        $this->hasMany('ClassDiscipline', [
            'foreignKey' => 'people_id'
        ]);
        $this->hasMany('Comments', [
            'foreignKey' => 'people_id'
        ]);
        $this->hasMany('Frequencies', [
            'foreignKey' => 'people_id'
        ]);
        $this->hasMany('Registrations', [
            'foreignKey' => 'people_id'
        ]);

        $this->addBehavior('Search.Search');
        $this->searchManager()
            ->add('id', 'Search.Value')
            ->add('active', 'Search.Value')
            ->add('name', 'Search.Like', [
                'before' => true,
                'after' => true,
                'field' => [$this->aliasField('name')]
            ])
            ->add('cpf', 'Search.Like', [
                'before' => true,
                'after' => true,
                'field' => [$this->aliasField('cpf')]
            ])
            ->add('phone', 'Search.Like', [
                'before' => true,
                'after' => true,
                'field' => [$this->aliasField('phone')]
            ])
            ->add('cell_phone', 'Search.Like', [
                'before' => true,
                'after' => true,
                'field' => [$this->aliasField('cell_phone')]
            ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->scalar('cpf')
            ->allowEmpty('cpf');

        $validator
            ->scalar('phone')
            ->allowEmpty('phone');

        $validator
            ->scalar('cell_phone')
            ->allowEmpty('cell_phone');


        $validator
            ->scalar('type')
            ->allowEmpty('type');

        $validator
            ->scalar('sexo')
            ->requirePresence('sexo', 'create')
            ->notEmpty('sexo');

        $validator
            ->scalar('foto')
            ->allowEmpty('foto');

        $validator
            ->scalar('father_name')
            ->allowEmpty('father_name');

        $validator
            ->scalar('mother_name')
            ->allowEmpty('mother_name');

        $validator
            ->email('email')
            ->allowEmpty('email');

        $validator
            ->boolean('responsible_financial');

        $validator
            ->boolean('responsible_educational');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->existsIn(['address_id'], 'Addresses'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['branch_id'], 'Branchs'));

        return $rules;
    }
}
