<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Branchs Model
 *
 * @property \App\Model\Table\AddressesTable|\Cake\ORM\Association\BelongsTo $Addresses
 * @property \App\Model\Table\SchoolsTable|\Cake\ORM\Association\BelongsTo $Schools
 * @property \App\Model\Table\CalendarsTable|\Cake\ORM\Association\HasMany $Calendars
 * @property \App\Model\Table\ClassTable|\Cake\ORM\Association\HasMany $Class
 * @property \App\Model\Table\ClassDisciplineTable|\Cake\ORM\Association\HasMany $ClassDiscipline
 * @property \App\Model\Table\CommentsTable|\Cake\ORM\Association\HasMany $Comments
 * @property \App\Model\Table\CoursesTable|\Cake\ORM\Association\HasMany $Courses
 * @property \App\Model\Table\DiciplineTable|\Cake\ORM\Association\HasMany $Dicipline
 * @property \App\Model\Table\EvaluationsTable|\Cake\ORM\Association\HasMany $Evaluations
 * @property \App\Model\Table\OrganizationsTable|\Cake\ORM\Association\HasMany $Organizations
 * @property \App\Model\Table\PeoplesTable|\Cake\ORM\Association\HasMany $Peoples
 *
 * @method \App\Model\Entity\Branch get($primaryKey, $options = [])
 * @method \App\Model\Entity\Branch newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Branch[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Branch|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Branch patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Branch[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Branch findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class BranchsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('branchs');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Addresses', [
            'foreignKey' => 'address_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Schools', [
            'foreignKey' => 'school_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Calendars', [
            'foreignKey' => 'branch_id'
        ]);
        $this->hasMany('Class', [
            'foreignKey' => 'branch_id'
        ]);
        $this->hasMany('ClassDiscipline', [
            'foreignKey' => 'branch_id'
        ]);
        $this->hasMany('Comments', [
            'foreignKey' => 'branch_id'
        ]);
        $this->hasMany('Courses', [
            'foreignKey' => 'branch_id'
        ]);
        $this->hasMany('Dicipline', [
            'foreignKey' => 'branch_id'
        ]);
        $this->hasMany('Evaluations', [
            'foreignKey' => 'branch_id'
        ]);
        $this->hasMany('Organizations', [
            'foreignKey' => 'branch_id'
        ]);
        $this->hasMany('Peoples', [
            'foreignKey' => 'branch_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->allowEmpty('name');

        $validator
            ->scalar('cnpj')
            ->allowEmpty('cnpj');

        $validator
            ->scalar('phone')
            ->allowEmpty('phone');

        $validator
            ->email('email')
            ->allowEmpty('email');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->existsIn(['address_id'], 'Addresses'));
        $rules->add($rules->existsIn(['school_id'], 'Schools'));

        return $rules;
    }
}
