<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Grid Model
 *
 * @property \App\Model\Table\ClassTable|\Cake\ORM\Association\BelongsTo $Class
 * @property \App\Model\Table\DiciplineTable|\Cake\ORM\Association\BelongsTo $Dicipline
 *
 * @method \App\Model\Entity\Grid get($primaryKey, $options = [])
 * @method \App\Model\Entity\Grid newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Grid[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Grid|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Grid patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Grid[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Grid findOrCreate($search, callable $callback = null, $options = [])
 */
class GridTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('grid');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Class', [
            'foreignKey' => 'class_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Dicipline', [
            'foreignKey' => 'dicipline_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('week_day')
            ->requirePresence('week_day', 'create')
            ->notEmpty('week_day');

        $validator
            ->time('init_hour')
            ->requirePresence('init_hour', 'create')
            ->notEmpty('init_hour');

        $validator
            ->time('end_hour')
            ->requirePresence('end_hour', 'create')
            ->notEmpty('end_hour');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['class_id'], 'Class'));
        $rules->add($rules->existsIn(['dicipline_id'], 'Dicipline'));

        return $rules;
    }
}
