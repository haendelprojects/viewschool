<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ClassDiscipline Model
 *
 * @property \App\Model\Table\ClassTable|\Cake\ORM\Association\BelongsTo $Class
 * @property |\Cake\ORM\Association\BelongsTo $Disciplines
 * @property \App\Model\Table\PeoplesTable|\Cake\ORM\Association\BelongsTo $Peoples
 * @property \App\Model\Table\SchoolsTable|\Cake\ORM\Association\BelongsTo $Schools
 * @property \App\Model\Table\FrequenciesTable|\Cake\ORM\Association\HasMany $Frequencies
 *
 * @method \App\Model\Entity\ClassDiscipline get($primaryKey, $options = [])
 * @method \App\Model\Entity\ClassDiscipline newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ClassDiscipline[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ClassDiscipline|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ClassDiscipline patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ClassDiscipline[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ClassDiscipline findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ClassDisciplineTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('class_discipline');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Class', [
            'foreignKey' => 'class_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Disciplines', [
            'foreignKey' => 'discipline_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Peoples', [
            'foreignKey' => 'people_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Schools', [
            'foreignKey' => 'school_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Frequencies', [
            'foreignKey' => 'class_discipline_id'
        ]);

        $this->hasMany('Times', [
            'foreignKey' => 'class_discipline_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['class_id'], 'Class'));
        $rules->add($rules->existsIn(['discipline_id'], 'Disciplines'));
        $rules->add($rules->existsIn(['people_id'], 'Peoples'));
        return $rules;
    }
}
