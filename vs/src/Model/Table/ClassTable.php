<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Class Model
 *
 * @property \App\Model\Table\CoursesTable|\Cake\ORM\Association\BelongsTo $Courses
 * @property \App\Model\Table\SchoolsTable|\Cake\ORM\Association\BelongsTo $Schools
 * @property \App\Model\Table\OrganizationsTable|\Cake\ORM\Association\BelongsTo $Organizations
 * @property \App\Model\Table\EvaluationsTable|\Cake\ORM\Association\BelongsTo $Evaluations
 * @property \App\Model\Table\PeoplesTable|\Cake\ORM\Association\BelongsTo $Peoples
 *
 * @method \App\Model\Entity\Clas get($primaryKey, $options = [])
 * @method \App\Model\Entity\Clas newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Clas[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Clas|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Clas patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Clas[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Clas findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ClassTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('class');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Courses', [
            'foreignKey' => 'course_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Schools', [
            'foreignKey' => 'school_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Organizations', [
            'foreignKey' => 'organization_id',
        ]);
        $this->belongsTo('Evaluations', [
            'foreignKey' => 'evaluation_id',
        ]);
        $this->belongsTo('Peoples', [
            'foreignKey' => 'people_id'
        ]);

        $this->hasMany('ClassDiscipline', [
            'foreignKey' => 'class_id'
        ]);
        $this->hasMany('Lessons', [
            'foreignKey' => 'class_id'
        ]);

        $this->hasMany('Registrations', [
            'foreignKey' => 'class_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('init_date');

        $validator
            ->allowEmpty('end_date');

        $validator
            ->scalar('period')
            ->allowEmpty('period');

        $validator
            ->scalar('name')
            ->allowEmpty('name');

        $validator
            ->scalar('wallet_name')
            ->allowEmpty('wallet_name');

        $validator
            ->scalar('status')
            ->allowEmpty('status');

        $validator
            ->scalar('class_time')
            ->allowEmpty('class_time');

        $validator
            ->scalar('school_year')
            ->allowEmpty('school_year');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['course_id'], 'Courses'));
        $rules->add($rules->existsIn(['people_id'], 'Peoples'));

        return $rules;
    }
}
