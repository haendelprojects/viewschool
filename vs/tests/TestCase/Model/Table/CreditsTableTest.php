<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CreditsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CreditsTable Test Case
 */
class CreditsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CreditsTable
     */
    public $Credits;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.credits',
        'app.users',
        'app.contracts',
        'app.enterprises',
        'app.contracts_tools',
        'app.tools',
        'app.services',
        'app.extracts',
        'app.funds',
        'app.occurrences',
        'app.services_contracts',
        'app.account_statements'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Credits') ? [] : ['className' => CreditsTable::class];
        $this->Credits = TableRegistry::get('Credits', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Credits);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
