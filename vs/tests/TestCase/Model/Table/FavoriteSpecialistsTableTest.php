<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FavoriteSpecialistsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FavoriteSpecialistsTable Test Case
 */
class FavoriteSpecialistsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FavoriteSpecialistsTable
     */
    public $FavoriteSpecialists;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.favorite_specialists',
        'app.users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('FavoriteSpecialists') ? [] : ['className' => FavoriteSpecialistsTable::class];
        $this->FavoriteSpecialists = TableRegistry::get('FavoriteSpecialists', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FavoriteSpecialists);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
