<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ClinicasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ClinicasTable Test Case
 */
class ClinicasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ClinicasTable
     */
    public $Clinicas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.clinicas',
        'app.cdusuarios',
        'app.cdusuarios_clinicas',
        'app.exames',
        'app.prontuarios',
        'app.ciusuarios',
        'app.ciusuarios_clinicas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Clinicas') ? [] : ['className' => ClinicasTable::class];
        $this->Clinicas = TableRegistry::get('Clinicas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Clinicas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
