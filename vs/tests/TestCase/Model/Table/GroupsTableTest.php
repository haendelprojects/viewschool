<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\GroupsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\GroupsTable Test Case
 */
class GroupsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\GroupsTable
     */
    public $Groups;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.groups',
        'app.roles',
        'app.users',
        'app.notifications',
        'app.enterprises',
        'app.addresses',
        'app.o_cities',
        'app.states',
        'app.countries',
        'app.holidays',
        'app.cities',
        'app.regions',
        'app.prices',
        'app.services_contracts',
        'app.contracts',
        'app.contracts_tools',
        'app.tools',
        'app.services',
        'app.occurrences',
        'app.coupons',
        'app.corporates',
        'app.disabled_reasons',
        'app.extracts',
        'app.funds',
        'app.funds_months',
        'app.phones',
        'app.service_providers',
        'app.individuals',
        'app.individuals_tests',
        'app.tests',
        'app.skills',
        'app.questions',
        'app.tests_questions',
        'app.answers',
        'app.individuals_skills',
        'app.services_skills',
        'app.trainings',
        'app.videos',
        'app.tutorials',
        'app.documents',
        'app.individuals_auths',
        'app.auths',
        'app.socialmedias',
        'app.links',
        'app.specialists',
        'app.availabilities',
        'app.comments',
        'app.configurations',
        'app.favorite_specialists',
        'app.historics',
        'app.medias',
        'app.messages',
        'app.proposed_occurrences',
        'app.transportations',
        'app.responsible',
        'app.ratings',
        'app.reviews',
        'app.ratings_reviews',
        'app.credits',
        'app.account_statements',
        'app.payment_methods',
        'app.o_states',
        'app.o_countries'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Groups') ? [] : ['className' => GroupsTable::class];
        $this->Groups = TableRegistry::get('Groups', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Groups);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
