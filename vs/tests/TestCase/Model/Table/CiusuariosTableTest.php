<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CiusuariosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CiusuariosTable Test Case
 */
class CiusuariosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CiusuariosTable
     */
    public $Ciusuarios;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ciusuarios',
        'app.clinicas',
        'app.ciusuarios_clinicas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Ciusuarios') ? [] : ['className' => CiusuariosTable::class];
        $this->Ciusuarios = TableRegistry::get('Ciusuarios', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Ciusuarios);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
