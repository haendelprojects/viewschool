<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CalculateTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CalculateTable Test Case
 */
class CalculateTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CalculateTable
     */
    public $Calculate;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.calculate'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Calculate') ? [] : ['className' => CalculateTable::class];
        $this->Calculate = TableRegistry::get('Calculate', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Calculate);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
