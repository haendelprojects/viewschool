<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AccountStatementsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AccountStatementsTable Test Case
 */
class AccountStatementsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AccountStatementsTable
     */
    public $AccountStatements;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.account_statements',
        'app.credits'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('AccountStatements') ? [] : ['className' => AccountStatementsTable::class];
        $this->AccountStatements = TableRegistry::get('AccountStatements', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AccountStatements);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
