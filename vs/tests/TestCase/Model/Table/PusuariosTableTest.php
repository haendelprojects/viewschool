<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PusuariosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PusuariosTable Test Case
 */
class PusuariosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PusuariosTable
     */
    public $Pusuarios;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.pusuarios',
        'app.convenios',
        'app.exames',
        'app.prontuarios',
        'app.clinicas',
        'app.cdusuarios',
        'app.cdusuarios_clinicas',
        'app.ciusuarios',
        'app.ciusuarios_clinicas',
        'app.servicos',
        'app.anexos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Pusuarios') ? [] : ['className' => PusuariosTable::class];
        $this->Pusuarios = TableRegistry::get('Pusuarios', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Pusuarios);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
