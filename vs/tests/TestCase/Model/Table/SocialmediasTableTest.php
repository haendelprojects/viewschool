<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SocialmediasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SocialmediasTable Test Case
 */
class SocialmediasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SocialmediasTable
     */
    public $Socialmedias;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.socialmedias',
        'app.individuals_auths',
        'app.auths',
        'app.individuals',
        'app.users',
        'app.individuals_tests',
        'app.tests',
        'app.addresses',
        'app.cities',
        'app.states',
        'app.regions',
        'app.prices',
        'app.services_contracts',
        'app.contracts',
        'app.enterprises',
        'app.disabled_reasons',
        'app.extracts',
        'app.funds',
        'app.funds_months',
        'app.occurrences',
        'app.external_os',
        'app.coupons',
        'app.services',
        'app.contracts_tools',
        'app.tools',
        'app.skills',
        'app.questions',
        'app.tests_questions',
        'app.answers',
        'app.individuals_skills',
        'app.services_skills',
        'app.comments',
        'app.historics',
        'app.medias',
        'app.ratings',
        'app.reviews',
        'app.ratings_reviews',
        'app.phones',
        'app.service_providers',
        'app.credits',
        'app.account_statements',
        'app.payment_methods',
        'app.holidays',
        'app.countries',
        'app.proposed_occurrences',
        'app.documents',
        'app.links'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Socialmedias') ? [] : ['className' => SocialmediasTable::class];
        $this->Socialmedias = TableRegistry::get('Socialmedias', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Socialmedias);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
