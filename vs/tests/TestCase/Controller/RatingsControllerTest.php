<?php
namespace App\Test\TestCase\Controller;

use App\Controller\RatingsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\RatingsController Test Case
 */
class RatingsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ratings',
        'app.occurrences',
        'app.contracts',
        'app.enterprises',
        'app.addresses',
        'app.o_cities',
        'app.states',
        'app.countries',
        'app.holidays',
        'app.cities',
        'app.regions',
        'app.prices',
        'app.services_contracts',
        'app.services',
        'app.contracts_tools',
        'app.tools',
        'app.skills',
        'app.questions',
        'app.tests',
        'app.users',
        'app.roles',
        'app.modules',
        'app.roles_modules',
        'app.notifications',
        'app.availabilities',
        'app.comments',
        'app.configurations',
        'app.disabled_reasons',
        'app.favorite_specialists',
        'app.historics',
        'app.individuals',
        'app.individuals_tests',
        'app.documents',
        'app.individuals_auths',
        'app.auths',
        'app.socialmedias',
        'app.links',
        'app.phones',
        'app.service_providers',
        'app.individuals_skills',
        'app.medias',
        'app.messages',
        'app.user_create',
        'app.proposed_occurrences',
        'app.transportations',
        'app.tests_questions',
        'app.answers',
        'app.trainings',
        'app.videos',
        'app.tutorials',
        'app.services_skills',
        'app.payment_methods',
        'app.o_states',
        'app.o_countries',
        'app.extracts',
        'app.funds',
        'app.funds_months',
        'app.credits',
        'app.account_statements',
        'app.coupons',
        'app.corporates',
        'app.clients',
        'app.specialists',
        'app.responsibles',
        'app.sender',
        'app.reviews',
        'app.ratings_reviews'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
