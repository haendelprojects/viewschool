<?php
namespace App\Test\TestCase\Controller;

use App\Controller\PeoplesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\PeoplesController Test Case
 */
class PeoplesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.peoples',
        'app.addresses',
        'app.branchs',
        'app.schools',
        'app.users',
        'app.calendars',
        'app.class',
        'app.courses',
        'app.organizations',
        'app.evaluations',
        'app.comments',
        'app.class_discipline',
        'app.disciplines',
        'app.lessons',
        'app.classes',
        'app.frequencies',
        'app.times',
        'app.registrations'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
