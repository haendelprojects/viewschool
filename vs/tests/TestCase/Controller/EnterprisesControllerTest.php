<?php
namespace App\Test\TestCase\Controller;

use App\Controller\EnterprisesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\EnterprisesController Test Case
 */
class EnterprisesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.enterprises',
        'app.addresses',
        'app.cities',
        'app.states',
        'app.countries',
        'app.holidays',
        'app.regions',
        'app.prices',
        'app.services_contracts',
        'app.contracts',
        'app.contracts_tools',
        'app.tools',
        'app.services',
        'app.occurrences',
        'app.external_os',
        'app.coupons',
        'app.users',
        'app.roles',
        'app.notifications',
        'app.facebooks',
        'app.availabilities',
        'app.comments',
        'app.configurations',
        'app.disabled_reasons',
        'app.favorite_specialists',
        'app.historics',
        'app.individuals',
        'app.individuals_tests',
        'app.tests',
        'app.skills',
        'app.questions',
        'app.tests_questions',
        'app.answers',
        'app.individuals_skills',
        'app.services_skills',
        'app.trainings',
        'app.videos',
        'app.tutorials',
        'app.documents',
        'app.individuals_auths',
        'app.auths',
        'app.socialmedias',
        'app.links',
        'app.phones',
        'app.service_providers',
        'app.medias',
        'app.messages',
        'app.proposed_occurrences',
        'app.transportations',
        'app.extracts',
        'app.funds',
        'app.funds_months',
        'app.ratings',
        'app.reviews',
        'app.ratings_reviews',
        'app.credits',
        'app.account_statements',
        'app.payment_methods'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
