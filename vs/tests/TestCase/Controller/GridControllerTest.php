<?php
namespace App\Test\TestCase\Controller;

use App\Controller\GridController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\GridController Test Case
 */
class GridControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.grid',
        'app.class',
        'app.courses',
        'app.branchs',
        'app.addresses',
        'app.peoples',
        'app.users',
        'app.schools',
        'app.calendars',
        'app.comments',
        'app.class_discipline',
        'app.disciplines',
        'app.lessons',
        'app.classes',
        'app.frequencies',
        'app.times',
        'app.registrations',
        'app.evaluations',
        'app.organizations'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
