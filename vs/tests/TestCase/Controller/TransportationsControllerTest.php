<?php
namespace App\Test\TestCase\Controller;

use App\Controller\TransportationsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\TransportationsController Test Case
 */
class TransportationsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.transportations',
        'app.users',
        'app.roles',
        'app.notifications',
        'app.enterprises',
        'app.addresses',
        'app.cities',
        'app.states',
        'app.countries',
        'app.holidays',
        'app.regions',
        'app.prices',
        'app.services_contracts',
        'app.contracts',
        'app.contracts_tools',
        'app.tools',
        'app.services',
        'app.occurrences',
        'app.external_os',
        'app.coupons',
        'app.comments',
        'app.extracts',
        'app.funds',
        'app.funds_months',
        'app.historics',
        'app.medias',
        'app.ratings',
        'app.reviews',
        'app.ratings_reviews',
        'app.skills',
        'app.questions',
        'app.tests',
        'app.tests_questions',
        'app.answers',
        'app.trainings',
        'app.videos',
        'app.tutorials',
        'app.individuals',
        'app.individuals_tests',
        'app.documents',
        'app.individuals_auths',
        'app.auths',
        'app.socialmedias',
        'app.links',
        'app.phones',
        'app.service_providers',
        'app.individuals_skills',
        'app.services_skills',
        'app.credits',
        'app.account_statements',
        'app.payment_methods',
        'app.proposed_occurrences',
        'app.disabled_reasons',
        'app.facebooks',
        'app.availabilities',
        'app.configurations',
        'app.favorite_specialists',
        'app.messages'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
