<?php
namespace App\Test\TestCase\Controller;

use App\Controller\ProposedOccurrencesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\ProposedOccurrencesController Test Case
 */
class ProposedOccurrencesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.proposed_occurrences',
        'app.cities',
        'app.states',
        'app.countries',
        'app.addresses',
        'app.individuals',
        'app.users',
        'app.roles',
        'app.notifications',
        'app.enterprises',
        'app.contracts',
        'app.contracts_tools',
        'app.tools',
        'app.services',
        'app.occurrences',
        'app.external_os',
        'app.prices',
        'app.regions',
        'app.services_contracts',
        'app.payment_methods',
        'app.coupons',
        'app.comments',
        'app.extracts',
        'app.funds',
        'app.funds_months',
        'app.historics',
        'app.medias',
        'app.ratings',
        'app.reviews',
        'app.ratings_reviews',
        'app.skills',
        'app.questions',
        'app.tests',
        'app.tests_questions',
        'app.answers',
        'app.trainings',
        'app.videos',
        'app.tutorials',
        'app.individuals_tests',
        'app.individuals_skills',
        'app.services_skills',
        'app.credits',
        'app.account_statements',
        'app.disabled_reasons',
        'app.phones',
        'app.service_providers',
        'app.facebooks',
        'app.availabilities',
        'app.configurations',
        'app.favorite_specialists',
        'app.messages',
        'app.transportations',
        'app.documents',
        'app.individuals_auths',
        'app.auths',
        'app.socialmedias',
        'app.links',
        'app.holidays'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
