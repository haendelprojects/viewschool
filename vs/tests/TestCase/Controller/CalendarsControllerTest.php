<?php
namespace App\Test\TestCase\Controller;

use App\Controller\CalendarsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\CalendarsController Test Case
 */
class CalendarsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.calendars',
        'app.users',
        'app.schools',
        'app.branchs',
        'app.addresses',
        'app.peoples',
        'app.class',
        'app.courses',
        'app.organizations',
        'app.evaluations',
        'app.class_discipline',
        'app.disciplines',
        'app.lessons',
        'app.classes',
        'app.frequencies',
        'app.times',
        'app.comments',
        'app.registrations'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
