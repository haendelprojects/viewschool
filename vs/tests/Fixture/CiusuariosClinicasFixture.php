<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CiusuariosClinicasFixture
 *
 */
class CiusuariosClinicasFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'ciUsuario_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'clinica_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'fk_ciUsuarios_clinicas_ciUsuarios1_idx' => ['type' => 'index', 'columns' => ['ciUsuario_id'], 'length' => []],
            'fk_ciUsuarios_clinicas_clinicas1_idx' => ['type' => 'index', 'columns' => ['clinica_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_ciUsuarios_clinicas_ciUsuarios1' => ['type' => 'foreign', 'columns' => ['ciUsuario_id'], 'references' => ['ciusuarios', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_ciUsuarios_clinicas_clinicas1' => ['type' => 'foreign', 'columns' => ['clinica_id'], 'references' => ['clinicas', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'ciUsuario_id' => 1,
            'clinica_id' => 1
        ],
    ];
}
