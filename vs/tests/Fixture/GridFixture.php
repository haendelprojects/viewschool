<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * GridFixture
 *
 */
class GridFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'grid';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'week_day' => ['type' => 'string', 'length' => 45, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'init_hour' => ['type' => 'time', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'end_hour' => ['type' => 'time', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'class_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'dicipline_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'fk_grid_class1_idx' => ['type' => 'index', 'columns' => ['class_id'], 'length' => []],
            'fk_grid_dicipline1_idx' => ['type' => 'index', 'columns' => ['dicipline_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_grid_class1' => ['type' => 'foreign', 'columns' => ['class_id'], 'references' => ['class', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_grid_dicipline1' => ['type' => 'foreign', 'columns' => ['dicipline_id'], 'references' => ['disciplines', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'week_day' => 'Lorem ipsum dolor sit amet',
            'init_hour' => '20:21:38',
            'end_hour' => '20:21:38',
            'class_id' => 1,
            'dicipline_id' => 1
        ],
    ];
}
