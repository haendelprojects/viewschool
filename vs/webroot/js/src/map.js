(function () {
    'use strict';

    /**
     * Elemento da div de mapa
     */
    var mapElement = $('.map');

    /**
     * Campos de latitude e long
     */
    var inputLat = $('.input-lat');
    var inputLng = $('.input-lng');

    /**
     * Opções do Mapa
     * @type {{zoom: number, center: Array}}
     */
    var optionsMap = {
        zoom: 14,
        center: [8, 32]
    }

    if (mapElement.length) {

        /**
         *Verificar as opções do mapa, e mesclar com o padrão
         */
        if (mapElement.attr('data-options')) {
            var dataOptions = JSON.parse(mapElement.attr('data-options'));
            optionsMap = Object.assign(optionsMap, dataOptions);
        }

        /**
         * Definir instancia do mapa, e ponto central
         */
        var map = L.map('mapid', {
            center: optionsMap.center,
            zoom: optionsMap.zoom
        });

        /**
         * Mapa a ser utilizado na plataforma
         */
        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">FindUP</a>'
        }).addTo(map);

        /**
         * Icons
         */
        var icons = {
            primary: L.icon({
                iconUrl: '/img/icon.png',
                iconAnchor: [24, 48]
            }),
            tech: L.icon({
                iconUrl: '/img/icon_tec.png',
                iconAnchor: [24, 48]
            })
        };

        /**
         * Se o marcador padrão do mapa for arrastavel, executar ações
         */
        if (mapElement.attr('data-marker')) {
            var markersInfo = JSON.parse(mapElement.attr('data-marker'));
            //Definir icon padrão da aplicação
            markersInfo.options.icon = icons.primary;
            // Criação do marcador principal
            var marker = L.marker(markersInfo.location, markersInfo.options).addTo(map);
            marker.on('dragend', function (e) {
                var position = e.target.getLatLng();
                inputLat.val(position.lat);
                inputLng.val(position.lng);
            })
        }

        /**
         * Se o marcador padrão do mapa for arrastavel, executar ações
         */
        if (mapElement.attr('data-marker-tech')) {
            var markersInfo = JSON.parse(mapElement.attr('data-marker-tech'));
            //Definir icon padrão da aplicação
            if (markersInfo.options) {
                markersInfo.options.icon = icons.tech;
            } else {
                markersInfo.options = {icon: icons.tech};
            }
            // Criação do marcador principal
            var markerTech = L.marker(markersInfo.location, markersInfo.options).addTo(map);
        }

        /**
         * Multiplos pontos
         */
        if (mapElement.attr('data-markers')) {
            var markersInfo = JSON.parse(mapElement.attr('data-markers'));

            markersInfo.forEach(function (m) {
                L.marker(m.position, {icon: icons.tech}).bindPopup(m.popup).addTo(map);
            });
        }

        if (mapElement.attr('data-multi-markers')) {
            var multiMarker = JSON.parse(mapElement.attr('data-multi-markers'));

            multiMarker.forEach(function (mm) {
                mm.markers.forEach(function (m) {
                    var icon = L.icon({
                        iconUrl: mm.icon,
                        iconAnchor: [24, 48]
                    });
                    L.marker(m.position, {icon: icon}).bindPopup(m.popup).addTo(map);
                });
            })

        }
    }
})();

