(function () {

    hexToRGB = function hexToRGB(hex, alpha) {
        var r = parseInt(hex.slice(1, 3), 16),
            g = parseInt(hex.slice(3, 5), 16),
            b = parseInt(hex.slice(5, 7), 16);

        if (alpha) {
            return "rgba(" + r + ", " + g + ", " + b + ", " + alpha + ")";
        } else {
            return "rgb(" + r + ", " + g + ", " + b + ")";
        }
    };

    var chartColor = "#FFFFFF";
    var gradientChartOptionsConfiguration = {
        maintainAspectRatio: false,
        legend: {
            display: false
        },
        tooltips: {
            bodySpacing: 4,
            mode: "nearest",
            intersect: 0,
            position: "nearest",
            xPadding: 10,
            yPadding: 10,
            caretPadding: 10
        },
        responsive: 1,
        scales: {
            yAxes: [{
                display: 0,
                gridLines: 0,
                ticks: {
                    display: false
                },
                gridLines: {
                    zeroLineColor: "transparent",
                    drawTicks: false,
                    display: false,
                    drawBorder: false
                }
            }],
            xAxes: [{
                display: 0,
                gridLines: 0,
                ticks: {
                    display: false
                },
                gridLines: {
                    zeroLineColor: "transparent",
                    drawTicks: false,
                    display: false,
                    drawBorder: false
                }
            }]
        },
        layout: {
            padding: {left: 0, right: 0, top: 15, bottom: 15}
        }
    };

    var gradientChartOptionsConfigurationWithNumbersAndGrid = {
        maintainAspectRatio: false,
        legend: {
            display: false
        },
        tooltips: {
            bodySpacing: 4,
            mode: "nearest",
            intersect: 0,
            position: "nearest",
            xPadding: 10,
            yPadding: 10,
            caretPadding: 10
        },
        responsive: true,
        scales: {
            yAxes: [{
                gridLines: 0,
                gridLines: {
                    zeroLineColor: "transparent",
                    drawBorder: false
                }
            }],
            xAxes: [{
                display: 0,
                gridLines: 0,
                ticks: {
                    display: false
                },
                gridLines: {
                    zeroLineColor: "transparent",
                    drawTicks: false,
                    display: false,
                    drawBorder: false
                }
            }]
        },
        layout: {
            padding: {left: 0, right: 0, top: 15, bottom: 15}
        }
    };

    /**
     * Chart de filiais
     */
    var classChartAddressOccurrences = $('.chart-address-occurrences');

    if (classChartAddressOccurrences.length) {
        var ctxa = document.getElementById("idChartOccurrencesAddress").getContext('2d');
        gradientStroke = ctxa.createLinearGradient(500, 0, 100, 0);
        gradientStroke.addColorStop(0, '#18ce0f');
        gradientStroke.addColorStop(1, chartColor);

        gradientFill = ctxa.createLinearGradient(0, 170, 0, 50);
        gradientFill.addColorStop(0, "rgba(128, 182, 244, 0)");
        gradientFill.addColorStop(1, hexToRGB('#18ce0f',0.4));

        var chartOccurrencesAddress = new Chart(ctxa, {
            type: 'bar',

            data: {
                labels: JSON.parse(classChartAddressOccurrences.attr('data-labels')),
                ids: JSON.parse(classChartAddressOccurrences.attr('data-ids')),
                datasets: [{
                    label: 'Total de ocorrências por filial',
                    data: JSON.parse(classChartAddressOccurrences.attr('data-values')),
                    borderColor: "#18ce0f",
                    pointBorderColor: "#FFF",
                    pointBackgroundColor: "#18ce0f",
                    pointBorderWidth: 1,
                    pointHoverRadius: 4,
                    pointHoverBorderWidth: 1,
                    pointRadius: 4,
                    fill: true,
                    backgroundColor: gradientFill,
                    borderWidth: 2,
                }]
            },
            options: gradientChartOptionsConfigurationWithNumbersAndGrid
        });

        classChartAddressOccurrences.on('click', function (evt) {
            var activePoint = chartOccurrencesAddress.getElementAtEvent(evt);
            var val = chartOccurrencesAddress.data.ids[activePoint[0]._index];
            var win = window.open('/occurrences?filial=' + val, '_blank');
            win.focus();
        });
    }

    /**
     * Chart serviços
     * @type {void|jQuery|HTMLElement}
     */
    var classChartServices = $('.chart-services');
    if (classChartServices.length) {
        var ctxs = document.getElementById("idChartServices").getContext('2d');

        gradientFill = ctxs.createLinearGradient(0, 170, 0, 50);
        gradientFill.addColorStop(0, "rgba(128, 182, 244, 0)");
        gradientFill.addColorStop(1, hexToRGB('#2CA8FF', 0.6));

        var chartServices = new Chart(ctxs, {
            type: 'bar',
            data: {
                labels: JSON.parse(classChartServices.attr('data-labels')),
                ids: JSON.parse(classChartServices.attr('data-ids')),
                datasets: [{
                    label: 'Total de ocorrências por serviço',
                    data: JSON.parse(classChartServices.attr('data-values')),
                    backgroundColor: gradientFill,
                    borderColor: "#2CA8FF",
                    pointBorderColor: "#FFF",
                    pointBackgroundColor: "#2CA8FF",
                    pointBorderWidth: 2,
                    pointHoverRadius: 4,
                    pointHoverBorderWidth: 1,
                    pointRadius: 4,
                    fill: true,
                    borderWidth: 1,
                }]
            },
            options: {
                maintainAspectRatio: false,
                legend: {
                    display: false
                },
                tooltips: {
                    bodySpacing: 4,
                    mode:"nearest",
                    intersect: 0,
                    position:"nearest",
                    xPadding:10,
                    yPadding:10,
                    caretPadding:10
                },
                responsive: 1,
                scales: {
                    yAxes: [{
                        gridLines:0,
                        gridLines: {
                            zeroLineColor: "transparent",
                            drawBorder: false
                        }
                    }],
                    xAxes: [{
                        display:0,
                        gridLines:0,
                        ticks: {
                            display: false
                        },
                        gridLines: {
                            zeroLineColor: "transparent",
                            drawTicks: false,
                            display: false,
                            drawBorder: false
                        }
                    }]
                },
                layout:{
                    padding:{left:0,right:0,top:15,bottom:15}
                }
            }
        });

        classChartServices.on('click', function (evt) {
            var activePoint = chartServices.getElementAtEvent(evt);
            var val = chartServices.data.ids[activePoint[0]._index];
            var win = window.open('/occurrences?service_id=' + val, '_blank');
            win.focus();
        });
    }

    /**
     * Chart ups
     * @type {void|jQuery|HTMLElement}
     */
    var classChartUps = $('.chart-ups');
    if (classChartUps.length) {
        var ctxu = document.getElementById("idChartUps").getContext('2d');

        var gradientStroke = ctxu.createLinearGradient(500, 0, 100, 0);
        gradientStroke.addColorStop(0, '#80b6f4');
        gradientStroke.addColorStop(1, chartColor);

        var gradientFill = ctxu.createLinearGradient(0, 200, 0, 50);
        gradientFill.addColorStop(0, "rgba(128, 182, 244, 0)");
        gradientFill.addColorStop(1, "rgba(255, 255, 255, 0.24)");

        var chartUps = new Chart(ctxu, {
            type: 'line',
            data: {
                labels: JSON.parse(classChartUps.attr('data-labels')),
                datasets: [{
                    label: 'Total de UPs no mês',
                    data: JSON.parse(classChartUps.attr('data-values')),
                    borderColor: chartColor,
                    pointBorderColor: chartColor,
                    pointBackgroundColor: "#1e3d60",
                    pointHoverBackgroundColor: "#1e3d60",
                    pointHoverBorderColor: chartColor,
                    pointBorderWidth: 1,
                    pointHoverRadius: 7,
                    pointHoverBorderWidth: 2,
                    pointRadius: 5,
                    fill: true,
                    backgroundColor: gradientFill,
                    borderWidth: 2,
                }]
            },
            options: {
                layout: {
                    padding: {
                        left: 20,
                        right: 20,
                        top: 0,
                        bottom: 0
                    }
                },
                maintainAspectRatio: false,
                tooltips: {
                    backgroundColor: '#fff',
                    titleFontColor: '#333',
                    bodyFontColor: '#666',
                    bodySpacing: 4,
                    xPadding: 12,
                    mode: "nearest",
                    intersect: 0,
                    position: "nearest"
                },
                legend: {
                    position: "bottom",
                    fillStyle: "#FFF",
                    display: false
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            fontColor: "rgba(255,255,255,0.4)",
                            fontStyle: "bold",
                            beginAtZero: true,
                            maxTicksLimit: 5,
                            padding: 10
                        },
                        gridLines: {
                            drawTicks: true,
                            drawBorder: false,
                            display: true,
                            color: "rgba(255,255,255,0.1)",
                            zeroLineColor: "transparent"
                        }

                    }],
                    xAxes: [{
                        gridLines: {
                            zeroLineColor: "transparent",
                            display: false,

                        },
                        ticks: {
                            padding: 10,
                            fontColor: "rgba(255,255,255,0.4)",
                            fontStyle: "bold"
                        }
                    }]
                }
            }
        });

        classChartUps.on('click', function (evt) {
            var activePoint = chartUps.getElementAtEvent(evt);
            var val = chartUps.data.labels[activePoint[0]._index];
            var win = window.open('/occurrences?month=' + val, '_blank');
            win.focus();
        });
    }

    /**
     * Chart de validações dadas
     * @type {void|jQuery|HTMLElement}
     */

    var classChartValidation = $('.chart-validation');
    if (classChartValidation.length) {
        var ctxv = document.getElementById("idValidation").getContext('2d');
        var chartValidation = new Chart(ctxv, {
            type: 'doughnut',
            data: {
                labels: JSON.parse(classChartValidation.attr('data-labels')),
                datasets: [{
                    label: 'Total de UPs no mês',
                    data: JSON.parse(classChartValidation.attr('data-values')),
                    backgroundColor: ["#dedede", "#d5fb42", "#70a2b5", "#3c00ff", "#42b7fb", "#42fbca"]
                }]
            }
        });

        classChartValidation.on('click', function (evt) {
            var activePoint = chartValidation.getElementAtEvent(evt);
            var val = chartValidation.data.labels[activePoint[0]._index];
            var win = window.open('/occurrences?ratings=' + val, '_blank');
            win.focus();
        });
    }

})();
