(function () {
    'use strict';


    var searchSelect = $('.search-select');
    /**
     * Instanciar o select2
     */
    searchSelect.select2();

    /**
     * Eventos
     */
    searchSelect.on('change', function () {
        var elementId = $(this).val();

        /**
         * Carregar informações de load
         */
        if ($(this).attr('data-load-selects')) {
            var loadSelects = JSON.parse($(this).attr('data-load-selects'));
        }

        var hideLoad = 0;

        if (loadSelects) {
            $.LoadingOverlay("show");
            loadSelects.forEach(function (select) {
                var $ele = $('#' + select.id);
                $ele.empty();
                // Criar url com base na enviada, subistuir ID pelo id selecionar no campo
                var url = select.url.replace('ID', elementId);
                $.get(url, function (data) {
                    $ele.append(data);
                    hideLoad++;
                    if (hideLoad == loadSelects.length) $.LoadingOverlay("hide");
                });
            });

        }
    });

})();