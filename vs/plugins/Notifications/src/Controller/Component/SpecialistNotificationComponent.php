<?php
/**
 * Created by PhpStorm.
 * User: Haendel
 * Date: 29/06/2016
 * Time: 14:05
 */

namespace Notifications\Controller\Component;

use Cake\Controller\Component;

/**
 * Class SpecialistNotificationComponent
 * @package Notifications\Controller
 * @property OneSignalComponent $OneSignal
 * @property OneSignalComponent $Trigger
 */
class SpecialistNotificationComponent extends Component
{
    public $components = ['Notifications.OneSignal'];
    private $Trigger;
    private $Admin;
    private $key = 'OTkzNjkxYzYtMmQxMS00NWZhLWE4OGMtYTg4NzBjNjkzNWRj';
    private $appId = 'a9385296-6302-40c7-a79d-7cb8e533681e';


    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->Trigger = $this->OneSignal;
    }

        /**
     * @param $user_id Recebedor da mensagem
     * @param $occurrence_id Ocorrencia trabalhada
     */
    public function message($title, $text, $user_id = null )
    {
        $send = $this->Trigger
             ->create($this->key , $this->appId)
            ->clear()
            ->setHeading('en', $title)
            ->setHeading('pt_BR', $title)
            ->setContent('en', $text)
            ->setContent('pt_BR', $text)
            ->setData('type', 'message');

            if($user_id){
                $send ->setTag('user_id', '=', $user_id);
            }

            debug($send->send());
    }



    /**
     * @param $user_id Recebedor da mensagem
     * @param $occurrence_id Ocorrencia trabalhada
     */
    public function accept($user_id, $occurrence_id)
    {
        $this->Trigger
             ->create($this->key , $this->appId)
            ->clear()
            ->setHeading('en', 'The expert accept occurrence.')
            ->setHeading('pt_BR', 'O Especialista aceitou sua ocorrência!')
            ->setContent('en', '')
            ->setContent('pt_BR', 'Em breve ele chegará.')
            ->setData('type', 'occurrence')
            ->setData('id', $occurrence_id)
            ->setTag('user_id', '=', $user_id)
            ->send();
    }


    /**
     * @param $user_id Recebedor da mensagem
     * @param $occurrence_id Ocorrencia trabalhada
     */
    public function going($user_id, $occurrence_id)
    {
        $this->Trigger
             ->create($this->key , $this->appId)
            ->clear()
            ->setHeading('en', 'Wait.')
            ->setHeading('pt_BR', 'Aguarde.')
            ->setContent('en', 'The expert is on the way')
            ->setContent('pt_BR', 'O Especialista está a caminho!')
            ->setData('type', 'occurrence')
            ->setData('id', $occurrence_id)
            ->setTag('user_id', '=', $user_id)
            ->send();
    }

    /**
     * @param $user_id Recebedor da mensagem
     * @param $occurrence_id Ocorrencia trabalhada
     */
    public function cancel($user_id, $occurrence_id)
    {
        $this->Trigger
             ->create($this->key , $this->appId)
            ->clear()
            ->setHeading('en', 'Sorry.')
            ->setHeading('pt_BR', 'Desculpe-nos.')
            ->setContent('en', 'The expert canceled the service.')
            ->setContent('pt_BR', 'O Especialista cancelou o atendimento do serviço!')
            ->setData('type', 'occurrence')
            ->setData('id', $occurrence_id)
            ->setTag('user_id', '=', $user_id)
            ->send();
    }


    /**
     * @param $user_id Recebedor da mensagem
     * @param $occurrence_id Ocorrencia trabalhada
     */
    public function checkin($user_id, $occurrence_id)
    {
        $this->Trigger
             ->create($this->key , $this->appId)
            ->clear()
            ->setHeading('en', 'The expert arrived')
            ->setHeading('pt_BR', 'O Especialista Chegou!')
            ->setContent('en', 'The service will start soon.')
            ->setContent('pt_BR', 'Em breve ele começará o serviço.')
            ->setData('type', 'occurrence')
            ->setData('id', $occurrence_id)
            ->setTag('user_id', '=', $user_id)
            ->send();
    }

    /**
     * O checkout foi confirmado
     * @param $user_id
     * @param $occurrence_id
     */
    public function checkout($user_id, $occurrence_id)
    {
        $this->Trigger
             ->create($this->key , $this->appId)
            ->clear()
            ->setHeading('en', 'Finished service.')
            ->setHeading('pt_BR', 'Serviço concluído.')
            ->setContent('en', 'The expert arrived')
            ->setContent('pt_BR', 'O Especialista concluiu o serviço!')
            ->setData('type', 'occurrence')
            ->setData('id', $occurrence_id)
            ->setTag('user_id', '=', $user_id)
            ->send();
    }
}