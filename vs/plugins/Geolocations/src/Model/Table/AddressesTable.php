<?php
namespace GeoLocations\Model\Table;

use GeoLocations\Model\Entity\Address;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Geo\Geocoder\Geocoder;
use Geocoder\Provider\OpenStreetMap;

/**
 * Addresses Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Individuals
 * @property \Cake\ORM\Association\BelongsTo $Enterprises
 * @property \Cake\ORM\Association\HasMany $Occurrences
 */
class AddressesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('addresses');
        $this->displayField('title');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Individuals', [
            'foreignKey' => 'individual_id'
        ]);
        $this->belongsTo('Enterprises', [
            'foreignKey' => 'enterprise_id'
        ]);

        $this->belongsTo('OCities', [
            'className' => 'Cities',
            'foreignKey' => 'city_id'
        ]);
        $this->hasMany('Occurrences', [
            'foreignKey' => 'address_id'
        ]);

        $this->addBehavior('Geo.Geocoder');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->allowEmpty('name');

        $validator
            ->allowEmpty('street');

        $validator
            ->allowEmpty('number');

        $validator
            ->allowEmpty('complement');

        $validator
            ->allowEmpty('neighborhood');

        $validator
            ->requirePresence('city', 'create')
            ->notEmpty('city');

        $validator
            ->allowEmpty('country');

        $validator
            ->requirePresence('state', 'create')
            ->notEmpty('state');

        $validator
            ->allowEmpty('title');

        $validator
            ->allowEmpty('zipcode');

        $validator
            ->numeric('latitude')
            ->allowEmpty('latitude');

        $validator
            ->numeric('longitude')
            ->allowEmpty('longitude');

        $validator
            ->boolean('active')
            ->requirePresence('active', 'create')
            ->notEmpty('active');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));
        $rules->add($rules->existsIn(['individual_id'], 'Individuals'));
        $rules->add($rules->existsIn(['enterprise_id'], 'Enterprises'));
        return $rules;
    }
}
