<?php
/**
 * Created by PhpStorm.
 * User: Haendel
 * Date: 18/08/2016
 * Time: 15:42
 */

namespace Flow\Controller\Component;

use App\Model\Table\ContractsTable;
use App\Model\Table\FundsMonthsTable;
use App\Model\Table\OccurrencesTable;
use Cake\Controller\Component;
use Cake\I18n\Date;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

/**
 * Class PointsComponent
 * @package Flow\Controller\Component
 *
 * @property OccurrencesTable $Occurrences
 * @property ContractsTable $Contracts
 * @property FundsMonthsTable $FundsMonths
 */
class PointsComponent extends Component
{
    private $FundsMonths;
    private $Funds;
    private $Cities;
    private $Occurrences;
    private $Coupons;
    private $Contracts;
    private $ServicesContracts;

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->FundsMonths = TableRegistry::get('FundsMonths');
        $this->Funds = TableRegistry::get('Funds');
        $this->Cities = TableRegistry::get('Cities');
        $this->Occurrences = TableRegistry::get('Occurrences');
        $this->Coupons = TableRegistry::get('Coupons');
        $this->Contracts = TableRegistry::get('Contracts');
        $this->ServicesContracts = TableRegistry::get('ServicesContracts');
    }

    /**
     * Criar FundsMonth
     * @param $contract
     * @return bool
     */
    public function createFundsMonth($contract)
    {
        $validity = $contract->fund->points_days_validity;
        if ($validity == null || $validity == 0) {
            $validity = 30;
        }

        $diff = $contract->initial_date->diff($contract->final_date);
        if ($diff->d > 0) {
            $diff->m = $diff->m + 1;
        }
        if ($diff->y > 0) {
            $diff->m = $diff->m + (12 * $diff->y);
        }
        if ($diff->m > 1) {
            $month_points = $contract->fund->total_points_quantity / ($diff->m);
        } else {
            $month_points = $contract->fund->total_points_quantity;
        }
        $date = $contract->initial_date;
        $initial_date = clone $date;
        $final_date = clone $date->addMonth(1);
        $final_date = clone $final_date->modify('-1 days');
        $stored_points_validity = clone $date;
        $stored_points_validity = $stored_points_validity->modify('+ ' . $validity . ' days');
        $i = 0;

        if ($contract->fund->funds_months == null) {
            if ($contract->fund->type == 'RECURRENT') {
                while ($initial_date < $contract->final_date) {

                    $data = [
                        'stored_points' => 0,
                        'fund_id' => $contract->fund->id,
                        'initial_date' => $initial_date,
                        'final_date' => $final_date,
                        'stored_points_validity' => $stored_points_validity,
                        'stored_points_expense' => 0,
                        'total_points_quantity' => (int)$month_points,// + $extra_points,
                        'total_expense' => 0,
                        'spare_points' => $contract->fund->month_points,// + $extra_points,
                        'owed_points' => 0,
                        'active' => true,
                        'depleted' => false
                    ];

                    if ($validity == 0 || !$validity) {
                        $validity = 30;
                    }

                    $fund_month = $this->Funds->FundsMonths->newEntity();

                    $fund_month = $this->Funds->FundsMonths->patchEntity($fund_month, $data);

                    if (!($this->Funds->FundsMonths->save($fund_month))) {
                        debug($fund_month->errors());
                    } else {
                        $i += 1;
                        $initial_date = clone $contract->initial_date->addMonth($i);
                        $date = clone $initial_date;
                        $date = $date->addMonth(1);
                        $final_date = clone $date->modify('-1 days');
                        $stored_points_validity = clone $initial_date;
                        $stored_points_validity = $stored_points_validity->modify('+ ' . $validity . ' days');
                    }
                }
            } else {
                $i = 0;
                while ($initial_date < $contract->final_date) {

                    $data = [
                        'stored_points' => 0,
                        'fund_id' => $contract->fund->id,
                        'initial_date' => $initial_date,
                        'final_date' => $final_date,
                        'stored_points_expense' => 0,
                        'total_points_quantity' => $contract->fund->total_points_quantity,// + $extra_points,
                        'total_expense' => 0,
                        'spare_points' => $contract->fund->total_points_quantity,// + $extra_points,
                        'owed_points' => 0,
                        'active' => true,
                        'depleted' => false
                    ];

                    $fund_month = $this->Funds->FundsMonths->newEntity();

                    $fund_month = $this->Funds->FundsMonths->patchEntity($fund_month, $data);

                    if (!($this->Funds->FundsMonths->save($fund_month))) {
                        debug($fund_month->errors());
                    } else {

                        $i += 1;
                        $initial_date = clone $contract->initial_date->addMonth($i);
                        $date = clone $initial_date;
                        $date = $date->addMonth(1);
                        $final_date = clone $date->modify('-1 days');
                    }
                }
            }
        }
        return true;
    }

    /**
     * Valores para criação da occorencia
     * @param null $service_id
     * @param null $enterprise_id
     * @param null $contract_id
     * @param null $state_id
     * @param null $city_id
     * @return array|bool
     */
    public function getPoints($service_id = null, $enterprise_id = null, $contract_id = null, $state_id = null, $city_id = null)
    {
        $conditions1 = [
            'Contracts.id' => $contract_id,
            'enterprise_id' => $enterprise_id,
        ];

        $contract = $this->Contracts
            ->find('all')
            ->where($conditions1)
            ->contain(['Funds'])
            ->first();


        if ($contract) {
            $conditions = [
                'active' => true,
                'service_id' => $service_id,
                'contract_id' => $contract->id
            ];

            $servicesContract = $this->ServicesContracts
                ->find('all')
                ->where($conditions)
                ->first();

            if ($servicesContract) {

                $specialist_price = $this->getSpecialistPrice($state_id, $city_id);

                if ($specialist_price) {

                    return [
                        'service_contract_id' => $servicesContract->id,
                        'occurrence_base_price' => $servicesContract->occurrence_base_points,
                        'occurrence_base_hours' => $servicesContract->occurrence_base_hours,
                        'occurrence_extra_price' => $servicesContract->occurrence_extra_points,
                        'occurrence_extra_hours' => $servicesContract->occurrence_extra_hours,
                        'specialist_base_price' => $specialist_price['specialist_base_price'],
                        'specialist_total_price' => $specialist_price['specialist_base_price'],
                        'specialist_extra_price' => $specialist_price['specialist_extra_price'],
                        'occurrence_total_price' => $servicesContract->occurrence_base_points,
                        'unique_point_value' => $contract->fund->unique_point_value,
                        'currency' => 'POINTS'
                    ];
                }
            }
        }
        return false;
    }

    /**
     * Valores relacionado o tecnico
     * @param $state_id
     * @param $city_id
     * @return array|bool
     */
    public function getSpecialistPrice($state_id, $city_id)
    {
        if ((is_numeric($state_id)) && (is_numeric($city_id))) {
            $city = $this->Cities
                ->find('all')
                ->where([
                    'States.id' => $state_id,
                    'Cities.id' => $city_id
                ])
                ->contain([
                    'States', 'Regions'
                ])
                ->first();
        } else if ((is_numeric($state_id)) && !(is_numeric($city_id))) {
            $city = $this->Cities
                ->find('all')
                ->where([
                    'States.id' => $state_id,
                    'Cities.name' => $city_id
                ])
                ->contain([
                    'States', 'Regions'
                ])
                ->first();
        } else if (!(is_numeric($state_id)) && (is_numeric($city_id))) {
            $city = $this->Cities
                ->find('all')
                ->where([
                    'States.abbrv' => $state_id,
                    'Cities.id' => $city_id
                ])
                ->contain([
                    'States', 'Regions'
                ])
                ->first();
        } else {
            $city = $this->Cities
                ->find('all')
                ->where([
                    'States.abbrv' => $state_id,
                    'Cities.name' => $city_id
                ])
                ->contain([
                    'States', 'Regions'
                ])
                ->first();
        }


        if ($city) {
            if ($city->region) {
                return [
                    'specialist_base_price' => $city->region->specialist_base_price,
                    'specialist_extra_price' => $city->region->specialist_extra_price,
                ];
            }
        }

        return false;
    }

    /**
     * Retirar pontos ao abrir chamado
     * @param $occurrence
     * @return bool
     */
    public function withdrawCreateOccurrencePoints($occurrence)
    {
        return $this->calculatePointsContract($occurrence->contract_id);
    }

    public function withdrawFinishOccurrencePoints($occurrence)
    {
        return $this->calculatePointsContract($occurrence->contract_id);
    }

    /**
     * Calcular pontos do contrato
     * @param $contractId
     */
    public function calculatePointsContract($contractId)
    {

        $contract = $this->Contracts
            ->find('all')
            ->contain(['Funds', 'Services', 'Funds.FundsMonths'])
            ->where([
                'Contracts.id' => $contractId
            ])
            ->first();

        if ($contract->fund->type == 'RECURRENT') {
            if (!$this->_calculateRecurrent($contract)) {
                $c = $this->Contracts->patchEntity($contract, ['active' => false]);
                $this->Contracts->save($c);
            }
        } else {


            if (!$this->_calculateDetached($contract)) {
                $c = $this->Contracts->patchEntity($contract, ['active' => false]);
                $this->Contracts->save($c);
            }
        }
    }


    /**
     * Calcular fundos de contrato recorrente
     * @param $contract
     * @return bool
     */
    private function _calculateRecurrent($contract)
    {
        $funds_months = $this->Funds->FundsMonths
            ->find('all')
            ->where([
                'fund_id' => $contract->fund->id,
                'active' => true
            ]);


        // Divisão dos pontos por mes
        $month_points = $this->_monthPoints($contract);

        // Pontos devedores do mes anterior
        $previusMonthDebtor = 0;
        // Balança de crédito
        $credit_balance = 0;

        foreach ($funds_months as $fund_month) {
            // Datas para busca
            $initial_date = \DateTime::createFromFormat('d/m/Y H:i', $fund_month->initial_date->format('d/m/Y') . ' 00:00');
            $final_date = \DateTime::createFromFormat('d/m/Y H:i', $fund_month->final_date->format('d/m/Y') . ' 23:59');
            $stored_points_validity = \DateTime::createFromFormat('d/m/Y H:i', $fund_month->stored_points_validity->format('d/m/Y') . ' 23:59');


            // Ocorrencias na faixa de tempo
            $occurrences = $this->Occurrences->find();
            $occurrences = $occurrences
                ->find('all')
                ->where([
                    'status NOT IN' => [
                        'INVALID', 'CANCELED'
                    ],
                    'Occurrences.active' => true,
                    'Occurrences.contract_id' => $contract->id,
                    'Occurrences.created >=' => $initial_date,
                    'Occurrences.created <=' => $final_date
                ])
                ->contain(['Prices'])
                ->select(['total' => $occurrences->func()->sum('Prices.occurrence_total_price')])
                ->first();


            // Total de pontos disponiveis no mes
            $totalPointsMonth = (int)$month_points + $fund_month->additive_points;
            $now = new \DateTime('now');
            if ($initial_date < $now) {
                // Total de Pontos Gastos no range
                $totalPointsUsed = (int)$occurrences->total;
                // Total de pontos usados no range do mes anteriores
                $useds = $this->_previusMonthsOwedPoints($initial_date, $final_date, $contract->fund->id, $contract->id, $totalPointsUsed);
                // Pontos que sobraram
                $sparePoints = (int)($totalPointsMonth - $totalPointsUsed + $useds);

                if ($sparePoints > $totalPointsMonth) {
                    $sparePoints = $totalPointsMonth;
                }

                $sparePoints = ($previusMonthDebtor < 0) ? $sparePoints + $previusMonthDebtor : $sparePoints;
                // Pontos que sobraram e estão para uso
                //$owedPoints = $this->_fc2($fund_month, $contract, $sparePoints);
                $owedPoints = ($sparePoints >= 0) ? $sparePoints : 0;
                $depleted = ($sparePoints >= 0) ? false : true;

                $data = [
                    'total_points_quantity' => $totalPointsMonth,
                    'total_expense' => $totalPointsUsed,
                    'spare_points' => $sparePoints,
                    'owed_points' => $owedPoints,
                    'depleted' => $depleted
                ];

                // Se o mes tem debtor.
                $previusMonthDebtor = ($sparePoints < 0) ? $sparePoints : 0;
            } else {
                $data = [
                    'total_points_quantity' => $totalPointsMonth,
                    'total_expense' => 0,
                    'spare_points' => 0,
                    'owed_points' => 0,
                    'depleted' => false
                ];
            }

            // Salvar novas Informações do mes
            $fund_month = $this->Funds->FundsMonths->patchEntity($fund_month, $data);
            $this->Funds->FundsMonths->save($fund_month);

            if (!$fund_month->depleted) {
                $credit_balance += $fund_month->spare_points;
            }
            // Jogar valores do mes atual no fundo geral
            if ($initial_date < $now && $now < $final_date) {
                $fund = $contract->fund;
                $fund = $this->Funds->patchEntity($fund, [
                    'month_points' => $fund_month->total_points_quantity,
                    'month_expense' => $fund_month->total_expense,
                    'credit_balance' => $fund_month->spare_points
                ]);
                $this->Funds->save($fund);
            }
        }

        $this->_fundsLosers($contract->fund->id);
        $now = new \DateTime('now');
        // Retorna falso se estiver fora da validade
        if($now > $fund_month->stored_points_validity){
            return false;
        }else{
            return true;
        }
    }

    private function _calculateDetached($contract)
    {
        $fundsMonths = $this->Funds->FundsMonths
            ->find('all')
            ->where([
                'fund_id' => $contract->fund->id,
                'active' => true
            ]);

        // Balança de crédito
        $credit_balance = 0;

        if ($fundsMonths->count() > 1) {
            $this->_removeFunds($fundsMonths);

            $data = [
                'stored_points' => 0,
                'fund_id' => $contract->fund->id,
                'initial_date' => $contract->initial_date,
                'final_date' => $contract->final_date,
                'stored_points_expense' => 0,
                'total_points_quantity' => $contract->fund->total_points_quantity,// + $extra_points,
                'total_expense' => 0,
                'spare_points' => $contract->fund->total_points_quantity,// + $extra_points,
                'owed_points' => 0,
                'active' => true,
                'depleted' => false
            ];

            $fundMonth = $this->Funds->FundsMonths->newEntity();
            $fundMonth = $this->Funds->FundsMonths->patchEntity($fundMonth, $data);
            $this->Funds->FundsMonths->save($fundMonth);
        } else {
            $fundMonth = $fundsMonths->first();
        }

        // Datas para busca
        $initial_date = \DateTime::createFromFormat('d/m/Y H:i', $fundMonth->initial_date->format('d/m/Y') . ' 00:00');
        $final_date = \DateTime::createFromFormat('d/m/Y H:i', $fundMonth->final_date->format('d/m/Y') . ' 23:59');
        $now = new \DateTime('now');

        // Ocorrencias na faixa de tempo
        $occurrences = $this->Occurrences->find();
        $occurrences = $occurrences
            ->find('all')
            ->where([
                'status NOT IN' => [
                    'INVALID', 'CANCELED'
                ],
                'Occurrences.active' => true,
                'Occurrences.contract_id' => $contract->id,
                'Occurrences.created >=' => $initial_date,
                'Occurrences.created <=' => $final_date
            ])
            ->contain(['Prices'])
            ->select(['total' => $occurrences->func()->sum('Prices.occurrence_total_price')])
            ->first();


        // Total de pontos disponiveis no mes
        $totalPointsMonth = (int)$contract->fund->total_points_quantity + $fundMonth->additive_points;
        // Total de Pontos Gastos no range
        $totalPointsUsed = (int)$occurrences->total;
        // Pontos que sobraram
        $sparePoints = (int)($totalPointsMonth - $totalPointsUsed);
        $data = [
            'total_points_quantity' => $totalPointsMonth,
            'total_expense' => $totalPointsUsed,
            'spare_points' => $sparePoints,
            'owed_points' => $sparePoints,
            'depleted' => ($initial_date < $now && $now < $final_date) ? false : true
        ];
        // Salvar novas Informações do mes
        $fund_month = $this->Funds->FundsMonths->patchEntity($fundMonth, $data);
        $this->Funds->FundsMonths->save($fund_month);
        // Jogar valores do mes atual no fundo geral

        if (!$fund_month->depleted) {
            $credit_balance += $fund_month->spare_points;
        }

        if ($initial_date < $now && $now < $final_date) {
            $fund = $contract->fund;
            $fund = $this->Funds->patchEntity($fund, [
                'month_points' => $fund_month->total_points_quantity,
                'month_expense' => $fund_month->total_expense,
                'credit_balance' => $credit_balance
            ]);
            $this->Funds->save($fund);
        }

        // Retorna falso se estiver fora da validade
        if($now > $contract->final_date){
            return false;
        }else{
            return true;
        }
    }

    /**
     * Remover fundos mes
     * @param $fundsMonths
     * @return bool
     */
    private function _removeFunds($fundsMonths)
    {
        foreach ($fundsMonths as $fundMonth) {
            $this->FundsMonths->delete($fundMonth);
        }

        return true;
    }

    /**
     * calcular valor de ups por mes
     * @param $contract
     * @return float|int
     */
    private function _monthPoints($contract)
    {
        $diff = $contract->initial_date->diff($contract->final_date);
        if ($diff->d > 0) {
            $diff->m = $diff->m + 1;
        }
        if ($diff->y > 0) {
            $diff->m = $diff->m + (12 * $diff->y);
        }

        // Buscar aditivos para retirar do total
        $funds_months = $this->Funds->FundsMonths->find();
        $additive = $funds_months->select(['total' => $funds_months->func()->sum('additive_points')])
            ->where([
                'fund_id' => $contract->fund->id
            ])
            ->first();

        if ($diff->m > 1) {
            return ($contract->fund->total_points_quantity - $additive->total) / ($diff->m);
        } else {
            return ($contract->fund->total_points_quantity - $additive->total);
        }
    }


    /**
     * Debitar ups de meses anteriores ainda validos
     * @param $initDate
     * @param $endDate
     * @param $fundId
     * @param $contractId
     * @param $totalPointsUsed
     * @return int
     */
    private function _previusMonthsOwedPoints($initDate, $endDate, $fundId, $contractId, $totalPointsUsed)
    {
        // Fundos antes do fundo atual, seria os fundos que ainda estão validos
        $fundsMonths = $this->Funds->FundsMonths
            ->find('all')
            ->where([
                'fund_id' => $fundId,
                'initial_date <' => $initDate->format('Y-m-d H:i'),
                'stored_points_validity > ' => $initDate->format('Y-m-d H:i'),
                'depleted' => false
            ]);

        // Pontos Consumidos
        $consumed = 0;
        // Limite de occorrencias a serem buscadas
        $limit = 0;
        // Data da ultima occorrencia abertura em uma busca, inicialmente tem o mesmo valor do inicio do fundo do mes.
        $initDateLastOccurrence = $initDate;
        $previusMonthLost = 0;
        foreach ($fundsMonths as $fundMonth) {
            $storedPointsValidity = \DateTime::createFromFormat('d/m/Y H:i', $fundMonth->stored_points_validity->format('d/m/Y') . ' 23:59');
            $where = [
                'status NOT IN' => [
                    'INVALID', 'CANCELED'
                ],
                'Occurrences.active' => true,
                'Occurrences.contract_id' => $contractId,
                'AND' => [
                    ['Occurrences.created >=' => $initDate],
                    ['Occurrences.created >' => $initDateLastOccurrence],
                    ['Occurrences.created <=' => $endDate],
                    ['Occurrences.created <=' => $storedPointsValidity]
                ]
            ];

            // Ocorrencias na faixa de tempo
            $occurrences = $this->Occurrences->find();
            $occurrences = $occurrences
                ->find('all')
                ->where($where)
                ->contain(['Prices'])
                ->select(['total' => $occurrences->func()->sum('Prices.occurrence_total_price')])
                ->first();

            // Retirar pontos do acumulado no mes
            if ($occurrences->total) {
                $owedPoints = $fundMonth->owed_points - ($occurrences->total);
            } else {
                $owedPoints = $fundMonth->owed_points - ($occurrences->total + $previusMonthLost);
            }
            // Se for menor que zero, quer dizer que consumiu todos os pntos acumulados
            if ($owedPoints < 0) {
                $consumed += $fundMonth->owed_points;
                // Buscar ultima ocorrencia que foi consumida nesse mes
                $lastOccurrence = $this->Occurrences->find('all')->select(['created'])->where($where)->limit($fundMonth->owed_points)->last();
                if ($lastOccurrence) {
                    $previusMonthLost = -1 * $owedPoints;
                    // Definir a data init como da ultima ocorrencia
                    $initDateLastOccurrence = $lastOccurrence->created;
                    // Zerar os pontos acumulado e definir mes como esgotado
                    $fundMonth = $this->FundsMonths->patchEntity($fundMonth, ['owed_points' => 0, 'depleted' => true]);
                    $this->FundsMonths->save($fundMonth);
                } else {
                    $previusMonthLost = -1 * $owedPoints;
                    // Zerar os pontos acumulado e definir mes como esgotado
                    $fundMonth = $this->FundsMonths->patchEntity($fundMonth, ['owed_points' => 0, 'depleted' => true]);
                    $this->FundsMonths->save($fundMonth);
                }
            } else {
                $consumed += ($occurrences->total) ? $occurrences->total : $occurrences->total + $previusMonthLost;
                $fundMonth = $this->FundsMonths->patchEntity($fundMonth, ['owed_points' => (int)$owedPoints, 'depleted' => false]);
                $this->FundsMonths->save($fundMonth);
                // Não ha nicessidade de buscar mais meses, se o mes ja satisfez o gasto
                break;
            }
        }
        return $consumed;
    }

    /**
     * Fundos que veceram, setar para depleted = true, fundos que ja venceram.
     * @param $fundId
     * @return bool
     */
    private function _fundsLosers($fundId)
    {
        $fundsMonths = $this->Funds->FundsMonths
            ->find('all')
            ->where([
                'fund_id' => $fundId,
                'stored_points_validity < ' => new \DateTime('now')
            ]);

        foreach ($fundsMonths as $fundMonth) {
            $fundMonth = $this->FundsMonths->patchEntity($fundMonth, ['depleted' => true]);
            $this->FundsMonths->save($fundMonth);
        }
        return true;
    }

    /**
     * Verificar se uma data esta expirada
     * @param $validity
     * @return int
     */
    private function _expired($validity)
    {
        $now = Time::now();
        return ($now > $validity) ? true : false;
    }
}