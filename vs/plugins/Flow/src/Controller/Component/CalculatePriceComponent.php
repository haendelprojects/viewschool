<?php
/**
 * Created by PhpStorm.
 * User: Haendel
 * Date: 20/07/2016
 * Time: 17:17
 */

namespace Flow\Controller\Component;


use Cake\Controller\Component;
use Cake\I18n\Time;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;

class CalculatePriceComponent extends Component
{
    private $Cities;
    private $Occurrences;
    private $Coupons;

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->Cities = TableRegistry::get('Cities');
        $this->Occurrences = TableRegistry::get('Occurrences');
        $this->Coupons = TableRegistry::get('Coupons');
    }

    /**
     * @param $success
     * @param $message
     * @return array
     */
    private function _getReturn($success, $message)
    {
        return [
            'success' => $success,
            'message' => $message
        ];
    }

    public function price($state_id = null, $city_id = null, $paymentMethod = null, $coupon_id = null)
    {
        $city = $this->Cities
            ->find('all')
            ->where([
                'States.id' => $state_id,
                'Cities.id' => $city_id
            ])
            ->contain([
                'States', 'Regions'
            ])
            ->first();

        $not_found = true;

        if ($city) {
            if ($city->region) {
                $not_found = false;

                if ($city->region->id) {
                    if ($coupon_id) {
                        $coupon = $this->Coupons
                            ->find('all')
                            ->where(['Coupons.id' => $coupon_id])
                            ->first();

                        if ($coupon->type == 'COIN') {
                            $total = $city->region->occurrence_base_price - $coupon->value;
                        } else {
                            $total = $city->region->occurrence_base_price - (($city->region->occurrence_base_price * $coupon->value) / 100);
                        }

                        return [
                            'region_id' => $city->region->id,
                            'occurrence_total_price' => $total,
                            'occurrence_base_price' => $city->region->occurrence_base_price,
                            'occurrence_base_hours' => $city->region->occurrence_base_hours,
                            'occurrence_extra_price' => $city->region->occurrence_extra_price,
                            'occurrence_extra_hours' => $city->region->occurrence_extra_hours,
                            'specialist_total_price' => $city->region->specialist_base_price,
                            'specialist_base_price' => $city->region->specialist_base_price,
                            'specialist_extra_price' => $city->region->specialist_extra_price,
                            'currency' => $city->region->currency,
                            'payment_method_id' => $paymentMethod,
                            'occurrence_discount_price' => $coupon->value,
                            'occurrence_discount_type' => $coupon->type,
                            'active' => 1
                        ];
                    } else {
                        return [
                            'region_id' => $city->region->id,
                            'occurrence_base_price' => $city->region->occurrence_base_price,
                            'occurrence_total_price' => $city->region->occurrence_base_price,
                            'occurrence_base_hours' => $city->region->occurrence_base_hours,
                            'occurrence_extra_price' => $city->region->occurrence_extra_price,
                            'occurrence_extra_hours' => $city->region->occurrence_extra_hours,
                            'specialist_base_price' => $city->region->specialist_base_price,
                            'specialist_total_price' => $city->region->specialist_base_price,
                            'specialist_extra_price' => $city->region->specialist_extra_price,
                            'currency' => $city->region->currency,
                            'payment_method_id' => $paymentMethod,
                            'active' => 1
                        ];
                    }
                }
            }
            if ($not_found) {
                return [];
            }
        } else {
            return [];
        }
    }


    /**
     *
     * @param $occurrence_id
     * @param $client_user_id
     * @return array
     */
    public function totalPrice($occurrence_id, $client_user_id)
    {
        $conditions = [
            'Occurrences.id' => $occurrence_id,
            'Occurrences.client_user_id' => $client_user_id
        ];

        $occurrence = $this->Occurrences
            ->find('all')
            ->where($conditions)
            ->contain([
                'Prices', 'Coupons'
            ])
            ->first();

        if ($occurrence) {
            $price = $this->Occurrences->Prices
                ->find('all')
                ->where([
                    'id' => $occurrence->price_id
                ])
                ->first();

            $date1 = $occurrence->checkin_adjustment;
            $date2 = $occurrence->checkout_adjustment;

            if (!$date1 || !$date2) {
                if (!$date1) {
                    $date1 = $occurrence->checkin_date;
                }
                if (!$date2) {
                    $date2 = $occurrence->checkout_date;
                }
            }
            $diff = $date2->diff($date1);

            $days = $diff->d;
            $hours = $diff->h;
            $minutes = $diff->i;

            if ($minutes > 5) {
                $hours = $hours + 1;
            }

            if ($days > 0) {
                $hours = $hours + ($days * 24);
            }
            $base_hours = $occurrence->price->occurrence_base_hours;
            $extra_hours = $occurrence->price->occurrence_extra_hours;
            $base_price = $occurrence->price->occurrence_base_price;
            $extra_price = $occurrence->price->occurrence_extra_price;
            $specialist_base_price = $occurrence->price->specialist_base_price;
            $specialist_extra_price = $occurrence->price->specialist_extra_price;
            $discount_price = $occurrence->price->occurrence_discount_price;
            $discount_type = $occurrence->price->occurrence_discount_type;

            if ($hours <= $base_hours) {
                if ($occurrence->coupon) {
                    if ($discount_type == 'PERCENT') // se o desconto for em porcentagem
                    {
                        $discount_price = $base_price * ($discount_price / 100);
                        $total_price = $base_price - $discount_price;

                    } else { // se for em dinheiro
                        $total_price = $base_price - $discount_price;
                    }
                } else {
                    $total_price = $base_price;
                }
                $data = [
                    'occurrence_total_price' => $total_price,
                    'specialist_total_price' => $specialist_base_price,
                    'occurrence_discount_price' => $discount_price
                ];

            } else {

                $extra_hours_quantity = ($hours - $base_hours) / $extra_hours;
                $specialist_extra_price_quantity = $specialist_extra_price * $extra_hours_quantity;
                $specialist_total_price = $specialist_base_price + $specialist_extra_price_quantity;

                if ($occurrence->coupon) {
                    if ($occurrence->coupon->total == 1) {
                        if ($discount_type == 'PERCENT') // se o desconto for em porcentagem
                        {
                            $discount_price = ($base_price + ($extra_price * $extra_hours_quantity)) * ($discount_price / 100);
                            $total_price = ($base_price + ($extra_price * $extra_hours_quantity)) - $discount_price;

                        } else { // se for em dinheiro
                            $total_price = $base_price + ($extra_price * $extra_hours_quantity) - $discount_price;
                        }
                    } else {
                        if ($discount_type == 'PERCENT') // se o desconto for em porcentagem
                        {
                            $discount_price = $base_price * ($discount_price / 100);
                            $total_price = $base_price + ($extra_price * $extra_hours_quantity) - $discount_price;
                        } else { // se for em dinheiro
                            $total_price = $base_price + ($extra_price * $extra_hours_quantity) - $discount_price;
                        }
                    }
                } else {
                    $total_price = $base_price + ($extra_price * $extra_hours_quantity);
                }
                $data = [
                    'occurrence_total_price' => $total_price,
                    'specialist_extra_price_quantity' => $specialist_extra_price_quantity,
                    'specialist_total_price' => $specialist_total_price,
                    'occurrence_extra_hours_quantity' => intval($extra_hours_quantity),
                    'occurrence_extra_price_quantity' => $extra_hours_quantity * $extra_price,
                    'occurrence_discount_price' => $discount_price
                ];
            }
            $price = $this->Occurrences->Prices
                ->patchEntity($price, $data);

            if ($this->Occurrences->Prices->save($price)) {
                return $this->_getReturn(true, __('SUCCESS'));
            } else {
                return $this->_getReturn(false, __('FAIL_SAVE'));
            }

        } else {
            return $this->_getReturn(false, __('NOT_FOUND'));
        }
    }


    public function calculatePoints($contract_id)
    {
        $contracts = TableRegistry::get('Contracts');
        $contracts = $contracts->find();

        $contract = $contracts
            ->contain(['Funds'])
            ->where([
                'Contracts.id' => $contract_id,
                'Contracts.active' => true
            ])
            ->first();

        if ($contract) {

            $time = Time::now();

            $occurrences = TableRegistry::get('Occurrences');
            $occurrences = $occurrences->find();

            $total_expense = $occurrences
                ->contain(['Prices'])
                ->select(['total' => $occurrences->func()->sum('Prices.occurrence_total_price')])
                ->where([
                    'contract_id' => $contract_id,
                    'status IN' => [
                        'OPENED', 'ACCEPTED', 'GOING', 'ON_SERVICE', 'AWAITING_VALIDATION', 'PAYED_SPECIALIST', 'FINISHED', 'UNPRODUCTIVE'
                    ],
                    'Occurrences.active' => true
                ])->first();

            $month_expense = $occurrences
                ->contain(['Prices'])
                ->select(['total' => $occurrences->func()->sum('Prices.occurrence_total_price')])
                ->where([
                    'contract_id' => $contract_id,
                    'status IN' => [
                        'OPENED', 'ACCEPTED', 'GOING', 'ON_SERVICE', 'AWAITING_VALIDATION', 'PAYED_SPECIALIST', 'FINISHED', 'UNPRODUCTIVE'
                    ],
                    'MONTH(schedule_time)' => $time->month,
                    'Occurrences.active' => true
                ])
                ->first();

            if ($contract->fund->type == 'DETACHED') {
                $data = [
                    'fund' => [
                        'total_expense' => $total_expense->total,
                        'month_expense' => $month_expense->total,
                        'credit_balance' => $contract->fund->total_points_quantity - $total_expense->total
                    ]
                ];
            } else if ($contract->fund->type == 'RECURRENT') {
                if ($total_expense->total < $contract->fund->total_points_quantity) {
                    $data = [
                        'fund' => [
                            'total_expense' => $total_expense->total,
                            'month_expense' => $month_expense->total,
                            'credit_balance' => $contract->fund->month_points - $month_expense->total
                        ]
                    ];
                } else {
                    $data = [
                        'fund' => [
                            'total_expense' => $total_expense->total,
                            'month_expense' => $month_expense->total,
                            'credit_balance' => $contract->fund->total_points_quantity - $total_expense->total
                        ]
                    ];
                }
            } else {
                $data = [
                    'fund' => [
                        'total_expense' => $total_expense->total,
                        'month_expense' => $month_expense->total,
                        'credit_balance' => $contract->fund->total_points_quantity - $total_expense->total
                    ]
                ];
            }

            $contract = $this->Contracts->patchEntity($contract, $data);
            if ($this->Contracts->save($contract)) {
                return true;
            } else {
                return false;
            }
        } else {
            throw new NotFoundException('NOT_FOUND');
        }
    }

}
