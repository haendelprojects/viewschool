<?php
/**
 * Created by PhpStorm.
 * User: Haendel
 * Date: 30/09/2016
 * Time: 13:54
 */

namespace Flow\Controller\Component;


use Cake\Controller\Component;

class UtilComponent extends Component
{

    public function compareRequisitionDates($init, $end)
    {
        if (gettype($init) == 'array') {
            $init_date = new \DateTime($init['year'] . "-" . $init['month'] . "-" . $init['day']);
            $end_date = new \DateTime($end['year'] . "-" . $end['month'] . "-" . $end['day']);
        } else {
            $init_date = \DateTime::createFromFormat('d/m/Y', $init);
            $end_date = \DateTime::createFromFormat('d/m/Y', $end);
        }
        $interval = $init_date->diff($end_date);
        return $interval;
    }

    public function compareRequisitionDatesMonth($init, $end)
    {
        $interval = $this->compareRequisitionDates($init, $end);
        $interval->m += $interval->y * 12;
        return $interval;
    }


    /**
     * Setar hora utc
     * @param $timezone
     * @return bool|string
     */
    public function getTimezoneValue($timezone)
    {
        if ($timezone == 'UTC') {
            return '';
        } else {
            $timezone = new \DateTimeZone($timezone);
            $date = new \DateTime('now', $timezone);
            return sprintf('%+03d:%02u', $date->getOffset() / 3600, abs($date->getOffset()) % 3600 / 60);
        }
    }

    
}