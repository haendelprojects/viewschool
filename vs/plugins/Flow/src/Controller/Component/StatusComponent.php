<?php
/**
 * Created by PhpStorm.
 * User: Haendel
 * Date: 20/07/2016
 * Time: 14:40
 */

namespace Flow\Controller\Component;


use App\Model\Table\CitiesTable;
use App\Model\Table\OccurrencesTable;
use AppNotifications\Controller\Component\ClientNotificationComponent;
use AppNotifications\Controller\Component\SpecialistNotificationComponent;
use Cake\Controller\Component;
use Cake\Network\Exception\BadRequestException;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Guzzle\Service\Exception\ValidationException;

/**
 * Class StatusComponent
 * @package App\Controller\Component
 *
 * @property OccurrencesTable $Occurrences
 * @property Component\AuthComponent $Auth
 * @property SpecialistNotificationComponent $SpecialistNotification
 * @property CitiesTable $Cities
 * @property ClientNotificationComponent $ClientNotification
 * @property CalculatePriceComponent $CalculatePrice
 */
class StatusComponent extends Component
{
    private $Occurrences;
    private $Cities;

    public $components = [
        'Auth',
        'RequestHandler',
        'EmailNotifications.Mail',
        'AppNotifications.ClientNotification',
        'AppNotifications.SpecialistNotification',
        'Flow.CalculatePrice',
        'Flow.Points',
    ];

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->Occurrences = TableRegistry::get('Occurrences');
        $this->Cities = TableRegistry::get('Cities');
    }

    /**
     * @param $success
     * @param $message
     * @return array
     */
    private function _getReturn($success, $message, $data = null)
    {
        return [
            'success' => $success,
            'message' => $message,
            'data' => $data
        ];
    }

    /**
     * Muda o status da ocorrência
     * Só é possivel mudar se a ocorrência estiver aceita.
     * @param null $id
     * @param null $specialist_user_id
     * @return array
     */
    public function going($id = null, $specialist_user_id = null)
    {

        if ($specialist_user_id) {
            // estabelece as condições a serem utilizadas na filtragem das ocorrências mais na frente na busca
            $conditions = [
                'specialist_user_id' => $specialist_user_id, // o id da resale ser igual ao id do usuário logado
                'id' => $id // o id da ocorrência deve ser o mesmo passado no parâmetro
            ];
        } else {
            $conditions = [
                'id' => $id // o id da ocorrência deve ser o mesmo passado no parâmetro
            ];
        }


        // busca as ocorrências que satisfaçam as condições
        $occurrence = $this->Occurrences
            ->find('all')
            ->where($conditions)// Checa as condições
            ->first();

        if ($occurrence) // checa se a ocorrência chamada no parâmetro existe
        {
            $occurrence_accepted = 'ACCEPTED' == $occurrence->status; // cria $occurrence_accepted que recebe o status de ACCEPTED caso esse seja o status da ocorrência

            if ($occurrence_accepted) // checa se o status da chamada 'ACCEPTED' é true
            {
                $specialist = $specialist_user_id;// $specialist recebe o id do usuário autenticado

                // dados a serem modificados na ocorrência
                $data = [
                    'status' => 'GOING' // o status da ocorrência é modificado para 'GOING'
                ];

                $occurrence = $this->Occurrences// transforma a ocorrência em objeto
                ->patchEntity($occurrence, $data); // 'remenda' as mudanças em $data no objeto ocorrência

                if ($this->Occurrences->save($occurrence)) // salva a 'nova' ocorrência depois das modificações
                {
                    $this->SpecialistNotification->going($occurrence->client_user_id, $id);
                    return $this->_getReturn(true, __('Salvo com sucesso'));
                } else {
                    return $this->_getReturn(false, __('Não foi possivel salvar!'));
                }
            } else {
                return $this->_getReturn(false, __('Não é possivel alterar o Status!'));
            }
        } else {
            return $this->_getReturn(false, __('A ocorrência não existe ou você não tem acesso!'));
        }
    }

    /**
     * @param null $id
     * @param null $specialist_id
     * @return array
     */
    public function checkin($id = null, $specialist_id = null)
    {
        if ($specialist_id) {
            // estabelece as condições a serem utilizadas na filtragem das ocorrências mais na frente na busca
            $conditions = [
                'Occurrences.specialist_user_id' => $specialist_id, // o id da resale ser igual ao id do usuário logado
                'Occurrences.id' => $id // o id da ocorrência deve ser o mesmo passado no parâmetro
            ];
        } else {
            $conditions = [
                'Occurrences.id' => $id // o id da ocorrência deve ser o mesmo passado no parâmetro
            ];
        }

        // busca as ocorrências que satisfaçam as condições
        $occurrence = $this->Occurrences
            ->find('all')
            ->where($conditions)// Checa as condições
            ->contain(['Addresses.OCities'])
            ->first();

        if ($occurrence) // checa se a ocorrência chamada no parâmetro existe
        {
            $occurrence_going = 'GOING' == $occurrence->status; // cria $occurrence_accepted que recebe o status de OUT FOR SHIPMENT caso esse seja o status da ocorrência

            if ($occurrence_going) // checa se o status da chamada 'OUT FOR SHIPMENT' é true
            {
                // dados a serem modificados na ocorrência
                $date = new \DateTime();
                $data = [
                    'status' => 'ON_SERVICE', // o status da ocorrência é modificado para 'ON_SERVICE'
                    'checkin_date' => $date->setTimezone(new \DateTimeZone('UTC'))
                ];

                $occurrence = $this->Occurrences// transforma a ocorrência em objeto
                ->patchEntity($occurrence, $data); // 'remenda' as mudanças em $data no objeto ocorrência

                if ($this->Occurrences->save($occurrence)) // salva a 'nova' ocorrência depois das modificações
                {
                    $this->SpecialistNotification->checkin($occurrence->client_user_id, $id);
                    return $this->_getReturn(true, __('Sucesso :D'));
                } else {
                    return $this->_getReturn(false, __('Não Salvou!'));
                }
            } else {
                return $this->_getReturn(false, __('Não é possivel alterar o Status !'));
            }
        } else {
            return $this->_getReturn(false, __('A ocorrência não existe ou você não tem acesso!'));
        }
    }

    /**
     * @param $id
     * @return array
     * Realizar checkout
     */
    public function checkout($id)
    {
        $conditions = [
            'Occurrences.id' => $id // o id da ocorrência deve ser o mesmo passado no parâmetro
        ];

        // busca as ocorrências que satisfaçam as condições
        $occurrence = $this->Occurrences
            ->find('all')
            ->where($conditions)// Checa as condições
            ->contain(['Prices', 'Coupons', 'Addresses.OCities'])
            ->first();

        if ($occurrence) {
            if ($occurrence->status == 'ON_SERVICE') {
                $date = new \DateTime();
                $data = [
                    'status' => 'AWAITING_VALIDATION', // o status da ocorrência é modificado para 'ON_SERVICE'
                    'checkout_date' => $date->setTimezone(new \DateTimeZone('UTC'))
                ];

                $occurrence = $this->Occurrences->patchEntity($occurrence, $data);

                if ($this->Occurrences->save($occurrence)) {
                    if ($occurrence->corporate_id) {
                        $price = $this->Points->getPoints($occurrence->service_id, $occurrence->corporate_id, $occurrence->contract_id, $occurrence->address->state_id, $occurrence->address->city_id);
                    } else {
                        $price = $this->CalculatePrice->price($occurrence->address->state, $occurrence->address->city, $occurrence->payment_method_id, $occurrence->coupon_id);
                    }
                    $price = array_merge($price, $this->calculateCheckout($occurrence, $price));
                    $data = array('price' => $price);
                    $occurrence = $this->Occurrences
                        ->patchEntity($occurrence, $data);

                    if ($this->Occurrences->save($occurrence)) {
                        $this->SpecialistNotification->checkout($occurrence->client_user_id, $id);
                        return $this->_getReturn(true, __('Checkout realizado!'), $occurrence);
                    } else {
                        return $this->_getReturn(false, __('Falha ao salvar checkout!'));
                    }
                }
            } else {
                return $this->_getReturn(false, __('Não é possivel mudar o status!'));
            }
        } else {
            return $this->_getReturn(false, __('A ocorrência não existe ou você não tem acesso!'));
        }
    }


    public function calculateCheckout($occurrence, $price)
    {
        //Informações
        $checkin = $occurrence->checkin_date;
        $checkout = $occurrence->checkout_date;
        $hours_total = $this->_getHours($checkin, $checkout);

        //Preços
        $occurrence_base_price = $price['occurrence_base_price'];
        $occurrence_extra_price = $price['occurrence_extra_price'];

        //Horas para serviços
        $occurrence_base_hours = $price['occurrence_base_hours'];
        $occurrence_extra_hours = $price['occurrence_extra_hours'];

        //Desconto
        $discount_price = $occurrence->price->occurrence_discount_price;
        $discount_type = $occurrence->price->occurrence_discount_type;

        //Especialista Preços
        $specialist_base_price = $price['specialist_base_price'];
        $specialist_extra_price = $price['specialist_extra_price'];

        //Especialista Horas
        $city = $this->_getRegion($occurrence->address->state_id, $occurrence->address->city_id);
        $specialist_base_hours = $city->region->occurrence_base_hours;
        $specialist_extra_hours = $city->region->occurrence_extra_hours;

        // ***
        //Valor do atendimento finalizado bas
        // ***
        //Se houver hora extra]
        if ($hours_total > $occurrence_base_hours) {
            $occurrence_extra_hours_quantity = ($hours_total - $occurrence_base_hours) / (($occurrence_extra_hours) ? $occurrence_extra_hours : 1);
            $occurrence_extra_hours_quantity = ceil($occurrence_extra_hours_quantity); //Arredondar o valor para cima !!!! :DDD
        } else {
            $occurrence_extra_hours_quantity = 0;
        }

        $occurrence_total_price = $occurrence_base_price + ($occurrence_extra_hours_quantity * $occurrence_extra_price);

        //**
        // Valor do especialista
        //**

        if ($hours_total > $specialist_base_hours) {
            $specialist_extra_hours_quantity = ($hours_total - $specialist_base_hours) / $specialist_extra_hours;
            $specialist_extra_hours_quantity = ceil($specialist_extra_hours_quantity); //Arredondar o valor para cima !!!! :DDD
        } else {
            $specialist_extra_hours_quantity = 0;
        }

        $specialist_extra_price_quantity = $specialist_extra_hours_quantity * $specialist_extra_price;
        $specialist_total_price = $specialist_base_price + $specialist_extra_price_quantity;

        //Calcuando Cupon
        if ($occurrence->has('coupon')) {
            if ($occurrence->coupon) {
                if ($occurrence->coupon->total) {
                    if ($discount_type == 'PERCENT') // se o desconto for em porcentagem
                    {
                        $discount_price = ($occurrence_base_price + ($occurrence_extra_price * $occurrence_extra_hours_quantity)) * ($discount_price / 100);
                        $occurrence_total_price = $occurrence_total_price - $discount_price;

                    } else { // se for em dinheiro
                        $occurrence_total_price = $occurrence_total_price - $discount_price;
                    }
                } else {
                    if ($discount_type == 'PERCENT') // se o desconto for em porcentagem
                    {
                        $discount_price = $occurrence_base_price * ($discount_price / 100);
                        $occurrence_total_price = $occurrence_total_price - $discount_price;

                    } else { // se for em dinheiro
                        $occurrence_total_price = $occurrence_total_price - $discount_price;
                    }
                }
            }
        }

        $data = [
            'occurrence_total_price' => $occurrence_total_price,
            'occurrence_total_hours' => $hours_total,
            'occurrence_discount_price' => $discount_price,
            'specialist_total_price' => (float)$specialist_total_price,
            'specialist_extra_price_quantity' => (float)$specialist_extra_price_quantity,
            'occurrence_extra_hours_quantity' => intval($occurrence_extra_hours_quantity),
            'occurrence_extra_price_quantity' => $occurrence_extra_hours_quantity * $occurrence_extra_price,
            'specialist_base_hour' => $specialist_base_hours,
            'specialist_extra_hours' => $specialist_extra_hours,
            'specialist_total_hours' => $hours_total
        ];

        return $data;
    }

    public function setValuesPrice($id)
    {

        $conditions = [
            'Occurrences.id' => $id // o id da ocorrência deve ser o mesmo passado no parâmetro
        ];

        // busca as ocorrências que satisfaçam as condições
        $occurrence = $this->Occurrences
            ->find('all')
            ->where($conditions)// Checa as condições
            ->contain(['Prices', 'Coupons', 'Addresses.OCities'])
            ->first();


        if ($occurrence->corporate_id) {
            $price = $this->Points->getPoints($occurrence->service_id, $occurrence->corporate_id, $occurrence->contract_id, $occurrence->address->state_id, $occurrence->address->city_id);
        } else {
            $price = $this->CalculatePrice->price($occurrence->address->state, $occurrence->address->city, $occurrence->payment_method_id, $occurrence->coupon_id);
        }
        $price = array_merge($price, $this->calculateCheckout($occurrence, $price));
        $data = array('price' => $price);
        $occurrence = $this->Occurrences
            ->patchEntity($occurrence, $data);

        if ($this->Occurrences->save($occurrence)) {
            $this->SpecialistNotification->checkout($occurrence->client_user_id, $id);
            return $this->_getReturn(true, __('Checkout realizado!'), $occurrence);
        } else {
            return $this->_getReturn(false, __('Falha ao salvar checkout!'));
        }
    }

    /**
     * Quantidade de horas
     * @param $checkin
     * @param $checkout
     * @return mixed
     */
    private function _getHours($checkin, $checkout)
    {
        $interval = $checkin->diff($checkout);

        $hours = $interval->h;

        if ($interval->i > 5) {
            $hours++;
        }

        if ($interval->d > 0) {
            $hours = $interval->d * 24;
        }

        return $hours;
    }

    /**
     * Buscar região
     * @param null $state
     * @param null $city
     * @return mixed|null
     */
    private function _getRegion($state = null, $city = null)
    {
        $city = $this->Cities
            ->find('all')
            ->where([
                //'States.id' => $state,
                'Cities.id' => $city
            ])
            ->contain([
                'States', 'Regions'
            ])
            ->first();

        return $city;
    }


    public function calculatePrice($id = null)
    {
        // estabelece as condições a serem utilizadas na filtragem das ocorrências mais na frente na busca
        $conditions = [
            'id' => $id // o id da ocorrência deve ser o mesmo passado no parâmetro
        ];

        // busca as ocorrências que satisfaçam as condições
        $occurrence = $this->Occurrences
            ->find('all')
            ->where($conditions)// Checa as condições
            ->first();

        if ($occurrence) // checa se a ocorrência chamada no parâmetro existe
        {
            $occurrence_finish = 'FINISHED' == $occurrence->status; // cria $occurrence_accepted que recebe o status de OUT FOR SHIPMENT caso esse seja o status da ocorrência

            if ($occurrence_finish) // checa se o status da chamada 'OUT FOR SHIPMENT' é true
            {
                if ($occurrence->corporate_id) {

                } else {
                    $this->CalculatePrice->totalPrice($id, $occurrence->client_user_id);
                }
                return $this->_getReturn(true, __('Salvo com sucesso !'));
            } else {
                return $this->_getReturn(false, __('Não é possivel alterar o Status!'));
            }
        } else {
            return $this->_getReturn(false, __('A ocorrência não existe ou você não tem acesso!'));
        }
    }

    /**
     * @param null $id
     * @param null $client_user_id
     * @return array
     */
    public function cancel($id = null, $client_user_id = null)
    {
        if ($client_user_id) {

            // Estabelece as condições a serem utilizadas na busca mais a frente
            $conditions = [
                'Occurrences.id' => $id,
                'client_user_id' => $client_user_id // Especificando que o usuário só pode ver suas ocorrências
            ];
        } else {
            $conditions = [
                'Occurrences.id' => $id,
            ];
        }

        // Busca a ocorrência que satisfaça as condições
        $occurrence = $this->Occurrences// Transforma em objeto
        ->find('all')
            ->contain(['Prices'])
            ->where($conditions)// Checa as condições
            ->first();

        if ($occurrence) // Se a ocorrência for válida
        {
            //$occurrence_accepted = 'ACCEPTED' == $occurrence->status; // Cria $occurrence_accepted que recebe o status de ACCEPTED caso esse seja o status da ocorrência
            //$occurrence_opened = 'OPENED' == $occurrence->status; // Cria $occurrence_opened que recebe o status de OPENED caso esse seja o status da ocorrência

            //if ($occurrence_accepted || $occurrence_opened) // Se o status da ocorrência for ACCEPTED ou OPENED ou OUT_FOR_SHIPMENT
            //{
            // Dado que será modificado na ocorrência
            $data = [
                'price' => [
                    'occurrence_base_price' => 0,
                    'occurrence_total_price' => 0,
                    'specialist_base_price' => 0,
                    'specialist_total_price' => 0,
                    'unique_point_value' => 0,
                    'specialist_extra_price_quantity' => 0,
                    'specialist_extra_price' => 0,
                    'occurrence_extra_price_quantity' => 0,
                    'occurrence_extra_price' => 0,
                ],
                'status' => 'CANCELED' // Substitui o status anterior por CANCELED
            ];
            $occurrence = $this->Occurrences// Tranforma em objeto
            ->patchEntity($occurrence, $data, ['associated' => ['Prices']]); // 'Remenda' as mudanças em $data no objeto ocorrência

            if ($this->Occurrences->save($occurrence)) // Salva a ocorrência após as modificações
            {
                $this->ClientNotification->cancel($occurrence->specialist_user_id, $id);
                return $this->_getReturn(true, __('Salvo com sucesso!'));
            } else {
                return $this->_getReturn(false, __('Não foi possivel salvar!'));
            }
//            } else {
//                return $this->_getReturn(false, __('Não é possivel alterar o Status!'));
//            }
        } else {
            return $this->_getReturn(false, __('A ocorrência não existe ou você não tem acesso!'));
        }
    }

    public function unproductive($id = null, $client_user_id = null)
    {
        if ($client_user_id) {
            // Estabelece as condições a serem utilizadas na busca mais a frente
            $conditions = [
                'id' => $id,
                'client_user_id' => $client_user_id // Especificando que o usuário só pode ver suas ocorrências
            ];
        } else {
            $conditions = [
                'id' => $id,
            ];
        }

        // Busca a ocorrência que satisfaça as condições
        $occurrence = $this->Occurrences// Transforma em objeto
        ->find('all')
            ->where($conditions)// Checa as condições
            ->first();

        if ($occurrence) // Se a ocorrência for válida
        {
            $data = [
                'status' => 'UNPRODUCTIVE' // Substitui o status anterior por CANCELED
            ];

            $occurrence = $this->Occurrences->patchEntity($occurrence, $data); // 'Remenda' as mudanças em $data no objeto ocorrência

            if ($this->Occurrences->save($occurrence)) // Salva a ocorrência após as modificações
            {
                $this->ClientNotification->cancel($occurrence->specialist_user_id, $id);
                return $this->_getReturn(true, __('Salvo com sucesso!'));
            } else {
                return $this->_getReturn(false, __('Não foi possivel salvar!'));
            }
//
        } else {
            return $this->_getReturn(false, __('A ocorrência não existe ou você não tem acesso!'));
        }
    }

    public function invalid($id = null, $client_user_id = null)
    {
        if ($client_user_id) {
            // Estabelece as condições a serem utilizadas na busca mais a frente
            $conditions = [
                'id' => $id,
                'client_user_id' => $client_user_id // Especificando que o usuário só pode ver suas ocorrências
            ];
        } else {
            $conditions = [
                'id' => $id,
            ];
        }

        // Busca a ocorrência que satisfaça as condições
        $occurrence = $this->Occurrences// Transforma em objeto
        ->find('all')
            ->where($conditions)// Checa as condições
            ->first();

        if ($occurrence) // Se a ocorrência for válida
        {
            $data = [
                'price' => [
                    'occurrence_base_price' => 0,
                    'occurrence_total_price' => 0,
                    'specialist_base_price' => 0,
                    'specialist_total_price' => 0,
                    'unique_point_value' => 0,
                    'specialist_extra_price_quantity' => 0,
                    'specialist_extra_price' => 0,
                    'occurrence_extra_price_quantity' => 0,
                    'occurrence_extra_price' => 0,
                ],
                'status' => 'INVALID' // Substitui o status anterior por CANCELED
            ];

            $occurrence = $this->Occurrences->patchEntity($occurrence, $data, ['associated' => ['Prices']]); // 'Remenda' as mudanças em $data no objeto ocorrência

            if ($this->Occurrences->save($occurrence)) // Salva a ocorrência após as modificações
            {
                $this->ClientNotification->cancel($occurrence->specialist_user_id, $id);
                return $this->_getReturn(true, __('Salvo com sucesso!'));
            } else {
                return $this->_getReturn(false, __('Não foi possivel salvar!'));
            }
//
        } else {
            return $this->_getReturn(false, __('A ocorrência não existe ou você não tem acesso!'));
        }
    }

    /**
     * @param null $id
     * @param null $client_user_id
     * @return array
     */
    public function reschedule($id = null, $client_user_id = null)
    {
        $year = $this->request->data('year');
        $month = $this->request->data('month');
        $day = $this->request->data('day');
        $hour = $this->request->data('hour');
        $minute = $this->request->data('minute');
        $second = $this->request->data('second');

        if ($client_user_id) {
            // Estabelece as condições a serem utilizadas na busca mais a frente
            $conditions = [
                'id' => $id,
                'client_user_id' => $client_user_id // Especificando que o usuário só pode ver suas ocorrências
            ];
        } else {
            $conditions = [
                'id' => $id,
            ];
        }

        // Busca a ocorrência que satisfaça as condições
        $occurrence = $this->Occurrences
            ->find('all')
            ->where($conditions)
            ->first();

        if ($occurrence) {
            $date2 = Time::now();
            $date2->year($this->request->data('schedule_time.year'));
            $date2->month($this->request->data('schedule_time.month'));
            $date2->day($this->request->data('schedule_time.day'));
            $date2->hour($this->request->data('schedule_time.hour'));
            $date2->minute($this->request->data('schedule_time.minute'));
            $date1 = Time::now();

            $diff = $date1->diff($date2);

            $days = $diff->d;
            $hours = $diff->h;
            $months = $diff->m;
            $years = $diff->y;

            if ($hours < 3 && $days < 1 && $months < 1 && $years < 1) {
                $occurrence_accepted = 'ACCEPTED' == $occurrence->status; // Cria $occurrence_accepted que recebe o status de ACCEPTED caso esse seja o status da ocorrência
                $occurrence_opened = 'OPENED' == $occurrence->status; // Cria $occurrence_opened que recebe o status de OPENED caso esse seja o status da ocorrência

                if ($occurrence_accepted || $occurrence_opened) // Se o status da ocorrência for ACCEPTED ou OPENED ou OUT_FOR_SHIPMENT
                {
                    // Dado que será modificado na ocorrência
                    $data = [
                        'schedule_time' => [
                            'year' => date($year),
                            'month' => date($month),
                            'day' => date($day),
                            'hour' => date($hour),
                            'minute' => date($minute),
                            'second' => date($second)
                        ]
                    ];
                    $occurrence = $this->Occurrences// Tranforma em objeto
                    ->patchEntity($occurrence, $data); // 'Remenda' as mudanças em $data no objeto ocorrência

                    if ($this->Occurrences->save($occurrence)) // Salva a ocorrência após as modificações
                    {
                        $this->ClientNotification->reschedule($occurrence->specialist_user_id, $id);
                        return $this->_getReturn(true, __('Salvo com sucesso!'));

                    } else {
                        return $this->_getReturn(false, __('Não foi possivel salvar!'));
                    }
                } else {
                    return $this->_getReturn(false, __('Não é possivel alterar o Status!'));
                }
            } else {
                return $this->_getReturn(false, __('Hora inválida!'));
            }
        } else {
            return $this->_getReturn(false, __('A ocorrência não existe ou você não tem acesso!'));
        }
    }


    public function adjustCheckin($id = null, $client_user_id, $date = null)
    {
        $year = $date['year'];
        $month = $date['month'];
        $day = $date['day'];
        $hour = $date['hour'];
        $minute = $date['minute'];
        $second = $date['second'];

        // Estabelece as condições a serem utilizadas na busca mais a frente
        $conditions = [
            'id' => $id, // Especificando que o id do chamado é o mesmo passado no endereço
        ];

        if (!$this->Auth->user('access_admin')) {
            $conditions['client_user_id'] = $client_user_id; // Especificando que o usuário logado deve ser o dono da ocorrência
        };

        // Busca a ocorrência que satisfaça as condições
        $occurrence = $this->Occurrences
            ->find('all')// Transforma em objeto
            ->where($conditions)// Checa as condições
            ->first();

        if ($occurrence) // Se a ocorrência passada no parâmetro for válida
        {
            $occurrence_on_service = 'ON_SERVICE' == $occurrence->status; // Cria $occurrence_ongoing que recebe o status de ONGOING caso esse seja o status da ocorrência

            if ($occurrence_on_service) // Se o status do chamado for ONGOING
            {
                if ($date) {
                    // Dado que será modificado na ocorrência
                    $data = [
                        'checkin_adjustment' => [
                            'year' => date($year),
                            'month' => date($month),
                            'day' => date($day),
                            'hour' => date($hour),
                            'minute' => date($minute),
                            'second' => date($second)
                        ]
                    ];
                } else {
                    $data = [
                        'checkin_adjustment' => $occurrence->checkin_date
                    ];
                }
                $occurrence = $this->Occurrences// Transforma em objeto
                ->patchEntity($occurrence, $data); // 'Remenda' as mudanças em $data no objeto ocorrência

                if ($this->Occurrences->save($occurrence)) // Salva a ocorrência após as modificações
                {

                    $this->ClientNotification->confirmCheckin($occurrence->specialist_user_id, $id);
                    return $this->_getReturn(true, __('Salvo com sucesso!'));
                } else {
                    return $this->_getReturn(false, __('Não foi possivel salvar!'));
                }
            } else {
                return $this->_getReturn(false, __('Não é possivel alterar o Status!'));
            }
        } else {
            return $this->_getReturn(false, __('A ocorrência não existe ou você não tem acesso!'));
        }
    }

    /**
     * @param null $id
     * @param null $client_user_id
     * @return array
     */
    public function adjustCheckout($id = null, $client_user_id = null, $date = null)
    {
        $year = $date['year'];
        $month = $date['month'];
        $day = $date['day'];
        $hour = $date['hour'];
        $minute = $date['minute'];
        $second = $date['second'];

        // Estabelece as condições a serem utilizadas na busca mais a frente
        $conditions = [
            'id' => $id, // Especificando que o id do chamado é o mesmo passado no endereço
            'client_user_id' => $client_user_id // Especificando que o usuário logado deve ser o dono da ocorrência
        ];

        // Busca a ocorrência que satisfaça as condições
        $occurrence = $this->Occurrences
            ->find('all')// Transforma em objeto
            ->where($conditions)// Checa as condições
            ->first();

        if ($occurrence) // Se a ocorrência passada no parâmetro for válida
        {
            $occurrence_awaiting_payment = 'AWAITING_VALIDATION' == $occurrence->status; // Cria $occurrence_ongoing que recebe o status de ONGOING caso esse seja o status da ocorrência

            if ($occurrence_awaiting_payment) // Se o status do chamado for ONGOING
            {
                if ($this->request->data()) {
                    // Dado que será modificado na ocorrência
                    $data = [
                        'checkout_adjustment' => [
                            'year' => date($year),
                            'month' => date($month),
                            'day' => date($day),
                            'hour' => date($hour),
                            'minute' => date($minute),
                            'second' => date($second)
                        ]
                    ];
                } else {
                    $data = [
                        'checkout_adjustment' => $occurrence->checkout_date
                    ];
                }
                $occurrence = $this->Occurrences
                    ->patchEntity($occurrence, $data); // 'Remenda' as mudanças em $data no objeto ocorrência

                $this->CalculatePrice->totalPrice($id, $occurrence->client_user_id);

                if ($this->Occurrences->save($occurrence)) // Salva a ocorrência após as modificações
                {

                    $this->ClientNotification->confirmCheckout($occurrence->specialist_user_id, $id);
                    return $this->_getReturn(true, __('Salvo com sucesso!'));
                } else {
                    return $this->_getReturn(false, __('Não foi possivel salvar!'));
                }
            } else {
                return $this->_getReturn(false, __('Não é possivel alterar o Status!'));
            }
        } else {
            return $this->_getReturn(false, __('A ocorrência não existe ou você não tem acesso!'));
        }
    }

    /**
     * Finalizar tod o proceso do chamado, te´cnico pago com sucesso
     * @param null $id
     * @param null $client_user_id
     * @param null $payment_token
     * @return array
     */
    public function finish($id = null, $client_user_id = null, $payment_token = null)
    {
        // Estabelece as condições a serem utilizadas na busca mais a frente
        $conditions = [
            'id' => $id, // Especificando que o id do chamado é o mesmo passado no endereço
            'client_user_id' => $client_user_id // Especificando que o usuário só pode ver suas ocorrências
        ];

        // Busca a ocorrência que satisfaça as condições
        $occurrence = $this->Occurrences
            ->find('all')
            ->where($conditions)// Checa as condições
            ->first();


        if ($occurrence) // Se a ocorrência passada no parâmetro for válida
        {
            $occurrence_payed_specialist = 'PAYED_SPECIALIST' == $occurrence->status; // Cria $occurrencee_waiting_payment que recebe o status de WAITING_PAYMENT caso esse seja o status da ocorrênci

            if ($occurrence_payed_specialist) {
                // Dados que serão modificados na ocorrência
                $data = [
                    'status' => 'FINISHED' // Substitui o status anterior por FINISHED
                ];
                $occurrence = $this->Occurrences->patchEntity($occurrence, $data); // 'Remenda' as mudanças em $data no objeto ocorrência

                if ($this->Occurrences->save($occurrence)) // Salva as mudanças efetuadas na ocorrência
                {

                    return $this->_getReturn(true, __('Salvo com sucesso!'));
                } else {
                    return $this->_getReturn(false, __('Não foi possivel salvar!'));
                }
            } else {
                return $this->_getReturn(false, __('Não é possivel alterar o Status!'));
            }
        } else {
            return $this->_getReturn(false, __('A ocorrência não existe ou você não tem acesso!'));
        }
    }

    /**
     * Validar chamado
     * @param null $id
     * @param null $client_user_id
     * @param null $payment_token
     * @return array
     */
    public function valid($id = null, $client_user_id = null, $payment_token = null)
    {
        // Estabelece as condições a serem utilizadas na busca mais a frente
        $conditions = [
            'id' => $id, // Especificando que o id do chamado é o mesmo passado no endereço
            'client_user_id' => $client_user_id // Especificando que o usuário só pode ver suas ocorrências
        ];

        // Busca a ocorrência que satisfaça as condições
        $occurrence = $this->Occurrences
            ->find('all')
            ->where($conditions)// Checa as condições
            ->first();

        if ($occurrence) // Se a ocorrência passada no parâmetro for válida
        {
            $occurrence_payed_specialist = 'AWAITING_PAYMENT' == $occurrence->status || 'AWAITING_VALIDATION' == $occurrence->status; // Cria $occurrencee_waiting_payment que recebe o status de WAITING_PAYMENT caso esse seja o status da ocorrênci

            if ($occurrence_payed_specialist) {
                // Dados que serão modificados na ocorrência
                $data = [
                    'status' => 'OPERATION_VALIDATION' // Substitui o status anterior por FINISHED
                ];
                $occurrence = $this->Occurrences// Transforma em objeto
                ->patchEntity($occurrence, $data); // 'Remenda' as mudanças em $data no objeto ocorrência

                if ($this->Occurrences->save($occurrence)) // Salva as mudanças efetuadas na ocorrência
                {
                    $this->ClientNotification->payed($occurrence->specialist_user_id, $id);

                    return $this->_getReturn(true, __('Salvo com sucesso!'));
                } else {
                    return $this->_getReturn(false, __('Não foi possivel salvar!'));
                }
            } else {
                return $this->_getReturn(false, __('Não é possivel alterar o Status!'));
            }
        } else {
            return $this->_getReturn(false, __('A ocorrência não existe ou você não tem acesso!'));
        }
    }

    /**
     * validar chamado empresarial
     * @param null $id
     * @param null $enterprise_id
     * @param null $payment_token
     * @return array
     */
    public function validByEnterprise($id = null, $enterprise_id = null, $payment_token = null)
    {
        // Estabelece as condições a serem utilizadas na busca mais a frente
        $conditions = [
            'id' => $id, // Especificando que o id do chamado é o mesmo passado no endereço
            'enterprise_id' => $enterprise_id // Especificando que o usuário só pode ver suas ocorrências
        ];

        // Busca a ocorrência que satisfaça as condições
        $occurrence = $this->Occurrences
            ->find('all')
            ->where($conditions)// Checa as condições
            ->first();


        if ($occurrence) // Se a ocorrência passada no parâmetro for válida
        {

            $occurrence_payed_specialist = 'AWAITING_VALIDATION' == $occurrence->status; // Cria $occurrencee_waiting_payment que recebe o status de WAITING_PAYMENT caso esse seja o status da ocorrênci

            if ($occurrence_payed_specialist) {
                // Dados que serão modificados na ocorrência
                $data = [
                    'status' => 'PAYED_SPECIALIST' // Substitui o status anterior por FINISHED
                ];
                $occurrence = $this->Occurrences// Transforma em objeto
                ->patchEntity($occurrence, $data); // 'Remenda' as mudanças em $data no objeto ocorrência

                if ($this->Occurrences->save($occurrence)) // Salva as mudanças efetuadas na ocorrência
                {
                    $this->ClientNotification->payed($occurrence->specialist_user_id, $id);
                    return $this->_getReturn(true, __('Salvo com sucesso!'));
                } else {
                    return $this->_getReturn(false, __('Não foi possivel salvar!'));
                }
            } else {
                return $this->_getReturn(false, __('Não é possivel alterar o Status!'));
            }
        } else {
            return $this->_getReturn(false, __('A ocorrência não existe ou você não tem acesso!'));
        }
    }


    /**
     * Cancelar a ocorrencia
     * Só é possivel cancelar se a ocorrência estiver aceita ou com o técnico a caminho.
     */
    public function reject($id = null, $specialist_user_id)
    {
        // estabelece as condições a serem utilizadas na filtragem das ocorrências mais na frente na busca
        $conditions = [
            'Occurrences.specialist_user_id' => $specialist_user_id, // o id da resale ser igual ao id do usuário logado
            'Occurrences.id' => $id // o id da ocorrência deve ser o mesmo passado no parâmetro
        ];

        // busca as ocorrências que satisfaçam as condições
        $occurrence = $this->Occurrences
            ->find('all')
            ->where($conditions)// Checa as condições
            ->contain([])
            ->first();

        if ($occurrence) // checa se a ocorrência chamada no parâmetro existe
        {
            $occurrence_accepted = 'ACCEPTED' == $occurrence->status; // cria $occurrence_accepted que recebe o status de ACCEPTED caso esse seja o status da ocorrência
            $occurrence_going = 'GOING' == $occurrence->status;

            if ($occurrence_accepted || $occurrence_going) // checa se o status da chamada 'ACCEPTED' ou 'OUT FOR SHIPMENT' é true
            {
                // dados a serem modificados na ocorrência
                $data = [
                    'status' => 'OPENED', // o status retorna de ACCEPTED para OPENED
                    'specialist_enterprise_id' => null, // o enterprise_id id é limpo
                    'specialist_user_id' => null, // o specialist_id é limpo
                ];

                $occurrence = $this->Occurrences// transforma a ocorrência em objeto
                ->patchEntity($occurrence, $data); // 'remenda' as mudanças em $data no objeto ocorrência

                if ($this->Occurrences->save($occurrence)) // salva a 'nova' ocorrência depois das modificações
                {
                    $this->SpecialistNotification->cancel($occurrence->client_user_id, $id);
                    return $this->_getReturn(true, __('Salvo com sucesso!'));
                } else {
                    return $this->_getReturn(false, __('Não foi possivel salvar!'));
                }
            } else {
                return $this->_getReturn(false, __('Não é possivel alterar o Status!'));
            }
        } else {
            return $this->_getReturn(false, __('A ocorrência não existe ou você não tem acesso!'));
        }
    }

    /**
     * @param $occurrence_id
     * @return \Cake\Datasource\EntityInterface|\Cake\ORM\Entity
     */
    public function newHistoric($occurrence_id, $description)
    {
        $occurrence = $this->Occurrences
            ->find('all')
            ->where([
                'id' => $occurrence_id
            ])
            ->first();

        $data = [
            'description' => $description,
            'occurrence_id' => $occurrence->id,
            'user_id' => $this->Auth->user('id'),
            'active' => true
        ];
        $historic = $this->Occurrences->Historics->newEntity($data);
        $historic = $this->Occurrences->Historics->patchEntity($historic, $data);

        if ($this->Occurrences->Historics->save($historic)) {
            return $historic;
        } else {
            debug($historic->errors());
        }
    }
}