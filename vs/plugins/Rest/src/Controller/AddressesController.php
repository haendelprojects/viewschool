<?php

namespace Rest\Controller;

use App\Controller\AppController;

/**
 * Addresses Controller
 *
 * @property \App\Model\Table\AddressesTable $Addresses
 *
 * @method \App\Model\Entity\Address[] paginate($object = null, array $settings = [])
 */
class AddressesController extends AppController
{

    public function initialize()
    {
        parent::initialize(); // TODO: Change the autogenerated stub

        $this->loadComponent('RequestHandler');

        if ($this->Auth->user()) {
            $this->Auth->allow(['states', 'cities', 'filiais' , 'selectStateName','selectCitiesByStateName']);
        }

        $this->loadModel('Addresses');
    }

    public function states($id)
    {
        $states = $this->Addresses->OStates->find('all')->where(['country_id' => $id])->select(['id', 'name', 'country_id'])->order(['name' => 'asc']);
        $this->set(compact('states'));

        $this->set('_serialize', ['states']);
        $this->set('data', $states);

        $this->viewBuilder()->layout('ajax');
        $this->render('index');


    }

    public function selectStateName($name)
    {
        $states = $this->Addresses->OStates->find('all')->where(['abbrv' => $name])->select(['id', 'name', 'country_id'])->order(['name' => 'asc']);
        $this->set(compact('states'));
        $this->set('_serialize', ['states']);
        $this->set('data', $states);
        $this->set('name', $name);
        $this->viewBuilder()->layout('ajax');
        $this->render('index');
    }

    public function selectCitiesByStateName($name)
    {
        $state = $this->Addresses->OStates->find('all')->where(['abbrv' => $name])->contain(['cities'])->order(['name' => 'asc'])->first();

        $this->set(compact('state'));
        $this->set('_serialize', ['state']);
        $this->set('data', $state->cities);
        $this->set('name', $name);

        $this->viewBuilder()->layout('ajax');
        $this->render('index');
    }

    public function cities($id)
    {
        $cities = $this->Addresses->OCities->find('all')->where(['state_id' => $id])->select(['id', 'name', 'state_id'])->order(['name' => 'asc']);
        $this->set(compact('cities'));
        $this->set('_serialize', ['cities']);
        $this->set('data', $cities);
        $this->viewBuilder()->layout('ajax');
        $this->render('index');
    }

    public function filiais($id)
    {
        $addresses = $this->Addresses->find('all')->select(['id', 'title'])->where(['enterprise_id' => $id])->order(['title' => 'asc']);

        $data = [];

        foreach ($addresses as $address) {
            $data[] = [
                'id' => $address->id,
                'name' => $address->title
            ];
        }


        $this->viewBuilder()->layout('ajax');
        $this->set(compact('data'));
        $this->set('_serialize', ['data']);
    }
}
