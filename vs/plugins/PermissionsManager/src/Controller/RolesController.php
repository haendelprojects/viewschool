<?php
namespace PermissionsManager\Controller;

use PermissionsManager\Controller\AppController;

/**
 * Roles Controller
 *
 * @property \PermissionsManager\Model\Table\RolesTable $Roles
 *
 * @method \PermissionsManager\Model\Entity\Role[] paginate($object = null, array $settings = [])
 */
class RolesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $roles = $this->paginate($this->Roles);

        $this->set(compact('roles'));
        $this->set('_serialize', ['roles']);
    }

    /**
     * View method
     *
     * @param string|null $id Role id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $role = $this->Roles->get($id, [
            'contain' => ['Modules', 'Users']
        ]);

        $this->set('role', $role);
        $this->set('_serialize', ['role']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $role = $this->Roles->newEntity();
        if ($this->request->is('post')) {


            foreach ($this->request->getData('modules') as $key => $value) {
                if ($value) {
                    $this->request->data['modules']['_ids'][] = $key;
                }
            }

            $role = $this->Roles->patchEntity($role, $this->request->getData(), ['associated' => ['Modules']]);

            if ($this->Roles->save($role)) {
                $this->Flash->success(__('The role has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The role could not be saved. Please, try again.'));
        }

        $modules = $this->Roles->Modules->find('threaded', [
            'keyField' => $this->Roles->Modules->getPrimaryKey(),
            'parentField' => 'parent_id'
        ]);
        $this->set(compact('role', 'modules'));
        $this->set('_serialize', ['role']);
    }


    /**
     * Edit method
     *
     * @param string|null $id Role id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $role = $this->Roles->get($id, [
            'contain' => ['Modules']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {

            foreach ($this->request->getData('modules') as $key => $value) {
                if ($value) {
                    $this->request->data['modules']['_ids'][] = $key;
                }
            }
            $role = $this->Roles->patchEntity($role, $this->request->getData(), ['associated' => ['Modules']]);

            if ($this->Roles->save($role)) {
                $this->Flash->success(__('The role has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The role could not be saved. Please, try again.'));
        }
        $modules = $this->Roles->Modules->find('threaded', [
            'keyField' => $this->Roles->Modules->getPrimaryKey(),
            'parentField' => 'parent_id'
        ]);
        $this->set(compact('role', 'modules'));
        $this->set('_serialize', ['role']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Role id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $role = $this->Roles->get($id);
        if ($this->Roles->delete($role)) {
            $this->Flash->success(__('The role has been deleted.'));
        } else {
            $this->Flash->error(__('The role could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
